<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

# The AE6EO OpenTNC project

This repository holds the firmware code, schematic, and
printed-circuit board design files for an AX.25 Terminal Node
Controller (TNC) suitable for amateur radio 1200-baud packet radio
communication (and, experimentally, 9600-baud as well).

OpenTNC is generally modeled after the original TAPR TNC-2 - it has
similar capabilities and a similar command interface.  It contains its
own AX.25 protocol stack for handling inbound and outbound
connections, works with any serial-port terminal emulator, is
USB-powered and USB-connected with no special drivers needed, and
supports FX.25 error correction.  It also supports KISS operation, for
use with other PC-based networking stacks.

Both the firmware and hardware are open-source (GPLv3, and Creative
Commons CC-BY-SA).  Ready-to-manufacture PC-board Gerber files are
provided.  The bill-of-materials cost is approximately $30/unit in
small quantities, not including shipping or an enclosure (and not
accounting for any U.S. import tariffs imposed in 2025 or later).
Hams with some electronics assembly experience should be able to put
one together in an afternoon, with no surface-mount soldering
equipment required.

Author: [Dave Platt](mailto:dplatt@radagast.org)

## 0.95 bill-of-materials, 3/11/2025

I found a couple of errors in the bill-of-materials files I'd released
for the 0.8 board (incorrect potentiometer part numbers).  I've uploaded
a new BOM spreadsheet into the schematics directory, with a version
number of 0.95 - I recommend using this to order parts from Mouser.

I'll be uploading the latest (0.95) schematic revision and PCB Gerbers
within the next few days.  I've switched to using the latest release of
KiCAD (9.0) and some of the plugins I've been using aren't yet compatible,
so I'll wait a few days and see if I can get a polished release together.

## Public release 4.0, 1/7/2025

This release adds two new major features (both somewhat
experimental):

-  9600-baud FSK modulation, compatible with the original
G3RUH design, TNC-2 "add-on" boards, and DireWolf.
This change requires minor modifications to the transmit audio
path filter circuitry - removing a few components and changing
a couple of values.  The result is a flatter frequency response
through the voice-audio range (up to 10 kHz) which I have learned
is essential for G3RUH modulation.  The changes are all compatible
with the current PC board layout, and will not affect 1200-baud
operation with the new firmware.

-  AX.25 V2.2 support, featuring extended packet sequence numbers,
larger receive windows, SREJ selective packet loss error recovery,
and XID negotiation of window and packet sizes.  V2.2 support is
automatic when receiving inbound connections, and optional when
making outbound connections.

## Public release 3.1, 11/3/2024

Adds support for using digipeaters, and for acting as a digipeater
(standard or APRS).  Adds LAMPTEST feature.  Improvements to HELP
subsystem.  See the [release notes](Documentation/Release-notes.md)
for details.

![OpenTNC-Pico, v0.5 board](photos/v0.5-small.jpg)

Quick start: here's [the PC board design Gerber file
collection](schematic/Raspberry_Pi_Pico/OpenTNC-Pico-v0.9-Gerbers.zip),
[the bill of
materials](schematic/Raspberry_Pi_Pico/OpenTNC-Pico-v0.8-BOM.csv), and
[the ready-to-flash firmware image](images).

The repository may be cloned via

```git clone https://gitlab.com/davidplatt/opentnc```

[Release notes](Documentation/Release-notes.md)

[License](License.md)

[Overview](Documentation/Overview.md)

[Features](Documentation/Features.md)

[Rationale](Documentation/Rationale.md)

[Frequently Asked Questions](Documentation/FAQs.md)

[Building one of your own](Documentation/Building.md)

[Component selection](Documentation/Components.md)

[Firmware - building and flashing](Documentation/Firmware.md)

[Operating-system compatibility](Documentation/Compatibility.md)

[Connector pinouts](Documentation/Pinouts.pdf)

[Calibrating transmit level](Documentation/Calibrating.md)

[Operation](Documentation/Operation.md)

[FX.25 error correction](Documentation/FX25.md)

[AX.25 Version 2.2](Documentation/V2.2.md)

[9600-baud operation](Documentation/G3RUH.md)

[Digipeaters and digipeating](Documentation/Digipeating.md)

[Technical details](Documentation/Technical.md)

[Limitations](Documentation/Limitations.md)

[Future possibilities](Documentation/Future.md)

[Porting to other boards or processors](Documentation/Porting.md)

</BODY>
</HTML>
