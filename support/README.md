<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## Microsoft Windows driver support

Windows version 10 (all recent revisions), and Windows version 11 have
a standard "class" driver for USB CDC-ADM communication devices.  This
driver will be loaded and used automatically when you connect an OpenTNC
to one of these Windows versions. The device will show up as a COM port
and can be selected in your packet application or terminal emulator.

Things are a bit more complex for Windows 7 and 8, since these versions
do have a CDC-ACM driver, but don't automatically associate this driver
with newly-connected devices.  A driver .inf file containing the
appropriate USB vendor and product IDs is needed.

For Windows 7, I've provided an "opentnc.inf" file in this directory
which will do the job.  You may need to use the Control Panel's
"hardware" pane to select the OpenTNC, click on "Update driver", and
then browse to the directory in which this file is stored, and instruct
Windows to use it.  You will receive a security warning saying that
Windows can't guarantee its integrity (it isn't digitally signed) but
you'll have the option to use it anyway.  It does _not_ install a
driver of its own on your machine - it simply tells Windows to use
a Microsoft-authored driver already present.

For Windows 8, I don't have a good solution to offer at this time.
Hardware-specific .inf driver-installation files _must_ have a
proper digital signature to be used on Windows 8, and as I am not
a Microsoft developer and do not have an authenticated key with
which to sign this .inf file I can't release one which will work.
Sorry 'bout that!

