//---------------------------------------------------------
// Yet Another Parameterized Projectbox generator
//
//  This is a box for the OpenTNC
//
//  Version 1.0 (10/16/2024)
//
// This design is parameterized based on the size of a PCB.
//---------------------------------------------------------

// Exact distances between the mounting holes, based on the coordinates
// in the KiCAD PCB file.

mountingDeltaX = 77.2;
mountingDeltaY = 90.7;
ledRadius = 3;
ledOffsetAboveBoard = 5;


include <YAPPgenerator_v3.scad>

debug = false;

// Note: length/lengte refers to X axis, 
//       width/breedte to Y, 
//       height/hoogte to Z

/*
      padding-back|<------pcb length --->|<padding-front
                            RIGHT
        0    X-as ---> 
        +----------------------------------------+   ---
        |                                        |    ^
        |                                        |   padding-right 
        |                                        |    v
        |    -5,y +----------------------+       |   ---              
 B    Y |         | 0,y              x,y |       |     ^              F
 A    - |         |                      |       |     |              R
 C    a |         |                      |       |     | pcb width    O
 K    s |         |                      |       |     |              N
        |         | 0,0              x,0 |       |     v              T
      ^ |   -5,0  +----------------------+       |   ---
      | |                                        |    padding-left
      0 +----------------------------------------+   ---
        0    X-as --->
                          LEFT
*/

printBaseShell      = false;
printLidShell       = true;

// Edit these parameters for your own board dimensions
wallThickness       = 1.4;
basePlaneThickness  = 1.8;
lidPlaneThickness   = 1.8;

baseWallHeight      = 24;
lidWallHeight       =  8;

// Total height of box = basePlaneThickness + lidPlaneThickness 
//                     + baseWallHeight + lidWallHeight
pcbLength           = 100;
pcbWidth            = 100;
pcbThickness        = 1.5;
                            
// padding between pcb and inside wall
paddingFront        = 5;
paddingBack         = 5;
paddingRight        = 5;
paddingLeft         = 5;

// ridge where base and lid off box can overlap
// Make sure this isn't less than lidWallHeight
ridgeHeight         = 3;
ridgeSlack          = 0.2;

roundRadius         = 2.0;

// How much the PCB needs to be raised from the base
// to leave room for solderings and whatnot
standoffHeight      = 4;
standoffPinDiameter = 3;  // 3.7mm NPTH called for
standoffHoleSlack   = 0.1;
standoffDiameter    = 5;


// Set the layer height of your printer
printerLayerHeight  = 0.2;


// My parameters
ledHeight = ledOffsetAboveBoard + standoffHeight + pcbThickness;
pcbCenter = (pcbWidth / 2) + paddingLeft;




//-- C O N T R O L ---------------//-> Default -----------------------------
showSideBySide        = true;     //-> true
previewQuality        = 5;        //-> from 1 to 32, Default = 5
renderQuality         = 6;        //-> from 1 to 32, Default = 8
onLidGap              = 3;
shiftLid              = 5;
colorLid              = "YellowGreen";   
alphaLid              = 1;
colorBase             = "BurlyWood";
alphaBase             = 1;
hideLidWalls          = false;    //-> false
hideBaseWalls         = false;    //-> false
showOrientation       = true;
showPCB               = false;
showSwitches          = false;
showMarkersBoxOutside = false;
showMarkersBoxInside  = false;
showMarkersPCB        = false;
showMarkersCenter     = false;
inspectX              = 0;        //-> 0=none (>0 from Back)
inspectY              = 0;        //-> 0=none (>0 from Right)
inspectZ              = 0;        //-> 0=none (>0 from Bottom)
inspectXfromBack      = true;     //-> View from the inspection cut foreward
inspectYfromLeft      = true;     //-> View from the inspection cut to the right
inspectZfromTop       = false;    //-> View from the inspection cut down
//-- C O N T R O L -----------------------------------------------------------


//===================================================================
// *** PCB Supports ***
// Pin and Socket standoffs 
//-------------------------------------------------------------------
//  Default origin =  yappCoordPCB : pcb[0,0,0]
//
//  Parameters:
//   Required:
//    p(0) = posx
//    p(1) = posy
//   Optional:
//    p(2) = Height to bottom of PCB : Default = standoffHeight
//    p(3) = PCB Gap : Default = -1 : Default for yappCoordPCB=pcbThickness, yappCoordBox=0
//    p(4) = standoffDiameter    Default = standoffDiameter;
//    p(5) = standoffPinDiameter Default = standoffPinDiameter;
//    p(6) = standoffHoleSlack   Default = standoffHoleSlack;
//    p(7) = filletRadius (0 = auto size)
//    n(a) = { <yappBoth> | yappLidOnly | yappBaseOnly }
//    n(b) = { <yappPin>, yappHole } // Baseplate support treatment
//    n(c) = { <yappAllCorners>, yappFrontLeft | yappFrontRight | yappBackLeft | yappBackRight }
//    n(d) = { <yappCoordPCB> | yappCoordBox | yappCoordBoxInside }
//    n(e) = { yappNoFillet }
//-------------------------------------------------------------------

// Four connectors, sized for M3 screws, with M3 heat-set inserts

pcbStands   =  
[
    [4.35, 4.30, yappBackLeft],
    [4.35 + mountingDeltaX, 4.30, yappBackLeft],
    [4.35, 4.30 + mountingDeltaY, yappBackLeft],
    [4.35 + mountingDeltaX, 4.30 + mountingDeltaY, yappBackLeft]
];
 
//pcbStands   =  
//[
//    [4.35, 4.30, 0, 3.2, 6, 3.9, 10, yappBackLeft],
//    [4.35 + mountingDeltaX, 4.30, 0, 3.2, 6, 3.9, 10, yappBackLeft],
//    [4.35, 4.30 + mountingDeltaY, 0, 3.2, 6, 3.9, 10, yappBackLeft],
//    [4.35 + mountingDeltaX, 4.30 + mountingDeltaY, 0, 3.2, 6, 3.9, 10, yappBackLeft]
//];
 

//===================================================================
//  *** Cutouts ***
//    There are 6 cutouts one for each surface:
//      cutoutsBase (Bottom), cutoutsLid (Top), cutoutsFront, cutoutsBack, cutoutsLeft, cutoutsRight
//-------------------------------------------------------------------
//  Default origin = yappCoordBox: box[0,0,0]
//
//                        Required                Not Used        Note
//----------------------+-----------------------+---------------+------------------------------------
//  yappRectangle       | width, length         | radius        |
//  yappCircle          | radius                | width, length |
//  yappRoundedRect     | width, length, radius |               |     
//  yappCircleWithFlats | width, radius         | length        | length=distance between flats
//  yappCircleWithKey   | width, length, radius |               | width = key width length=key depth
//  yappPolygon         | width, length         | radius        | yappPolygonDef object must be
//                      |                       |               | provided
//----------------------+-----------------------+---------------+------------------------------------
//
//  Parameters:
//   Required:
//    p(0) = from Back
//    p(1) = from Left
//    p(2) = width
//    p(3) = length
//    p(4) = radius
//    p(5) = shape : {yappRectangle | yappCircle | yappPolygon | yappRoundedRect 
//                    | yappCircleWithFlats | yappCircleWithKey}
//  Optional:
//    p(6) = depth : Default = 0/Auto : 0 = Auto (plane thickness)
//    p(7) = angle : Default = 0
//    n(a) = { yappPolygonDef } : Required if shape = yappPolygon specified -
//    n(b) = { yappMaskDef } : If a yappMaskDef object is added it will be used as a mask 
//                             for the cutout.
//    n(c) = { [yappMaskDef, hOffset, vOffset, rotation] } : If a list for a mask is added 
//                              it will be used as a mask for the cutout. With the Rotation 
//                              and offsets applied. This can be used to fine tune the mask
//                              placement within the opening.
//    n(d) = { <yappCoordPCB> | yappCoordBox | yappCoordBoxInside }
//    n(e) = { <yappOrigin>, yappCenter }
//    n(f) = { <yappGlobalOrigin>, yappLeftOrigin } // Only affects Top(lid), Back and Right Faces
//-------------------------------------------------------------------
cutoutsLid =  
[
];

//===================================================================
//  Parameters:
//   Required:
//    p(0) = from Back
//    p(1) = from Left
//    p(2) = width
//    p(3) = length
//    p(4) = radius
//    p(5) = shape : {yappRectangle | yappCircle | yappPolygon | yappRoundedRect 
//                    | yappCircleWithFlats | yappCircleWithKey}
//  Optional:
//    p(6) = depth : Default = 0/Auto : 0 = Auto (plane thickness)
//    p(7) = angle : Default = 0
//    n(a) = { yappPolygonDef } : Required if shape = yappPolygon specified -
//    n(b) = { yappMaskDef } : If a yappMaskDef object is added it will be used as a mask 
//                             for the cutout.
//    n(c) = { [yappMaskDef, hOffset, vOffset, rotation] } : If a list for a mask is added 
//                              it will be used as a mask for the cutout. With the Rotation 
//                              and offsets applied. This can be used to fine tune the mask
//                              placement within the opening.
//    n(d) = { <yappCoordPCB> | yappCoordBox | yappCoordBoxInside }
//    n(e) = { <yappOrigin>, yappCenter }
//    n(f) = { <yappGlobalOrigin>, yappLeftOrigin } // Only affects Top(lid), Back and Right Faces
//-------------------------------------------------------------------
cutoutsBase =   
[
];

//-- front plane  -- origin is pcb[0,0,0]
// (0) = posy
// (1) = posz

echo("led", ledOffsetAboveBoard=ledOffsetAboveBoard,
ledRadius=ledRadius);

cutoutsFront =  
[
    [(pcbWidth / 2), ledOffsetAboveBoard, 0, 0, ledRadius, yappCircle, yappCenter]
   ,[(pcbWidth / 2) + 20, ledOffsetAboveBoard, 0, 0, ledRadius, yappCircle, yappCenter]
   ,[(pcbWidth / 2) + 40, ledOffsetAboveBoard, 0, 0, ledRadius, yappCircle, yappCenter]
   ,[(pcbWidth / 2) - 20, ledOffsetAboveBoard, 0, 0, ledRadius, yappCircle, yappCenter]
   ,[(pcbWidth / 2) - 40, ledOffsetAboveBoard, 0, 0, ledRadius, yappCircle, yappCenter]
];

//===================================================================
//  Parameters:
//   Required:
//    p(0) = from Back
//    p(1) = from Left
//    p(2) = width
//    p(3) = length
//    p(4) = radius
//    p(5) = shape : {yappRectangle | yappCircle | yappPolygon | yappRoundedRect 
//                    | yappCircleWithFlats | yappCircleWithKey}
//  Optional:
//    p(6) = depth : Default = 0/Auto : 0 = Auto (plane thickness)
//    p(7) = angle : Default = 0
//    n(a) = { yappPolygonDef } : Required if shape = yappPolygon specified -
//    n(b) = { yappMaskDef } : If a yappMaskDef object is added it will be used as a mask 
//                             for the cutout.
//    n(c) = { [yappMaskDef, hOffset, vOffset, rotation] } : If a list for a mask is added 
//                              it will be used as a mask for the cutout. With the Rotation 
//                              and offsets applied. This can be used to fine tune the mask
//                              placement within the opening.
//    n(d) = { <yappCoordPCB> | yappCoordBox | yappCoordBoxInside }
//    n(e) = { <yappOrigin>, yappCenter }
//    n(f) = { <yappGlobalOrigin>, yappLeftOrigin } // Only affects Top(lid), Back and Right Faces
//-------------------------------------------------------------------
//cutoutsBack =   
//[
//    [71.2, -1, 0, 0, 6, yappCircle, yappCoordPCB] // USB
//   ,[43.2, 0, 0, 0, 9, yappCircle, yappCoordPCB] // DIN-5
//   ,[9, 0, 32, 19, 0, yappRectangle, yappCoordPCB] // DE-9
//
//];

// Simple rectangular cutout for access to all ports, with a slight
// enlargement for the body of the DIN-5 TNC plug.
cutoutsBack =   
[
    [7, -1, 82, 23, 0, yappRectangle, yappCoordPCB] 
   ,[43.2, 0, 0, 0, 13, yappCircle, yappCoordPCB] // DIN-5
];


//-- left plane   -- origin is pcb[0,0,0]
// (0) = posx
// (1) = posz
cutoutsLeft =   
[
];

//===================================================================
//  Parameters:
//   Required:
//    p(0) = from Back
//    p(1) = from Left
//    p(2) = width
//    p(3) = length
//    p(4) = radius
//    p(5) = shape : {yappRectangle | yappCircle | yappPolygon | yappRoundedRect 
//                    | yappCircleWithFlats | yappCircleWithKey}
//  Optional:
//    p(6) = depth : Default = 0/Auto : 0 = Auto (plane thickness)
//    p(7) = angle : Default = 0
//    n(a) = { yappPolygonDef } : Required if shape = yappPolygon specified -
//    n(b) = { yappMaskDef } : If a yappMaskDef object is added it will be used as a mask 
//                             for the cutout.
//    n(c) = { [yappMaskDef, hOffset, vOffset, rotation] } : If a list for a mask is added 
//                              it will be used as a mask for the cutout. With the Rotation 
//                              and offsets applied. This can be used to fine tune the mask
//                              placement within the opening.
//    n(d) = { <yappCoordPCB> | yappCoordBox | yappCoordBoxInside }
//    n(e) = { <yappOrigin>, yappCenter }
//    n(f) = { <yappGlobalOrigin>, yappLeftOrigin } // Only affects Top(lid), Back and Right Faces
//-------------------------------------------------------------------
cutoutsRight =  
[
];

///-- connectors 
//-- normal         : origen = box[0,0,0]
//-- yappConnWithPCB: origen = pcb[0,0,0]
// (0) = posx
// (1) = posy
// (1) = pcbStandHeight
// (3) = screwDiameter
// (4) = screwHeadDiameter
// (5) = insertDiameter
// (6) = outsideDiameter
// (7) = flangeHeight
// (8) = flangeDiam
// (9) = { yappConnWithPCB }
// (10) = { yappAllCorners | yappFrontLeft | yappFrontRight | yappBackLeft | yappBackRight }



//-- base mounts -- origen = box[x0,y0]
// (0) = posx | posy
// (1) = screwDiameter
// (2) = width
// (3) = height
// (4..7) = yappLeft / yappRight / yappFront / yappBack (one or more)
// (5) = { yappCenter }
boxMounts   = 
[
];

//-- snap Joins -- origen = box[x0,y0]
// (0) = posx | posy
// (1) = width
// (2..5) = yappLeft / yappRight / yappFront / yappBack (one or more)
// (n) = { yappSymmetric }
snapJoins   =     
[
    [20,               5, yappLeft, yappRight, yappSymmetric],
];
               
//-- origin of labels is box [0,0,0]
// (0) = posx
// (1) = posy/z
// (2) = orientation
// (3) = plane {lid | base | left | right | front | back }
// (4) = font
// (5) = size
// (6) = "label text"
labelsPlane =  
[
    [ 25, 25, 0, .5, yappFront,     "Liberation Mono:style=bold", 6, "AE6EO OpenTNC"  ]
   ,[  10, 17, 0, .5, yappFront, "Liberation Mono:style=bold", 5, "Run"  ]
   ,[ 27.5, 17, 0, .5, yappFront, "Liberation Mono:style=bold", 5, "Conn"  ]
   ,[ 48, 17, 0, .5, yappFront, "Liberation Mono:style=bold", 5, "Carr"  ]
   ,[ 70.5, 17, 0, .5, yappFront, "Liberation Mono:style=bold", 5, "Pkt"  ]
   ,[ 90, 17, 0, .5, yappFront, "Liberation Mono:style=bold", 5, "PTT"  ]
];
               
module hookBaseOutside()
{
} //  hookBaseOutside()

module lidHook()
{
  
} //  lidHook()

//---- This is where the magic happens ----
YAPPgenerate();

