#!/bin/bash
set -e
major=$(fgrep OPENTNC_MAJOR_VERSION src/globals.h | awk '{print $3}') || exit 1
minor=$(fgrep OPENTNC_MINOR_VERSION src/globals.h | awk '{print $3}') || exit 1
builddate=$(date +%+4Y%m%d)
echo "Build stamp will be V$major.$minor-$builddate"
west build -p -b blackpill_f411ce
if [ -d images ] ; then
    elf2dfuse build/zephyr/zephyr.elf images/OpenTNC-BlackPill-V$major.$minor-$builddate.dfu
fi
west build -p -b rpi_pico
if [ -d images ] ; then
    cp build/zephyr/zephyr.uf2 images/OpenTNC-Pico-V$major.$minor-$builddate.uf2
fi
 
