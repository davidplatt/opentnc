<HTML>
<meta charset="UTF-8"> 
<HEAD>
<TITLE>The AE6EO OpenTNC project</TITLE>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

# OpenTNC-Pico PCB and schematic history

![OpenTNC printed circuit board, version 0.5](../../photos/OpenTNC-Pico-board-brighter.jpg)  

## 0.3

First board manufactured.  Two usable prototypes built.

Found one error in the Gerbers - an open trace in the RX-audio line,
between the radio connectors and the ferrite bead.  The "Round traces"
KiCAD script I'd used mis-handled the trace junction for some reason,
and (my bad!) I failed to run the design-rules check after rounding
the traces.  Won't make that mistake again any time soon.  One
blue-wire fix for this error is required to make the 0.3 board usable.

## 0.4

Never manufactured.  Fixed the open trace in 0.3, and moved the
front-panel LEDs onto exact 2.00 cm spacing to make it easier to
3D-print or drill a front panel.

## 0.5

Added optional (but highly recommended!) circuitry to disable the
noisy-but-efficient switching regulator on the Pico board, and provide
cleaner 3.3-volt power via a low-dropout linear regulator.  Cutting a
new jumper will re-enable the switching regulator if necessary.

Changed LED series resistors from 1k to 470R to increase brightness.

Manufactured. One good prototype built, using parts I ordered from
Mouser using the BOM CSV file.  Receive performance is quite good
(as good as the better of the 0.3 prototypes, rather better than
the other) and appears consistent from run to run.  Tested successfully
over-the-air using an Icom IC-W32 (via TNC-2 connector) and a Kenwood
TM-G707 (via the DB-9 connector).

## 0.6

Not yet manufactured.  Minor tweak to 0.5, changing the linear-regulator
footprint to a "wide in-line" one which is a better fit for available
parts that come with their leads pre-bent.  Moved the bypass caps a
smidge to make room for the larger footprint.

## v0.7

Added a 1k resistor in series with the Pico's ADC input
pin.  This will limit current into the pin (and its
ESD-protection diodes) to a safe level, if the RxA
input is overdriven or if the TNC and Pico are powered
off.

Added connector pads for accessing TxA, RxA, and
earth-ground, to facilitate oscilloscope examination
of the audio signals.

This board revision passes KiCAD design rule checks,
but has not yet been manufactured.  I recommend using
it for new builds, as the changes from the most recent
fully-tested board (0.5) are minor.

</BODY>
</HTML>
