# Copyright (c) 2019 Nordic Semiconductor ASA
# SPDX-License-Identifier: Apache-2.0

# config USB_DEVICE_PID
# 	default USB_PID_CDC_ACM_SAMPLE

config ADC_BUFFER_BYTES_LOGARITHM
       int "Log(2) of number of bytes per ADC input buffer"
       default 10
       help
           Specifies the log2 of the number of bytes per ADC input
	   buffer (there are two buffers required).  This value is used
	   to compute the actual buffer size and memory alignment of
	   the buffer, and is used directly as the "ring bytes" value
	   for some architectures' circular DMA.  The default is 10,
	   or 1024 bytes per buffer - this is roughly 1 millisecond
	   for G3RUH 9600-baud, and 13 milliseconds for 1200-baud AFSK.

config OUTPUT_BUFFER_BYTES_LOGARITHM
       int "Log(2) of number of bytes per modulator buffer"
       default 10
       help
           Specifies the log2 of the number of bytes per PWM/D-S output
	   buffer (there are two buffers required).  This value is used
	   to compute the actual buffer size and memory alignment of
	   the buffer, and is used directly as the "ring bytes" value
	   for some architectures' circular DMA.

config BUFFER_COMPLETION_RATE
       int "Desired frequency in Hz of DMA 'buffer full' interrupts"
       default 200
       help
	   Specifies the suggested frequency in Hz at which an ADC or PWM
	   "buffer full" interrupt will occur, and thus (by
	   implication) the number of samples per ADC or PWM transfer,
	   and the amount of latency in signal processing.  The
	   service frequency may actually be higher than specified
	   here, if the buffer size isn't large enough to hold the
	   necessary number of samples at the current receive baud
	   rate.

config G3RUH_SUPPORT
       bool "G3RUH 9600-baud mode support"
       default n
       help
           Specifies whether G3RUH 9600-baud support is available.

config WORK_AREA_SIZE
       int "Size (bytes) of occasional-big-stuff work area"
       default 2048
       help
           Specifies the size of a mutex-guarded static-memory area
	   which can be used for occasional, space-intensive tasks.

config MAX_IFRAME_PAYLOAD
       int "Size (bytes) of the largest AX.25 I-frame size supported"
       default 512
       help
           Specifies the size of the largest I-frame payload which
	   can be sent or received.  A minimum of 256 is required for
	   compatibility with most other TNCs.  This value will be
	   shared with peer TNCs during AX25 v2.2 XID negotiation.

source "Kconfig.zephyr"
