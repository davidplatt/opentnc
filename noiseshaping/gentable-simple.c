/*
 * Copyright (c) 2023, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <tgmath.h>
#include <unistd.h>
#include <getopt.h>

#include <gsl_rng.h>

// System clock rate
uint32_t SYSTEM_CLOCK_RATE =  96000000;

// Clock delta-sigma bits out at 1.5 MHz
uint32_t BIT_RATE = 1500000;

// Signal baud rate
uint32_t BAUD_RATE = 1200;

// Byte ordering is LSB-first
bool BYTE_ORDERING_IS_LSB = false;

// PWM oversampling rate (not the same as the "oversampling" variable
// used internally by the delta-sigma calculations
#define PWM_OVERSAMPLING_RATIO 4

uint32_t FREQ_1 = 1200;
uint32_t FREQ_2 = 2200;

uint32_t GREATEST_COMMON_FACTOR;

uint32_t FREQ_1_REDUCED;
uint32_t FREQ_2_REDUCED;
uint32_t PHASES;
uint32_t STEPS_1;
uint32_t STEPS_2;
uint32_t BURSTS_PER_CYCLE_1;
uint32_t BURSTS_PER_CYCLE_2;
uint32_t BURSTS_PER_BAUD_1;
uint32_t BURSTS_PER_BAUD_2;

uint32_t BURSTS_PER_BAUD;

int phases;
int increments_1;
int increments_2;
int bytes_per_baud = -1;
int pwm_period = -1;
int order = 1;
float sysclk_divisor;

void get_options(int argc, char **argv) {
  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    static struct option long_options[] = {
      {"bitclock", required_argument, 0, 'r' },
      {"tone1",    required_argument, 0, '1' },
      {"tone2",    required_argument, 0, '2' },
      {"bytes",    required_argument, 0, 'b' },
      {"lsb",      no_argument,       0, 'l' },
      {"order",    required_argument, 0, 'O' },
      {"pwm",      required_argument, 0, 'p' },
      {0,         0,                  0,  0 }
    };
    int c = getopt_long(argc, argv, "r:1:2:b:O:p:l",
			long_options, &option_index);
    if (c == -1)
      break;

    switch (c) {
    case 'r':
      SYSTEM_CLOCK_RATE = atoi(optarg);
      break;
    case '1':
      FREQ_1 = atoi(optarg);
      break;
    case '2':
      FREQ_2 = atoi(optarg);
      break;
    case 'b':
      bytes_per_baud = atoi(optarg);
      break;
    case 'O':
      order = atoi(optarg);
      if (order != 1 && order != 2) {
	fprintf(stderr, "Order of %d is not supported - must be 1 or 2\n", order);
	order = 1;
      }
      break;
    case 'p':
      pwm_period = atoi(optarg);
      if (pwm_period < 2 || pwm_period > 65535) {
	fprintf(stderr, "--pwm must be between 2 and 65535\n");
	pwm_period = -1;
      }
      break;
    case 'l':
      BYTE_ORDERING_IS_LSB = true;
      break;
    default:
      fprintf(stderr, "getopt returned character code 0%0x\n", c);
    }
  }

  if (optind < argc) {
    fprintf(stderr, "non-option ARGV-elements: ");
    while (optind < argc)
      fprintf(stderr, "%s ", argv[optind++]);
    fprintf(stderr, "\n");
  }
}


uint32_t gcd(uint32_t a, uint32_t b) {
  if (a == 0 || b == 0) return 0;
  if (a == b) return a;
  if (a > b) return gcd(a - b, b);
  return gcd(b - a, a);
}

void do_the_math() {

  GREATEST_COMMON_FACTOR = gcd(FREQ_1, FREQ_2);
  
  FREQ_1_REDUCED = (FREQ_1 / GREATEST_COMMON_FACTOR);
  FREQ_2_REDUCED = (FREQ_2 / GREATEST_COMMON_FACTOR);

  PHASES = (FREQ_1_REDUCED * FREQ_2_REDUCED);

  STEPS_1 = (PHASES / FREQ_2_REDUCED);
  STEPS_2 = (PHASES / FREQ_1_REDUCED);

  BURSTS_PER_CYCLE_1 = (PHASES / FREQ_1_REDUCED);
  BURSTS_PER_CYCLE_2 = (PHASES / FREQ_2_REDUCED);

  BURSTS_PER_BAUD_1 = (PHASES * FREQ_1 / BAUD_RATE);
  BURSTS_PER_BAUD_2 = (PHASES * FREQ_2 / BAUD_RATE);

  BURSTS_PER_BAUD = (BURSTS_PER_CYCLE_1 > BURSTS_PER_CYCLE_2 ? BURSTS_PER_CYCLE_1 : BURSTS_PER_CYCLE_2);

  phases = PHASES;
  increments_1 = (BURSTS_PER_BAUD_1) % PHASES;
  increments_2 = (BURSTS_PER_BAUD_2) % PHASES;

}

#define PI 3.14159265

#define DAMPING 0.99
#define ERROR_SCALING 1
#define DITHER_SCALING 1.0
#define SIGNAL_SCALING 0.97
#define PWM_SIGNAL_SCALING 0.99

#define DITHER_TPDF 1

#define MAX_BYTES_PER 8192
#define MAX_PHASE_ANGLES 32


int oversampling;

uint8_t bytearray[2][MAX_PHASE_ANGLES][MAX_BYTES_PER];

int pwm_sampling_rate;
int clocks_per_pwm;
int pwm_entries;
int *pwm_values;

void buildTable(void) {
  // Start out by reducing the number of phases needed, to the
  // minimum permitted.
  gsl_rng *randomizer = gsl_rng_alloc(gsl_rng_taus);
  for (int i = 2; i < phases; i++) {
    while (phases % i == 0 &&
	   increments_1 % i == 0 &&
	   increments_2 % i == 0) {
      phases /= i;
      increments_1 /= i;
      increments_2 /= i;
    }
  }
  assert(phases <= MAX_PHASE_ANGLES);
  float bits_per_baud;
  if (bytes_per_baud > 0) {
    // If the user has forced a specific number of bytes per baud, compute
    // the necessary bit rate.
    bits_per_baud = bytes_per_baud * 8;
    BIT_RATE = bits_per_baud * BAUD_RATE;
  } else  {
    // Figure out the oversampling ratio (number of bits per baud) which:
    // (1) is a multiple of 8, and
    // (2) results in the least deviation from the desired baud rate
    bits_per_baud = BIT_RATE / BAUD_RATE;
    bytes_per_baud = (int) (bits_per_baud / 8 + .5);
  }
  sysclk_divisor = (float) SYSTEM_CLOCK_RATE / BIT_RATE;

  int bits_per_cycle_1 = bits_per_baud * FREQ_1 / BAUD_RATE;
  float bits_per_burst_1 = bits_per_cycle_1 / BURSTS_PER_CYCLE_1;
  printf("// Bit ordering: %s-first\n", BYTE_ORDERING_IS_LSB ? "LSB" : "MSB");
  printf("// System clock rate: %d\n", SYSTEM_CLOCK_RATE);
  printf("// Bit rate: %d\n", BIT_RATE);
  printf("// Bit-clock divisor: %f\n", sysclk_divisor);
  printf("// Baud rate: %d\n", BAUD_RATE);
  printf("// Bits per baud: %f\n", bits_per_baud);
  printf("// Bytes per baud (rounded): %d\n", bytes_per_baud);
  printf("// Phases per cycle: %d\n", phases);
  printf("// Phase bursts per baud: %d\n", BURSTS_PER_BAUD);
  printf("// Tone 1 phase increment: %d\n", increments_1);
  printf("// Tone 2 phase increment: %d\n", increments_2);
  oversampling = bytes_per_baud * 8;
  printf("// Oversampling (computed bits per burst), tone 1: %d\n", oversampling);
  for (int tone = 0; tone < 2; tone++) {
    // Tone 0 (low frequency) advances more slowly.
    int freq = tone ? FREQ_2 : FREQ_1;
    float angleCovered = 2 * PI * freq / BAUD_RATE;  // Angle covered in 1 baud
    float angleIncrement = angleCovered / oversampling;
    for (int starting_phase = 0; starting_phase < phases; starting_phase++) {
      float starting_angle = 2.0 * PI * (float) starting_phase / phases;
      float integrator = 0.0;
      float integrator2 = 0.0;
      float current = 0.0;
      int quantized = 1;
      uint8_t byte_construct = 0;
      int bits_packed = 0, bytes_packed = 0;
      float angle = -999.0;
      // Experiment to try to avoid startup transients in the integrator(s).
      // Try generating a bitstream which starts one full baud "in the past",
      // but only start storing it once we reach "time zero".
      for (int i = -oversampling; i < oversampling; i++) {
	int bit;
#if DITHER_TPDF      
	float dither = (gsl_rng_uniform(randomizer) + gsl_rng_uniform(randomizer) - 1) * DITHER_SCALING / 2;
#else
	float dither = (gsl_rng_uniform(randomizer) - .5) * DITHER_SCALING;
#endif
	//      float dither = ((float) (random() - 0x3FFFFFFF)) / 0x7FFFFFFF * DITHER_SCALING;
	angle = starting_angle + angleIncrement * i;
	if (order == 1) {
	  float desired = sin(angle) * SIGNAL_SCALING;
	  float summed = integrator + dither;
	  int quantized = (summed > 0) ? 1 : -1;
	  bit = (summed > 0) ? 1 : 0;
	  float error = quantized * ERROR_SCALING;
	  integrator += desired - error;
	} else /* must be 2 */ {
	  float desired = sin(angle) * SIGNAL_SCALING / 4;
	  float summed = integrator2 * 16 + dither;
	  int quantized = (summed > 0) ? 1 : -1;
	  bit = (summed > 0) ? 1 : 0;
	  float error = quantized * ERROR_SCALING;
	  integrator2 += integrator / 4 - error / 8;
	  integrator += desired - error / 4;
	}
	current = (current * DAMPING) + (quantized * (1.0 - DAMPING));
	//	  printf("%d", bit);
	if (i >= 0) {
	  if (BYTE_ORDERING_IS_LSB) {
	    byte_construct = (byte_construct >> 1) | (bit << 7);
	  } else {
	    byte_construct = (byte_construct << 1) | bit;
	  }
	  if (++bits_packed == 8) {
	    bytearray[tone][starting_phase][bytes_packed++] = byte_construct;
	    bits_packed = 0;
	  }
	}
      }
      float next_angle = angle + angleIncrement;
      printf("// Tone %d phase %d starting angle %f ending angle %f residue %f next %f\n",
	     tone, starting_phase, starting_angle, angle,
	     fmod(angle, 2 * PI),
	     fmod(next_angle, 2 * PI));
    }
  }
}

void printTable(void) {
  printf("const struct noiseshaping {\n");
  printf("  bool bit_ordering_is_lsb_first;\n");
  printf("  int clock_rate;\n");
  printf("  int baud_rate;\n");
  printf("  float bitclock_divisor;\n");
  printf("  int phases;\n");
  printf("  int frequency_divisor;\n");
  printf("  int bytes_per_baud;\n");
  printf("  struct {  // array, indexed by tone\n");
  printf("      uint32_t frequency;\n");
  printf("      uint8_t phase_increment;\n");
  printf("      struct {\n");
  printf("          uint8_t samples[%d];\n", bytes_per_baud);
  printf("      } phases[%d];\n", phases);
  printf("  } tones[2];\n");
  printf("  uint8_t idle[%d];\n", bytes_per_baud);
  printf("} noiseshaping = {\n");
  printf("  .bit_ordering_is_lsb_first = %s,\n", BYTE_ORDERING_IS_LSB ? "true" : "false");
  printf("  .clock_rate = %d,\n", BIT_RATE);
  printf("  .baud_rate = %d,\n", BAUD_RATE);
  printf("  .bitclock_divisor = %f,\n", sysclk_divisor);
  printf("  .phases = %d,\n", phases);
  printf("  .frequency_divisor = %d,\n", GREATEST_COMMON_FACTOR);
  printf("  .bytes_per_baud = %d,\n", bytes_per_baud);
  for (int tone = 0; tone < 2; tone++) {
    int frequency = tone ? FREQ_2 : FREQ_1;
    int phase_increment = tone ? increments_2 : increments_1;
    printf("  .tones[%d].frequency = %d,\n", tone, frequency);
    printf("  .tones[%d].phase_increment = %d,\n", tone, phase_increment);
    for (int phase = 0; phase < phases; phase++) {
      printf("  .tones[%d].phases[%d].samples = {", tone, phase);
      for (int samp = 0; samp < bytes_per_baud; samp++) {
	printf("0x%02X,", bytearray[tone][phase][samp]);
      }
      printf("},\n");
    }
  }
  printf("  .idle = {");
    for (int samp = 0; samp < bytes_per_baud; samp++) {
      printf("0x55,");
    }
    printf("},\n");
  printf(" };\n");
}

/*
  Modulator 0 uses timer-based pulse-width modulation to generate
  a sinewave-type signal.  TIM3 is clocked at full speed (96 MHz,
  on the '411).  The PWM value is updated at the output oversampling
  rate (4 * 9600 samples/second, or 38400 samples/second).  This means
  that there are 96000000/38400 or 2500 clocks available per update.

  The TIM3 period can be set anywhere from 1 to 2500 clocks.  Larger
  periods mean greater resolution, but fewer updates per second,
  and the PWM switching noise is at a lower frequency.  Smaller periods
  push the PWM switching noise up in frequency (easier to filter out)
  but reduce the PWM resolution and thus increase the quantization noise
  in the generated signal.

  As a crude compromise, this code defaults to setting the period to
  sqrt(96000000/38400), thus having the same number of
  PWM-levels-per-period and PWM-periods- per-output-sample.  This choice
  can be overridden by using the -p (or --pwm) option (e.g. --pwm 256
  for a traditional 8-bit range).

  The PWM sample table (which represents the sinewave) has values which
  are scaled to the PWM period, centered at half that period, and have
  a (scaled) peak value matching that used by the delta-sigma modulator
  calculations.  The size of the table is somewhat arbitrary - making it
  larger helps reduce time-based quantization noise in cases where the
  interrupt service routine is delayed somewhat in running.
*/

void computePWM() {
  pwm_sampling_rate = BAUD_RATE * 8 * PWM_OVERSAMPLING_RATIO;
  clocks_per_pwm = SYSTEM_CLOCK_RATE / pwm_sampling_rate;
  if (pwm_period < 0) {
    pwm_period = sqrt(clocks_per_pwm);
  }
  pwm_entries = 128; // somewhat arbitrary
  pwm_values = (int *) malloc(pwm_entries * sizeof(*pwm_values));
  for (int i = 0; i < pwm_entries; i++) {
    pwm_values[i] = (pwm_period / 2) +
      pwm_period * PWM_SIGNAL_SCALING * sin((2.0 * PI * i) / pwm_entries) / 2;
  }
}

void printPWM() {
  printf("\n\n");
  printf("// System clock rate %d\n", SYSTEM_CLOCK_RATE);
  printf("// PWM sample rate %d\n", pwm_sampling_rate);
  printf("// Clocks per PWM period %d\n", clocks_per_pwm);
  printf("// PWM period %d\n", pwm_period);
  printf("// PWM entries %d\n", pwm_entries);
  printf("\n");
  printf("#define PWM_PERIOD %d\n", pwm_period);
  printf("#define PWM_ENTRIES %d\n", pwm_entries);
  printf("\n");
  printf("const int32_t pwmtable[PWM_ENTRIES] = {\n");
  for (int i = 0; i < pwm_entries; i++) {
    if (i == 0) {
      printf("\t");
    } else if (i % 16 == 0) {
      printf("\n\t");
    }
    printf("%3d, ", pwm_values[i]);
  }
  printf("\n};\n\n");
}


int main (int argc, char **argv) {
  get_options(argc, argv);
  do_the_math();
  buildTable();
  computePWM();
  printf("\n");
  printf("#include <stdint.h>\n");
  printf("#include <stdbool.h>\n");
  printf("\n");
  printTable();
  printPWM();
  return 0;
}

