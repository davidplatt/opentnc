#ifndef _H_WS2812
# define _H_WS2812

#include <zephyr/device.h>
#include <zephyr/devicetree.h>

// The LED-to-WS2812 library code doesn't implement any sort of
// mutex or semaphore synchronization around either the LED or
// WS2812 data.  If desired, it's the responsibility of higher-
// level code to do so.

#define NUM_WS2812 DT_PROP_OR(ZEPHYR_USER_NODE, ws2812_leds, 0)
#define WS2812_PADDED DT_PROP_OR(ZEPHYR_USER_NODE, ws2812_padded, 0)

#if NUM_WS2812 > 0

// The WS2812 expects data to arrive most-significant-bit first,
// green, red, blue.  This implies that PIO will be taking the
// 32-bit words we DMA, and shifting them out MSB-first, and
// *this* implies that the first byte to be shifted out must
// lie at the highest memory address in the data.  So, this
// structure looks a bit backwards, but that's necessary.

struct ws2812_led {
  #if WS2812_PADDED
  uint8_t pad;
  #endif
  uint8_t blue;
  uint8_t red;
  uint8_t green;
};

extern struct ws2812_led ws2812_leds[NUM_WS2812];

void encode_leds_to_ws2812(void);

#endif

#endif  // _H_WS2812


