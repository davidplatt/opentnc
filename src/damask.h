/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_DAMASK
#define _H_DAMASK

#include <stdint.h>
#include <stdbool.h>

#include "ax25.h"
#include "ax25buffer.h"
#include "connection.h"

#include <zephyr/sys/dlist.h>

typedef struct damask_client {
  sys_dnode_t node;
  encoded_callsign dest;  // client's
  encoded_callsign src;   // ours
  bool uses_fx25; // We've seen fx.25 from the client
  bool uses_non_fx25; // We've seen a non-fx.25 packet from the client
  int32_t priority;
  int32_t time_since_traffic;
  enum S_frame_ss last_state;
  int32_t last_n_s, last_n_r;
  sys_dlist_t outbound_buffers;
} damask_client;

enum damask_mode {
  DAMASK_OFF = 0,
  DAMASK_ROUND_ROBIN = 1,
  DAMASK_BY_PRIORITY = 2,
  DAMASK_KEEPALIVE = 3,
  DAMASK_FORCE_DAMA = 4
};

void damask_init(void);
void damask_examine_incoming(ax25buffer *buffer);
void damask_accept_outgoing(ax25buffer *buffer);
void damask_run(void);

bool damask_has_traffic(void);

#endif  // _H_DAMASK


