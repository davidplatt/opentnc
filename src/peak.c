/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include "peak.h"

void reset_peak_f(peak_f *peak) {
  for (int i = 0 ; i < peak->count; i++) {
    peak->samples[i] = 0.0f;
  }
  peak->cursor = 0;
}

float update_peak_f(peak_f *peak, float new_sample) {
  peak->samples[peak->cursor] = new_sample;
  peak->cursor = (peak->cursor + 1) % peak->count;
  float new_peak = peak->samples[0];
  for (int i = 1; i < peak->count; i++) {
    if (new_peak < peak->samples[i]) {
      new_peak =  peak->samples[i];
    }
  }
  return new_peak;
}


