/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/crc.h>

#include "ax25.h"
#include "ax25buffer.h"
#include "globals.h"

#define AX25_HEAP_SIZE 8192

LOG_MODULE_REGISTER(ax25buffer, LOG_LEVEL_INF);

K_MUTEX_DEFINE(ax25buffer_list_mutex);

K_HEAP_DEFINE(ax25_heap, AX25_HEAP_SIZE);

ax25buffer *ax25buffer_alloc(enum ax25buffer_type type, size_t bytes_needed) {
  LOG_DBG("Allocate a buffer of %d bytes", bytes_needed);
  uint8_t *data = k_heap_alloc(&ax25_heap, bytes_needed, K_NO_WAIT);
  if (!data) {
    return NULL;
  }
  ax25buffer *buffer = k_heap_alloc(&ax25_heap, sizeof(ax25buffer), K_NO_WAIT);
  if (!buffer) {
    k_heap_free(&ax25_heap, data);
    return NULL;
  }
  tnc_state.stats.heap_allocs += 2;
  memset(buffer, 0, sizeof(*buffer));
  memset(data, 0, bytes_needed);
  sys_dnode_init(&buffer->node);
  buffer->type = type;
  buffer->data = data;
  buffer->buffer_size = bytes_needed;
  LOG_DBG(" It's at %p", buffer);
  return buffer;
}

// "Clone me, Doctor Memory!"
ax25buffer *ax25buffer_clone(ax25buffer *orig) {
  size_t bytes_needed = (orig->bits_written + 7) / 8;
  LOG_DBG("Allocate a buffer of %d bytes", bytes_needed);
  uint8_t *data = k_heap_alloc(&ax25_heap, bytes_needed, K_NO_WAIT);
  if (!data) {
    return NULL;
  }
  ax25buffer *buffer = k_heap_alloc(&ax25_heap, sizeof(ax25buffer), K_NO_WAIT);
  if (!buffer) {
    k_heap_free(&ax25_heap, data);
    return NULL;
  }
  tnc_state.stats.heap_allocs += 2;
  memset(buffer, 0, sizeof(*buffer));
  memset(data, 0, bytes_needed);
  sys_dnode_init(&buffer->node);
  buffer->type = orig->type;
  buffer->data = data;
  buffer->buffer_size = bytes_needed;
  buffer->bits_written = orig->bits_written;
  buffer->bits_read = orig->bits_read;
  memcpy(buffer->data, orig->data, bytes_needed);
  LOG_DBG(" It's at %p", buffer);
  return buffer;
}

// Free a buffer.  Locks the heap mutex.
void ax25buffer_free(ax25buffer *buffer) {
  if (!buffer) {
    return;
  }
  LOG_DBG("Free buffer %p", buffer);
  k_mutex_lock(&ax25buffer_list_mutex, K_FOREVER);
  if (sys_dnode_is_linked(&buffer->node)) {
    sys_dlist_remove(&buffer->node);
  }
  k_mutex_unlock(&ax25buffer_list_mutex);
  if (buffer->data) {
    memset(buffer->data, 0, buffer->buffer_size);
    k_heap_free(&ax25_heap, buffer->data);
    tnc_state.stats.heap_frees++;
  }
  memset(buffer, 0, sizeof(*buffer));
  k_heap_free(&ax25_heap, buffer);
  tnc_state.stats.heap_frees++;
}

// Unlink and free the buffer at the head of a specific list.
void ax25buffer_free_head(sys_dlist_t *list) {
  LOG_DBG("Free head of list %p", list);
  ax25buffer_lock_mutex();
  ax25buffer *buffer = ax25buffer_peek_head(list);
  if (buffer) {
    ax25buffer_free(buffer);
  }
  ax25buffer_unlock_mutex();
}

// Return the buffer at the head of a list;
ax25buffer *ax25buffer_peek_head(sys_dlist_t *list) {
  LOG_DBG("Get head of list %p", list);
  ax25buffer_lock_mutex();
  sys_dnode_t *head = sys_dlist_peek_head(list);
  ax25buffer *buffer = NULL;
  if (head) {
    LOG_DBG(" Head is %p", head);
    buffer = SYS_DLIST_CONTAINER(head, buffer, node);
  }
  ax25buffer_unlock_mutex();
  LOG_DBG(" Buffer is %p", buffer);
  return buffer;
}

// Return the buffer at the head of a list, unlinking it from
// the list.
ax25buffer *ax25buffer_take_head(sys_dlist_t *list) {
  LOG_DBG("Get head of list %p", list);
  ax25buffer_lock_mutex();
  sys_dnode_t *head = sys_dlist_peek_head(list);
  ax25buffer *buffer = NULL;
  if (head) {
    LOG_DBG(" Head is %p", head);
    buffer = SYS_DLIST_CONTAINER(head, buffer, node);
    sys_dlist_remove(head);
  }
  ax25buffer_unlock_mutex();
  LOG_DBG(" Buffer is %p", buffer);
  return buffer;
}

// Remove a buffer from whatever list (if any) it's on, and append or
// prepend it to the tail or head of the specified list.  Locks the
// buffer-management mutex during execution.
void ax25buffer_append_to_dlist(ax25buffer *buffer, sys_dlist_t *list) {
  LOG_DBG("Append buffer %p to %p", buffer, list);
  k_mutex_lock(&ax25buffer_list_mutex, K_FOREVER);
  if (sys_dnode_is_linked(&buffer->node)) {
    LOG_DBG("  Unlinking it first");
    sys_dlist_remove(&buffer->node);
  }
  sys_dlist_append(list, &buffer->node);
  k_mutex_unlock(&ax25buffer_list_mutex);
}

void ax25buffer_prepend_to_dlist(ax25buffer *buffer, sys_dlist_t *list) {
  LOG_DBG("Prepend buffer %p to %p", buffer, list);
  k_mutex_lock(&ax25buffer_list_mutex, K_FOREVER);
  if (sys_dnode_is_linked(&buffer->node)) {
    sys_dlist_remove(&buffer->node);
  }
  sys_dlist_prepend(list, &buffer->node);
  k_mutex_unlock(&ax25buffer_list_mutex);
}

// Manually lock and unlock the buffer-management mutex.  Use these if
// you're going to be iterating through the lists, to ensure that no other
// thread alters a list you're iterating.  The lock function will wait
// K_FOREVER until the mutex is available.

void ax25buffer_lock_mutex(void) {
  k_mutex_lock(&ax25buffer_list_mutex, K_FOREVER);
}

void ax25buffer_unlock_mutex(void) {
  k_mutex_unlock(&ax25buffer_list_mutex);
}

void ax25buffer_put_byte(struct ax25buffer *buffer, uint8_t byte) {
  if (buffer->buffer_size * 8 - buffer->bits_written < 8) {
    buffer->bad = true;
    return;
  }
  switch (buffer->type) {
  case AX25_FRAME:
  case AX25_PAYLOAD:
    if (buffer->bits_written % 8 != 0) {
      buffer->bad = true;
      return;
    }
    buffer->data[buffer->bits_written / 8] = byte;
    buffer->bits_written += 8;
    break;
  case AX25_RAW:
    for (int i = 0; i < 8; i++) {
      ax25buffer_put_bit(buffer, byte & 1);
      byte = byte >> 1;
    }
    break;
  default:
    buffer->bad = true;
    break;
  }
  return;
}

void ax25buffer_put_bit(struct ax25buffer *buffer, uint8_t bit) {
  if (buffer->buffer_size * 8 - buffer->bits_written < 1) {
    buffer->bad = true;
    return;
  }
  switch (buffer->type) {
  case AX25_FRAME:
  case AX25_PAYLOAD:
    buffer->bad = true;
    break;
  case AX25_RAW:
    buffer->data[buffer->bits_written / 8] |= (bit & 1) << (buffer->bits_written % 8);
    buffer->bits_written += 1;
    break;
  default:
    buffer->bad = true;
    break;
  }
  return;
}

void ax25buffer_put_bytes(struct ax25buffer *buffer, const uint8_t *bytes,
			  int count) {
  if (buffer->buffer_size * 8 - buffer->bits_written < count * 8) {
    buffer->bad = true;
    return;
  }
  switch (buffer->type) {
  case AX25_FRAME:
  case AX25_PAYLOAD:
    if (buffer->bits_written % 8 != 0) {
      buffer->bad = true;
      return;
    }
    memcpy(&buffer->data[buffer->bits_written / 8], bytes, count);
    buffer->bits_written += count * 8;
    break;
  case AX25_RAW:
    for (int i = 0; i < count; i++) {
      ax25buffer_put_byte(buffer, bytes[i]);
    }
    break;
  default:
    buffer->bad = true;
    break;
  }
  return;
}

void ax25buffer_set_command(struct ax25buffer *buffer, uint8_t byte) {
  buffer->command = byte;
}

void ax25buffer_set_callback(ax25buffer *buffer,
			     struct connection *conn,
			     buffer_callback callback,
			     void *callback_data) {
  buffer->callback = callback;
  buffer->conn = conn;
  buffer->callback_data = callback_data;
}

bool ax25buffer_congested(int32_t wanted) {
  // For now, always return false, since we cannot yet monitor
  // the actual amount of free space in the heap.
  return false;
}

// May need to make this a bit smarter, to zero out what we are
// retracting?
void ax25buffer_retract_bytes(struct ax25buffer *buffer, uint32_t count) {
  if (buffer->bits_written < count * 8) {
    buffer->bits_written = 0;
  } else {
    buffer->bits_written -= count * 8;
  }
}

void ax25buffer_reset(struct ax25buffer *buffer) {
  memset(buffer->data, 0, buffer->buffer_size);
  buffer->bits_written = buffer->bits_read = 0;
  buffer->bad = false;
}

void ax25buffer_reset_read(struct ax25buffer *buffer) {
  buffer->bits_read = 0;
}

int32_t ax25buffer_get_byte(struct ax25buffer *buffer) {
  if (buffer->bits_written - buffer->bits_read < 8) {
    return -1;
  }
  switch (buffer->type) {
  case AX25_FRAME:
    if (buffer->bits_read % 8 != 0) {
      return -1;
    }
    int32_t result = buffer->data[buffer->bits_read / 8];
    buffer->bits_read += 8;
    return result;
  case AX25_RAW:  // Not implemented until we know what's meaningful
    return -1;
  default:
    return -1;
    break;
  }
  return -1;
}

int32_t ax25buffer_get_bit(struct ax25buffer *buffer) {
  if (buffer->bits_written - buffer->bits_read < 1) {
    return -1;
  }
  switch (buffer->type) {
  case AX25_FRAME:   // Not implemented until we know what's meaningful
    return -1;
  case AX25_RAW:
    int32_t the_bit = 1 & (buffer->data[buffer->bits_read / 8] >> (buffer->bits_read % 8));
    buffer->bits_read ++;
    return the_bit;
  default:
    return -1;
    break;
  }
  return -1;
}

int32_t ax25buffer_get_bytes(struct ax25buffer *buffer, uint8_t *bytes, uint32_t limit) {
  int32_t available_bits = buffer->bits_written = buffer->bits_read;
  int32_t available_bytes = available_bits / 8;
  if (limit > available_bytes) {
    limit = available_bytes;
  }
  switch (buffer->type) {
  case AX25_FRAME:
    if (buffer->bits_read % 8 != 0) {
      return -1;
    }
    memcpy(bytes, &buffer->data[buffer->bits_read / 8], limit);
    buffer->bits_read += 8 * limit;
    return limit;
  case AX25_RAW:  // Not implemented until we know what's meaningful
    return -1;
  default:
    return -1;
  }
  return -1;
}

int32_t ax25buffer_get_command(struct ax25buffer *buffer) {
  return buffer->command;
}

int32_t ax25buffer_byte_count(struct ax25buffer *buffer) {
  return (buffer->bits_written + 7) / 8;
}
