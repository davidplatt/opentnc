/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/ring_buffer.h>

#include <version.h>

#include <zephyr/usb/usb_device.h>
#include <zephyr/logging/log.h>

#include "board.h"
#include "connection.h"
#include "console.h"
#include "converse.h"
#include "demod.h"
#include "globals.h"
#include "hdlc.h"
#include "kiss.h"
#include "layer2.h"
#include "packetutil.h"
#include "sharebuffer.h"

#include "shell/shell.h"

#include "git_version.h"

#define CONSOLE_STACK_SIZE 2048
#define CONSOLE_PRIORITY 5

LOG_MODULE_REGISTER(console, LOG_LEVEL_INF);

K_MUTEX_DEFINE(console_mutex);

static bool was_connected = false;

static int console_putc(char c) {
  if (tnc_state.kiss_mode) {
    return 0;
  }
  k_mutex_lock(&console_mutex, K_FOREVER);
  (void) ring_buf_put(&host_outputbuffer, &c, 1);
  board_kick_host_tx();
  k_mutex_unlock(&console_mutex);
  return 0;
}

void console_printchar(char c) {
  if (tnc_state.kiss_mode) {
    return;
  }
  k_mutex_lock(&console_mutex, K_FOREVER);
  (void) ring_buf_put(&host_outputbuffer, &c, sizeof(c));
  k_mutex_unlock(&console_mutex);
  board_kick_host_tx();
}

void console_printstr(const char *s) {
  if (tnc_state.kiss_mode) {
    return;
  }
  k_mutex_lock(&console_mutex, K_FOREVER);
  (void) ring_buf_put(&host_outputbuffer, s, strlen(s));
  k_mutex_unlock(&console_mutex);
  board_kick_host_tx();
}

void console_printline(const char *s) {
  if (tnc_state.kiss_mode) {
    return;
  }
  k_mutex_lock(&console_mutex, K_FOREVER);
  (void) ring_buf_put(&host_outputbuffer, s, strlen(s));
  (void) ring_buf_put(&host_outputbuffer, "\r\n", 2);
  k_mutex_unlock(&console_mutex);
  board_kick_host_tx();
}

void console_printbytes(uint8_t *bytes, size_t len) {
  if (tnc_state.kiss_mode) {
    return;
  }
  k_mutex_lock(&console_mutex, K_FOREVER);
  for (int i = 0; i < len; i++) {
    (void) ring_buf_put(&host_outputbuffer, &bytes[i], 1);
    if (bytes[i] == '\r' && tnc_state.auto_newline) {
      (void) ring_buf_put(&host_outputbuffer, "\n", 1);
    }
  }
  k_mutex_unlock(&console_mutex);
  board_kick_host_tx();
}

// Returns an index for a selected option, or -1 if there's no
// suitable match.
static int choose_one_of(const char *word, const char **choices) {
  int partial_len = strlen(word);
  int sel = 0;
  while (choices[sel] != NULL) {
    // Accept full option, case-insensitive.
    if (strcasecmp(word, choices[sel]) == 0) {
      return sel;
    } else if (partial_len < strlen(choices[sel]) &&
               strncasecmp(choices[sel], word, partial_len) == 0 &&
               islower(choices[sel][partial_len])) {
      return sel;
    }
    sel++;
  }
  return -1;
}

static bool parse_integer(int32_t *var, const char *word, int32_t min, int32_t max) {
  bool range_limit = false;
  char *endptr;
  long l;
  char line[64];
  if (word[0] == '$') {
    l = strtol(word+1, &endptr, 16);
  } else {
    l = strtol(word, &endptr, 0);
  }
  if (*endptr != '\0') {
    return true; // it's bad
  }
  if (l < min) {
    l = min;
    range_limit = true;
  } else if (l > max) {
    l = max;
    range_limit = true;
  }
  *var = (int32_t) l;
  if (range_limit) {
    snprintf(line, sizeof(line), "Value out of range; is now %d", *var);
    shell_put_line(line);
  }
  return false;
}

// Command handlers

static int plugh_handler (int argc, const char *argv[]) {
  if (argc < 0) {
    shell_put_line("Syntax: PLUGH!  (if you dare)");
    return 0;
  }
  shell_put_line("Returning to the wellhouse.");
  k_msleep(1000);
  board_enter_bootloader();
  shell_put_line("Oops!  That didn't work.");
  return 0;
}

static int choice_handler(int argc, const char *argv[], bool *var, char *true_str, char *false_str) {
  char line[80];
  bool prior = *var;
  const char *state = "was";
  bool bad = false;
  if (argc < 0) { // help xxx
    bad = true;
  } else if (argc == 1) {
    state = "is";
  } else if (argc > 2) {
    bad = true;
  } else if (strcasecmp(argv[1], true_str) == 0) {
    *var = true;
  } else if (strcasecmp(argv[1], false_str) == 0) {
    *var = false;
  } else {
    bad = true;
  }
  if (bad) {
    snprintf(line, sizeof(line), "Syntax: %s [(%s|%s)]", argv[0],
             true_str, false_str);
  } else {
    snprintf(line, sizeof(line), "%s %s", state, prior ? true_str : false_str);
  }
  shell_put_line(line);
  return 0;
}

static int nchoice_handler(int argc, const char *argv[], int32_t *var, const char **choices) {
  char line[80];
  int32_t prior = *var;
  const char *state = "was";
  bool bad = false;
  int32_t sel = 0;
  if (argc < 0) { // help xxx
    bad = true;
  } else if (argc == 1) {
    state = "is";
    sel = prior;
  } else if (argc > 2) {
    bad = true;
  } else {
    sel = choose_one_of(argv[1], choices);
    if (sel < 0) {
      bad = true;
    }
  }
  if (bad) {
    snprintf(line, sizeof(line), "Syntax: %s [(%s", argv[0],
             choices[0]);
    shell_put_string(line);
    int alt = 1;
    while (choices[alt] != NULL) {
      shell_put_string("|");
      shell_put_string(choices[alt]);
      alt++;
    }
    shell_put_line(")]");
  } else {
    *var = sel;
    snprintf(line, sizeof(line), "%s %s", state, choices[prior]);
    shell_put_line(line);
  }
  return 0;
}

static int boolean_handler(int argc, const char *argv[], bool *var) {
  return choice_handler(argc, argv, var, "on", "off");
}

static int integer_handler(int argc, const char *argv[], int32_t *var,
                           int32_t min, int32_t max, bool ischar) {
  char line[80];
  int32_t prior = *var;
  const char *state = "was";
  bool bad = false;
  if (argc < 0) { // help xxx
    bad = true;
  } else if (argc == 1) {
    state = "is";
  } else if (argc > 2) {
    bad = true;
  } else {
    bad = parse_integer(var, argv[1], min, max);
  }
  if (bad) {
    snprintf(line, sizeof(line), "Syntax: %s [value] (limits %d, %d)",
             argv[0], min, max);
  } else {
    if (ischar) {
      if (isprint(prior)) {
        snprintf(line, sizeof(line), "%s 0x%X (%c)", state, prior, prior);
      } else if (prior >= 1 && prior <= 26) {
        snprintf(line, sizeof(line), "%s 0x%X (CTRL-%c)", state, prior, prior + 'A' - 1);
      } else {
        snprintf(line, sizeof(line), "%s 0x%X", state, prior);
      }
    } else {
      snprintf(line, sizeof(line), "%s %d", state, prior);
    }
  }
  shell_put_line(line);
  return 0;
}

static int set_or_display_callsign(encoded_callsign *enc, int argc, const char *argv[]) {
  char response[32];
  const char *state = "was";
  plaintext_callsign prior;
  bool bad = false;
  decode_callsign(prior, enc);
  if (argc == 1) {
    state = "is";
  } else if (argc < 0 || argc > 2) {
    snprintf(response, sizeof(response), "Syntax: %s callsign[-ssid]",
             argv[0]);
    bad = true;
  } else {
    encoded_callsign encoded;
    if (encode_callsign(&encoded, argv[1])) {
      memcpy(enc, &encoded, sizeof *enc);
      state = "was";
    } else {
      snprintf(response, sizeof(response), "Incorrect callsign format");
      bad = true;
    }
  }
  if (!bad) {
    snprintf(response, sizeof(response), "%s %s", state, prior);
  }
  shell_put_line(response);
  return 0;
}

static int mycall_handler (int argc, const char *argv[]) {
  return set_or_display_callsign(&tnc_state.mycall, argc, argv);
}

static int echocall_handler (int argc, const char *argv[]) {
  return set_or_display_callsign(&tnc_state.echoback_call, argc, argv);
}

static int unproto_handler (int argc, const char *argv[]) {
  return set_or_display_callsign(&tnc_state.unproto_call, argc, argv);
}

static int audiocarrier_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.audio_carrier_threshold, 0, 32767, false);
}

static int ax25version_handler(int argc, const char *argv[]) {
  static const char *modes[] = { "2.0", "2.2", NULL };
  return nchoice_handler(argc, argv, &tnc_state.ax25_version, modes);
}

static int connect_handler (int argc, const char *argv[]) {
  encoded_callsign enc, digis[MAX_DIGIS];
  static const char *keywords[] = { "VIa", "V2.2", "Maxframe", "Paclen", "Retries", "Frack" };
  if (argc == -1 || argc < 2) {
    shell_put_line("Syntax: connect callsign[-ssid] [Via callsign[-ssid] [Via callsign[-ssid] ...]");
    shell_put_line("                [V2.2] [Maxframe #] [Paclen #] [Retries #] [Frack #]");
    return 0;
  }
  if (memcmp(&tnc_state.mycall, &nocall, sizeof nocall) == 0) {
    shell_put_line("MYCALL has not been set");
    return 0;
  }
  if (!encode_callsign(&enc, argv[1])) {
    shell_put_line("Improper callsign format");
    return 0;
  }
  int argno = 2;
  int set_maxframe = -1;
  int set_paclen = -1;
  int set_retries = -1;
  int set_frack = -1;
  bool bad_syntax = false;
  bool extended_sequence = false;
  int num_digis = 0;
  while (argno < argc && !bad_syntax) {
    int32_t keyword = choose_one_of(argv[argno], keywords);
    argno++;
    if (keyword < 0) {
      bad_syntax = true;
      break;
    }
    switch (keyword) {
    case 0: // "via"
      if (num_digis == MAX_DIGIS) {
        shell_put_line("Too many digipeaters!");
        return 0;
      }
      if (argno >= argc || !encode_callsign(&digis[num_digis], argv[argno])) {
        shell_put_line("Improper callsign format");
        return 0;
      }
      num_digis ++;
      argno++;
      break;
    case 1: // "v2.2"
      extended_sequence = true;
      break;
    case 2: // "Maxframe"
      if (argno >= argc) {
        bad_syntax = true;
        break;
      }
      if (parse_integer(&set_maxframe, argv[argno], 1, 8)) {
        shell_put_line("MAXFRAME value must be an integer");
        return 0;
      }
      argno++;
      break;
    case 3: // "Paclen"
      if (argno >= argc) {
        bad_syntax = true;
        break;
      }
      if (parse_integer(&set_paclen, argv[argno], 1, MAX_IFRAME_PAYLOAD)) {
        shell_put_line("PACLEN value must be an integer");
        return 0;
      }
      argno++;
      break;
    case 4: // "Retries"
      if (argno >= argc) {
        bad_syntax = true;
        break;
      }
      if (parse_integer(&set_retries, argv[argno], 1, 32)) {
        shell_put_line("RETRIES value must be an integer");
        return 0;
      }
      argno++;
      break;
    case 5: // "Frack"
      if (argno >= argc) {
        bad_syntax = true;
        break;
      }
      if (parse_integer(&set_frack, argv[argno], 1, 60)) {
        shell_put_line("FRACK value must be an integer");
        return 0;
      }
      argno++;
      break;
    default:
      bad_syntax = true;
      break;
    }
  }
  if (bad_syntax) {
    return connect_handler(-1, NULL);
  }
  connection *conn = find_free_connection_for_endpoint(&console_endpoint);
  if (!conn) {
    shell_put_line("Can't establish connection (already connected?)");
    return 0;
  }
  configure_default_capabilities(conn, tnc_state.ax25_version == AX25_VERSION_22 ||
                                 extended_sequence);
  if (set_paclen > 0) {
    conn->capabilities.paclen = set_paclen;
  }
  if (set_maxframe > 0) {
    conn->capabilities.maxframe = set_maxframe;
  }
  if (set_retries > 0) {
    conn->capabilities.retries = set_retries;
  }
  if (set_frack > 0) {
    conn->capabilities.t1 = set_frack * 1000;
  }
  (void) start_console_connection(conn, &enc, digis, num_digis);
  was_connected = true;
  converse(true, tnc_state.conmode_is_transparent);
  return 0;
}

static int disconnect_handler (int argc, const char *argv[]) {
  if (argc != 1) {
    shell_put_line("Syntax: Disconnect");
    return 0;
  }
  stop_connection(console_connection); // FIXME some day
  return 0;
}

static int converse_handler (int argc, const char *argv[]) {
  if (argc != 1) {
    char line[128];
    snprintf(line, sizeof(line), "Syntax: %s", argv[0]);
    shell_put_line(line);
    return 0;
  }
  if (memcmp(&tnc_state.mycall, &nocall, sizeof nocall) == 0) {
    shell_put_line("MYCALL has not been set");
    return 0;
  }
  if (console_connection->state == CONN_DISCONNECTED) { // FIXME
    connection *conn = find_free_connection_for_endpoint(&console_endpoint);
    if (!conn) {
      shell_put_line("Can't establish connection (already connected?)");
      return 0;
    }
    if (!start_unproto_connection(conn, &tnc_state.unproto_call)) {
      return 0;
    }
    plaintext_callsign plaintext;
    decode_callsign(plaintext, &tnc_state.unproto_call);
    console_printstr("Entering UNPROTO conversation mode to ");
    console_printline(plaintext);
    converse(false, false);
    console_printline("Exited UNPROTO mode");
    reinit_connection(conn);
  } else {
    console_printline("Entering converse mode");
    converse(true, tnc_state.conmode_is_transparent);
    console_printline("Exited converse mode");
  }
  return 0;
}

static int monitor_handler (int argc, const char *argv[]) {
  return boolean_handler(argc, argv, &tnc_state.monitor);
}

static int maxframe_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.maxframe, 1, 4, false);
}

static int paclen_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.paclen, 32, MAX_IFRAME_PAYLOAD, false);
}

static int sendpac_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.sendpac, 0, 0x7F, true);
}

static int canline_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.canline, 0, 0x7F, true);
}

static int delete_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.delete, 0, 0x7F, true);
}

static int persist_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.p, 0, 255, false);
}

static int slottime_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.slot_time, 0, 255, false);
}

static int frack_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.t1, 1, 15, false);
}

static int check_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.t3, 0, 250, false);
}

static int droprx_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.drop_rx, 0, 255, false);
}

static int droptx_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.drop_tx, 0, 255, false);
}

static int smashfx25rx_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.smash_fx25_rx, 0, 32767, false);
}

static int smashfx25tx_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.smash_fx25_tx, 0, 32767, false);
}

static int rnr_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.not_ready_simulation, 0, 255, false);
}

static int command_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.command, 0, 0x7F, true);
}

static int pactime_handler(int argc, const char *argv[]) {
  char line[80];
  int32_t prior = tnc_state.pactime;
  const char *state = "was";
  bool bad = false;
  enum pactime_mode mode;
  enum pactime_mode prior_mode = tnc_state.pactime_mode;
  long l = 0;
  if (argc < 0) { // help xxx
    bad = true;
  } else if (argc == 1) {
    state = "is";
  } else if (argc != 3) {
    bad = true;
  } else {
    char *endptr;
    state = "was";
    l = strtol(argv[2], &endptr, 0);
    if (*endptr != '\0' || l < 0) {
      bad = true; // not a valid integer, trailing characters, etc.
    } else  if (strcasecmp(argv[1], "every") == 0) {
      mode = PACTIME_EVERY;
    } else if (strcasecmp(argv[1], "after") == 0) {
      mode = PACTIME_AFTER;
    } else if (strcasecmp(argv[1], "sendpac") == 0) {
      mode = PACTIME_SENDPAC;
    } else {
      bad = true;
    }
    if (!bad) {
      tnc_state.pactime = l;
      tnc_state.pactime_mode = mode;
    }
  }
  if (bad) {
    shell_put_line("Syntax: PACTime (AFTER|EVERY|SENDPAC) n");
  } else {
    const char *modestring = NULL;
    switch (prior_mode) {
    case PACTIME_EVERY:
      modestring = "every";
      break;
    case PACTIME_AFTER:
      modestring = "after";
      break;
    case PACTIME_SENDPAC:
      modestring = "sendpac";
      break;
    }
    snprintf(line, sizeof(line), "%s %s %d", state, modestring,
             abs(prior));
    shell_put_line(line);
  }
  return 0;
}

#ifdef CONFIG_G3RUH_SUPPORT

static int baud_handler(int argc, const char *argv[]) {
  static const char *modes[] = { "1200", "9600", NULL };
  int32_t old = tnc_state.radio_baud_rate;
  int32_t new = old;
  int result = nchoice_handler(argc, argv, &new, modes);
  if (argc == 1) {
    return result;
  }
  quiesce_transceiver();
  tnc_state.radio_baud_rate = new;
  switch (new) {
  case RADIO_BAUD_1200:
  default:
    tnc_state.modulator = DELTA_SIGMA_MODULATOR;
    tnc_state.demodulator = BIQUAD_FILTER_DEMODULATOR;
    break;
  case RADIO_BAUD_9600:
    tnc_state.modulator = G3RUH_PWM_MODULATOR;
    tnc_state.demodulator = G3RUH_DEMODULATOR;
    break;
  }
  k_msleep(100); // may be unnecessary now
  unquiesce_transceiver();
  return result;
}

static int bert_handler (int argc, const char *argv[]) {
  console_printline("Enter any character exit");
  LOG_INF("In BERT handler");
  while (true) {
    k_msleep(1000);
    if (!ring_buf_is_empty(&host_inputbuffer)) {
      uint8_t one_char;
      (void) ring_buf_get(&host_inputbuffer, &one_char, sizeof(one_char));
      break;
    }
    G3RUH_Bit_Counts();
  }
  Demodulator_Statistics();
  return 0;
}

static int pulse_handler(int argc, const char *argv[]) {
  static const char *modes[] = { "sinc", "rc50", "rc90", "rc95", NULL };
  return nchoice_handler(argc, argv, &tnc_state.pulse_shape, modes);
}

static int txeq_handler(int argc, const char *argv[]) {
  static const char *modes[] = { "Flat", "Leading", "Noshift", "Trailing", NULL };
  char buffer[128];
  if (argc == 1) {
    switch (tnc_state.txeq_mode) {
    case TX_EQ_FLAT:
    default:
      snprintf(buffer, sizeof(buffer), "is %s", modes[tnc_state.txeq_mode]);
      break;
    case TX_EQ_LEADING:
    case TX_EQ_NOSHIFT:
    case TX_EQ_TRAILING:
      snprintf(buffer, sizeof(buffer), "is %s %d %d", modes[tnc_state.txeq_mode],
               tnc_state.txeq_peak_frequency,
               tnc_state.txeq_peak_gain_centibels);
      break;
    }
    shell_put_line(buffer);
    return 0;
  }
  bool bad = false;
  int32_t new_selection = TX_EQ_FLAT;
  int32_t new_freq = 0, new_gain = 0;
  switch (argc) {
  case 2:
  case 4:
    new_selection = choose_one_of(argv[1], modes);
    if (new_selection < 0) {
      bad = true;
      break;
    }
    switch (new_selection) {
    case TX_EQ_FLAT:
    default:
      if (argc != 2) {
        bad = true;
      }
      break;
    case TX_EQ_LEADING:
    case TX_EQ_NOSHIFT:
    case TX_EQ_TRAILING:
      if (argc != 4) {
        bad = true;
        break;
      }
      bad |= parse_integer(&new_freq, argv[2], 2400, 10000);  // guessing at reasonable limits
      bad |= parse_integer(&new_gain, argv[3], -100, 100);   // +/- 10 dB
      break;
    }
    break;
  default:
    bad = true;
  }
  if (bad) {
    // Hard-code this because the convenience routines can't handle
    // anything this complex.
    shell_put_line("Syntax: TXEQ [(Flat | [Leading | Noshift | Trailing] frequency centibel_gain)");
    return 0;
  }
  tnc_state.txeq_mode = new_selection;
  tnc_state.txeq_peak_frequency = new_freq;
  tnc_state.txeq_peak_gain_centibels = new_gain;
  return 0;
}

#endif

static int bkondel_handler (int argc, const char *argv[]) {
  return boolean_handler(argc, argv, &tnc_state.bkondel);
}

static int conok_handler(int argc, const char *argv[]) {
  return boolean_handler(argc, argv, &tnc_state.conok);
}

static int conmode_handler(int argc, const char *argv[]) {
  return choice_handler(argc, argv, &tnc_state.conmode_is_transparent,
                        "trans", "convers");
}

static int mode_handler(int argc, const char *argv[]) {
  static const char *modes[] = { "Normal", "Expert", NULL };
  return nchoice_handler(argc, argv, &tnc_state.operating_mode, modes);
}

static int aprsdigipeatmode_handler(int argc, const char *argv[]) {
  static const char *modes[] = { "Off", "WIDE1-1", "WIDE2-1", "WIDE2-2", NULL };
  return nchoice_handler(argc, argv, &tnc_state.aprs_digi_mode, modes);
}

static int cpactime_handler(int argc, const char *argv[]) {
  return boolean_handler(argc, argv, &tnc_state.cpactime);
}

static int credits_handler(int argc, const char *argv[]) {
  extern char *credits[];
  int i = 0;
  while(credits[i] != NULL) {
    shell_put_line(credits[i]);
    i++;
  }
  return 0;
}

static int digipeat_handler(int argc, const char *argv[]) {
  return boolean_handler(argc, argv, &tnc_state.digipeating);
}

static int fastpoll_handler(int argc, const char *argv[]) {
  return boolean_handler(argc, argv, &tnc_state.poll_on_last);
}

static int modulator_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.modulator,
                         PWM_MODULATOR, G3RUH_PWM_MODULATOR, false);
}

static int demodulator_handler(int argc, const char *argv[]) {
  int old = tnc_state.demodulator;
  int new = old;
  int result = integer_handler(argc, argv, &new,
                               GOERTZEL_DEMODULATOR, G3RUH_DEMODULATOR, false);
  // FIXME!
  if (new != old) {
    shell_put_line("Switching demodulator modes");
    quiesce_transceiver();
    tnc_state.demodulator = new;
    unquiesce_transceiver();
  }
  return result;
}

static int resptime_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.t2,
                         1, 20, false);
}

static int retry_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.retries,
                         1, 20, false);
}

static int initial_syncs_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.initial_syncs,
                         1, 60, false);
}

static int txdelay_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.txdelay,
                         0, 120, false);
}

static int txtail_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.txtail,
                         0, 120, false);
}

static int outpost_handler(int argc, const char *argv[]) {
  return integer_handler(argc, argv, &tnc_state.outpost_delay,
                         0, 10000, false);
}

static int calibrate_handler(int argc, const char *argv[]) {
  bool sending = false;
  bool looping = true;
  uint8_t pattern = 0x00;
  bool restart_needed = false;
  bool restart_now = false;
  int32_t new_modulator = 0;
  const int32_t timeout_limit = 8 * 60 * 100;  // 8 minutes, with 10-millisecond sleeps
  int32_t timeout = timeout_limit;
  if (argc < 0) { // help calibrate
    char line[128];
    snprintf(line, sizeof(line), "Syntax: %s", argv[0]);
    shell_put_line(line);
    return 0;
  }
  console_printline("Enter: control-C to exit");
  console_printline("       Q to stop");
  console_printline("       H to send high tone");
  console_printline("       L to send low tone");
  console_printline("       ENTER to send alternating tones");
  console_printline("       0 to send with PWM modulator");
  console_printline("       1 to send with delta-sigma modulator");
  LOG_INF("In calibration handler");
  while (looping) {
    bool stop_now = false;
    bool start_now = false;
    k_msleep(10);
    if (!ring_buf_is_empty(&host_inputbuffer)) {
      uint8_t one_char;
      (void) ring_buf_get(&host_inputbuffer, &one_char, sizeof(one_char));
      if (one_char == 0x03) {
        looping = false;
        stop_now = true;
      } else {
        bool stopping = false;
        switch (one_char) {
        case 'H':
        case 'h':
          HDLC_Set_NRZI_History(true);
          pattern = 0xFF;
          console_printline("Sending high tone");
          break;
        case 'L':
        case 'l':
          HDLC_Set_NRZI_History(false);
          pattern = 0xFF;
          console_printline("Sending low tone");
          break;
        case '\r':
        case '\n':
          pattern = 0x00;
          console_printline("Sending alternating tones");
          break;
        case 'Q':
        case 'q':
          stopping = true;
          console_printline("Stopping");
          break;
        case '0':
        case '1':
          new_modulator = one_char - '0';
          if (sending) {
            stopping = true;
            restart_needed = true;
          }
          console_printline(new_modulator ?
                            "Will use delta-sigma modulator" :
                            "Will use PWM modulator");
          break;
        default:
          break;
        }
        stop_now = stopping && sending;
        start_now = !sending;
      }
    }
    if (sending && timeout && !--timeout) {
      stop_now = true;
      console_printline("Stopping transmit (timeout)");
    }
    if (start_now || restart_now) {
      common_disable_rx();
      common_enable_tx();
      board_enable_ptt();
      sending = true;
      restart_now = false;
      timeout = timeout_limit;
    } else if (stop_now) {
      board_disable_ptt();
      common_disable_tx();
      common_enable_rx();
      sending = false;
      if (restart_needed) {
        tnc_state.modulator = new_modulator;
        restart_now = true;
        restart_needed = false;
      }
    }
    if (sending) {
      // Fill up the HDLC buffer as much as possible
      while (HDLC_Ready()) {
        (void) HDLC_Append_Raw_Byte(pattern, 8);
      }
    }
  }
  return 0;
}

// This is hacky...
static void generate_test_packet(void) {
  static int test_packet_number = 1;
  full_address_path test_path;
  static const char header[] = "OpenTNC test packet %6d ";
  static const char pattern[] = " ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789";
  char construct[sizeof(header) + 10];
  if (!sys_dlist_is_empty(&tx_packets)) {
    return;
  }
  test_path.digipeaters = 0;
  test_path.src = tnc_state.mycall;
  encode_callsign(&test_path.dest, "TEST");
  snprintf(construct, sizeof(construct), header, test_packet_number);
  ax25buffer *b = ax25buffer_alloc(AX25_FRAME, strlen(construct) + strlen(pattern)  +
                                       MAX_PACKET_OVERHEAD);
  if (!b) {
    LOG_ERR("Could not allocate ax25buffer for test packet");
    return;
  }
  prepare_packet_header(b, &test_path, FRAME_IS_NEITHER,
                        false,
                        encode_u_control(U_FRAME_UI, false),
                        AX25_P_TEXT);
  ax25buffer_put_bytes(b, construct, strlen(construct));
  int32_t offset = test_packet_number % strlen(pattern);
  ax25buffer_put_bytes(b, pattern + offset, strlen(pattern) - offset);
  if (offset != 0) {
    ax25buffer_put_bytes(b, pattern, offset);
  }
  b->fx25 = tnc_state.fx25_connection_mode;
  LOG_DBG("Sending test packet %d", test_packet_number);
  layer2_accept_outgoing(b);
  test_packet_number ++;
}

static int testpackets_handler(int argc, const char *argv[]) {
  int32_t timeout = 8 * 60 * 100;  // 8 minutes, with 10-millisecond sleeps
  if (argc < 0) { // help testpackets
    char line[128];
    snprintf(line, sizeof(line), "Syntax: %s", argv[0]);
    shell_put_line(line);
    return 0;
  }
  if (memcmp(&tnc_state.mycall, &nocall, sizeof nocall) == 0) {
    shell_put_line("MYCALL has not been set");
    return 0;
  }
  shell_put_line("Enter any character to exit");
  LOG_INF("In testpackets handler");
  while (true) {
    k_msleep(10);
    if (--timeout == 0) {
      shell_put_line("Timeout");
      break;
    }
    if (!ring_buf_is_empty(&host_inputbuffer)) {
      uint8_t one_char;
      (void) ring_buf_get(&host_inputbuffer, &one_char, sizeof(one_char));
      break;
    }
    generate_test_packet();
  }
  return 0;
}

static int testwaveform_handler(int argc, const char *argv[]) {
  int32_t timeout = 8 * 60 * 100;  // 8 minutes, with 10-millisecond sleeps
  if (argc < 0) { // help testpackets
    char line[128];
    snprintf(line, sizeof(line), "Syntax: %s", argv[0]);
    shell_put_line(line);
    return 0;
  }
  shell_put_line("Enter any character to exit");
  tnc_state.waveform_test = true;
  common_disable_rx();
  common_enable_tx();
  board_enable_ptt();
  LOG_INF("Running waveform test");
  while (true) {
    k_msleep(10);
    if (--timeout == 0) {
      shell_put_line("Timeout");
      break;
    }
    if (!ring_buf_is_empty(&host_inputbuffer)) {
      uint8_t one_char;
      (void) ring_buf_get(&host_inputbuffer, &one_char, sizeof(one_char));
      break;
    }
    while (HDLC_Ready()) {
      (void) HDLC_Append_Raw_Byte(0, 8);
    }
  }
  board_disable_ptt();
  common_disable_tx();
  common_enable_rx();
  tnc_state.waveform_test = false;
  return 0;
}

static int carrierdetect_handler(int argc, const char *argv[]) {
  static const char *modes[] = { "Hdlc", "Audio", "Squelch", NULL };
  return nchoice_handler(argc, argv, (int32_t *) &tnc_state.carrier_detect_mode, modes);
}

// This is a very blunt instrument!  May have undesired
// side effects if you do it while connected.
static int reset_handler(int argc, const char *argv[]) {
  if (argc < 0) { // help
    char line[128];
    snprintf(line, sizeof(line), "Syntax: %s", argv[0]);
    shell_put_line(line);
    return 0;
  }
  tnc_state = default_tnc_state;
  if (!encode_callsign(&tnc_state.mycall, "NOCALL")) {
    LOG_ERR("Ooops, can't set callsign");
  }
  if (!encode_callsign(&tnc_state.echoback_call, "NOCALL")) {
    LOG_ERR("Ooops, can't set callsign");
  }
  if (!encode_callsign(&tnc_state.unproto_call, "CQ")) {
    LOG_ERR("Ooops, can't set callsign");
  }
  console_printline("Working parameters reset.  Issue SAVE! to make changes persist.");
  return 0;
}

static int save_handler(int argc, const char *argv[]) {
  board_save_parameters();
  return 0;
}

static int kiss_handler(int argc, const char *argv[]) {
  char line[80];
  if (argc == 1) {
    snprintf(line, sizeof(line), "is %s",
             tnc_state.kiss_auto ? "auto" : "off");
    shell_put_line(line);
    return 0;
  }
  if (argc != 2) {
    shell_put_line("Syntax: KISS (on|off|auto)");
    return 0;
  }
  if (strcasecmp(argv[1], "on") == 0) {
    shell_put_line("Entering KISS mode.  Send 0xC0 0xFF 0xC0 to exit.");
    k_msleep(1000);
    tnc_state.kiss_mode = true;
  } else if (strcasecmp(argv[1], "off") == 0) {
    if (tnc_state.kiss_auto) {
      tnc_state.kiss_auto = false;
      board_save_parameters();
      shell_put_line("KISS AUTO is no longer in effect");
    }
  } else if (strcasecmp(argv[1], "auto") == 0) {
    tnc_state.kiss_auto = true;
    board_save_parameters();
    shell_put_line("TNC will enter KISS mode when started.  Send 0xFE 0xFF 0xFE to exit.");
  } else {
    shell_put_line("Syntax: KISS (on|off|auto)");
  }
  return 0;
}

static int ekiss_handler(int argc, const char *argv[]) {
  static const char *modes[] = { "Off", "Partial", "All", NULL };
  return nchoice_handler(argc, argv, (int32_t *) &tnc_state.ekiss_mode, modes);
}

static int damask_handler(int argc, const char *argv[]) {
  static const char *modes[] = { "Off", "Roundrobin", "Priority", "Keepalive", "Force", NULL };
  return nchoice_handler(argc, argv, (int32_t *) &tnc_state.damask_mode, modes);
}

static int fx25_handler(int argc, const char *argv[]) {
  static const char *modes[] = { "Off", "Automatic", "MInimum", "MAximum", NULL };
  int32_t temp = (int32_t) tnc_state.fx25_encoding;
  int result = nchoice_handler(argc, argv, &temp, modes);
  tnc_state.fx25_encoding = (enum fx25_encoding) temp;
  return result;
}

static int fx25_connection_handler(int argc, const char *argv[]) {
  static const char *modes[] = { "OFf", "Responsive", "OPportunistic", "ON", NULL };
  int32_t temp = (int32_t) tnc_state.fx25_connection_mode;
  int result = nchoice_handler(argc, argv, &temp, modes);
  tnc_state.fx25_connection_mode = (enum fx25_connection_mode) temp;
  return result;
}

static int lamptest_handler(int argc, const char *argv[]) {
  int result =
    boolean_handler(argc, argv, &tnc_state.lamp_test);
  if (tnc_state.lamp_test) {
    tnc_state.lamp_test_time = -1; // Run pretty much forever...
  } else {
    tnc_state.lamp_test_time = 1;  // This will force a restore of the proper LED states
  }
  return result;
}



// Interface to the simple-shell library
static const sShellImpl consoleShellImpl = {
  .send_char = &console_putc
};

static int display_handler(int argc, const char *argv[]);

static const sShellCommand s_shell_commands[] = {
  {"*APRSDigipeatmode", aprsdigipeatmode_handler, "Set APRS digipeating mode", true},
  {"AUDIOcarrier", audiocarrier_handler, "Set audio threshold for squelch-closed operation", true},
  {"AX25Version", ax25version_handler, "Set AX.25 protocol version", true},
#ifdef CONFIG_G3RUH_SUPPORT
  {"*BERT", bert_handler, "Display bit-error-rate data from HDLC decoder", false},
  {"BAUD", baud_handler, "Set or display radio baud rate", true},
#endif
  {"BKondel", bkondel_handler, "Enable/disable backspace-blank on character deletion", true},
  {"CALibrate", calibrate_handler, "Send repeated high/low calibration tone", false},
  {"CANline", canline_handler, "Set character to cancel input line", true},
  {"CARrierdetect", carrierdetect_handler, "Set method used to detect carrier (radio-busy)", true},
  {"CHeck", check_handler, "Set/reset connection-keepalive timeout (x 10 seconds)", true},
  {"COMmand", command_handler, "Set character to exit converse mode", true},
  {"CONMode", conmode_handler, "Select connection mode", true},
  {"CONOk", conok_handler, "Allow/disallow automatic inbound connections", true},
  {"Connect", connect_handler, "Connect to another node", false},
  {"CONVerse", converse_handler, "Enter conversation mode", false},
  {"CPactime", cpactime_handler, "Enable/disable timer-based packet sending", true},
  {"CREdits", credits_handler, "Display open-source credits", false},
  {"*DAMASk", damask_handler, "Set DAMASK scheduling option for KISS mode", true},
  {"DELete", delete_handler, "Set character to backspace/delete input", true},
  {"*DEMODUlator", demodulator_handler, "Selects AFSK demodulator", true},
  {"DIGIpeat", digipeat_handler, "Enable/disable digipeating", true},
  {"Disconnect", disconnect_handler, "End connection to another node", false},
  {"DISPlay", display_handler, "Display TNC parameters and statistics", false},
  {"*DROPRx", droprx_handler, "Set drop-frame-on-reception ratio", true},
  {"*DROPTx", droptx_handler, "Set drop-frame-on-transmission ratio", true},
  {"*ECHOcall", echocall_handler, "Set or query TNC callsign for echoback service", true},
  {"*EKiss", ekiss_handler, "Set or query extended-KISS hybrid operation", true},
  {"FASTpoll", fastpoll_handler, "Set/reset the 'fast poll during transmission' flag", true},
  {"FRack", frack_handler, "Set frame-acknowledgement timeout (seconds)", true},
  {"FX25Encoding", fx25_handler, "Set/display FX.25 packet error correction level", true},
  {"FX25Connection", fx25_connection_handler, "Set/display use of FX.25 packet error correction", true},
  {"KISS", kiss_handler, "Set KISS operating mode", true},
  {"LAMPtest", lamptest_handler, "Test the LEDs", true},
  {"MAXframe", maxframe_handler, "Set size of transmit window", true},
  {"MODE", mode_handler, "Set operating mode (normal or expert)", true},
  {"MODUlator", modulator_handler, "Selects AFSK modulator", true},
  {"Monitor", monitor_handler, "Enable, disable, or query packet-monitor", true},
  {"MYcall", mycall_handler, "Set or query TNC callsign", true},
  {"*OUTPOST", outpost_handler, "Set command-prompt delay for Outpost (ms)", true},
  {"Paclen", paclen_handler, "Set max payload length of transmitted packets", true},
  {"PACTime", pactime_handler, "Set time for automatic packet push (100 ms units)", true},
  {"PERsist", persist_handler, "Set P-persist transmit probability", true},
  {"*PLUGH!", plugh_handler, "Enter the bootloader for firmware update", false},
#ifdef CONFIG_G3RUH_SUPPORT
  {"*PULSE", pulse_handler, "Set/display G3RUH pulse shape", true},
#endif
  {"RESET", reset_handler, "Reset all parameters to factory defaults", false},
  {"RESptime", resptime_handler, "Sets minimum acknowledgement delay", true},
  {"RETry", retry_handler, "Sets maximum number of retries", true},
  {"*RNR", rnr_handler, "Sets threshold for simulating no-room-to-receive", true},
  {"SAVE!", save_handler, "Save current TNC parameters to flash", false},
  {"SEndpac", sendpac_handler, "Set character to trigger packet transmission", true},
  {"SLottime", slottime_handler, "Set P-persist slot time (10 ms units)", true},
  {"*SMASHFX25Rx", smashfx25rx_handler, "Set corrupt-FX.25-codeblock-on-reception ratio", true},
  {"*SMASHFX25Tx", smashfx25tx_handler, "Set corrupt-FX.25-codeblock-on-transmission ratio", true},
  {"*SYNCs", initial_syncs_handler, "Sets number of frame syncs sent", true},
  {"*TESTPACKETS", testpackets_handler, "Sends a sequence of test packets", false},
#ifdef CONFIG_G3RUH_SUPPORT
  {"*TESTWAVEFORM", testwaveform_handler, "Sends test waveforms", false},
#endif
  {"TXDelay", txdelay_handler, "Sets key-down delay on transmitter", true},
#ifdef CONFIG_G3RUH_SUPPORT
  {"*TXEQ", txeq_handler, "Sets/displays transmit pre-equalization for G3RUH 1200-baud", true},
#endif
  {"TXTail", txtail_handler, "Sets end-of-PTT delay on transmitter", true},
  {"UNproto", unproto_handler, "Set or query UNPROTO callsign", true},
  {"Help", shell_help_handler, "Lists commands available", false},
};

const sShellCommand *const g_shell_commands = s_shell_commands;
const size_t g_num_shell_commands = ARRAY_SIZE(s_shell_commands);

static void display_counter(const char *name, uint32_t val) {
  char line[128];
  snprintf(line, sizeof(line), "%s: %u",
           name, val);
  shell_put_line(line);
}


static int display_handler(int argc, const char *argv[]) {
  if (argc < 0) { // help
    char line[128];
    snprintf(line, sizeof(line), "Syntax: %s", argv[0]);
    shell_put_line(line);
    return 0;
  }
  for (int i = 0; i < g_num_shell_commands; i++) {
    if (g_shell_commands[i].on_display) {
      const char *argv[1];
      const char *cmd = g_shell_commands[i].command;
      // Commands whose names are prefixed with "!" are treated specially;
      // they're accepted as usual, but "help" doesn't show them.  Commands
      // whose name starts with '*" are accepted and shown only in expert
      // operating mode.
      if (*cmd == '!') {
        cmd++;
      } else if (*cmd == '*') {
        if (tnc_state.operating_mode == OPERATING_MODE_EXPERT) {
          cmd++;
        } else {
          continue;
        }
      }
      shell_put_string(cmd);
      shell_put_string(" ");
      argv[0] = NULL;
      (void)(*g_shell_commands[i].handler)(1, &argv[0]);
    }
  }
  shell_put_line("");

  display_counter("Heap allocs", tnc_state.stats.heap_allocs);
  display_counter("Heap frees", tnc_state.stats.heap_frees);
  display_counter("Total frames sent", tnc_state.stats.frames_sent);
  display_counter("Total frames accepted", tnc_state.stats.frames_accepted);
  display_counter("Total frames filtered", tnc_state.stats.frames_filtered);
  display_counter("FX.25 frames sent", tnc_state.stats.fx25_codeblocks_sent);
  display_counter("FX.25 frames seen", tnc_state.stats.fx25_codeblocks_seen);
  display_counter("FX.25 frames accepted", tnc_state.stats.fx25_codeblocks_accepted);
  display_counter("FX.25 frames corrected", tnc_state.stats.fx25_codeblocks_corrected);
  display_counter("FX.25 errors corrected", tnc_state.stats.fx25_samples_corrected);
  display_counter("Frames digipeated (standard forwarding)", tnc_state.stats.frames_digipeated);
  display_counter("Frames digipeated (APRS WIDE)", tnc_state.stats.aprs_digipeated);
  shell_put_line("");

  return 0;
}

int console_thread(void)
{
  LOG_INF("Console thread started");
  bool booted = false;
  while (1) {
    k_msleep(10); // Might want to do this differently, with a
    // semaphore wait with timeout?
    if (tnc_state.kiss_mode) {
      continue;
    }
    if (!booted) {
      // Ugly hack to wait a while for the CDC_ACM "UART" to configure
      // itself, and then flush any input gunk which arrived during
      // the first moments of the connection.
      k_msleep(2000);
      while (!ring_buf_is_empty(&host_inputbuffer)) {
        uint8_t one_char;
        (void) ring_buf_get(&host_inputbuffer, &one_char, sizeof(one_char));
      }
      char temp_string[128];
      snprintf(temp_string, sizeof(temp_string),
               "\r\n\nAE6EO OpenTNC %d.%d%s, built from git version %s",
               OPENTNC_MAJOR_VERSION, OPENTNC_MINOR_VERSION,
               OPENTNC_VERSION_VARIANT, kGitHash);
      console_printline(temp_string);
      console_printline("Firmware Copyright (c) 2024, David C Platt");
      console_printline("Licensed for use under the GNU General Public License, Version 3");
      console_printline("Running on Zephyr OS, version " STRINGIFY(BUILD_VERSION));
      console_printline("");
      k_msleep(100);
      shell_boot(&consoleShellImpl);
      booted = true;
    }
    bool in_command = true;
    do {
      if (!ring_buf_is_empty(&host_inputbuffer)) {
        uint8_t one_char;
        (void) ring_buf_get(&host_inputbuffer, &one_char, sizeof(one_char));
        board_check_input_flow_control();
        if (one_char == '\r') {
          one_char = '\n';
        }
        if (one_char == '\n') {
          in_command = false;
        }
        shell_receive_char(one_char);
      } else {
        k_msleep(10);
      }
      // Automatically enter converse mode if we've answered an
      // inbound connection request.
      if (console_connection->state == CONN_CONNECTED) {
        if (!was_connected) {
          was_connected = true;
          console_printline("Inbound call, entering converse mode");
          converse(true, tnc_state.conmode_is_transparent);
          // Restart the shell, as there may have been a
          // command in progress.
          shell_boot(&consoleShellImpl);
        }
      } else {
        was_connected = false;
      }
    } while (in_command);
  }
  return 0;
}

K_THREAD_DEFINE(console_tid, CONSOLE_STACK_SIZE,
                console_thread, NULL, NULL, NULL,
                CONSOLE_PRIORITY, 0, 700);
