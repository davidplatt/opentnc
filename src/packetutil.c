/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <stdint.h>
#include <string.h>
#include <zephyr/kernel.h>

#include <zephyr/logging/log.h>

#include "ax25buffer.h"
#include "board.h"
#include "globals.h"
#include "packetutil.h"

LOG_MODULE_REGISTER(sink, LOG_LEVEL_INF);

static int goodPackets;

static void safe_append(char *dest, size_t dest_len, char *src) {
  ssize_t src_len = strlen(src);
  size_t limit = dest_len - strlen(dest) - 1;
  if (src_len > limit) {
    src_len = limit;
  }
  if (src_len <= 0) {
    return;
  }
  dest[strlen(dest) + src_len] = '\0';
  memcpy(dest + strlen(dest), src, src_len);
}

void DecodePacketFromBuffer(char prefix, ax25buffer *buf, char *line, size_t line_size,
			    bool uses_extended_sequence) {
  int i;
  int inAddress = 1;
  int inControl = 0;
  int inPid = 0;
  bool extended = uses_extended_sequence;
  int32_t b;
  char segment[128];
  int bailout = 512;
  memset(line, 0, line_size);
  snprintf(line, line_size, "%d: %c[To ", goodPackets ++, prefix);
  ax25buffer_reset_read(buf);
  for (i = 0; ; i++) {
    b = ax25buffer_get_byte(buf);
    if (b < 0) {
      break;
    }
    if (--bailout < 1) {
      LOG_ERR("Packet printer bailout, b is %d", b);
      break;
    }
    if (inAddress) {
      if (i%7 == 0) {
	if (i/7 == 1) {
	  safe_append(line, line_size, " from ");
	} else if (i/7 > 0) {
	  safe_append(line, line_size, " via ");
	}
      }
      if (i%7 == 6) {
	uint8_t ssid = (b >> 1) & 0x0F;
	if (i == 6 && (b & SSID_EXTENDED_SEQUENCE) == 0) {
	  extended = true;
	}
	if (ssid > 0) {
	  snprintf(segment, sizeof(segment), "-%d", ssid);
	  safe_append(line, line_size, segment);
	}
	if (b&0x80) {
	  safe_append(line, line_size, "*");
	}
      } else {
	uint8_t callsign = b>>1;
	if (callsign != ' ') {
	  snprintf(segment, sizeof(segment), "%c", callsign);
	  safe_append(line, line_size, segment);
	}
      }
      if (b & 1) {
	inAddress = 0;
	inControl = 1;
	safe_append(line, line_size, ":");
      }
    } else if (inControl) {
      if ((b & 0x3) == 0x3) {
	snprintf(segment, sizeof(segment), " %02X", b);
	safe_append(line, line_size, segment);
	uint8_t mmmmm = ((b & 0xE0) >> 3) | ((b & 0x0C) >> 2);
	switch (mmmmm) {
	case 0:
	  safe_append(line, line_size, "(UI) ");
	  inPid = 1;
	  break;
	case 3:
	  safe_append(line, line_size, "(DM) ");
	  break;
	case 7:
	  safe_append(line, line_size, "(SABM) ");
	  break;
	case 8:
	  safe_append(line, line_size, "(DISC) ");
	  break;
	case 12:
	  safe_append(line, line_size, "(UA) ");
	  break;
	case 15:
	  safe_append(line, line_size, "(SABME) ");
	  break;
	case 17:
	  safe_append(line, line_size, "(FRMR) ");
	  break;
	case 23:
	  safe_append(line, line_size, "(XID) ");
	  break;
	case 28:
	  safe_append(line, line_size, "(TEST) ");
	  break;
	default:
	  safe_append(line, line_size, "(U) ");
	  break;
	}
      } else if ((b & 0x01) == 0x01) {
	int n_r, p;
	if (extended) {
	  b = (ax25buffer_get_byte(buf) << 8) | b;
	  snprintf(segment, sizeof(segment), " %04X", b);
	  n_r = (b >> 9) & 0x7F;
	  p = (b >> 8) & 1;
	} else {
	  snprintf(segment, sizeof(segment), " %02X", b);
	  n_r = (b >> 5) & 0x07;
	  p = (b >> 4) & 1;
	}
	safe_append(line, line_size, segment);
	const char *S_frame_type[4] = { "RR", "RNR", "REJ", "SREJ" };
	snprintf(segment, sizeof(segment), "(%s n_r=%d %d) ",
		 S_frame_type[(b >> 2) & 3], n_r, p);
	safe_append(line, line_size, segment);
      } else {
	int n_r, n_s, p;
	if (extended) {
	  b = (ax25buffer_get_byte(buf) << 8) | b;
	  snprintf(segment, sizeof(segment), " %04X", b);
	  n_r = (b >> 9) & 0x7F;
	  p = (b >> 8) & 1;
	  n_s = (b >> 1) & 0x7F;
	} else {
	  snprintf(segment, sizeof(segment), " %02X", b);
	  n_r = (b >> 5) & 0x07;
	  p = (b >> 4) & 1;
	  n_s = (b >> 1) & 0x07;
	}
	safe_append(line, line_size, segment);
	snprintf(segment, sizeof(segment), "(I n_r=%d %d n_s=%d) ",
		 n_r, p, n_s);
	safe_append(line, line_size, segment);
      }
      inControl = 0;
      inPid = 1;
    } else if (inPid) {
      snprintf(segment, sizeof(segment), "PID ^%02X ", b);
      safe_append(line, line_size, segment);
      inPid = 0;
    } else {
      if (isprint(b)) {
	snprintf(segment, sizeof(segment), "%c", b);
      } else {
	snprintf(segment, sizeof(segment), " ^%02X ", b);
      }
      safe_append(line, line_size, segment);
    }
  }
  safe_append(line, line_size, "]");
  ax25buffer_reset_read(buf);
}

void LogPacket(char prefix, ax25buffer *b) {
  char line[256];
  DecodePacketFromBuffer(prefix, b, line, sizeof(line), b->extended_sequence);
  LOG_INF("%s", line);
}

void ConsumeAndPrintPackets(void) {
  while (1) {
    ax25buffer *b;
    if ((b = ax25buffer_take_head(&rx_packets)) == NULL) {
      k_msleep(100);
      continue;
    }
    LogPacket('<', b);
    ax25buffer_free(b);
  }
}
