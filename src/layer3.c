/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

/*
  This file contains the code which handles incoming AX.25
  frames in non-KISS mode.  It keeps track of the connection
  state (e.g. reacting to SABM, DISC, DM, etc.), tracks
  acknowledgment of I-frames we've sent, forwards the data
  from incoming I-frames to the connection consumer, and
  so forth.
*/

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/ring_buffer.h>

#include "ax25.h"
#include "ax25buffer.h"
#include "board.h"
#include "connection.h"
#include "console.h"
#include "demod.h"
#include "globals.h"
#include "hdlc.h"
#include "kiss.h"
#include "layer2.h"
#include "packetutil.h"
#include "sharebuffer.h"

#include "shell/shell.h"

#define LAYER3_STACK_SIZE 2000

LOG_MODULE_REGISTER(layer3, LOG_LEVEL_INF);

const char* frame_roles[] = FRAME_ROLES;

bool release_oldest_l3_packet(connection *conn) {
  if (conn->pending_packets == 0) {
    return false;
  }
  // Release any console sharebuffer space used by this packet,
  // or any payload buffer held by it.
  if (conn->pending_output[0].claimed) {
    sharebuffer_unclaim_range(&console_sharebuffer,
			      conn->pending_output[0].sharebuffer_offset,
			      conn->pending_output[0].length);
  }
  if (conn->pending_output[0].buffer) {
    ax25buffer_free(conn->pending_output[0].buffer);
  }
  // Simply shift the rest of the array downwards, and zero out
  // the last entry.
  for (int i = 0; i < CONNECTION_PACKET_COUNT - 1; i++) {
    memcpy(&conn->pending_output[i],
	   &conn->pending_output[i+1],
	   sizeof(conn->pending_output[i]));
  }
  memset(&conn->pending_output[CONNECTION_PACKET_COUNT - 1],
	 0, sizeof(conn->pending_output[CONNECTION_PACKET_COUNT - 1]));
  conn->pending_packets --;
  conn->sent_packets ++;
  conn->retries_used = 0;
  LOG_DBG("There are now %d packets pending, %d queued, %d ack'ed", conn->pending_packets,
	  conn->queued_packets, conn->sent_packets);
  return true;
}

void relist_l3_packets(connection *conn) {
  k_mutex_lock(&conn->mutex, K_FOREVER);
  bool used_a_retry = false;
  LOG_DBG("Relist all packets");
  for (int i = 0; i < CONNECTION_PACKET_COUNT - 1; i++) {
    if (conn->pending_output[i].sent_once) {
      used_a_retry = true;
    }
    conn->pending_output[i].sent_once = false;
  }
  if (used_a_retry) {
    conn->retries_used ++;
  }
  k_mutex_unlock(&conn->mutex);
}

void relist_l3_packet_numbered(connection *conn, int32_t n) {
  k_mutex_lock(&conn->mutex, K_FOREVER);
  bool used_a_retry = false;
  LOG_DBG("Selective relist of sequence %d", n);
  for (int i = 0; i < CONNECTION_PACKET_COUNT - 1; i++) {
    LOG_DBG(" Check index %d sequence %d", i, sequence_mod(conn, conn->v_s + i));
    if (n == ((conn->v_s + i) & conn->capabilities.sequence_mask)) {
      LOG_DBG("  Bingo!");
      if (conn->pending_output[i].sent_once) {
	used_a_retry = true;
      }
      conn->pending_output[i].sent_once = false;
    }
  }
  if (used_a_retry) {
    conn->retries_used ++;
  }
  k_mutex_unlock(&conn->mutex);
}

void handle_acknowledged_packets(connection *conn, uint8_t sender_n_r) {
  k_mutex_lock(&conn->mutex, K_FOREVER);
  while (conn->v_s != sender_n_r) {
    LOG_DBG("Packet %d acknowledged", conn->v_s);
    conn->v_s = (conn->v_s + 1) & conn->capabilities.sequence_mask;
    // Here we unclaim and flush the oldest packet in the prepared
    // queue, and decrement the packets-to-be-sent counter.
    if (!release_oldest_l3_packet(conn)) {
      LOG_ERR("Pending-packet error!");
      continue;
    }
  }
  uint32_t unacknowledged = 0;
  for (int i = 0; i < CONNECTION_PACKET_COUNT - 1; i++) {
    LOG_DBG("Index %d v_s %d sent-once %d", i,
	    (conn->v_s + i) & conn->capabilities.sequence_mask,
	    conn->pending_output[i].sent_once);
    if (conn->pending_output[i].sent_once) {
      unacknowledged ++;
    }
  }
  // If all the packets we've sent have been acknowledged.
  // and we don't have an RR/RNR poll command outstanding,
  // and we aren't in the middle of SREJ, we can clear T1 and start T3.
  if (unacknowledged == 0 &&
      !conn->poll_for_rr_or_rnr_sent &&
      !conn->send_srej) {
    stop_conn_timer(&conn->t1);
    start_conn_timer(&conn->t3);
  }
  k_mutex_unlock(&conn->mutex);
}

const char *process_I_frame(connection *conn, packet_parse *p, bool sender_p) {
  // If we get an I-frame while we're in NEGOTIATING, it's
  // an indication that we're talking with an incomplete AX.25 V2.2
  // peer (didn't send XID), or the peer's XID response was lost.
  // In this case, we'll punt... assume for safety that the peer
  // does not support SREJ, and go right to the CONNECTED phase.
  const char *action = "accept";
  if (conn->state == CONN_NEGOTIATING) {
    LOG_INF("I-frame when negotiating.. XID lost or not sent?");
    console_printline("*** CONNECTED");
    conn->capabilities.supports_srej = false;
    conn->state = CONN_CONNECTED;
    conn->peer_ready = true;
  }
  // Our implementation handles only the "null" AX.25 Layer 3 (text
  // only), not IP or any other Layer 3 protocol.  We could force a
  // link reset or a disconnect.  For now, just /dev/null the data.
  // If this packet is the one we expect, accepts its contents and
  // bump our receive sequence number.
  if (p->pid != conn->pid) {
    LOG_INF(" Frame PID not for this connection, discard");
  } else {
    // Send the data to the connection - it may print it to the
    // console, handle it internally, etc.  If the consumer can't
    // take the data due to buffer-space limitations, we'll simply
    // pretend that we never received the packet.
    conn->received_packets ++;
    if (!(*conn->consumer)(conn, p)) {
      LOG_ERR("Receive buffer full, dropping I-frame");
      return "dropped by connection";
    }
  }
  conn->v_r = sequence_mod(conn, conn->v_r + 1);
  LOG_DBG("Our V_R now %d", conn->v_r);
  conn->retries_used = 0;
  if (conn->rej_sent) {
    LOG_DBG("This clears our REJ condition");
    conn->rej_sent = false;
    conn->send_rej = false;
    release_deferred_input(conn);
    action = "accept, clear REJ";
  }
  if (conn->send_srej) {
    LOG_DBG("This clears our SREJ condition");
    conn->srej_sent = false;
    conn->send_srej = false;
    conn->n_s_needed = -1;
    action = "accept, clear SREJ";
  }
  // If the sender has set P, they want an immediate acknowledgment.
  // TNC-2 seems to do this on the last packet of a burst, when its
  // transmit window is full.  JNOS doesn't seem to do this, I think.
  // If the sender hasn't set P, start timer T2 so that we send them
  // an acknowledgement "soon".
  if (sender_p) {
    conn->i_frame_poll_received = true;
  } else {
    start_conn_timer(&conn->t2);
  }
  return action;
}


// Search the deferred-input list for frames, starting with
// our next expected sequence number and going up through
// our receive window size.  The frames in the list may
// not necessarily be in sequence-number order, so we
// use a search routine to find 'em.  For each in-order
// frame we find, consume it.  If we don't find a frame,
// but the list isn't empty, send an SREJ for the missing
// frame.
void process_deferred_I_frames(connection *conn) {
  ax25buffer *buffer;
  packet_parse p;
  bool fail = false;
  if (sys_dlist_is_empty(&conn->deferred_input)) {
    return;
  }
  for (int32_t i = 0 ; !fail && i < conn->capabilities.filter_window; i++) {
    buffer = find_deferred_input(conn, conn->v_r);
    if (buffer) {
      LOG_DBG(" Found I-frame %p frame %d", buffer, buffer->opaque_data);
      if (!parse_packet(&p, buffer->data, ax25buffer_byte_count(buffer),
			conn->capabilities.extended_sequence)) {
	LOG_ERR("  Discard, did not parse - this should not happen!");
	ax25buffer_free(buffer);
	fail = true;
	continue;
      }
      (void) process_I_frame(conn, &p, false);
      ax25buffer_free(buffer);
    } else {
      LOG_DBG(" Did not find frame %d", conn->v_r);
      fail = true;
    }
  }
  // If we still have entries on the deferred list, we know that
  // we are missing at least one, so we'll want to do another
  // SREJ.
  if (fail && !sys_dlist_is_empty(&conn->deferred_input)) {
    LOG_DBG(" Still in SREJ condition");
    conn->send_srej = true;
  }
}

void layer3_I_frame(connection *conn, ax25buffer *b, packet_parse *p, full_address_path *path, bool is_connected_to) {
  if (!is_connected_to) {
    LOG_INF("Unexpected I frame");
    enqueue_packet(path, conn, FRAME_IS_NEITHER,
		   false, false,
		   encode_u_control(U_FRAME_DM, true),
		   AX25_NO_PID, NULL, 0, fx25_policy(conn));
    return;
  }
  bool sender_p;
  uint8_t sender_n_r;
  uint8_t sender_n_s;
  const char *action = "???";
  bool process = false;
  LOG_DBG("Packet control is 0x%04X, extended %d",
	  p->control, conn->capabilities.extended_sequence);
  if (conn->capabilities.extended_sequence) {
    sender_p =   (p->control & 0x0100) ? true : false;
    sender_n_r = (p->control & 0xFE00) >> 9;
    sender_n_s = (p->control & 0x00FE) >> 1;
  } else {
    sender_p =   (p->control & 0x10) ? true : false;
    sender_n_r = (p->control & 0xE0) >> 5;
    sender_n_s = (p->control & 0x0E) >> 1;
  }
  // Clean up any packets the sender is acknowledging.  Do this early so
  // we move our outbound-packet window forwards even if we have to
  // drop the packet due to insufficient receive space.
  handle_acknowledged_packets(conn, sender_n_r);
  // If this packet isn't in order, we must have lost at least one.
  // Either request a REJ (for V2.0), or store this packet for possible
  // use after we SREJ, or drop it as being a stale retransmission
  // (out of the receive window).
  do {
    if (sender_n_s == conn->v_r) {
      action = "accept";
      process = true;
      break;
    }
    if (conn->capabilities.supports_srej) {
      if (!sequence_in_window(conn, sender_n_s)) {
	action = "out of window, discard";
	break;
      }
      // If there's already a buffer for this sequence number on the
      // list, get rid of it safely.
      ax25buffer *stale = find_deferred_input(conn, sender_n_s);
      if (stale) {
	LOG_DBG("Deleting stale copy of frame %d from defer list",
		sender_n_s);
	ax25buffer_free(stale);
      }
      ax25buffer *clone = ax25buffer_clone(b);
      if (clone) {
	action = "defer";
	clone->opaque_data = sender_n_s;
	ax25buffer_append_to_dlist(clone, &conn->deferred_input);
	// Pay attention to "poll now" at this time, since we won't do it
	// after the SREJ.
	if (sender_p) {
	  conn->i_frame_poll_received = true;
	}
      } else {
	action = "defer DROP!";
      }
      conn->send_srej = true; // We know we missed at least one.
    } else {
      action = "out of sequence, REJ";
      conn->send_rej = true;
    }
  } while (false);
  if (process) {
    if (conn->send_rej) {
      LOG_INF("Canceling requested REJ, we're back in sync");
      conn->send_rej = false;
    }
    action = process_I_frame(conn, p, sender_p);
  }
  LOG_INF("I frame, N_S %d, N_R %d, P %d, pid 0x%02X: %s", sender_n_s, sender_n_r, sender_p,
	  p->pid, action);
  if (process) {
    process_deferred_I_frames(conn);
  }
}

void layer3_S_frame(connection *conn, packet_parse *p, full_address_path *path, bool is_connected_to) {
  LOG_DBG("S frame");
  if (conn->state != CONN_CONNECTED || !is_connected_to) {
    plaintext_callsign path_src, path_dest;
    decode_callsign(path_src, &path->src);
    decode_callsign(path_dest, &path->dest);
    LOG_ERR("Unexpected S frame, send DM");
    LOG_ERR("  Connection state %d, is-connected %d", conn->state, is_connected_to);
    LOG_ERR("  Sent from %s to %s", path_dest, path_src);
    enqueue_packet(path, conn, FRAME_IS_COMMAND,
 		   false, false,
		   encode_u_control(U_FRAME_DM, true),
		   AX25_NO_PID, NULL, 0, fx25_policy(conn));
    return;
  }
  enum S_frame_ss ss = (p->control & 0xC) >> 2;
  bool sender_p;
  uint8_t sender_n_r;
  if (conn->capabilities.extended_sequence) {
    sender_p =   (p->control & 0x0100) ? true : false;
    sender_n_r = (p->control & 0xFE00) >> 9;
  } else {
    sender_p =   (p->control & 0x10) ? true : false;
    sender_n_r = (p->control & 0xE0) >> 5;
  }
  // Log before doing anything much...
  const char *role = frame_roles[p->frame_role];
  switch (ss) {
  case S_FRAME_RR:
    LOG_INF("RR %s, seq %d, p %d", role, sender_n_r, sender_p);
    break;
  case S_FRAME_RNR:
    LOG_INF("RNR %s, seq %d, p %d", role, sender_n_r, sender_p);
    break;
  case S_FRAME_REJ:
    LOG_INF("REJ %s, seq %d, p %d", role, sender_n_r, sender_p);
    break;
  case S_FRAME_SREJ:
    LOG_INF("SREJ %s, seq %d, p %d", role, sender_n_r, sender_p);
    break;
  default:
    LOG_ERR("Bad S frame");
    // Should reset the link here.
    break;
  }
  // Clean up any packets the sender is acknowledging... except for SREJ
  // frames which don't have P set, which don't act as an acknowledgement.
  // We might see these if our peer implements multi-rej and doesn't
  // take note of the fact that we don't announce support for it.
  if (ss != S_FRAME_SREJ || sender_p) {
    handle_acknowledged_packets(conn, sender_n_r);
  }
  // We know they're alive.  Restart the T3 keepalive.
  start_conn_timer(&conn->t3);
  // What we should really do here:
  // (1) Validate that the N_R value we were sent, makes sense.  If it falls
  // outside of the window that's reasonable, we have an irrecoverable error;
  // reset the link.
  //
  // (2) If their N_R has advanced from what we've seen before, go through all
  // of the packets it's acknowledging, and pop them from our
  // prepared-packet queue - unclaim the ringbuffer storage, shift all of
  // the remaining prepared-packets downwards in the queue, and increment
  // N_R accordingly.  We thus process acknowledgements for all packets up
  // to N_R - 1.
  //
  // If a REJ has occurred, schedule to transmit the remaining packet(s) in the
  // prepared-packet queue.
  switch (ss) {
  case S_FRAME_RR:
    conn->peer_ready = true;
    conn->rej_received = false;
    break;
  case S_FRAME_RNR:
    conn->peer_ready = false;
    conn->rej_received = false;
    break;
  case S_FRAME_REJ:
    conn->rej_received = true;
    conn->peer_ready = true;
    relist_l3_packets(conn);  // Make oldest-unacknowledged available for immediate retransmission
    break;
  case S_FRAME_SREJ:
    conn->peer_ready = true;
    relist_l3_packet_numbered(conn, sender_n_r);
    break;
  default:
    break;
  }
  // If we receive one of these frames with the P bit set, it could
  // mean either of two very different things.
  //
  // If it's a command frame, our peer is asking for a status update.
  // We must send an RR/RNR/REJ response (depending on our status) with
  // the F bit set.
  //
  // If it's a response frame, it's a reply to an RR/RNR/REJ-with-P
  // that we sent previously to request status.  In this case we don't
  // send a reply.  We just clear our poll flag, and (unless it's an
  // SREJ frame) make all of our unacknowledged packets available for
  // retransmission.
  if (sender_p) {
    if (p->frame_role == FRAME_IS_COMMAND) {
      conn->polled_for_rr_or_rnr = true;
      conn->send_final_bit = true;
    } else {
      conn->poll_for_rr_or_rnr_sent = false;
      if (ss != S_FRAME_SREJ) {
	relist_l3_packets(conn);
      }
    }
  }
}

void send_XID_frame(connection *conn, bool is_command) {
  // somewhat ugly way of constructing an XID frame payload
  struct {
    uint8_t FI;
    uint8_t GI;
    uint8_t GL[2];
    uint8_t payload[30];
  } XID_frame;
  XID_frame.FI = XID_FI;
  XID_frame.GI = XID_GI;
  XID_frame.payload[0] = PI_CLASS_OF_PROCEDURES;
  XID_frame.payload[1] = 2;
  XID_frame.payload[2] = 0x21; // Balanced ABM, half-duplex only
  XID_frame.payload[3] = 0;
  XID_frame.payload[4] = PI_HDLC_OPTIONAL_FUNCTIONS;
  XID_frame.payload[5] = 3;
  XID_frame.payload[6] =
    HDLC_BYTE0_2_REJ_supported |
    HDLC_BYTE0_3A_SREJ_supported |
    HDLC_BYTE0_7B_extended_address;
  XID_frame.payload[7] =
    HDLC_BYTE1_10A_modulo_8 |
    HDLC_BYTE1_10B_modulo_128 |
    HDLC_BYTE1_12_TEST_supported |
    HDLC_BYTE1_14A_16_bit_FCS;
  XID_frame.payload[8] =
    HDLC_BYTE2_15A_synchronous_TX;
  // If we are being queried by the peer, shift down to the best
  // compatible mode error mode.
  if (!is_command) {
    if (conn->capabilities.supports_srej) {
      XID_frame.payload[6] &= ~HDLC_BYTE0_2_REJ_supported;
    } else {
      XID_frame.payload[6] &= ~HDLC_BYTE0_3A_SREJ_supported;
    }
  }
  // Only set one modulo bit.
  if (conn->capabilities.extended_sequence) {
    XID_frame.payload[7] &= ~HDLC_BYTE1_10A_modulo_8;
  } else {
    XID_frame.payload[7] &= ~HDLC_BYTE1_10B_modulo_128;
  }
  XID_frame.payload[9] = PI_I_FIELD_LENGTH_TX;
  XID_frame.payload[10] = 2;
  // We'll transmit our PACLEN default, or the peer's RX limit from its
  // XID, whichever is less.
  uint32_t tx_paclen_bits = conn->capabilities.paclen * 8;
  XID_frame.payload[11] = tx_paclen_bits >> 8;
  XID_frame.payload[12] = tx_paclen_bits & 0xFF;
  XID_frame.payload[13] = PI_I_FIELD_LENGTH_RX;
  XID_frame.payload[14] = 2;
  uint32_t rx_paclen_bits = MAX_IFRAME_PAYLOAD * 8;  // Expose our true upper limit
  XID_frame.payload[15] = rx_paclen_bits >> 8;
  XID_frame.payload[16] = rx_paclen_bits & 0xFF;
  // Our TX frame count limit is our MAXFRAME, or the limit
  // from the peer's XID, whichever is less.
  XID_frame.payload[17] = PI_I_WINDOW_SIZE_TX;
  XID_frame.payload[18] = 1;
  XID_frame.payload[19] = conn->capabilities.maxframe;
  // Our RX frame limit is limited by two factors - memory use
  // (for extended-sequence-number operation), and our desire to
  // have a window size no larger than half of the sequence number
  // range (for legacy operation).
  XID_frame.payload[20] = PI_I_WINDOW_SIZE_RX;
  XID_frame.payload[21] = 1;
  XID_frame.payload[22] = conn->capabilities.filter_window;
  XID_frame.payload[23] = PI_ACK_TIMER;
  XID_frame.payload[24] = 2;
  // Send our default, or the peer's suggested timeout, whichever is
  // greater.
  XID_frame.payload[25] = conn->capabilities.t1 >> 8;
  XID_frame.payload[26] = conn->capabilities.t1 & 0xFF;
  XID_frame.payload[27] = PI_RETRYS;
  XID_frame.payload[28] = 1;
  // Send our default, or the peer's suggested retry count, whichever
  // is greater.
  XID_frame.payload[29] = conn->capabilities.retries;
  XID_frame.GL[0] = 0;
  XID_frame.GL[1] = sizeof(XID_frame.payload);
  LOG_INF("Sending XID %s", is_command ? "command" : "response");
  LOG_INF(" TX max frame length: %d bits", tx_paclen_bits);
  LOG_INF(" RX max frame length: %d bits", rx_paclen_bits);
  LOG_INF(" TX window size:      %d frames", XID_frame.payload[19]);
  LOG_INF(" RX window size:      %d frames", XID_frame.payload[22]);
  LOG_INF(" ACK timer-timeout:   %d milliseconds", conn->capabilities.t1);
  LOG_INF(" Retry limit:         %d attempts", conn->capabilities.retries);
  enqueue_packet(&conn->path, conn,
		 is_command ? FRAME_IS_COMMAND : FRAME_IS_RESPONSE,
		 false, is_command,
		 encode_u_control(U_FRAME_XID, true),
		 AX25_NO_PID, (uint8_t *) &XID_frame, sizeof(XID_frame),
		 fx25_policy(conn));
}

void process_XID_frame(connection *conn, packet_parse *p) {
  uint8_t *payload = p->payload;
  if (p->payload_length < 4) {
    LOG_ERR("Short XID frame");
    return;
  }
  if (payload[0] != XID_FI || payload[1] != XID_GI) {
    LOG_ERR("XID not formatted as expected");
    return;
  }
  uint32_t GL = (payload[2] << 8) + payload[3];
  LOG_DBG("XID GL %d", GL);
  uint32_t offset = 4;
  uint32_t hits_the_wall = offset + GL;
  if (hits_the_wall > p->payload_length) {
    LOG_ERR("XID GL extends past end of packet payload (length %d)", p->payload_length);
    return;
  }
  while (offset < hits_the_wall - 2) {
    uint32_t PI = payload[offset];
    uint32_t PL = payload[offset + 1];
    if (offset + PL + 2 > hits_the_wall) {
      LOG_ERR("XID PI %d / PL %d at offset %d extends past end of packet payload (length %d)", PI, PL, offset, p->payload_length);
      break;
    }
    uint32_t parameter = 0;
    for (int i = 0; i < PL; i++) {
      parameter = (parameter << 8) | payload[offset + i + 2];
    }
    offset = offset + PL + 2;
    LOG_DBG("PI %d PL %d offset %d contained 0x%X", PI, PL, offset, parameter);
    switch (PI) {
    case PI_CLASS_OF_PROCEDURES:
      LOG_DBG("XID Class of Procedures 0x%02X", parameter);
      break;
    case PI_HDLC_OPTIONAL_FUNCTIONS:
      LOG_DBG("XID Optional Functions 0x%06X", parameter);
      if ((parameter >> 16) & HDLC_BYTE0_3A_SREJ_supported) {
	conn->capabilities.supports_srej = true;
	LOG_INF("  Peer supports SREJ");
      }
      if ((parameter >> 16) & HDLC_BYTE0_2_REJ_supported) {
	LOG_INF("  Peer supports REJ");
      }
      break;
    case PI_I_FIELD_LENGTH_TX:
      LOG_INF("  Peer TX max field length %d bits", parameter);
      break;
    case PI_I_FIELD_LENGTH_RX:
      LOG_INF("  Peer RX max field length %d bits", parameter);
      parameter /= 8;
      if (parameter < conn->capabilities.paclen) {
	LOG_INF("    Reducing our PACLEN from %d to %d", conn->capabilities.paclen, parameter);
	conn->capabilities.paclen = parameter;
      }
      break;
    case PI_I_WINDOW_SIZE_TX:
      LOG_INF("  Peer TX max window size %d packets", parameter);
      break;
    case PI_I_WINDOW_SIZE_RX:
      LOG_INF("  Peer RX max window size %d packets", parameter);
      if (parameter < conn->capabilities.maxframe) {
	LOG_INF("    Reducing our window from %d to %d", conn->capabilities.maxframe, parameter);
	conn->capabilities.maxframe = parameter;
      }
      break;
    case PI_ACK_TIMER:
      LOG_DBG("  Peer ACK timer %d milliseconds", parameter);
      if (parameter > conn->capabilities.t1) {
	LOG_INF("    Increasing our T1 from %d to %d milliseconds", conn->capabilities.t1, parameter);
	conn->capabilities.t1 = parameter;
      }
      break;
    case PI_RETRYS:
      LOG_DBG("  Peer retries %d", parameter);
      if (parameter > conn->capabilities.retries) {
	LOG_INF("    Increasing our retry limit from %d to %d", conn->capabilities.retries, parameter);
	conn->capabilities.retries = parameter;
      }
      break;
    default:
      LOG_WRN("    Unknown XID Parameter Identifier %d", PI);
      break;
    }
  }
  if (p->frame_role == FRAME_IS_COMMAND) {
    send_XID_frame(conn, false);
  } else if (conn->state == CONN_NEGOTIATING) {
    LOG_INF("Got good XID response during negotiation, we're really connected!");
    console_printline("*** CONNECTED");
    conn->state = CONN_CONNECTED;
    conn->peer_ready = true;
    stop_conn_timer(&conn->t1);
    start_conn_timer(&conn->t1);
  }
}

void layer3_U_frame(connection *conn, packet_parse *p, full_address_path *path, bool is_connected_to) {
  int mmmmm = ((p->control & 0xE0) >> 3) |
    ((p->control & 0x0C) >> 2);
  bool p_f = (p->control >> 4) & 0x01;
  LOG_DBG("U frame, M = %d", mmmmm);
  switch (mmmmm) {
  case U_FRAME_UI:
    LOG_DBG("UI frame, P/F = %d", p_f);
    // May want to filter on the PID here?
    if (conn->state == CONN_UNPROTO) {
      (*conn->consumer)(conn, p);
    }
    break;
  case U_FRAME_DM:
    LOG_INF("DM frame, P/F = %d", p_f);
    // A DM from somebody we were (at least partially) connected to, is
    // a go-away which merits an immediate teardown.
    switch (conn->state) {
    case CONN_CONNECTED:
    case CONN_CONNECTING:
    case CONN_DISCONNECTING:
      if (conn->endpoint->is_console) {
	console_printline("*** DISCONNECTED");
      }
      conn->state = CONN_CLEANUP;
      break;
    default:
      LOG_INF("Unexpected DM frame, ignoring");
      break;
    }
    break;
  case U_FRAME_SABM:
  case U_FRAME_SABME:
    bool extended_sequence = (mmmmm == U_FRAME_SABME);
    LOG_INF("%s frame, P/F = %d", extended_sequence ? "SABME" : "SABM", p_f);
    if (conn->state == CONN_DISCONNECTED) {
      plaintext_callsign caller;
      decode_callsign(caller, &path->dest);
      if (!tnc_state.conok ||
	  memcmp(&tnc_state.mycall, &nocall, sizeof nocall) == 0) {
	LOG_INF("Reject an offered connection from %s", caller);
	console_printstr("*** Connection request from ");
	console_printline(caller);
	enqueue_packet(path, conn, FRAME_IS_RESPONSE,
		       false, false,
		       encode_u_control(U_FRAME_DM, true),
		       AX25_NO_PID, NULL, 0, fx25_policy(conn));
	break;
      }
      LOG_INF("Start a new connection");
      // Record the FX25 state of the SABM frame, so we can UA appropriately per
      // our policy.
      conn->peer_uses_fx25 = p->fx25;
      conn->peer_uses_non_fx25 = !p->fx25;
      start_inbound_connection(conn, path, extended_sequence);
    } else {
      LOG_INF("Refuse connection, we're busy");
      enqueue_packet(path, NULL, FRAME_IS_RESPONSE,
		     false, false,
		     encode_u_control(U_FRAME_DM, true),
		     AX25_NO_PID, NULL, 0, fx25_policy(conn));
    }
    break;
  case U_FRAME_DISC:
    LOG_INF("DISC frame, P/F = %d", p_f);
    // If this was a station we were connected with, we should acknowledge the
    // disconnection.  If not, send it a DM.
    if (is_connected_to) {
      // TODO - if it was CONNECTED, mark it for teardown and cleanup
      conn->state = CONN_CLEANUP;
      enqueue_packet(path, conn, FRAME_IS_RESPONSE,
		     false, false,
		     encode_u_control(U_FRAME_UA, true),
		     AX25_NO_PID, NULL, 0, fx25_policy(conn));
      if (conn->endpoint->is_console) {
	console_printline("*** DISCONNECTED");
      }
    } else {
      enqueue_packet(path, NULL, FRAME_IS_RESPONSE,
		     false, false,
		     encode_u_control(U_FRAME_DM, true),
		     AX25_NO_PID, NULL, 0, fx25_policy(conn));
    }
    break;
  case U_FRAME_UA:
    LOG_INF("UA frame, P/F = %d", p_f);
    // Need to extend this to handle UA after we sent a SABM on a live
    // connection in order to reset after an error.
    if (conn->state == CONN_CONNECTING && is_connected_to) {
      reset_connection(conn, true); // not sure this is the right place to do this
      (void) fx25_policy(conn);
      start_conn_timer(&conn->t3);
      if (conn->capabilities.extended_sequence) {
	LOG_INF("Connected, starting negotiation");
	console_printline("*** NEGOTIATING");
	LOG_INF("Starting XID negotiation");
	conn->state = CONN_NEGOTIATING;
	send_XID_frame(conn, true);
	start_conn_timer(&conn->t1);
      } else {
	LOG_INF("Connected!");
	console_printline("*** CONNECTED");
	conn->state = CONN_CONNECTED;
	conn->peer_ready = true;
      }
    } else if (conn->state == CONN_DISCONNECTING) {
      LOG_INF("Disconnected!");
      conn->state = CONN_CLEANUP;
      if (conn->endpoint->is_console) {
	console_printline("*** DISCONNECTED");
      }
      conn->disconnect = false;
    } else {
      LOG_INF("  Unexpected, send DM!");
      enqueue_packet(path, conn, FRAME_IS_RESPONSE,
		     false, false,
		     encode_u_control(U_FRAME_DM, true),
		     AX25_NO_PID, NULL, 0, fx25_policy(conn));
    }
    break;
  case U_FRAME_FRMR:
    LOG_INF("FRMR frame, P/F = %d", p_f);
    if (is_connected_to) {
      if (conn->state == CONN_CONNECTING &&
	  conn->attempting_sabme) {
	// FRMR after we send a SABME means we're talking to a v2.0 (or
	// earlier) TNC which doesn't understand extended sequence
	// numbers. Fall back to legacy behavior, and let the next
	// timer expiration send out a SABM instead.
	LOG_INF("Must be a v2.0 or earlier TNC");
	conn->attempting_sabme = false;
	conn->capabilities.extended_sequence = false;
	if (conn->capabilities.maxframe > 4) {
	  conn->capabilities.maxframe = 4;
	}
	break;
      }
      // FRMR during the negotiating phase means we sent an XID frame
      // to an AX25 v2.0 node, which doesn't support it.  Simply move
      // the connection to the CONNECTED state and go on from there.
      if (conn->state == CONN_NEGOTIATING) {
	LOG_INF("  Guess we're talking an AX25 v2.0 node.  No worries.");
	conn->state = CONN_CONNECTED;
	console_printline("*** CONNECTED");
	conn->peer_ready = true;
	break;
      }
      // Irrecoverable error reported by the peer.  Since FRMR recovery
      // in AX.25 2.0 seems to have proven to be a pit of rabid snakes, and
      // was explicitly abandoned by AX.25 2.2, let's do as the latter
      // does - just reset the connection and try to proceed as best as
      // possible.  The other choice would be to force a disconnect.
      reset_connection(conn, true);
      start_conn_timer(&conn->t3);
      enqueue_packet(path, conn, FRAME_IS_RESPONSE,
		     false, false,
		     encode_u_control(U_FRAME_UA, true),
		     AX25_NO_PID, NULL, 0, fx25_policy(conn));
    } else {
      enqueue_packet(path, conn, FRAME_IS_COMMAND,
		     false, false,
		     encode_u_control(U_FRAME_DM, true),
		     AX25_NO_PID, NULL, 0, fx25_policy(conn));
    }
    break;
  case U_FRAME_XID:
    LOG_INF("XID frame");
    process_XID_frame(conn, p);
    break;
  case U_FRAME_TEST:
    LOG_INF("TEST frame");
    break;
  default:
    LOG_INF("Unimplemented frame, P/F = %d", p_f);
    break;
  }
}

// For the sake of simplicity, this thread will hold the
// connection mutex across all of the processing done by
// the individual frame handlers.
void layer3_packet(connection *conn, ax25buffer *b, packet_parse *p) {
  LOG_DBG("Consume a packet, control 0x%02X", p->control);
  k_mutex_lock(&conn->mutex, K_FOREVER);
  if (p->fx25) {
    LOG_DBG("FX.25 packet seen");
    conn->peer_uses_fx25 = true;
  } else {
    LOG_DBG("Non-FX.25 packet seen");
    conn->peer_uses_non_fx25 = true;
  }
  // Connections which have a filter are special.  Once they've
  // decided to match a packet, they take all responsibility
  // for handling that packet, and the local stack should
  // not attempt to do anything further.
  if (conn->filter) {
    if (conn->consumer) {
      LOG_DBG("Handling packet to special-connection consumer");
      (*conn->consumer)(conn, p);
    } else {
      LOG_ERR("Connection has a filter but no consumer!");
    }
    k_mutex_unlock(&conn->mutex);
    return;
  }
  bool is_connected_to = (conn->state != CONN_DISCONNECTED) &&
    same_callsign(&conn->path.dest, &p->path.src) &&
    same_callsign(&conn->path.src, &p->path.dest);
  full_address_path reverse_path;
  invert_path(&reverse_path, &p->path);
  if ((p->control & 0x01) == 0x00) {
    layer3_I_frame(conn, b, p, &reverse_path, is_connected_to);
  } else if ((p->control & 0x03) == 0x01) {
    layer3_S_frame(conn, p, &reverse_path, is_connected_to);
  } else {
    layer3_U_frame(conn, p, &reverse_path, is_connected_to);
  }
  k_mutex_unlock(&conn->mutex);
}

int layer3_thread(void)
{
  LOG_DBG("Layer 3 receiver thread started");
  init_connections();
  while (1) {
    k_sem_take(&layer3_incoming_sem, K_MSEC(1000));
    ax25buffer *b;
    while ((b = ax25buffer_take_head(&rx_packets)) != NULL) {
      bool extended_sequence = false;
      if (tnc_state.kiss_mode) {
	// Handle KISS mode, according to the EKISS setting.
	// EKISS OFF is traditional KISS - all incoming packets are handed
	// to the host, none are handled locally.
	if (tnc_state.ekiss_mode == EKISS_OFF) {
	  ax25buffer_append_to_dlist(b, &kiss_packets);
	  // Continue to next packet, without freeing this one... KISS owns
	  // it!
	  continue;
	}
	// EKISS ALL allows all packets to be examined and handled
	// by the TNC, and they're also sent to the host via KISS
	// (for monitoring, for handling modes like TCP/IP that the
	// TNC doesn't, for a BBS on a different callsign or SSID,
	// etc.
	if (tnc_state.ekiss_mode == EKISS_ALL) {
	  ax25buffer *clone = ax25buffer_clone(b);
	  if (clone) {
	    ax25buffer_append_to_dlist(clone, &kiss_packets);
	  }
	}
	// EKISS PARTIAL is handled down below...
      }
      int32_t len = ax25buffer_byte_count(b);
      LOG_DBG("Found %d bytes of packet to process", len);
      packet_parse p;
      if (!parse_packet(&p, b->data, len, false)) {
	LOG_ERR("Packet did not parse");
	ax25buffer_free(b);
	continue;
      }
      p.fx25 = b->fx25;  // carry along this metadata
      connection *conn = match_packet_to_active_connection(b, &p);
      if (conn) {
	LOG_DBG("Matched an active connection");
	extended_sequence = conn->capabilities.extended_sequence;
      } else {
	conn = match_packet_to_some_connection(b, &p);
	if (conn) {
	  LOG_DBG("Matched an endpoint but not an active connection");
	}
      }
      if (tnc_state.monitor) {
	char line[128];
	DecodePacketFromBuffer('<', b, line, sizeof(line), extended_sequence);
	console_printline(line);
      }
      if (conn) {
	// If the connection is using extended sequence numbers,
	// and the packet contains something other than a U frame,
	// we must re-parse it.
	if ((p.control & 0x03) != 0x03 && conn->capabilities.extended_sequence) {
	  LOG_DBG("Re-parsing packet with extended sequence number");
	  if (!parse_packet(&p, b->data, len, true)) {
	    LOG_ERR("Extended-sequence packet parse failed");
	    ax25buffer_free(b);
	    continue;
	  }
	}
	layer3_packet(conn, b, &p);
      }
      if (!conn && tnc_state.kiss_mode && tnc_state.ekiss_mode == EKISS_PARTIAL) {
	// If we're in EKISS_PARTIAL mode, we send the host only those packets which
	// the TNC did not handle internally.
	ax25buffer_append_to_dlist(b, &kiss_packets);
	b = NULL;
      }
      ax25buffer_free(b); // is safe if we set to NULL right above..
    }
  }
  return 0;
}

K_THREAD_DEFINE(layer3_tid, LAYER3_STACK_SIZE,
                layer3_thread, NULL, NULL, NULL,
                LAYER3_THREAD_PRIORITY, 0, 500);

bool encode_callsign(encoded_callsign *enc, const char *plaintext) {
  memset(enc, 0, sizeof *enc);
  int len = strlen(plaintext);
  if (len > 9) {
    return false;
  }
  bool in_ssid = false;
  int ssid = 0;
  for (int i = 0; i < 6; i++) {
    enc->base_callsign[i] = ' ' << 1;
  }
  for (int i = 0; i < len; i++) {
    char c = plaintext[i];
    if (in_ssid) {
      if (!isdigit(c)) {
	return false;
      }
      ssid = ssid * 10 + c - '0';
    } else {
      if (c == '-') {
	in_ssid = true;
	continue;
      }
      bool accept = false;
      if (isupper(c) || isdigit(c)) {
	accept = true;
      } else if (islower(c)) {
	c = c - 'a' + 'A';
	accept = true;
      }
      if (!accept) {
	return false;
      }
      enc->base_callsign[i] = c << 1;
    }
  }
  if (ssid > 15) {
    return false;
  }
  enc->ssid = ssid << 1;
  enc->ssid |= SSID_RESERVED_MASK;
  return true;
}

void decode_callsign(plaintext_callsign plaintext, const encoded_callsign *enc) {
  int dest = 0;
  for (int i = 0; i < 6; i++) {
    if (enc->base_callsign[i] != ' ' << 1) {
      plaintext[dest++] = enc->base_callsign[i] >> 1;
    }
  }
  if (dest == 0) {
    memcpy(plaintext, "NONE", 4);
    dest = 4;
  }
  int ssid = (enc->ssid >> 1) & 0xF;
  if (ssid != 0) {
    plaintext[dest++] = '-';
    if (ssid <= 9) {
      plaintext[dest++] = ssid + '0';
    } else {
      plaintext[dest++] = '1';
      plaintext[dest++] = (ssid - 10) + '0';
    }
  }
  plaintext[dest] = 0x00;
}

bool same_base_callsign(const encoded_callsign *call1, const encoded_callsign *call2) {
  if (memcmp(call1->base_callsign, call2->base_callsign, sizeof(call1->base_callsign)) != 0) {
    return false;
  }
  return true;
}

bool same_callsign(const encoded_callsign *call1, const encoded_callsign *call2) {
  if (!same_base_callsign(call1, call2)) {
    return false;
  }
  if (((call1->ssid >> 1) & 0xF) != ((call2->ssid >> 1) & 0xF)) {
    return false;
  }
  return true;
}

void invert_path(full_address_path *out, const full_address_path *in) {
  memset(out, 0, sizeof(*out));
  out->digipeaters = in->digipeaters;
  out->dest = in->src;
  out->dest.ssid &= SSID_BYTE_SSID_BITS | SSID_BYTE_C_H_BIT;
  out->src = in->dest;
  out->src.ssid &= SSID_BYTE_SSID_BITS | SSID_BYTE_C_H_BIT;
  for (int i = 0; i < out->digipeaters; i++) {
    out->digi[i] = in->digi[out->digipeaters - i - 1];
    out->digi[i].ssid &= SSID_BYTE_SSID_BITS;  // strip the "has been repeated" flag
  }
  if (out->digipeaters == 0) {
    out->src.ssid |= SSID_BYTE_ADDRESS_EXTENSION_BIT;
  } else {
    out->digi[out->digipeaters - 1].ssid |= SSID_BYTE_ADDRESS_EXTENSION_BIT;
  }
}

bool parse_packet(packet_parse *parsed, uint8_t *packet, uint32_t length, bool two_byte_control) {
  memset(parsed, 0, sizeof *parsed);
  int i;
  int control_offset = -1;
  for (i = 0; i < length; i++) {
    if (packet[i] & SSID_BYTE_ADDRESS_EXTENSION_BIT) {
      control_offset = i + 1;
      break;
    }
  }
  // The address portion of the packet must be a multiple of
  // 7 bytes, at least 14 long, and no longer than 56 (dest,
  // source, 6 digipeaters, at 7 bytes each).
  if (control_offset < 2*7 ||
      control_offset > 8*7 ||
      control_offset % 7 != 0 ||
      control_offset >= length) {
    return false;
  }
  parsed->path.digipeaters = control_offset / 7 - 2;
  memcpy(&parsed->path.dest, packet, sizeof(parsed->path.dest));
  memcpy(&parsed->path.src, packet + 7, sizeof(parsed->path.src));
  if (parsed->path.digipeaters > 0) {
    memcpy(&parsed->path.digi[0], packet + 14,
	   parsed->path.digipeaters * sizeof(parsed->path.digi[0]));
  }
  if ((parsed->path.dest.ssid & SSID_BYTE_C_H_BIT) != 0) {
    parsed->frame_role = FRAME_IS_COMMAND;
  } else if ((parsed->path.src.ssid & SSID_BYTE_C_H_BIT) != 0) {
    parsed->frame_role = FRAME_IS_RESPONSE;
  } else {
    parsed->frame_role = FRAME_IS_NEITHER;
  }
  LOG_DBG("Control offset is %d", control_offset);
  if (two_byte_control) {
    parsed->control = (packet[control_offset + 1] << 8) | packet[control_offset];
    control_offset ++;
  } else {
    parsed->control = packet[control_offset];
  }
  parsed->two_byte_control = two_byte_control;
  LOG_DBG("Control is 0x%04X", parsed->control);
  int32_t payload_starts_at;
  bool can_have_pid;
  if ((parsed->control & 0x01) == 0x00) { // I frames are the only ones with PIDs
    can_have_pid = true;
    payload_starts_at = control_offset + 2;
  } else {
    can_have_pid = false;
    payload_starts_at = control_offset + 1;
  }
  parsed->pid = AX25_NO_PID;
  if (control_offset + 1 >= length || !can_have_pid) {
    LOG_DBG("No PID");
  } else {
    parsed->pid = packet[control_offset + 1];
    LOG_DBG("PID is 0x%02X", parsed->pid);
  }
  int32_t possible_payload_length = length - payload_starts_at;
  if (possible_payload_length > 0) {
    parsed->payload_length = possible_payload_length;
    parsed->payload = &packet[payload_starts_at];
    LOG_DBG("Packet parse: payload length is %d", possible_payload_length);
  } else {
    LOG_DBG("No payload");
  }
  return true;
}

void prepare_packet_header(ax25buffer *buffer,
			   const full_address_path *path,
			   enum command_or_response frame_role,
			   bool two_byte_control,
			   uint16_t control,
			   int16_t pid) {
  LOG_DBG("Encode packet header, role %d, control byte 0x%02X, pid 0x%X, %d digis", frame_role,
	  control, pid, path->digipeaters);
  encoded_callsign src, dest;
  src = path->src;
  dest = path->dest;
  // Set the C bits per AX.25 2.0 specification.  Think about supporting
  // older versions of the protocol at some point.
  switch (frame_role) {
  case FRAME_IS_COMMAND:
    dest.ssid |= SSID_BYTE_C_H_BIT;
    src.ssid &= ~SSID_BYTE_C_H_BIT;
    break;
  case FRAME_IS_RESPONSE:
    src.ssid |= SSID_BYTE_C_H_BIT;
    dest.ssid &= ~SSID_BYTE_C_H_BIT;
    break;
  case FRAME_IS_NEITHER:
  default:
    src.ssid &= ~SSID_BYTE_C_H_BIT;
    dest.ssid &= ~SSID_BYTE_C_H_BIT;
    break;
  }
  // Try flagging any packets using modulo-128 extended-sequence mode with
  // the nonstandard "clear the leftmost bit in the destination SSID"
  // method.
  if (two_byte_control) {
    set_reserved_callsign_bits(&dest, get_reserved_callsign_bits(&dest) & ~SSID_EXTENDED_SEQUENCE);
  }
  ax25buffer_put_bytes(buffer, (uint8_t *) &dest, sizeof(dest));
  for (int i = 0; i <= path->digipeaters; i++) {
    encoded_callsign next_call;
    if (i == 0) {
      next_call = src;
    } else {
      next_call = path->digi[i-1];
    }
    if (i == path->digipeaters) {
      next_call.ssid |= SSID_BYTE_ADDRESS_EXTENSION_BIT;  // last
    } else {
      next_call.ssid &= ~SSID_BYTE_ADDRESS_EXTENSION_BIT; // not last
    }
    ax25buffer_put_bytes(buffer, (uint8_t *) &next_call, sizeof(next_call));
  }
  if (two_byte_control) {
    ax25buffer_put_byte(buffer, control & 0xFF);
    ax25buffer_put_byte(buffer, control >> 8);
  } else {
    ax25buffer_put_byte(buffer, control);
  }
  if (pid != AX25_NO_PID) {
    ax25buffer_put_byte(buffer, (uint8_t) pid);
  }
}

void enqueue_packet(const full_address_path *path,
		    connection *conn,
		    enum command_or_response frame_role,
		    bool two_byte_control,
		    bool should_start_t1,
		    uint16_t control,
		    int16_t pid,
		    uint8_t *payload,
		    size_t payload_length, bool fx25) {
  ax25buffer *buffer = ax25buffer_alloc(AX25_FRAME,
					payload_length + MAX_PACKET_OVERHEAD);
  if (!buffer) {
    LOG_ERR("Can't alloc ax25buffer for %d bytes, dropping frame",
	    payload_length + MAX_PACKET_OVERHEAD);
    return;
  }
  prepare_packet_header(buffer, path, frame_role, two_byte_control, control, pid);
  if (payload && payload_length > 0) {
    ax25buffer_put_bytes(buffer, payload, payload_length);
  }
  buffer->fx25 = fx25;
  if (should_start_t1) {
    buffer->start_t1 = true;
    if (conn) {
      ax25buffer_set_callback(buffer, conn, start_t1_timer, NULL);
    } else {
      LOG_ERR("Oops - a packet which wanted T1 started, came with no connection");
    }
  }
  buffer->fx25 = fx25_policy(conn);
  buffer->extended_sequence = two_byte_control;
  layer2_accept_outgoing(buffer);
}
