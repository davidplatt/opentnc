/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_AX25BUFFER
#define _H_AX25BUFFER

#include <stdint.h>
#include <stdbool.h>

#include <zephyr/sys/dlist.h>

// AX25_FRAME buffers are ordinary, byte-oriented buffers of data
// as one would expect to see in memory.

// AX25_RAW buffers hold raw bits as seen "on the wire", sent without
// any further zero-bit stuffing, received without zero-bit removal.
// By convention, data is written to (and read from) these buffers
// only through the APIs, LSB-first if you're using the put-bit API,
// and the internal storage within the buffer is opaque.

// AX25_PAYLOAD buffers carry only packet payload data, with no
// headers or framing at all.
enum ax25buffer_type {
  AX25_FRAME,   // Full AX.25 frame without SYNCs, not zero-stuffed
  AX25_RAW,     // AX.25 raw bits (not NRZI-encoded, but zero-stuffed).
  AX25_PAYLOAD  // Payload data, with no framing
};

struct connection;
typedef struct ax25buffer ax25buffer;

typedef void (*buffer_callback)(ax25buffer *);

struct ax25buffer {
  sys_dnode_t node;
  enum ax25buffer_type type;
  size_t buffer_size;
  int64_t time_start;
  int64_t time_end;
  int32_t bits_written;  // gives offset of next available bit to write
  int32_t bits_read;     // gives offset of next available bit to read
  uint8_t command;       // Auxiliary data for KISS
  int fx25_code;
  int priority; // An "enum frame_priority" from hdlc.h, via the HDLC decoder
  bool bad;
  bool fx25;
  bool loopback;
  bool start_t1;
  bool extended_sequence;
  uint8_t *data;
  void *context;         // Auxiliary context for buffer user
  buffer_callback callback;
  struct connection *conn;
  void *callback_data;
  uint32_t opaque_data;  // More auxiliary data for buffer user
};

// Allocate a buffer from the heap, and initialize it.  Returns NULL if
// the heap does not have sufficient space.  Locks the heap mutex
// during allocation.
ax25buffer *ax25buffer_alloc(enum ax25buffer_type type, size_t bytes_needed);

// Clone an existing buffer, with the clone having identical contents but
// only as much buffer space as is needed to carry the actual payload
// from the original buffer.
ax25buffer *ax25buffer_clone(ax25buffer *orig);

// Frees a buffer, first un-linking it if it's on a list.  Locks the heap mutex.
void ax25buffer_free(ax25buffer *buffer);

// Unlink and free the buffer at the head of a specific list.
void ax25buffer_free_head(sys_dlist_t *list);

// If the list is empty, returns NULL.  If the list is not
// empty, unlinks the buffer at the head of the list and
// returns a pointer to it.
ax25buffer *ax25buffer_take_head(sys_dlist_t *list);

// Return the buffer at the head of a list (or NULL if the
// list is empty).  Does not alter the list.
ax25buffer *ax25buffer_peek_head(sys_dlist_t *list);

// Remove a buffer from whatever list (if any) it's on, and append or
// prepend it to the tail or head of the specified list.  Locks the
// buffer-management mutex during execution.
void ax25buffer_append_to_dlist(ax25buffer *buffer, sys_dlist_t *list);
void ax25buffer_prepend_to_dlist(ax25buffer *buffer, sys_dlist_t *list);

// Manually lock and unlock the buffer-management mutex.  Use these if
// you're going to be iterating through the lists, to ensure that no other
// thread alters a list you're iterating.  The lock function will wait
// K_FOREVER until the mutex is available.
void ax25buffer_lock_mutex(void);
void ax25buffer_unlock_mutex(void);

// Set a callback on the buffer.  Currently used when transmitting
// I-frames, to start the T1 retransmission timer.
void ax25buffer_set_callback(struct ax25buffer *buffer,
			     struct connection *conn,
			     buffer_callback callback,
			     void *callback_data);

// Return true if the packetbuffer heap free space is falling below
// an arbitrary threshold.  This is used to throttle the queueing of
// additional outbound frames when we might not have enough memory
// to handle inbound data.
bool ax25buffer_congested(int32_t wanted);

// The following functions do not lock or hold any mutex during
// operation.
void ax25buffer_put_byte(struct ax25buffer *buffer, uint8_t byte);
void ax25buffer_put_bit(struct ax25buffer *buffer, uint8_t bit);
void ax25buffer_put_bytes(struct ax25buffer *buffer, const uint8_t *bytes,
			  int count);
void ax25buffer_set_command(struct ax25buffer *buffer, uint8_t byte);
void ax25buffer_retract_bytes(struct ax25buffer *buffer, uint32_t count);
void ax25buffer_reset(struct ax25buffer *buffer);
void ax25buffer_reset_read(struct ax25buffer *buffer);

int32_t ax25buffer_get_byte(struct ax25buffer *buffer);
int32_t ax25buffer_get_bit(struct ax25buffer *buffer);
int32_t ax25buffer_get_bytes(struct ax25buffer *buffer, uint8_t *bytes, uint32_t limit);
int32_t ax25buffer_get_command(struct ax25buffer *buffer);
int32_t ax25buffer_byte_count(struct ax25buffer *buffer);

inline uint8_t *ax25buffer_get_data(struct ax25buffer *buffer) {
  return buffer->data;
}

#endif // ifdef _H_AX25BUFFER
