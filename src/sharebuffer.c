/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stdint.h>
#include <stdbool.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>

#include "sharebuffer.h"

LOG_MODULE_REGISTER(sharebuffer, LOG_LEVEL_INF);

void sharebuffer_init(struct sharebuffer *sb) {
  uint32_t buffer_size = sb->buffer_size;
  memset(sb, 0, sizeof *sb);
  sb->buffer_size = buffer_size;
  k_mutex_init(&sb->mutex);
};

uint32_t sharebuffer_get_tail(struct sharebuffer *sb) {
  return sb->tail;
}

uint32_t sharebuffer_get_head(struct sharebuffer *sb) {
  return sb->head;
}

uint32_t sharebuffer_get_size(struct sharebuffer *sb) {
  return sb->buffer_size;
}

uint32_t sharebuffer_get_space_avail(struct sharebuffer *sb) {
  k_mutex_lock(&sb->mutex, K_FOREVER);
  int32_t in_use = sb->tail - sb->head;
  if (in_use < 0) {
    in_use += sb->buffer_size;
  }
  uint32_t available = sb->buffer_size - 1 - in_use;
  k_mutex_unlock(&sb->mutex);
  return available;
}

void sharebuffer_put_byte(struct sharebuffer *sb, uint8_t byte) {
  k_mutex_lock(&sb->mutex, K_FOREVER);
  uint32_t available = sharebuffer_get_space_avail(sb);
  if (available > 0) {
    sb->buffer[sb->tail] = byte;
    sb->tail = (sb->tail + 1) % sb->buffer_size;
  }
  k_mutex_unlock(&sb->mutex);
}

void sharebuffer_put_bytes(struct sharebuffer *sb, const uint8_t *bytes, uint32_t count) {
  LOG_INF("Put %d bytes at tail %d", count, sb->tail);
  k_mutex_lock(&sb->mutex, K_FOREVER);
  uint32_t available = sharebuffer_get_space_avail(sb);
  while (count > 0 && available > 0) {
    sb->buffer[sb->tail] = *bytes;
    sb->tail = (sb->tail + 1) % sb->buffer_size;
    bytes++;
    count--;
    available--;
  }
  LOG_DBG("Tail is now at %d", sb->tail);
  if (count != 0) {
    LOG_ERR("Sharebuffer overrun - this is Not Good!");
  }
  k_mutex_unlock(&sb->mutex);
}

// Crude but effective...
void sharebuffer_pop_from_tail(struct sharebuffer *sb, uint32_t count) {
  while (count && sb->tail != sb->claimed && sb->tail != sb->head) {
    count--;
    if (sb->tail == 0) {
      sb->tail = sb->buffer_size - 1;
    } else {
      sb->tail--;
    }
  }
}

uint32_t sharebuffer_get_data_avail(struct sharebuffer *sb) {
  k_mutex_lock(&sb->mutex, K_FOREVER);
  int32_t in_use = sb->tail - sb->head;
  if (in_use < 0) {
    in_use += sb->buffer_size;
  }
  k_mutex_unlock(&sb->mutex);
  return in_use;
}

void sharebuffer_get_unclaimed_data_avail(struct sharebuffer *sb, uint32_t *offset, uint32_t *count) {
  k_mutex_lock(&sb->mutex, K_FOREVER);
  int32_t unclaimed = sb->tail - sb->claimed;
  if (unclaimed < 0) {
    unclaimed += sb->buffer_size;
  }
  *offset = sb->claimed;
  *count = unclaimed;
  k_mutex_unlock(&sb->mutex);
}

// Grab and consume the byte at the head.  Returns 0 for attempts to
// grab from an empty buffer.
uint8_t sharebuffer_get_byte(struct sharebuffer *sb) {
  uint8_t result;
  k_mutex_lock(&sb->mutex, K_FOREVER);
  if (sb->head == sb->tail) {
    result = 0;
    LOG_ERR("Tried to read from empty sharebuffer");
  } else {
    result = sb->buffer[sb->head];
    uint32_t old_head = sb->head;
    sb->head = (sb->head + 1) % sb->buffer_size;
    if (sb->claimed == old_head) {
      sb->claimed = sb->head;
    }
  }
  k_mutex_unlock(&sb->mutex);
  return result;
}

// Get data from a given offset, automatically wrapping around the end
// if necessary.  This is only safe if the data in that range has
// been claimed.
void sharebuffer_get_data_range(struct sharebuffer *sb, uint32_t offset, uint8_t *bytes, uint32_t count) {
  k_mutex_lock(&sb->mutex, K_FOREVER);
  for (int i = 0; i < count; i++) {
    bytes[i] = sb->buffer[offset];
    offset = (offset + 1) % sb->buffer_size;
  }
  k_mutex_unlock(&sb->mutex);
}

// Claim and unclaim bytes within the buffer.  Bytes must be claimed from the
// head forward, and released in the same order.  Bytes which have been
// claimed are immune to buffer flushing (e.g. from a control-C with data
// waiting to be sent).  Unclaiming a range (which necessarily must start
// at the head) moves the head to after the range, thus freeing the unclaimed
// data.

uint32_t sharebuffer_claim_range(struct sharebuffer *sb, uint32_t offset, uint32_t count) {
  LOG_DBG("Request to claim %d at offset %d", count, offset);
  k_mutex_lock(&sb->mutex, K_FOREVER);
  if (offset != sb->claimed) {
    LOG_ERR("Discontiguous claim!");
  } else {
    sb->claimed = (sb->claimed + count) % sb->buffer_size;
  }
  k_mutex_unlock(&sb->mutex);
  return sb->claimed;
}

void sharebuffer_unclaim_range(struct sharebuffer *sb, uint32_t offset, uint32_t count) {
  LOG_DBG("Request to unclaim %d at offset %d", count, offset);
  k_mutex_lock(&sb->mutex, K_FOREVER);
  if (offset != sb->head) {
    LOG_ERR("Unclaim not at head!");
  } else {
    sb->head = (sb->head + count) % sb->buffer_size;
    LOG_DBG(" Head is now at %d", sb->head);
  }
  k_mutex_unlock(&sb->mutex);
}

void sharebuffer_flush_unclaimed(struct sharebuffer *sb) {
  LOG_DBG("Sharebuffer flush");
  k_mutex_lock(&sb->mutex, K_FOREVER);
  sb->tail = sb->claimed;
  k_mutex_unlock(&sb->mutex);
}



