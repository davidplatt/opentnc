/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_GLOBALS
#define _H_GLOBALS

#include <stdint.h>
#include <stdbool.h>

#include <zephyr/sys/ring_buffer.h>

#include "ax25.h"
#include "ax25buffer.h"
#include "connection.h"
#include "damask.h"
#include "fx25.h"
#include "hdlc.h"
#include "sharebuffer.h"

#define OPENTNC_MAJOR_VERSION 4
#define OPENTNC_MINOR_VERSION 0
#define OPENTNC_VERSION_VARIANT ""

enum modulator
  {
    PWM_MODULATOR = 0,
    DELTA_SIGMA_MODULATOR = 1,
    G3RUH_PWM_MODULATOR = 2,
  };

enum demodulator
  {
   GOERTZEL_DEMODULATOR = 0,
   ZERO_CROSSING_COUNTER_DEMODULATOR = 1,
   ZERO_CROSSING_TIME_DEMODULATOR = 2,
   DIRECT_SIGN_DEMODULATOR = 3,
   BIQUAD_FILTER_DEMODULATOR = 4,
   G3RUH_DEMODULATOR = 5,
  };

enum pactime_mode {
  PACTIME_AFTER = 0,
  PACTIME_EVERY = 1,
  PACTIME_SENDPAC = 2
};

enum carrier_detect_mode {
  CARRIER_DETECT_HDLC = 0,
  CARRIER_DETECT_AUDIO = 2,
  CARRIER_DETECT_SQUELCH = 3
};

enum operating_mode {
  OPERATING_MODE_NORMAL = 0,
  OPERATING_MODE_EXPERT = 1
};

enum aprs_digipeating_mode {
  APRS_DIGI_MODE_OFF = 0,
  APRS_DIGI_MODE_WIDE1_1 = 1,
  APRS_DIGI_MODE_WIDE2_1 = 2,
  APRS_DIGI_MODE_WIDE2_2 = 3,
};

enum radio_baud_rate {
  RADIO_BAUD_1200 = 0,
  RADIO_BAUD_9600 = 1,
};

enum pulse_shape {
  PULSE_SINC = 0,
  PULSE_RAISED_COSINE_050 = 1,
  PULSE_RAISED_COSINE_090 = 2,
  PULSE_RAISED_COSINE_095 = 3,
};

// ekiss mode controls whether the TNC's internal connection/digipeating
// endpoints are to function when the TNC is operating in KISS mode.
// OFF means that they're disabled, and that the KISS interface receives
// all packets (this is traditional KISS mode).  PARTIAL means that they're
// functional, and that the KISS interface should be sent only those
// packets not handled internally.  ALL means that they're functional,
// and that the KISS interface should receive all packets (handled
// internally or not).
enum ekiss_mode {
  EKISS_OFF = 0,
  EKISS_PARTIAL = 1,
  EKISS_ALL = 2,
};

// Transmit pre-equalization characteristic - flat frequency response,
// or high-boost with leading or no or trailing phase adjustment.
enum tx_adjust_phase {
  TX_EQ_FLAT = 0,
  TX_EQ_LEADING = 1,
  TX_EQ_NOSHIFT = 2,
  TX_EQ_TRAILING = 3
};

enum ax25_version {
  AX25_VERSION_20 = 0,
  AX25_VERSION_22 = 1
};

struct rx_push_state {
  bool working;
  bool append_checksum;
  bool checksumming_high;
  bool final;
  bool done;
  bool send_first_byte;
  bool send_second_byte;
  uint8_t checksum_high;
  uint8_t first_byte;
  uint8_t second_byte;
  uint16_t crc;
  ax25buffer *current;
};

#define COMMAND_LIMIT 16

struct rx_pull_state {
  bool in_frame;
  bool data_frame;
  bool command_frame;
  bool flex_crc;
  bool smack_crc;
  bool escaped;
  uint16_t crc;
  uint8_t command_code;
  uint32_t command_len;
  uint8_t command[COMMAND_LIMIT];
};

struct tx_push_state {
  bool working;
  bool need_fcs;
  bool send_fcs_lsb;
  bool send_fcs_msb;
  uint8_t slot_delay;
  uint16_t fcs;
  int32_t send_zeros;
  int32_t send_syncs;
  int32_t send_final_sync;
  int32_t send_aborts;
  ax25buffer *current;
};

struct tnc_state {
  encoded_callsign mycall;
  encoded_callsign echoback_call;
  encoded_callsign unproto_call;
  bool input_throttled;
  bool kiss_mode;
  bool kiss_auto;
  bool echo;
  bool auto_newline;
  bool bkondel;
  bool monitor;
  bool hdlc_loopback;
  bool adc_loopback;
  bool poll_on_last;
  bool ptt;
  bool hdlc_locked;
  bool audio_detect;
  bool squelch_open_detect;
  // Common TNC-2-settable parameters
  bool conok;
  bool conmode_is_transparent;
  bool cpactime;
  bool digipeating;
  // Settable integer parameters are all stored as 32-bit signed,
  // to make the console code's life a bit easier.
  int32_t sendpac;
  int32_t canline;
  int32_t command;
  int32_t delete;
  int32_t paclen;
  int32_t maxframe;
  int32_t pactime;
  enum pactime_mode pactime_mode;
  int32_t audio_carrier_threshold;
  enum carrier_detect_mode carrier_detect_mode;
  int32_t t1; // 1-second units
  int32_t t2; // 100-millisecond units
  int32_t t3; // 10-second units
  int32_t carrier_effect_on_t1;  // if e.g. 5, T1 decrements at 1/5 normal rate when carrier is detected
  int32_t outpost_delay;  // milliseconds of delay in sending cmd: prompt
  // P-persistence
  int32_t p;
  int32_t slot_time;
  // Operating state
  int32_t modulator;
  int32_t demodulator;
  int32_t retries;
  int32_t initial_syncs;
  int32_t txdelay;
  int32_t txtail;
  int32_t drop_rx; // if nonzero, 1/N chance of packet drop during RX
  int32_t drop_tx; // if nonzero, 1/N chance of packet drop during TX
  int32_t smash_fx25_rx; // if nonzero, 1/N chance of corrupting Reed-Solomon data during TX
  int32_t smash_fx25_tx; // if nonzero, 1/N chance of corrupting Reed-Solomon data during TX
  int32_t not_ready_simulation; // if nonzero, N/256 chance we'll simulate no-room-to-receive.
  int32_t operating_mode; // hide, or show expert-user commands
  int32_t aprs_digi_mode; // APRS digipeating mode
  int32_t ekiss_mode;
  int32_t pulse_shape;
  int32_t radio_baud_rate;
  int32_t ax25_version;
  // DAMASK packet scheduling
  enum damask_mode damask_mode;
  int32_t damask_short_quiet_time; // 100-millisecond increments
  int32_t damask_long_quiet_time; // 100-millisecond increments
  // FX.25
  enum fx25_encoding fx25_encoding;  // Disable, or select roots/robustness used
  enum fx25_connection_mode fx25_connection_mode; // Select use on individual connections
  // Internal values, not settable from the console
  bool tx_inhibit;
  bool rx_inhibit;
  bool tx_running;
  bool rx_running;
  uint8_t modulator_transmitting;
  uint8_t lock_threshold;
  uint8_t zero_crossing_threshold;
  uint8_t zero_crossing_hysteresis;
  uint8_t minimum_zeros;
  uint8_t minimum_aborts;
  uint8_t slot_delay;
  uint8_t full_duplex;
  int64_t hdlc_hold_until;
  int64_t hdlc_processed_through;
  int32_t audio_peak;
  int32_t lamp_test_time;
  bool lamp_test;
  bool waveform_test;
  uint32_t board_bad_ADC_sample_flag;
  int32_t txeq_mode;
  int32_t txeq_peak_frequency;
  int32_t txeq_peak_gain_centibels;

  // Statistics
  struct {
    uint32_t heap_allocs;
    uint32_t heap_frees;
    uint32_t frames_sent;
    uint32_t frames_accepted;
    uint32_t frames_filtered;
    uint32_t fx25_codeblocks_sent;
    uint32_t fx25_codeblocks_seen;
    uint32_t fx25_codeblocks_accepted;
    uint32_t fx25_codeblocks_corrected;
    uint32_t fx25_samples_corrected;
    uint32_t frames_digipeated;  // standard digipeating
    uint32_t aprs_digipeated;    // WIDE1 or WIDE2 digipeating of APRS UI frames
  } stats;
};

extern encoded_callsign nocall;
extern encoded_callsign wide1;
extern encoded_callsign wide2;

extern int32_t demod_samples_processed;

extern struct rx_push_state rx_push;
extern struct rx_pull_state rx_pull;
extern struct tx_push_state tx_push;

extern sys_dlist_t rx_packets;
extern sys_dlist_t tx_packets;

extern sys_dlist_t kiss_packets; // To be sent to host via KISS

extern struct ring_buf host_inputbuffer;
extern struct ring_buf host_outputbuffer;
extern struct sharebuffer console_sharebuffer;

extern struct tnc_state tnc_state;
extern const struct tnc_state default_tnc_state;

extern connection_endpoint console_endpoint;
extern connection_endpoint unproto_endpoint;

extern connection * const console_connection;

extern struct hdlc_rcv hdlc_context_flat;

#endif
