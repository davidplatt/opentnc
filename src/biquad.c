/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <tgmath.h>
#include <stdio.h>
#include <stdlib.h>

#include "biquad.h"
#include "board.h"

void reset_biquad_f(biquad_f *bq) {
  bq->x1 = bq->x2 = bq->y1 = bq->y2 = 0.0f;
  bq->x1_fp = bq->x2_fp = bq->y1_fp = bq->y2_fp = bq->residue_fp = int32_to_fp32(0);
}

float TIME_CRITICAL(iterate_biquad_f)(biquad_f *bq, float x) {
  float y = bq->b0 * x + bq->b1 * bq->x1 + bq->b2 * bq->x2 - bq->a1 * bq->y1 - bq->a2 * bq->y2;
  bq->x2 = bq->x1;
  bq->x1 = x;
  bq->y2 = bq->y1;
  bq->y1 = y;
  return y;
}

fp32_t TIME_CRITICAL(iterate_biquad_fp)(biquad_f *bq, fp32_t x_fp) {
  fp64_t y_fp = fp32_x_fp32(bq->b0_fp, x_fp) +
    fp32_x_fp32(bq->b1_fp, bq->x1_fp) +
    fp32_x_fp32(bq->b2_fp, bq->x2_fp) -
    fp32_x_fp32(bq->a1_fp, bq->y1_fp) -
    fp32_x_fp32(bq->a2_fp, bq->y2_fp) +
    bq->residue_fp;
  bq->x2_fp = bq->x1_fp;
  bq->x1_fp = x_fp;
  bq->y2_fp = bq->y1_fp;
  bq->y1_fp = fp64_to_fp32(y_fp);
  bq->residue_fp = fp64_residue(y_fp);
  return bq->y1_fp;
}

void compute_bandpass_biquad_f(biquad_f *bq, float Fs, float F0, float Q) {
  float w0 = 2 * 3.14159265F * F0 / Fs;
  float alpha = sin(w0) / (2 * Q);
  // compute non-normalized coefficients
  float b0 = alpha;
  float b1 = 0;
  float b2 = -alpha;
  float a0 = 1 + alpha;
  float a1 = -2 * cos(w0);
  float a2 = 1 - alpha;
  // Store the coefficients in the biquad structure in normalized form (a0 = 1)
  bq->a1 = a1 / a0;
  bq->a2 = a2 / a0;
  bq->b0 = b0 / a0;
  bq->b1 = b1 / a0;
  bq->b2 = b2 / a0;
  // Convert to fixed point
  bq->a1_fp = float_to_fp32(bq->a1);
  bq->a2_fp = float_to_fp32(bq->a2);
  bq->b0_fp = float_to_fp32(bq->b0);
  bq->b1_fp = float_to_fp32(bq->b1);
  bq->b2_fp = float_to_fp32(bq->b2);
}

void compute_lowpass_biquad_f(biquad_f *bq, float Fs, float F0, float Q) {
  float w0 = 2 * 3.14159265F * F0 / Fs;
  float alpha = sin(w0) / (2 * Q);
  // compute non-normalized coefficients
  float b1 = 1 - cos(w0);
  float b0 = b1 / 2;
  float b2 = b1 / 2;
  float a0 = 1 + alpha;
  float a1 = -2 * cos(w0);
  float a2 = 1 - alpha;
  // Store the coefficients in the biquad structure in normalized form (a0 = 1)
  bq->a1 = a1 / a0;
  bq->a2 = a2 / a0;
  bq->b0 = b0 / a0;
  bq->b1 = b1 / a0;
  bq->b2 = b2 / a0;
  // Convert to fixed point
  bq->a1_fp = float_to_fp32(bq->a1);
  bq->a2_fp = float_to_fp32(bq->a2);
  bq->b0_fp = float_to_fp32(bq->b0);
  bq->b1_fp = float_to_fp32(bq->b1);
  bq->b2_fp = float_to_fp32(bq->b2);
}

void dump_biquad(biquad_f *bq) {
  printf("Biquad:\n");
  printf("  a1 0x%08X %lf %lf\n", bq->a1_fp, (double) fp32_to_float(bq->a1_fp), (double) bq->a1);
  printf("  a2 0x%08X %lf %lf\n", bq->a2_fp, (double) fp32_to_float(bq->a2_fp), (double) bq->a2);
  printf("  b0 0x%08X %lf %lf\n", bq->b0_fp, (double) fp32_to_float(bq->b0_fp), (double) bq->b0);
  printf("  b1 0x%08X %lf %lf\n", bq->b1_fp, (double) fp32_to_float(bq->b1_fp), (double) bq->b1);
  printf("  b2 0x%08X %lf %lf\n", bq->b2_fp, (double) fp32_to_float(bq->b2_fp), (double) bq->b2);
  printf("  x1 0x%08X %lf %lf\n", bq->x1_fp, (double) fp32_to_float(bq->x1_fp), (double) bq->x1);
  printf("  x2 0x%08X %lf %lf\n", bq->x2_fp, (double) fp32_to_float(bq->x2_fp), (double) bq->x2);
  printf("  y1 0x%08X %lf %lf\n", bq->y1_fp, (double) fp32_to_float(bq->y1_fp), (double) bq->y1);
  printf("  y2 0x%08X %lf %lf\n", bq->y2_fp, (double) fp32_to_float(bq->y2_fp), (double) bq->y2);
  printf("  r  0x%08X %lf\n",     bq->residue_fp, (double) fp32_to_float(bq->residue_fp));
}
