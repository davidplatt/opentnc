/*
 * Copyright (c) 2023, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/crc.h>

#include "ax25.h"
#include "packetring.h"

LOG_MODULE_REGISTER(ring, LOG_LEVEL_INF);

int32_t packetring_space_avail(struct packetring *ring) {
  int32_t inuse;
  inuse = ring->tail - ring->head;
  if (inuse < 0) {
    inuse += ring->buffer_size;
  }
  return (ring->buffer_size - 1) - inuse;
}

static inline bool packetring_buffer_full(struct packetring *ring) {
  return packetring_space_avail(ring) == 0;
}

void packetring_init(struct packetring *ring)
{
  memset(ring, 0, sizeof *ring);
  ring->buffer_size = RING_NBYTES;
  ring->num_packets = RING_NPACKETS;
  ring->state = RING_CONSTRUCTING;
  for (int i = 0; i < RING_NPACKETS; i++) {
    ring->packets[i].state = PACKET_FREE;
  }
  LOG_INF("Init ring");
}

int32_t packetring_new_packet(struct packetring *ring)
{
  struct packet *p = &ring->packets[ring->current_packet];
  // Fast path - we can use this immediately.
  if (p->state == PACKET_CONSTRUCTING && p->count == 0) {
    return 0;
  }
  if (p->state == PACKET_CONSTRUCTING && p->count > 0) {
    p->state = PACKET_COMPLETE;
    ring->current_packet = (ring->current_packet + 1) % ring->num_packets;
    p = &ring->packets[ring->current_packet];
  } else if (p->state == PACKET_ABORTED) {
    ring->tail = p->head;
    p->count = 0;
    p->state = PACKET_FREE;
  }
  if (packetring_buffer_full(ring) || p->state != PACKET_FREE) {
    LOG_INF("Discarding packet(s)");
    ring->state = RING_DISCARDING;
    return -EAGAIN;
  }
  p->count = 0;
  p->head = ring->tail;
  p->state = PACKET_CONSTRUCTING;
  p->callback = NULL;
  p->callback_data = NULL;
  ring->state = RING_CONSTRUCTING;
  return 0;
}

void packetring_set_callback(struct packetring *ring, packet_callback callback, void *callback_data) {
  if (ring->state != RING_CONSTRUCTING) {
    return;
  }
  struct packet *p = &ring->packets[ring->current_packet];
  p->callback = callback;
  p->callback_data = callback_data;
}

void packetring_put_byte(struct packetring *ring, uint8_t byte) {
  if (ring->state != RING_CONSTRUCTING) {
    return;
  }
  struct packet *p = &ring->packets[ring->current_packet];
  if (packetring_buffer_full(ring)) {
    ring->state = RING_DISCARDING;
    p->state = PACKET_ABORTED;
  }
  ring->buffer[(p->head + p->count) % ring->buffer_size] = byte;
  p->count ++;
  ring->tail ++;
}

void packetring_put_bytes(struct packetring *ring, const uint8_t *bytes, 
			  int count) {
  if (ring->state != RING_CONSTRUCTING) {
    return;
  }
  for (int i = 0; i < count; i++) {
    packetring_put_byte(ring, bytes[i]);
  }
}

void packetring_set_command(struct packetring *ring, uint8_t byte) {
  if (ring->state != RING_CONSTRUCTING) {
    return;
  }
  struct packet *p = &ring->packets[ring->current_packet];
  p->command_byte = byte;
}

void packetring_retract_bytes(struct packetring *ring, uint32_t count) {
  if (ring->state != RING_CONSTRUCTING) {
    return;
  }
  struct packet *p = &ring->packets[ring->current_packet];
  if (p->count < count) {
    count = p->count;
  }
  ring->tail = (ring->tail - count) % ring->buffer_size;
  p->count -= count;
}
  
void packetring_retract_packet(struct packetring *ring) {
  if (ring->state != RING_CONSTRUCTING) {
    return;
  }
  struct packet *p = &ring->packets[ring->current_packet];
  ring->tail = (ring->tail - p->count) % ring->buffer_size;
  p->count = 0;
}

void packetring_abort_packet(struct packetring *ring) {
  struct packet *p = &ring->packets[ring->current_packet];
  p->state = PACKET_ABORTED;
  ring->state = RING_DISCARDING;
}

bool packetring_has_one(struct packetring *ring) {
  if (ring->oldest_packet >= 0 &&
      ring->packets[ring->oldest_packet].state == PACKET_COMPLETE) {
    return true;
  }
  return false;
}

int32_t packetring_fetch_packet(struct packetring *ring) {
  if (ring->oldest_packet < 0) {
    return -EAGAIN;
  }
  struct packet *p = &ring->packets[ring->oldest_packet];
  if (p->state != PACKET_COMPLETE) {
    return -EAGAIN;
  }
  p->cursor = 0;
  p->state = PACKET_CONSUMING;
  return p->count;
}
  
int32_t packetring_refetch_packet(struct packetring *ring) {
  if (ring->oldest_packet < 0) {
    return -EAGAIN;
  }
  struct packet *p = &ring->packets[ring->oldest_packet];
  if (p->state != PACKET_CONSUMING) {
    return -EAGAIN;
  }
  p->cursor = 0;
  p->state = PACKET_CONSUMING;
  return p->count;
}
  
int32_t packetring_get_byte(struct packetring *ring) {
  if (ring->oldest_packet < 0) {
    return -EINVAL;
  }
  struct packet *p = &ring->packets[ring->oldest_packet];
  if (p->state != PACKET_CONSUMING) {
    return -EINVAL;
  }
  if (p->cursor >= p->count) {
    return -EIO;
  }
  uint8_t byte = ring->buffer[(p->head + p->cursor) % ring->buffer_size];
  p->cursor ++;
  return byte;
}

int32_t packetring_get_bytes(struct packetring *ring, uint8_t *bytes, uint32_t limit) {
  if (ring->oldest_packet < 0) {
    return -EINVAL;
  }
  struct packet *p = &ring->packets[ring->oldest_packet];
  if (p->state != PACKET_CONSUMING) {
    return -EINVAL;
  }
  if (p->cursor >= p->count) {
    return -EIO;
  }
  int32_t consumed = 0;
  while (p->cursor < p->count && limit > 0) {
    bytes[consumed++] = ring->buffer[(p->head + p->cursor) % ring->buffer_size];
    p->cursor ++;
    limit --;
  }
  return consumed;
}

int32_t packetring_get_command(struct packetring *ring) {
  if (ring->oldest_packet < 0) {
    LOG_ERR("get_command on empty ring");
    return -EINVAL;
  }
  struct packet *p = &ring->packets[ring->oldest_packet];
  if (p->state != PACKET_CONSUMING) {
    LOG_ERR("get_command on bad packet");
    return -EINVAL;
  }
  return p->command_byte;
}

void packetring_consume_packet(struct packetring *ring)
{
  if (ring->oldest_packet < 0) {
    return;
  }
  struct packet *p = &ring->packets[ring->oldest_packet];
  if (p->state != PACKET_CONSUMING) {
    return;
  }
  if (p->callback) {
    (*p->callback)(p->callback_data);
  }
  ring->head = (p->head + p->count) % ring->buffer_size;
  ring->oldest_packet = (ring->oldest_packet + 1) % ring->num_packets;
  p->state = PACKET_FREE;
}

bool packetring_congested(struct packetring *ring, int32_t payload_size) {
  if (packetring_space_avail(ring) < payload_size + MAX_PACKET_OVERHEAD) {
    return true;
  }
  int busy = 0;
  for (int i = 0; i < ring->num_packets; i++) {
    if (ring->packets[i].state != PACKET_FREE) {
      busy ++;
    }
  }
  return busy >= ring->num_packets - 3;
}
