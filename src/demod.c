/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stdio.h>
#include <stdlib.h>
#include <tgmath.h>
#include <ctype.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/param.h>

#if defined(CONFIG_SOC_POSIX)
#include <fcntl.h>
#include <unistd.h>
#endif

#include <zephyr/logging/log.h>

#include "console.h"
#include "demod.h"
#include "globals.h"
#include "board.h"
#include "hdlc.h"
#include "leds.h"

#include "biquad.h"
#include "peak.h"

LOG_MODULE_REGISTER(demod, LOG_LEVEL_INF);

#define sampleRate 9600
#define baudRate 1200
#define nPerBin (sampleRate/baudRate)
#define RAWBUF 512
#define pi 3.14159265
#define SINEBITS 12

#if defined(CONFIG_SOC_POSIX)
int bq = -1;
#endif

/*
  Data types used by the Goertzel demodulator.
*/

/*
  Coefficient variables use a 32-bit signed integer; the
  high order bit is the sign, next bit is the integer part,
  and the remaining 30 bits are the fractional part.
*/

typedef int32_t coefficient;

/*
  I/Q demodulator, using multiplication of samples using a carrier.
  Uses two essentially-identical multiply-and-accumulate banks
  in quadrature.
*/

typedef struct phasebank {
  int windowSize; /* number of sample values in product array */
  int step; /* counter, 0 mod windowSize */
  int phaseIndex; /* phase-step index in table */
  int phaseStepIndex; /* phase-step increment in table */
  int32_t productsFP[nPerBin]; /* fixed-point product array */
  int32_t runningSumFP; /* optimized summation of fixed-point products */
} phasebank;

typedef struct iqdemod {
  float peak;
  phasebank i;
  phasebank q;
} iqdemod;

#define SINETABLESIZE 256

static int tone1 = 1200;
static int tone2 = 2200;

/*
  Data used by the simple zero-crossing-counting discriminator
*/

static const uint8_t crossing_count[256] = {
	0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4,
	2, 3, 3, 4, 3, 4, 4, 5, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 1, 2, 2, 3, 2, 3, 3, 4,
	2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6,
	4, 5, 5, 6, 5, 6, 6, 7, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5,
	3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6,
	4, 5, 5, 6, 5, 6, 6, 7, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
	4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8,
};

iqdemod lowToneIQ, highToneIQ;
int bitNumber;
int subPhase;
int outputBit, previousBit;
int i;
bool prior_positive;
uint8_t crossings;

/*
  Biquad filters and peak detectors for the filter-and-peak
  detector.  This all assumes 9600 samples/second.

  There are three complete sets - one for the demodulator tuned for
  discriminator output (no deemphasis), one for the demodulator tuned
  for speaker audio (deemphasis has been applied, and must be reversed
  by the initial bandpass filter), and one tuned for over-emphasized
  signals (preemphasized by the sender, and received without
  deemphasis).
*/

static biquad_f initial_bandpass_flat;
static biquad_f lowtone_bandpass_flat;
static biquad_f hightone_bandpass_flat;
static biquad_f discriminator_lowpass_flat;

static biquad_f initial_bandpass_highboost;
static biquad_f lowtone_bandpass_highboost;
static biquad_f hightone_bandpass_highboost;
static biquad_f discriminator_lowpass_highboost;

static biquad_f initial_bandpass_highcut;
static biquad_f lowtone_bandpass_highcut;
static biquad_f hightone_bandpass_highcut;
static biquad_f discriminator_lowpass_highcut;

struct demodulator_state G3RUH_state;

/*
  The incoming sample must in the range -32768 to 32767.
*/

void InitIQ(iqdemod *iq, int32_t tone, int nSamples) {
  memset(iq, 0, sizeof(*iq));
  iq->i.windowSize = iq->q.windowSize = nSamples;
  iq->i.step = iq->q.step = 0;
  iq->i.phaseIndex = 0;
  iq->q.phaseIndex = SINETABLESIZE / 4;  /* pi/2 radians */
  iq->i.phaseStepIndex = iq->q.phaseStepIndex =
    (tone * SINETABLESIZE) / sampleRate;
  iq->i.runningSumFP = iq->q.runningSumFP = 0;
}

void IteratePhasebankFP (phasebank *pb, int32_t x) {
  int32_t oldProduct, newProduct;
  newProduct = x * sinetable[pb->phaseIndex];
  pb->phaseIndex = (pb->phaseIndex + pb->phaseStepIndex) % SINETABLESIZE;
  oldProduct = pb->productsFP[pb->step];
  pb->productsFP[pb->step] = newProduct;
  pb->step = (pb->step + 1) % pb->windowSize;
  pb->runningSumFP = pb->runningSumFP + newProduct - oldProduct;
}

void IterateIQFP (iqdemod *iq, int32_t x) {
  IteratePhasebankFP(&iq->i, x);
  IteratePhasebankFP(&iq->q, x);
}

/*
  Ideally the following would use 32*32=64 multiply
*/

int64_t CalculateIQFP (iqdemod *iq) {
  /*
  int64_t iEnergy = iq->i.runningSumFP;
  int64_t qEnergy = iq->q.runningSumFP;
  return (iEnergy * iEnergy + qEnergy * qEnergy);
  */
  int32_t iEnergy = iq->i.runningSumFP;
  int32_t qEnergy = iq->q.runningSumFP;
  return ((int64_t) iEnergy * iEnergy) + ((int64_t) qEnergy * qEnergy);
}

void InitDemodulator(void) {
  InitHDLC(&hdlc_context_flat);
  InitHDLC(&hdlc_context_highboost);
  InitHDLC(&hdlc_context_highcut);
  InitIQ(&lowToneIQ, tone1, nPerBin);
  InitIQ(&highToneIQ, tone2, nPerBin);
  // For discriminator input
  compute_bandpass_biquad_f(&initial_bandpass_flat, 9600.0, 1700.0, 0.7);
  compute_bandpass_biquad_f(&lowtone_bandpass_flat, 9600.0, 1200.0, 1.7);
  compute_bandpass_biquad_f(&hightone_bandpass_flat, 9600.0, 2200.0, 1.7);
  compute_lowpass_biquad_f(&discriminator_lowpass_flat, 9600.0, 1700.0, 0.7);
  reset_biquad_f(&initial_bandpass_flat);
  reset_biquad_f(&lowtone_bandpass_flat);
  reset_biquad_f(&hightone_bandpass_flat);
  reset_biquad_f(&discriminator_lowpass_flat);
  // For de-emphasized input
  compute_bandpass_biquad_f(&initial_bandpass_highboost, 9600.0, 3900.0, 0.6);
  compute_bandpass_biquad_f(&lowtone_bandpass_highboost, 9600.0, 1200.0, 1.7);
  compute_bandpass_biquad_f(&hightone_bandpass_highboost, 9600.0, 2200.0, 1.7);
  compute_lowpass_biquad_f(&discriminator_lowpass_highboost, 9600.0, 1700.0, 0.7);
  reset_biquad_f(&initial_bandpass_highboost);
  reset_biquad_f(&lowtone_bandpass_highboost);
  reset_biquad_f(&hightone_bandpass_highboost);
  reset_biquad_f(&discriminator_lowpass_highboost);
  // For over-emphasized signals
  compute_bandpass_biquad_f(&initial_bandpass_highcut, 9600.0, 1000.0, 0.6);
  compute_bandpass_biquad_f(&lowtone_bandpass_highcut, 9600.0, 1200.0, 1.7);
  compute_bandpass_biquad_f(&hightone_bandpass_highcut, 9600.0, 2200.0, 1.7);
  compute_lowpass_biquad_f(&discriminator_lowpass_highcut, 9600.0, 1700.0, 0.7);
  reset_biquad_f(&initial_bandpass_highcut);
  reset_biquad_f(&lowtone_bandpass_highcut);
  reset_biquad_f(&hightone_bandpass_highcut);
  reset_biquad_f(&discriminator_lowpass_highcut);
#if defined(CONFIG_SOC_POSIX)
  char *path = getenv("BQ");
  if (path) {
    bq = open(path, O_WRONLY | O_CREAT, 0644);
    if (bq >= 0) {
      LOG_INF("Writing %s\n", path);
    } else {
      LOG_ERR("Could not open %s for write\n", path);
    }
  }
#endif
}

void ReInitDemodulator(void) {
  InitHDLC(&hdlc_context_flat);
  InitHDLC(&hdlc_context_highboost);
  InitHDLC(&hdlc_context_highcut);
  board_configure_demodulator(&G3RUH_state);
}

static bool force_log = false;

bool TIME_CRITICAL(G3RUH_Demodulate)(struct demodulator_state *state, input_sample *samples,
				     uint32_t sample_count, struct hdlc_rcv *hdlc_context) {
  demod_fraction time_limit = INT32_TO_SCALED(sample_count);
  // Assumption - all of the demod_fraction values in the state are, at this point,
  // relative to zero-being-the-beginning-of-this-buffer - they have already
  // been adjusted (we do that at the end of this routine).
  static const demod_fraction one_sample_time = INT32_TO_SCALED(1);
  bool log_this = false;
  if (force_log) {
    log_this = true;
    force_log = false;
  }
  if (log_this) {
    hexdump(samples, "Start of sample buffer", 256);
  }
  int loops = 250;
  if (state->next_look_time < 0 ||
      state->next_sample_time < 0 ||
      state->previous_look_time >= 0) {
    LOG_ERR("Time botch?");
    LOG_ERR(" next look %d next_sample %d previous_look %d",
	    state->next_look_time,
	    state->next_sample_time,
	    state->previous_look_time);
  }
  if (state->mean < 0 && state->mean > INT32_TO_SCALED(1<<11)) {
    LOG_ERR("Mean botch: 0x%x", state->mean);
  }
  while (true) {
    if (--loops == 0) {
      log_this = false;
    }
    if (state->next_look_time <= state->next_sample_time &&
	state->next_look_time < time_limit) {
      // We're going to look for a zero-crossing between these two
      // times.  We first check to see if the "next expected zero
      // crossing" is more than a half-baud in the past - if so,
      // there wasn't one, so move our expectation time forwards by
      // one full baud.
      if (state->next_look_time - state->next_zero_crossing_expected >=
	  state->samples_per_baud / 2) {
	state->next_zero_crossing_expected += state->samples_per_baud;
	state->zero_crossings_missed ++;
	if (log_this) {
	  LOG_INF("ZCEA: %d", state->next_zero_crossing_expected);
	}
      }
      // FIXME - use a board-specific lookup to grab the sample.  The RP2040
      // ADC can throw bad samples (flagged as such) due to metastability,
      // and we may need to look one or more samples on either side to find
      // a valid one.  Write a board wrapper to do this.
      if (state->next_look_time < 0) {
	LOG_ERR("next_look_time < 0");
      }
      input_sample this_sample = samples[SCALED_TO_INT32(state->next_look_time)];
      int32_t bad_sample = this_sample & tnc_state.board_bad_ADC_sample_flag;
      if (this_sample > 0x3FFF) {
	LOG_ERR("Got sample 0x%x", this_sample);
      }
      if (bad_sample) {
	LOG_ERR("Bad ADC sample!");
      }
      if (log_this) {
	LOG_DBG("ZCSA: %d at %d, mean %d", this_sample, state->next_look_time,
		SCALED_TO_INT32(state->mean));
      }
      state->mean = IIR_FILTER(state->mean, INT32_TO_SCALED(this_sample), 256);
      int32_t int_mean = SCALED_TO_INT32(state->mean);
      if (!bad_sample && state->previous_look_value_valid &&
	  ((this_sample > int_mean && state->previous_look_value < int_mean) ||
	   (this_sample < int_mean && state->previous_look_value > int_mean))) {
	// We have a zero crossing, in one direction or the other, between these
	// two time points.  Do some linear interpolation to estimate the point
	// at which it actually crossed zero, without having to do further
	// searches... it should be good enough.
	demod_fraction delta_x = state->next_look_time - state->previous_look_time;
	int32_t delta_y = (int32_t) this_sample - state->previous_look_value;
	int32_t rise_y = int_mean - state->previous_look_value;
	demod_fraction since_previous = delta_x * rise_y / delta_y;
	demod_fraction when = state->previous_look_time + since_previous;
	demod_fraction delta_t = when - state->next_zero_crossing_expected;
	if (log_this) {
	  LOG_INF("ZC! interpolated at %d expected at %d delta %d",
		  when, state->next_zero_crossing_expected, delta_t);
	  LOG_INF("    delta_x %d delta_y %d rise_y %d since_previous %d",
		  delta_x, delta_y, rise_y, since_previous);
	}
	// If we aren't locked, we will periodically auto-lock to the first
	// edge we see,
	char effect;
	if (!state->pll_locked &&
	    state->fast_lock_countdown <= 0) {
	  LOG_DBG("Fast-lock attempt");
	  state->fast_lock_countdown = state->fast_lock_period;
	  state->next_zero_crossing_expected = when + state->samples_per_baud;
	  state->next_sample_time = when + state->samples_per_baud / 2;
	  state->average_jitter = state->average_jitter / 2;
	  state->average_mismatch = state->average_mismatch / 2;
	  state->average_sampling_value = state->average_sampling_value / 2;
	  effect = '!';
	} else {
	  // Drift the PLL, depending on whether the crossing came "early"
	  // or "late".  We adjust both the expectation of the next zero
	  // crossing, and the next sampling time, by the same amount.
	  demod_fraction drift_rate;
	  drift_rate = state->zero_crossing_shift_rate;
	  if (delta_t > 0) {
	    state->next_zero_crossing_expected += drift_rate;
	    state->next_sample_time += drift_rate;
	    effect = '+';
	  } else {
	    state->next_zero_crossing_expected -= drift_rate;
	    state->next_sample_time -= drift_rate;
	    effect = '-';
	  }
	}
	// Divide the time error by the number of zero-crossing periods it
	// covers, which will be one more than the number of crossing we expected
	// to see but did not.
	state->average_mismatch = IIR_FILTER(state->average_mismatch,
					     delta_t / (state->zero_crossings_missed + 1), 256);
	demod_fraction old_jitter = state->average_jitter;
	state->average_jitter = IIR_FILTER(state->average_jitter, abs(delta_t), 256);
	state->zero_crossings_missed = 0;
	if (log_this) {
	  LOG_INF("PLL: adjust %c, next expected at %d", effect, state->next_zero_crossing_expected);
	  LOG_INF("Average jitter: old %d, delta-t %d, new %d", old_jitter, delta_t, state->average_jitter);
	}
	// Count the crossings.
	state->zero_crossings++;
	// Track the jitter.
	int32_t jitter = abs(SCALED_TO_INT32(delta_t));
	if (jitter >= JITTER_BUCKETS) {
	  jitter = JITTER_BUCKETS - 1;
	}
	state->jitter[jitter]++;
      }
      // Update the state, for the next go-around.
      state->previous_look_value = this_sample;
      state->previous_look_value_valid = !bad_sample;
      state->previous_look_time = state->next_look_time;
      state->next_look_time += state->samples_per_zero_crossing_check;
    } else if (state->next_sample_time <= state->next_look_time &&
	       state->next_sample_time < time_limit) {
      // Count down the fast-lock time.
      state->fast_lock_countdown --;
      // Count the samplings, so demodulator time moves forward properly.
      // This is a bit clumsy since it has to account for the fact that all
      // of the other demodulators tick up this counter multiple times per bit.
      demod_samples_processed += SLICES_PER_BIT;
      // Take a sample, adjust the mean, and pass the value along to
      // HDLC.  FIXME - see above.
      input_sample this_sample = samples[SCALED_TO_INT32(state->next_sample_time)];
      if (this_sample > 0x3FFF) {
	LOG_ERR("Got sample 0x%x", this_sample);
      }
      if (this_sample & tnc_state.board_bad_ADC_sample_flag) {
	LOG_ERR("Bad sample!");
	// I'm going to assume that all sample buffers contain at least two
	// samples.  Pick one of the two adjacent samples as a backup.
	if (state->next_sample_time >= one_sample_time) {
	  this_sample = samples[SCALED_TO_INT32(state->next_sample_time) - 1];
	} else {
	  this_sample = samples[SCALED_TO_INT32(state->next_sample_time) + 1];
	}
	if (this_sample & tnc_state.board_bad_ADC_sample_flag) {
	  LOG_ERR("Bad neighbor sample!");
	  // Ugh.  Fall back and punt - use the sample from the most recent
	  // zero-crossing probe.  Unless the waveform is totally botched,
	  // it'll be on the same side of zero as the one we wanted to really
	  // look at.
	  this_sample = state->previous_look_value;
	}
      }
      if (log_this) {
	LOG_INF("SAMPLE: %d at time %d, mean %d, %d crossings",
		this_sample, state->next_sample_time, SCALED_TO_INT32(state->mean),
		state->zero_crossings);
      }
      demod_fraction scaled_sample = INT32_TO_SCALED(this_sample);
      bool high = (scaled_sample > state->mean);
      state->mean = IIR_FILTER(state->mean, scaled_sample, 256);
      // Track the average value of the positive samples, and the variation.
      if (high) {
	state->average_sampling_value = IIR_FILTER(state->average_sampling_value,
						   this_sample, 256);
	state->average_sampling_AM = IIR_FILTER(state->average_sampling_AM,
						abs(state->average_sampling_value -
						    this_sample),
						256);
      }
      // Bits coming out of this demodulator are descrambled, and then go right
      // to the HDLC NRZI layer, as we've done all of the sub-bit-level work in
      // this routine.
      bool out = 1 & (high ^ (state->descrambler >> 11) ^
		      (state->descrambler >> 16));
      state->descrambler = (state->descrambler << 1) | high;
      state->output_bit_count[out] ++;
      ConsumeNRZI(hdlc_context, out);
      // Get set for the next bit-capture.
      state->next_sample_time += state->samples_per_baud;
      // Update the zero-crossing rate and reset the counter
      state->zero_crossing_rate = IIR_FILTER(state->zero_crossing_rate,
					     INT32_TO_SCALED(state->zero_crossings * 100),
					     256);
      if (log_this) {
	LOG_INF("  Zero crossing rate now %d / 256", state->zero_crossing_rate);
      }
      state->zero_crossings = 0;
    } else {
      // Both points of interest are beyond the end of this sample buffer.
      break;
    }
  }
  // Now, adjust all of the times so that they're meaningful the next time around.
  state->previous_look_time -= time_limit;
  state->next_look_time -= time_limit;
  state->next_sample_time -= time_limit;
  state->next_zero_crossing_expected -= time_limit;
  if (state->next_sample_time < 0) {
    LOG_ERR("next_sample_time < 0");
    state->next_sample_time = 0;
  }
  if (state->next_look_time < 0) {
    LOG_ERR("next_look_time < 0");
    state->next_look_time = 0;
  }
  // Adjust the lock state.  We consider ourselves to be unlocked
  // if we're seeing an unreasonable number of zero crossings, or
  // if our average jitter is large.
  state->pll_locked = state->zero_crossing_rate < state->upper_zero_crossing_rate &&
    state->zero_crossing_rate > state->lower_zero_crossing_rate &&
    state->average_jitter < state->zero_crossing_jitter_limit &&
    state->fast_lock_countdown < 0;
  // Let the caller know whether we're locked.
  return state->pll_locked;
}

/* The tweak gain (adjusting the high-tone power to compensate for
 * non-flat frequency response in the signal path) assumes a unity
 * factor of 16.
 */
#define GOERTZEL_TWEAK_GAIN 24

void TIME_CRITICAL(Demodulate)(input_sample *samples, uint32_t sample_count) {
  input_sample nativeSigned;
  int64_t lowPowerFP, highPowerFP;
  int32_t outputBit;
  bool goodPacket = false;
  bool locked = false;
  static demod_fraction mean = INT32_TO_SCALED(0x100); // arbitrary starting point
  input_sample peak = 0;
  LOG_DBG("Demodulate %d samples at %p demod %d", sample_count, samples, tnc_state.demodulator);
#if CONFIG_FPU
  float peak_float = 0.0;
#else
  fp32_t peak_fp = 0;
#endif
  if (tnc_state.hdlc_loopback) {
    return;
  }
  switch (tnc_state.demodulator) {
  case GOERTZEL_DEMODULATOR:
    while (sample_count > 0) {
      nativeSigned = *samples;
      mean = IIR_FILTER(mean, INT32_TO_SCALED(nativeSigned), 256);
      nativeSigned -= SCALED_TO_INT32(mean);
      peak = MAX(peak, abs(nativeSigned));
      samples++;
      sample_count--;
      demod_samples_processed++;
      IterateIQFP(&lowToneIQ, nativeSigned);
      IterateIQFP(&highToneIQ, nativeSigned);
      lowPowerFP = CalculateIQFP(&lowToneIQ);
      highPowerFP = CalculateIQFP(&highToneIQ);
#if defined(GOERTZEL_TWEAK_GAIN)
      outputBit = (highPowerFP * GOERTZEL_TWEAK_GAIN > lowPowerFP * 16);
#else
      outputBit = (highPowerFP > lowPowerFP);
#endif
      goodPacket = ConsumeSubbit(&hdlc_context_flat, outputBit);
    }
    break;
  case ZERO_CROSSING_COUNTER_DEMODULATOR:
    /*
      Discriminate between tones based on the number of
      zero crossings during the last baud interval.
    */
    while (sample_count > 0) {
      bool positive;
      static input_sample hysteresis = 0;
      nativeSigned = *samples;
      mean = IIR_FILTER(mean, INT32_TO_SCALED(nativeSigned), 256);
      nativeSigned -= SCALED_TO_INT32(mean);
      peak = MAX(peak, abs(nativeSigned));
      samples++;
      sample_count--;
      demod_samples_processed++;
      positive = (nativeSigned + hysteresis > 0);
      if (positive) {
	hysteresis = tnc_state.zero_crossing_hysteresis;
      } else {
	hysteresis = -tnc_state.zero_crossing_hysteresis;
      }
      if (positive ^ prior_positive) {
	crossings = crossings << 1;
      } else {
	crossings = (crossings << 1) | 1;
      }
      prior_positive = positive;
      outputBit = (crossing_count[crossings] >= tnc_state.zero_crossing_threshold);
      goodPacket = ConsumeSubbit(&hdlc_context_flat, outputBit);
      locked = HDLCLocked(&hdlc_context_flat);
      set_data_leds(outputBit);
    }
    break;
  case ZERO_CROSSING_TIME_DEMODULATOR:
    /*
      Discriminate between tones based on the time since
      the last zero crossing (with some hysteresis applied
      to try to minimize the effect of noise).
    */
    while (sample_count > 0) {
      bool positive;
      static input_sample hysteresis = 0;
      static int time_since_crossing = 0;
      nativeSigned = *samples;
      mean = IIR_FILTER(mean, INT32_TO_SCALED(nativeSigned), 256);
      nativeSigned -= SCALED_TO_INT32(mean);
      peak = MAX(peak, abs(nativeSigned));
      samples++;
      sample_count--;
      demod_samples_processed++;
      positive = (nativeSigned + hysteresis > 0);
      if (positive) {
	hysteresis = tnc_state.zero_crossing_hysteresis;
      } else {
	hysteresis = -tnc_state.zero_crossing_hysteresis;
      }
      bool crossing = positive ^ prior_positive ^ 1;
      prior_positive = positive;
      if (crossing) {
	time_since_crossing = 0;
      } else {
	++time_since_crossing;
      }
      outputBit = (time_since_crossing < tnc_state.zero_crossing_threshold);
      set_data_leds(outputBit);
      goodPacket = ConsumeSubbit(&hdlc_context_flat, outputBit);
      locked = HDLCLocked(&hdlc_context_flat);
    }
    break;
  case DIRECT_SIGN_DEMODULATOR:
    /*
      This is experimental, never tested.  It would be used when
      receiving a direct-modulated signal (e.g. 9600 bit/second
      GMSK).
    */
    while (sample_count > 0) {
      bool positive;
      nativeSigned = *samples;
      mean = IIR_FILTER(mean, INT32_TO_SCALED(nativeSigned), 256);
      nativeSigned -= SCALED_TO_INT32(mean);
      peak = MAX(peak, abs(nativeSigned));
      samples++;
      sample_count--;
      demod_samples_processed++;
      positive = (nativeSigned > 0);
      goodPacket = ConsumeSubbit(&hdlc_context_flat, positive);
      set_data_leds(positive);
      locked = HDLCLocked(&hdlc_context_flat);
    }
  case BIQUAD_FILTER_DEMODULATOR:
    // Currently, on the STM32F411, at 98 MHz, using single-precision
    // hardware floating point, each biquad demodulator
    // consumes less than 10% of the CPU.  On the RP2040 Pico, at
    // 125 MHz, using the fixed-point version costs about 20% of the
    // CPU per demodulator (software floating point is too slow to use
    // on this platform).
    LOG_DBG("Start biquad demod");
    while (sample_count > 0) {
      nativeSigned = *samples;
      mean = IIR_FILTER(mean, INT32_TO_SCALED(nativeSigned), 256);
      nativeSigned -= SCALED_TO_INT32(mean);
      samples++;
      sample_count--;
      demod_samples_processed++;
      // Discriminate and decode, using a flat frequency response.  Good
      // for direct-modulator / direct-demodulator signal path (no use of
      // preemphasis), and for microphone-input / speaker-output path
      // (preemphasis during transmission, deemphasis during reception).
      // The output of this discriminator's initial bandpass filter is
      // used for the audio-level-peak detection... this should help
      // reject out-of-band noise from a radio's audio amp circuit.
#if CONFIG_FPU
      float nativeFloat, bandpassed, lowTone, highTone;
      float discrim, lpf;
      nativeFloat = nativeSigned;
      bandpassed = iterate_biquad_f(&initial_bandpass_flat, nativeFloat);
      peak_float = MAX(peak_float, bandpassed);
      lowTone = iterate_biquad_f(&lowtone_bandpass_flat, bandpassed);
      highTone = iterate_biquad_f(&hightone_bandpass_flat, bandpassed);
      discrim = fabsf(highTone) - fabsf(lowTone);;
      lpf = iterate_biquad_f(&discriminator_lowpass_flat, discrim);
      bool bit = lpf > 0;
#else
      fp32_t native_fp = int32_to_fp32(nativeSigned);
      fp32_t bandpassed_fp, lowTone_fp, highTone_fp, discrim_fp, lpf_fp;
      bandpassed_fp = iterate_biquad_fp(&initial_bandpass_flat, native_fp);
      peak_fp = MAX(peak_fp, bandpassed_fp);
      lowTone_fp = iterate_biquad_fp(&lowtone_bandpass_flat, bandpassed_fp);
      highTone_fp = iterate_biquad_fp(&hightone_bandpass_flat, bandpassed_fp);
      discrim_fp = abs(highTone_fp) - abs(lowTone_fp);
      lpf_fp = iterate_biquad_fp(&discriminator_lowpass_flat, discrim_fp);
      bool bit = lpf_fp > 0;
#endif
      bool goodPacket_flat = ConsumeSubbit(&hdlc_context_flat, bit);
       // We can only report one of the three choices...
      set_data_leds(bit);
      // Discriminate and decode, using a tweaked bandpass filter which
      // applies a high-boost.  This works well for signals which were
      // transmitted without preemphasis (e.g. via direct modulator input)
      // and received with deemphasis (e.g. via the speaker path).
#if CONFIG_FPU
      bandpassed = iterate_biquad_f(&initial_bandpass_highboost, nativeFloat);
      lowTone = iterate_biquad_f(&lowtone_bandpass_highboost, bandpassed);
      highTone = iterate_biquad_f(&hightone_bandpass_highboost, bandpassed);
      discrim = highTone*highTone - lowTone*lowTone;
      lpf = iterate_biquad_f(&discriminator_lowpass_highboost, discrim);
      bit = lpf > 0;
#else
      bandpassed_fp = iterate_biquad_fp(&initial_bandpass_highboost, native_fp);
      lowTone_fp = iterate_biquad_fp(&lowtone_bandpass_highboost, bandpassed_fp);
      highTone_fp = iterate_biquad_fp(&hightone_bandpass_highboost, bandpassed_fp);
      discrim_fp = abs(highTone_fp) - abs(lowTone_fp);
      lpf_fp = iterate_biquad_fp(&discriminator_lowpass_highboost, discrim_fp);
      bit = lpf_fp > 0;
#endif
      bool goodPacket_highboost = ConsumeSubbit(&hdlc_context_highboost, bit);
      // Discriminate and decode, using a tweaked bandpass filter which
      // applies a high-cut.  This works well for signals which were
      // transmitted with preemphasis (via the microphone path) and
      // received without deemphasis (e.g. discriminator path).
#if CONFIG_FPU
      bandpassed = iterate_biquad_f(&initial_bandpass_highcut, nativeFloat);
      lowTone = iterate_biquad_f(&lowtone_bandpass_highcut, bandpassed);
      highTone = iterate_biquad_f(&hightone_bandpass_highcut, bandpassed);
      discrim = highTone*highTone - lowTone*lowTone;
      lpf = iterate_biquad_f(&discriminator_lowpass_highcut, discrim);
      bit = lpf > 0;
#else
      bandpassed_fp = iterate_biquad_fp(&initial_bandpass_highcut, native_fp);
      lowTone_fp = iterate_biquad_fp(&lowtone_bandpass_highcut, bandpassed_fp);
      highTone_fp = iterate_biquad_fp(&hightone_bandpass_highcut, bandpassed_fp);
      discrim_fp = abs(highTone_fp) - abs(lowTone_fp);
      lpf_fp = iterate_biquad_fp(&discriminator_lowpass_highcut, discrim_fp);
      bit = lpf_fp > 0;
#endif
      bool goodPacket_highcut = ConsumeSubbit(&hdlc_context_highcut, bit);
      goodPacket |= goodPacket_flat | goodPacket_highboost | goodPacket_highcut;
      locked = HDLCLocked(&hdlc_context_flat) | HDLCLocked(&hdlc_context_highboost) |
	HDLCLocked(&hdlc_context_highcut);
    }
#if CONFIG_FPU
    peak = peak_float;
#else
    peak = fp32_to_int32(peak_fp);
#endif
    break;
  case G3RUH_DEMODULATOR:
    locked = G3RUH_Demodulate(&G3RUH_state, samples, sample_count, &hdlc_context_flat);
    break;
  }
  set_hdlc_lock_led(locked);
  tnc_state.hdlc_locked = locked;
  tnc_state.audio_peak = peak;
}

/*
 * Print G3RUH jitter statistics
 */

static void format_fraction (char *buffer, size_t buffer_len, char *format, demod_fraction f) {
  // Convert F from 256'ths to 100'ths
  f = (f * 25 + 32) / 64;
  bool invert = false;
  if (f < 0) {
    f = -f;
    invert = true;
  }
  int32_t integer = f / 100;
  int32_t fraction = f % 100;
  if (invert) {
    integer = -integer;
  }
  char converted[32];
  snprintf(converted, sizeof(converted), "%d.%02d", integer, fraction);
  snprintf(buffer, buffer_len, format, converted);
}

void Demodulator_Statistics(void) {
  console_printline("G3RUH demodulator jitter:");
  char buffer[64];
  for (int i = 0; i < JITTER_BUCKETS; i++) {
    snprintf(buffer, sizeof buffer, "%2d: %d", i, G3RUH_state.jitter[i]);
    console_printline(buffer);
  }
  format_fraction(buffer, sizeof(buffer), "Average zero-crossing   rate: %s%%", G3RUH_state.zero_crossing_rate);
  console_printline(buffer);
  format_fraction(buffer, sizeof(buffer), "Average zero-crossing  error: %s", G3RUH_state.average_mismatch);
  console_printline(buffer);
  format_fraction(buffer, sizeof(buffer), "Average zero-crossing jitter: %s", G3RUH_state.average_jitter);
  console_printline(buffer);
  snprintf(buffer, sizeof(buffer), "Average zero-crossing jitter (raw): %d",  G3RUH_state.average_jitter);
  console_printline(buffer);
  format_fraction(buffer, sizeof(buffer), "Average sample amplitude: %s", G3RUH_state.average_sampling_value);
  console_printline(buffer);
  int32_t asv = G3RUH_state.average_sampling_value ? G3RUH_state.average_sampling_value : 1;
  format_fraction(buffer, sizeof(buffer), "Average sample variation: %s%%", (100 * G3RUH_state.average_sampling_AM + asv / 2) /
									     asv);
  console_printline(buffer);
  snprintf(buffer, sizeof(buffer), "Post-descrambler bits: %d %d",
          G3RUH_state.output_bit_count[0],
          G3RUH_state.output_bit_count[1]);
  console_printline(buffer);
}

void G3RUH_Bit_Counts(void) {
  char buffer[256];
  char rate[32];
  char error[32];
  char jitter[32];
  format_fraction(rate, sizeof(rate), "ZC: %s%%", G3RUH_state.zero_crossing_rate);
  format_fraction(error, sizeof(error), "err: %s", G3RUH_state.average_mismatch);
  format_fraction(jitter, sizeof(jitter), "jitter: %s", G3RUH_state.average_jitter);
  int32_t asv = G3RUH_state.average_sampling_value ? G3RUH_state.average_sampling_value : 1;
  snprintf(buffer, sizeof(buffer), "Post-descrambler bits: %d %d; %s %s %s A: %d AV: %d%%",
	   G3RUH_state.output_bit_count[0],
	   G3RUH_state.output_bit_count[1], rate, error, jitter,
	   G3RUH_state.average_sampling_value,
	   (100 * G3RUH_state.average_sampling_AM + asv / 2) / asv);
  console_printline(buffer);
}
