/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stdio.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/ring_buffer.h>

#include <zephyr/usb/usb_device.h>
#include <zephyr/logging/log.h>

#include "board.h"
#include "demod.h"
#include "globals.h"
#include "hdlc.h"
#include "kiss.h"
#include "leds.h"
#include "packetutil.h"

LOG_MODULE_REGISTER(main, LOG_LEVEL_INF);

int main(void)
{
  init_leds();
  tnc_state = default_tnc_state;
  board_init_default_tnc_state();
  tnc_state.lamp_test_time = 60;
  if (!encode_callsign(&nocall, "NOCALL")) {
    LOG_ERR("Ooops, can't set callsign");
  }
  if (!encode_callsign(&wide1, "WIDE1")) {
    LOG_ERR("Ooops, can't set callsign");
  }
  if (!encode_callsign(&wide2, "WIDE2")) {
    LOG_ERR("Ooops, can't set callsign");
  }
  tnc_state.mycall = nocall;
  tnc_state.echoback_call = nocall;
  if (!encode_callsign(&tnc_state.unproto_call, "CQ")) {
    LOG_ERR("Ooops, can't set callsign");
  }
  sharebuffer_init(&console_sharebuffer);
  damask_init();
  InitDemodulator();
  HDLC_Reset_Encoder();
  board_init();
  board_load_saved_parameters();
  k_thread_priority_set(k_current_get(), PACKET_THREAD_PRIORITY);
  /* Run the packet management code (including the packet-to-HDLC
     encoder) in the main (existing) thread, at a moderate
     preemptable priority - above heartbeat but below all of the
     time-sensitive stuff.
   */
  if (tnc_state.kiss_auto) {
    tnc_state.kiss_mode = true;
    hello_kiss();
  }
  common_enable_rx();
  LOG_INF("Startup completed");
  if (tnc_state.kiss_mode) {
    LOG_INF("KISS AUTO mode, starting KISS");
  }
  while (1) {
    k_msleep(10); // Might want to do this differently, with a
    // semaphore wait with timeout?
    if (tnc_state.kiss_mode) {
      kiss_push_to_host();
      kiss_read_from_host();
      damask_run();
    }
    board_push_to_radio();
  }
  return 0;
}
