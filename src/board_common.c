/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/ring_buffer.h>
#include <zephyr/sys/crc.h>

#include <zephyr/logging/log.h>

#include "board.h"
#include "connection.h"
#include "demod.h"
#include "globals.h"
#include "hdlc.h"
#include "kiss.h"
#include "leds.h"
#include "layer2.h"
#include "packetutil.h"
#include "xoshiro128.h"

LOG_MODULE_REGISTER(board_common, LOG_LEVEL_INF);

uint64_t samples_processed;
uint32_t overruns;

int goodPacketTime;

uint32_t cycles_per_sine_step;
uint32_t sine_offset;

uint32_t cycles_before;

input_sample *sample_set_base = (input_sample *) &sample_sets;
input_sample *sample_set_0 = sample_sets.set_0;
input_sample *sample_set_1 = sample_sets.set_1;

// 14-bit fixed-point sinewave.  Used by the fixed-point demodulator
// logic, and by the modulator in the Posix board.

const int32_t sinetable[] =
  {0x00000000, 0x00000064, 0x000000c8, 0x0000012d, 0x00000191, 0x000001f5,
   0x00000259, 0x000002bc, 0x0000031f, 0x00000381, 0x000003e3, 0x00000444, 0x000004a5,
   0x00000504, 0x00000563, 0x000005c2, 0x0000061f, 0x0000067b, 0x000006d7, 0x00000731,
   0x0000078a, 0x000007e2, 0x00000839, 0x0000088f, 0x000008e3, 0x00000936, 0x00000987,
   0x000009d7, 0x00000a26, 0x00000a73, 0x00000abe, 0x00000b08, 0x00000b50, 0x00000b96,
   0x00000bda, 0x00000c1d, 0x00000c5e, 0x00000c9d, 0x00000cd9, 0x00000d14, 0x00000d4d,
   0x00000d84, 0x00000db9, 0x00000deb, 0x00000e1c, 0x00000e4a, 0x00000e76, 0x00000ea0,
   0x00000ec8, 0x00000eed, 0x00000f10, 0x00000f31, 0x00000f4f, 0x00000f6b, 0x00000f85,
   0x00000f9c, 0x00000fb1, 0x00000fc3, 0x00000fd3, 0x00000fe1, 0x00000fec, 0x00000ff4,
   0x00000ffb, 0x00000ffe, 0x00001000, 0x00000ffe, 0x00000ffb, 0x00000ff4, 0x00000fec,
   0x00000fe1, 0x00000fd3, 0x00000fc3, 0x00000fb1, 0x00000f9c, 0x00000f85, 0x00000f6b,
   0x00000f4f, 0x00000f31, 0x00000f10, 0x00000eed, 0x00000ec8, 0x00000ea0, 0x00000e76,
   0x00000e4a, 0x00000e1c, 0x00000deb, 0x00000db9, 0x00000d84, 0x00000d4d, 0x00000d14,
   0x00000cd9, 0x00000c9d, 0x00000c5e, 0x00000c1d, 0x00000bda, 0x00000b96, 0x00000b50,
   0x00000b08, 0x00000abe, 0x00000a73, 0x00000a26, 0x000009d7, 0x00000987, 0x00000936,
   0x000008e3, 0x0000088f, 0x00000839, 0x000007e2, 0x0000078a, 0x00000731, 0x000006d7,
   0x0000067b, 0x0000061f, 0x000005c2, 0x00000563, 0x00000504, 0x000004a5, 0x00000444,
   0x000003e3, 0x00000381, 0x0000031f, 0x000002bc, 0x00000259, 0x000001f5, 0x00000191,
   0x0000012d, 0x000000c8, 0x00000064, 0x00000000, 0xffffff9c, 0xffffff38, 0xfffffed3,
   0xfffffe6f, 0xfffffe0b, 0xfffffda7, 0xfffffd44, 0xfffffce1, 0xfffffc7f, 0xfffffc1d,
   0xfffffbbc, 0xfffffb5b, 0xfffffafc, 0xfffffa9d, 0xfffffa3e, 0xfffff9e1, 0xfffff985,
   0xfffff929, 0xfffff8cf, 0xfffff876, 0xfffff81e, 0xfffff7c7, 0xfffff771, 0xfffff71d,
   0xfffff6ca, 0xfffff679, 0xfffff629, 0xfffff5da, 0xfffff58d, 0xfffff542, 0xfffff4f8,
   0xfffff4b0, 0xfffff46a, 0xfffff426, 0xfffff3e3, 0xfffff3a2, 0xfffff363, 0xfffff327,
   0xfffff2ec, 0xfffff2b3, 0xfffff27c, 0xfffff247, 0xfffff215, 0xfffff1e4, 0xfffff1b6,
   0xfffff18a, 0xfffff160, 0xfffff138, 0xfffff113, 0xfffff0f0, 0xfffff0cf, 0xfffff0b1,
   0xfffff095, 0xfffff07b, 0xfffff064, 0xfffff04f, 0xfffff03d, 0xfffff02d, 0xfffff01f,
   0xfffff014, 0xfffff00c, 0xfffff005, 0xfffff002, 0xfffff000, 0xfffff002, 0xfffff005,
   0xfffff00c, 0xfffff014, 0xfffff01f, 0xfffff02d, 0xfffff03d, 0xfffff04f, 0xfffff064,
   0xfffff07b, 0xfffff095, 0xfffff0b1, 0xfffff0cf, 0xfffff0f0, 0xfffff113, 0xfffff138,
   0xfffff160, 0xfffff18a, 0xfffff1b6, 0xfffff1e4, 0xfffff215, 0xfffff247, 0xfffff27c,
   0xfffff2b3, 0xfffff2ec, 0xfffff327, 0xfffff363, 0xfffff3a2, 0xfffff3e3, 0xfffff426,
   0xfffff46a, 0xfffff4b0, 0xfffff4f8, 0xfffff542, 0xfffff58d, 0xfffff5da, 0xfffff629,
   0xfffff679, 0xfffff6ca, 0xfffff71d, 0xfffff771, 0xfffff7c7, 0xfffff81e, 0xfffff876,
   0xfffff8cf, 0xfffff929, 0xfffff985, 0xfffff9e1, 0xfffffa3e, 0xfffffa9d, 0xfffffafc,
   0xfffffb5b, 0xfffffbbc, 0xfffffc1d, 0xfffffc7f, 0xfffffce1, 0xfffffd44, 0xfffffda7,
   0xfffffe0b, 0xfffffe6f, 0xfffffed3, 0xffffff38, 0xffffff9c};

#define SINETABLE_COUNT (sizeof(sinetable)/sizeof(sinetable[0]))

const int32_t sinetable_count = SINETABLE_COUNT;

K_MUTEX_DEFINE(work_area_mutex);
uint8_t work_area[CONFIG_WORK_AREA_SIZE];

/* This TNC doesn't detect RF carrier and is designed to run open-
 * squelch so it can't report carrier based on audio energy.  Instead
 * it reports carrier if there's a good enough HDLC clock lock.
 *
 * We'll ask the primary (discriminator-path) HDLC decoder about
 * the lock.
 */
bool board_detecting_carrier(void) {
  return HDLCLocked(&hdlc_context_flat);
}

/* The "set hardware" KISS command is defined, for this system, to carry
 * an even number of bytes of payload.  The first byte in each pair
 * contains the identity of a board-specific state variable to change,
 * and the second contains the new value.
 *
 * On Linux you can access this via "kissparm -p PORTNAME -X 6 -X index -X value ...",
 * setting any number of these local parameters with a single invocation.
 */

#define HARDWARE_PARAM_CORE(index, field) \
  case index: \
    tnc_state.field = kiss_command_payload[1]; \
    LOG_INF(#field " now %d", tnc_state.field);

#define HARDWARE_PARAM(index, field) HARDWARE_PARAM_CORE(index, field); break

#define HARDWARE_PARAM_RANGED_CORE(index, field, min, max)	\
  case index: \
    if (kiss_command_payload[1] >= min && kiss_command_payload[1] <= max) { \
      tnc_state.field = kiss_command_payload[1]; \
      LOG_INF(#field " now %d", tnc_state.field); \
    } else { \
      LOG_ERR(#field " value %d out of range", kiss_command_payload[1]); \
    }

#define HARDWARE_PARAM_RANGED(index, field, min, max) HARDWARE_PARAM_CORE(index, field); break

void board_set_hardware(uint8_t *kiss_command_payload, uint32_t len) {
  while (len >= 2) {
    switch (kiss_command_payload[0]) {
      HARDWARE_PARAM_RANGED(0, minimum_zeros, 2, 255);
      HARDWARE_PARAM_RANGED(1, minimum_aborts, 1, 255);
      HARDWARE_PARAM(2, hdlc_loopback);
      HARDWARE_PARAM(3, lock_threshold);
      HARDWARE_PARAM_RANGED(4, modulator, PWM_MODULATOR, DELTA_SIGMA_MODULATOR);
      HARDWARE_PARAM_RANGED(5, demodulator, GOERTZEL_DEMODULATOR, BIQUAD_FILTER_DEMODULATOR);
      HARDWARE_PARAM(7, adc_loopback);
      HARDWARE_PARAM(8, zero_crossing_threshold);
      HARDWARE_PARAM(9, zero_crossing_hysteresis);
      HARDWARE_PARAM_RANGED(10, damask_mode, DAMASK_OFF, DAMASK_FORCE_DAMA);
      HARDWARE_PARAM(11, damask_short_quiet_time);
      HARDWARE_PARAM(12, damask_long_quiet_time);
      HARDWARE_PARAM_RANGED(13, fx25_encoding, FX25_ENCODING_OFF, FX25_ENCODING_MAX);
      HARDWARE_PARAM_RANGED(14, fx25_connection_mode, FX25_CONNECTION_OFF, FX25_CONNECTION_ON);
      HARDWARE_PARAM(15, digipeating);
      HARDWARE_PARAM_RANGED(16, aprs_digi_mode, APRS_DIGI_MODE_OFF, APRS_DIGI_MODE_WIDE2_2);
      HARDWARE_PARAM_RANGED(17, ekiss_mode, EKISS_OFF, EKISS_ALL);
      HARDWARE_PARAM_RANGED(18, carrier_detect_mode, CARRIER_DETECT_HDLC, CARRIER_DETECT_SQUELCH);
      HARDWARE_PARAM_RANGED(19, pulse_shape, PULSE_SINC, PULSE_RAISED_COSINE_095);

      HARDWARE_PARAM_RANGED_CORE(6, radio_baud_rate, RADIO_BAUD_1200, RADIO_BAUD_9600);
      quiesce_transceiver();
      k_msleep(1000);
      unquiesce_transceiver();
      break;

    default:
      LOG_INF("Unknown hardware parameter %d", kiss_command_payload[0]);
      break;
    }
    kiss_command_payload += 2;
    len -= 2;
  }
}

void hexdump(void *p, char *name, size_t len) {
  LOG_INF("Dumping %s at 0x%p", name, p);
  char line[128];
  int offset = 0;
  uint8_t *byte = (uint8_t *) p;
  while (len) {
    if ((offset % 16) == 0) {
      if (offset > 0) {
	LOG_INF("%s", line);
      }
      snprintf(line, sizeof(line), "%08lX: ", offset + (long) p);
    }
    char hexed[12];
    snprintf(hexed, sizeof(hexed), " %02X", *byte);
    strcat(line, hexed);
    byte++;
    offset += 1;
    len -= 1;
  }
  LOG_INF("%s", line);
}

enum carrier_led board_report_carrier_state(void)
{
  if (tnc_state.hdlc_locked) {
    if (tnc_state.radio_baud_rate == RADIO_BAUD_9600) {
      return CARRIER_HDLC_9600;
    } else {
      return CARRIER_HDLC_1200;
    }
  } else if (tnc_state.carrier_detect_mode == CARRIER_DETECT_AUDIO &&
	     tnc_state.audio_peak > tnc_state.audio_carrier_threshold) {
    return CARRIER_AUDIO;
  } else if (tnc_state.carrier_detect_mode == CARRIER_DETECT_SQUELCH &&
	     tnc_state.squelch_open_detect) {
    return CARRIER_SQUELCH_OPEN;
  } else {
    return CARRIER_OFF;
  }
}

static void common_update_leds(uint32_t ticks) {
  // Run the LED hang-time and periodic-blink logic
  update_leds(ticks);
  // If lamp test is running, don't override
  if (tnc_state.lamp_test_time != 0) {
    return;
  }
  // Update the CONN LED state.  Easier and simpler to do it
  // her, periodically, than trying to update it in lots of places
  // in the connection state machine.
  if (tnc_state.kiss_mode) {
    set_connected_led(LED_KISS_MODE);
  } else {
    switch (console_connection->state) {
    case CONN_DISCONNECTED:
    default:
      set_connected_led(LED_DISCONNECTED);
      break;
    case CONN_CONNECTING:
      set_connected_led(LED_CONNECTING);
      break;
    case CONN_CONNECTED:
      if (console_connection->we_use_fx25 && console_connection->peer_uses_fx25) {
	set_connected_led(LED_CONNECTED_FX25);
      } else {
	set_connected_led(LED_CONNECTED);
      }
      break;
    case CONN_DISCONNECTING:
    case CONN_CLEANUP:
    case CONN_UNPROTO:
      set_connected_led(LED_DISCONNECTING);
      break;
    }
  }
  // Light the STATION LED (traffic is pending) if appropriate.
  if (layer2_has_traffic() || some_connection_has_traffic() ||
      damask_has_traffic() || tx_push.current) {
    set_station_led(true);
  }
  // Update the carrier-detect LED, based on the desired
  // carrier-detect mode and on what we're actually
  // seeing.  HDLC-detect always takes priority.
  set_carrier_led(board_report_carrier_state());
}

// This is the lowest-priority thread in the system.  It runs
// roughly 60 times a second, updating the LEDs as required
// by their period and hang-time, and calls the board-specific
// heartbeat routine to do anything needed there.
//
// The heartbeat logic tries to keep the calls "on time" as
// best as possible.  If it's preempted for long enough to
// delay a heartbeat past its due-date, it'll attempt to
// catch up slowly on subsequent ticks.
void common_heartbeat(void *arg1, void *arg2, void *arg3) {
  static uint32_t ticks = 0;
  uint64_t uptime = k_uptime_get();
  uint64_t next_heartbeat = uptime + 15;
  while (1) {
    int64_t delay = next_heartbeat - k_uptime_get();
    if (delay <= 5) {
      delay = 5;
    }
    k_msleep(delay);
    next_heartbeat += 15;
    ticks++;
    common_update_leds(ticks);
    board_heartbeat(ticks);
  }
}

void load_saved_parameters(struct saved_parameters *sp) {
  switch (sp->version) {
  default:
  case 4:
    tnc_state.pulse_shape = sp->pulse_shape;
    tnc_state.radio_baud_rate = sp->radio_baud_rate;
    tnc_state.ekiss_mode = sp->ekiss_mode;
  case 3:
    tnc_state.aprs_digi_mode = sp->aprs_digi_mode;
    tnc_state.digipeating = sp->digipeating;
    tnc_state.echoback_call = sp->echoback_call;
  case 2:
    tnc_state.fx25_connection_mode = sp->fx25_connection_mode;
    tnc_state.carrier_detect_mode = sp->carrier_detect_mode;
    tnc_state.audio_carrier_threshold = sp->audio_carrier_threshold;
    tnc_state.txtail = sp->txtail;
    tnc_state.operating_mode = sp->operating_mode;
    /* drop through */
  case 1:
    tnc_state.mycall = sp->mycall;
    tnc_state.unproto_call = sp->unproto_call;
    tnc_state.bkondel = sp->bkondel;
    tnc_state.conok = sp->conok;
    tnc_state.conmode_is_transparent = sp->conmode_is_transparent;
    tnc_state.cpactime = sp->cpactime;
    tnc_state.pactime_mode = sp->pactime_mode;
    tnc_state.kiss_auto = sp->kiss_auto;
    tnc_state.sendpac = sp->sendpac;
    tnc_state.canline = sp->canline;
    tnc_state.command = sp->command;
    tnc_state.delete = sp->delete;
    tnc_state.paclen = sp->paclen;
    tnc_state.maxframe = sp->maxframe;
    tnc_state.pactime = sp->pactime;
    tnc_state.t1 = sp->t1;
    tnc_state.t2 = sp->t2;
    tnc_state.t3 = sp->t3;
    tnc_state.carrier_effect_on_t1 = sp->carrier_effect_on_t1;
    tnc_state.p = sp->p;
    tnc_state.slot_time = sp->slot_time;
    tnc_state.modulator = sp->modulator;
    tnc_state.demodulator = sp->demodulator;
    tnc_state.retries = sp->retries;
    tnc_state.initial_syncs = sp->initial_syncs;
    tnc_state.txdelay = sp->txdelay;
    tnc_state.damask_mode = sp->damask_mode;
    tnc_state.damask_short_quiet_time = sp->damask_short_quiet_time;
    tnc_state.damask_long_quiet_time = sp->damask_long_quiet_time;
    tnc_state.fx25_encoding = sp->fx25_encoding;
    break;
  }
}

void prepare_to_save_parameters(struct saved_parameters *sp, int32_t revision) {
  memset(sp, 0, sizeof(*sp));
  sp->version = 4;
  sp->revision = revision;
  sp->magic = SAVED_PARAMETER_MAGIC;
  sp->sizeof_me = sizeof(*sp);
  sp->mycall = tnc_state.mycall;
  sp->unproto_call = tnc_state.unproto_call;
  sp->bkondel = tnc_state.bkondel;
  sp->conok = tnc_state.conok;
  sp->conmode_is_transparent = tnc_state.conmode_is_transparent;
  sp->cpactime = tnc_state.cpactime;
  sp->pactime_mode = tnc_state.pactime_mode;
  sp->kiss_auto = tnc_state.kiss_auto;
  sp->sendpac = tnc_state.sendpac;
  sp->canline = tnc_state.canline;
  sp->delete = tnc_state.delete;
  sp->command = tnc_state.command;
  sp->paclen = tnc_state.paclen;
  sp->maxframe = tnc_state.maxframe;
  sp->pactime = tnc_state.pactime;
  sp->t1 = tnc_state.t1;
  sp->t2 = tnc_state.t2;
  sp->t3 = tnc_state.t3;
  sp->carrier_effect_on_t1 = tnc_state.carrier_effect_on_t1;
  sp->p = tnc_state.p;
  sp->slot_time = tnc_state.slot_time;
  sp->modulator = tnc_state.modulator;
  sp->demodulator = tnc_state.demodulator;
  sp->retries = tnc_state.retries;
  sp->initial_syncs = tnc_state.initial_syncs;
  sp->txdelay = tnc_state.txdelay;
  sp->damask_mode = tnc_state.damask_mode;
  sp->damask_short_quiet_time = tnc_state.damask_short_quiet_time;
  sp->damask_long_quiet_time = tnc_state.damask_long_quiet_time;
  sp->fx25_encoding = tnc_state.fx25_encoding;
  // Version 2 entries start here
  sp->fx25_connection_mode = tnc_state.fx25_connection_mode;
  sp->carrier_detect_mode = tnc_state.carrier_detect_mode;
  sp->audio_carrier_threshold = tnc_state.audio_carrier_threshold;
  sp->txtail = tnc_state.txtail;
  sp->operating_mode = tnc_state.operating_mode;
  // Version 3 entries start here
  sp->echoback_call = tnc_state.echoback_call;
  sp->digipeating = tnc_state.digipeating;
  sp->aprs_digi_mode = tnc_state.aprs_digi_mode;
  // Version 4 entries start here
  sp->ekiss_mode = tnc_state.ekiss_mode;
  sp->pulse_shape = tnc_state.pulse_shape;
  sp->radio_baud_rate = tnc_state.radio_baud_rate;
  sp->ekiss_mode = tnc_state.ekiss_mode;
  // Wrap it up
  sp->checksum = 0xFFFFFFFF;
  sp->checksum = crc32_ieee((uint8_t *) sp, sp->sizeof_me);
}

// This version uses the Farey method.
// Constraints:  f is in [0,1], limit is nonzero.
struct rational rationalize(float f, uint32_t limit) {
  struct rational left = {.numerator = 0, .denominator = 1};
  struct rational right = {.numerator = 1, .denominator = 1};
  struct rational mediant;
  bool exact_match = false;
  // Speed-up for the initial part of the mediant search - move
  // the upper limit down to just above the inverse.
  if (f < 0.5f) {
    right.denominator = ((uint32_t) (1.0f/f)) - 1;
  }
  while (left.denominator <= limit && right.denominator <= limit) {
    mediant.numerator = left.numerator + right.numerator;
    mediant.denominator = left.denominator + right.denominator;
    LOG_DBG("Rationalize:  %d/%d vs %d/%d, mediant %d/%d",
	    left.numerator, left.denominator,
	    right.numerator, right.denominator,
	    mediant.numerator, mediant.denominator);
    if (mediant.denominator > limit) {
      break;
    }
    float median = ((float) mediant.numerator) / mediant.denominator;
    if (median == f) { // unlikely best case
      exact_match = true;
      break;
    }
    if (median < f) { // below desired, move the left side
      left = mediant;
    } else { // above desired, more the right side
      right = mediant;
    }
  }
  if (exact_match) {
    return mediant;
  }
  float left_err = fabs(f - ((float) left.numerator) / left.denominator);
  float right_err = fabs(f - ((float) right.numerator) / right.denominator);
  if (left_err < right_err) {
    return left;
  } else {
    return right;
  }
}
