/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_FX25
#define _H_FX25

#include <stdint.h>
#include <stdbool.h>

enum fx25_encoding {
  FX25_ENCODING_OFF = 0,  // No FX.25 encoding done
  FX25_ENCODING_AUTO = 1, // FX.25 encoding, with automatic selection of roots
  FX25_ENCODING_MIN = 2,  // FX.25 encoding, with the least number of roots (16)
  FX25_ENCODING_MAX = 3   // FX.25 encoding, with the largest number of roots (64)
};

enum fx25_connection_mode {
  FX25_CONNECTION_OFF = 0,           // FX.25 will not be used on this connection
  FX25_CONNECTION_RESPONSIVE = 1,    // Use FX.25 iff the peer sends FX.25 to us
  FX25_CONNECTION_OPPORTUNISTIC = 2, // Try sending FX.25, turn it off if peer doesn't send it in return.
  FX25_CONNECTION_ON = 3             // FX.25 will always be used on this connection
};

struct fx25_code {
  uint64_t correlation_tag;
  uint32_t codeblock_bytes;
  uint32_t data_bytes;
};

extern const struct fx25_code fx25_codes[];

extern const int fx25_code_count;

extern const uint8_t mismatching_bits[256];

void fx25_init(void);

int fx25_select_encoding(size_t data_size, enum fx25_encoding fx25_encoding);

// Encode a message block using a specific Reed-Solomon coding.
// The msglen must be <= the data_bytes (codeblock size minus number
// of roots) for the chosen encoding.  The message area holds the
// message, and the parity bytes are appended to it (and hence this
// buffer area must be large enough for the whole codeblock).
//
// Returns the number of bytes written back to *message (should
// equal the codeblock size), or -1 in case of error (bad
// correlation tag, no memory, etc.).
ssize_t fx25_encode(int correlation_tag_index, uint8_t *message, size_t msglen);

// Decode a Reed-Solomon codeblock, writing the corrected data (with the
// parity bytes removed) to the output buffer.  Returns the number of bytes
// written, or <0 to indicate a decoding/correction error.
ssize_t fx25_decode(int correlation_tag_index, uint8_t *input, uint8_t output[255]);

#endif  // _H_FX25
