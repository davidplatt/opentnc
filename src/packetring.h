/*
 * Copyright (c) 2023, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_PACKETRING
#define _H_PACKETRING

#include <stdint.h>
#include <stdbool.h>

// We want to be able to buffer up either a bunch of
// small packets, or two largest-size (512-byte payload)
// packets with their worst-case headers, plus a few small
// administrative frames.

#define RING_NPACKETS 10
#define RING_NBYTES 1500

enum packet_state
  {
   PACKET_FREE = 0,
   PACKET_CONSTRUCTING,
   PACKET_ABORTED,
   PACKET_COMPLETE,
   PACKET_CONSUMING
  };

enum ring_state
  {
   RING_CONSTRUCTING,
   RING_DISCARDING
  };

typedef void (*packet_callback)(void *);

struct packet {
  enum packet_state state;
  uint32_t head;
  uint32_t count;
  uint32_t cursor;
  uint8_t command_byte;
  packet_callback callback;
  void *callback_data;
};

struct packetring {
  enum ring_state state;
  int32_t head;
  int32_t tail;  /* if head == tail, ring is empty */
  uint32_t buffer_size;
  uint8_t buffer[RING_NBYTES];
  uint32_t num_packets;
  struct packet packets[RING_NPACKETS];
  int32_t oldest_packet;  /* oldest constructed/completed packet */
  int32_t current_packet; /* packet under construction */
};

void packetring_init(struct packetring *ring);

int32_t packetring_space_avail(struct packetring *ring);
bool packetring_congested(struct packetring *ring, int32_t payload_size);

int32_t packetring_new_packet(struct packetring *ring);
void packetring_set_callback(struct packetring *ring, packet_callback callback, void *callback_data);
void packetring_put_byte(struct packetring *ring, uint8_t byte);
void packetring_put_bytes(struct packetring *ring, const uint8_t *bytes,
			  int count);
void packetring_set_command(struct packetring *ring, uint8_t byte);
void packetring_retract_bytes(struct packetring *ring, uint32_t count);
void packetring_retract_packet(struct packetring *ring);
void packetring_abort_packet(struct packetring *ring);

bool packetring_has_one(struct packetring *ring);
int32_t packetring_fetch_packet(struct packetring *ring);
int32_t packetring_refetch_packet(struct packetring *ring);
int32_t packetring_get_byte(struct packetring *ring);
int32_t packetring_get_bytes(struct packetring *ring, uint8_t *bytes, uint32_t limit);
int32_t packetring_get_command(struct packetring *ring);
void packetring_consume_packet(struct packetring *ring);

#endif // ifdef _H_PACKETRING
