/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

/*
  This file contains the code which handles the outbound
  side of Layer 3 text connections.  It does most of the
  management of the Layer 3 timers (T1, T2, and T3),
  transmits and re-transmits I-frames, and pushes RR/RNR/REJ
  frames out to the peer.  It's also responsible for
  forwarding the data from incoming I-frames to the
  ultimate consumer (currently the console, but there
  might be a separate consumer for an onboard BBS if
  one is ever implemented).
*/

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/crc.h>
#include <zephyr/sys/ring_buffer.h>

#include "ax25.h"
#include "board.h"
#include "connection.h"
#include "console.h"
#include "converse.h"
#include "demod.h"
#include "globals.h"
#include "hdlc.h"
#include "kiss.h"
#include "layer2.h"
#include "packetutil.h"
#include "sharebuffer.h"

#include "shell/shell.h"

#define CONNECTION_STACK_SIZE 2000
#define CONNECTION_PRIORITY 3

LOG_MODULE_REGISTER(connection, LOG_LEVEL_INF);

// This callback is invoked when the lower layer finishes consuming
// a packet in the ring.  It will start (or restart) the T1 timer
// at that time, so we don't mistakenly consider the transmission
// time of one or more packets as part of T1.  As it's called from
// a rather time-critical part of the code, and could cause
// a transmitter under-run if it blocks for too long, it'll time
// out if the connection mutex isn't available quickly.

void start_t1_timer(ax25buffer *buffer) {
  if (buffer->start_t1) {
    connection *conn = buffer->conn;
    if (!k_mutex_lock(&conn->mutex, K_MSEC(1))) {
      start_conn_timer_for(&conn->t1, conn->capabilities.t1 / 100);
      LOG_DBG("(re)Started T1 for %d ticks from within callback", conn->capabilities.t1 / 100 );
      k_mutex_unlock(&conn->mutex);
    } else {
      LOG_WRN("start_t1_timer callback timed out on mutex");
    }
  }
}

// Callback functions for the host-console endpoint

static bool console_consumer(connection *conn, packet_parse *p) {
  // If we were in the middle of a connection, and KISS mode was
  // then enabled (bad operator!  No Diet Doctor Pepper!) we can't
  // do anything good with incoming I-frames since we no longer "own"
  // the console.  Trigger disconnection and discard the packet.
  if (tnc_state.kiss_mode) {
    conn->disconnect = true;
    return false;
  }
  // This is a distasteful level violation, as it presumes what the
  // implementation of console_printbytes is.  Ought to be rewritten with
  // a cleaner interface to the console code.
  if (ring_buf_space_get(&host_outputbuffer) < p->payload_length) {
    return false;
  }
  console_printbytes(p->payload, p->payload_length);
  return true;
}

static void console_configure(connection *conn) {
}

// We can't receive, if the console serial port buffers are
// full or close to it.
static bool console_can_receive(connection *conn) {
  return !board_check_output_flow_control();
};

static bool unproto_consumer(connection *conn, packet_parse *p) {
  plaintext_callsign plaintext;
  // The source and destination were swapped around when the packet parser
  // built the reverse path
  decode_callsign(plaintext, &p->path.dest);
  console_printstr(plaintext);
  console_printstr(">");
  decode_callsign(plaintext, &p->path.src);
  console_printstr(plaintext);
  console_printstr(": ");
  console_printbytes(p->payload, p->payload_length);
  return true;
}

// Callback functions for the echo endpoint
static bool echo_consumer(connection *conn, packet_parse *p) {
  // If we've already queued up as many echoback frames than we can send in
  // a single transmission, drop any further ones
  if (conn->pending_packets >= conn->capabilities.maxframe) {
    LOG_ERR("Received too many echo frames, dropping");
    return false;
  }
  ax25buffer *b = ax25buffer_alloc(AX25_PAYLOAD, p->payload_length);
  if (!b) {
    LOG_ERR("Cannot alloc ax25buffer of %d bytes",  p->payload_length);
    return false;
  }
  ax25buffer_put_bytes(b, p->payload, p->payload_length);
  LOG_DBG("Echoing packet");
  bool sent = append_l3_packet(conn, b, 0, 0);
  if (!sent) {
    LOG_ERR("Echo packet dropped by L3");
    ax25buffer_free(b);
    return false;
  }
  return true;
}

static void echo_configure(connection *conn) {
}

// We can't receive more data to echo, if we have as many (or more)
// unacknowledged echo packets in the queue as we can send in a single
// burst.
static bool echo_can_receive(connection *conn) {
  return conn->pending_packets < conn->capabilities.maxframe;
};

// Callback functions for the digipeater endpoint
static bool digipeater_filter(connection *conn, packet_parse *p) {
  LOG_DBG("Performing digipeater filter");
  if (!tnc_state.digipeating) {
    LOG_DBG(" Digipeating disabled");
    return false;
  }
  if (p->path.digipeaters == 0) {
    LOG_DBG(" No digipeaters in path");
    return false;
  }
  for (int i = 0; i < p->path.digipeaters; i++) {
    plaintext_callsign pt;
    decode_callsign(pt, &p->path.digi[i]);
    if (!same_callsign(&p->path.digi[i], &tnc_state.mycall)) {
      LOG_DBG(" Digipeater %s is not me", pt);
      continue;
    }
    if (p->path.digi[i].ssid & SSID_BYTE_C_H_BIT) {
      LOG_DBG(" Already repeated through %s", pt);
      continue;
    }
    LOG_DBG(" Can digipeat this as %s", pt);
    p->filter_data = i; // Avoid the need to re-parse the digipeater list
    return true;
  }
  return false;
}

static bool digipeater_consumer(connection *conn, packet_parse *p) {
  // Need some way to keep digipeating on a very busy link from chewing
  // up too many buffers
  //  if (conn->pending_packets >= conn->capabilities.maxframe) {  // change this
  //    LOG_ERR("Too too many digipeater frames, dropping");
  //    return false;
  //  }
  LOG_DBG("Performing digipeating");
  if (p->path.digipeaters == 0) {
    LOG_ERR(" No digipeaters in path, this should not happen");
    return false;
  }
  if (p->filter_data >= p->path.digipeaters) {
    LOG_ERR(" My digipeater index is too high, this should not happen");
    return false;
  }
  plaintext_callsign pt;
  decode_callsign(pt, &p->path.digi[p->filter_data]);
  LOG_DBG(" Can digipeat this as %s", pt);
  p->path.digi[p->filter_data].ssid |= SSID_BYTE_C_H_BIT;
  ax25buffer *buffer = ax25buffer_alloc(AX25_FRAME,
					p->payload_length + MAX_PACKET_OVERHEAD);
  if (!buffer) {
    LOG_ERR("Can't alloc ax25buffer for %d bytes, dropping digipeat frame",
	    p->payload_length + MAX_PACKET_OVERHEAD);
    return false;
  }
  LOG_INF("Digipeat packet, payload length %d", p->payload_length);
  prepare_packet_header(buffer, &p->path, p->frame_role,
			p->two_byte_control,
			p->control, p->pid);
  if (p->payload && p->payload_length > 0) {
    ax25buffer_put_bytes(buffer, p->payload, p->payload_length);
  }
  buffer->fx25 = p->fx25; // Propagate FX.25
  layer2_transmit(buffer);
  tnc_state.stats.frames_digipeated++;
  return true;
}

static void digipeater_configure(connection *conn) {
}

// We can't receive more data to digipeater, if we have as many (or more)
// unacknowledged digipeater packets in the queue as we can send in a single
// burst.
static bool digipeater_can_receive(connection *conn) {
  return conn->pending_packets < conn->capabilities.maxframe;
};

struct {
  uint32_t crc;
  uint64_t time;
} remembered_ui_payload[APRS_CRC_PACKET_COUNT];

// Callback functions for the digipeater endpoint
static bool aprsdigi_filter(connection *conn, packet_parse *p) {
  LOG_DBG("Performing APRS digipeater filter");
  if (tnc_state.aprs_digi_mode == APRS_DIGI_MODE_OFF) {
    LOG_DBG(" APRS digipeating disabled");
    return false;
  }
  if (p->path.digipeaters == 0) {
    LOG_DBG(" No digipeaters in path");
    return false;
  }
  if (p->path.digipeaters >= MAX_DIGIS) {
    LOG_DBG(" Too many digipeaters already in path");
    return false;
  }
  int mmmmm = ((p->control & 0xE0) >> 3) |
    ((p->control & 0x0C) >> 2);
  if (mmmmm != U_FRAME_UI) {
    LOG_DBG(" Not a UI frame");
    return false;
  }
  if (!p->payload) {
    LOG_DBG(" No payload?!?");
    return false;
  }
  LOG_DBG(" Starting exam");
  int maximum_ssid = 0;
  for (int i = 0; i < p->path.digipeaters; i++) {
    plaintext_callsign pt;
    decode_callsign(pt, &p->path.digi[i]);
    LOG_DBG(" Check for %s", pt);
    if (p->path.digi[i].ssid & SSID_BYTE_C_H_BIT) {
      LOG_DBG(" Already repeated through %s", pt);
      continue;
    }
    // We only want to match on the first not-yet-exhausted WIDE1 (or
    // WIDE2) for our selected mode.  If we are configured for WIDE2,
    // we will match either WIDE1- or WIDE2-.
    bool match = false;
    switch (tnc_state.aprs_digi_mode) {
    case APRS_DIGI_MODE_WIDE2_2:
    case APRS_DIGI_MODE_WIDE2_1:
      if (same_base_callsign(&p->path.digi[i], &wide2)) {
	maximum_ssid = (tnc_state.aprs_digi_mode == APRS_DIGI_MODE_WIDE2_1) ? 1 : 2;
	match = true;
	break;
      }
      // fall through...
    case APRS_DIGI_MODE_WIDE1_1:
    default:
      if (same_base_callsign(&p->path.digi[i], &wide1)) {
	maximum_ssid = 1;
	match = true;
	break;
      }
    }
    if (!match) {
      LOG_DBG(" Digipeat path wants %s (not us)", pt);
      return false;
    }
    // Check to make sure that the ssid is in the range
    // we allow.  We never allow malformed/abusive things
    // like WIDE2-5, and won't allow WIDE2-2 if we're in
    // WIDE2-1 mode.
    int ssid = (p->path.digi[i].ssid >> 1) & 0xF;
    if (ssid > maximum_ssid) {
      LOG_INF("SSID in %s exceeds our limit", pt);
      return false;
    }
    if (ssid == 0) {
      LOG_INF("Dropping obsolete zero-SSID %s", pt);
      return false;
    }
    // Check to see if we've recently digipeated a packet with this
    // same payload.
    uint32_t payload_crc = crc32_ieee(p->payload, p->payload_length);
    uint64_t time_now = k_uptime_ticks();
    for (int n = 0; n < APRS_CRC_PACKET_COUNT; n++) {
      // Stop searching when we hit a CRC older than our window.
      if (time_now > remembered_ui_payload[n].time +
	  CONFIG_SYS_CLOCK_TICKS_PER_SEC * APRS_CRC_PACKET_TIMEOUT) {
	break;
      }
      if (remembered_ui_payload[n].crc == payload_crc) {
	LOG_INF("Duplicate CRC within %d seconds, dropping", APRS_CRC_PACKET_TIMEOUT);
	return false;
      }
    }
    // At this point we commit to digipeating it, so we'll add its payload
    // CRC to the table.
    memmove(&remembered_ui_payload[1], &remembered_ui_payload[0],
	    (APRS_CRC_PACKET_COUNT - 1) * sizeof(remembered_ui_payload[0]));
    remembered_ui_payload[0].crc = payload_crc;
    remembered_ui_payload[0].time = time_now;
    LOG_INF(" Can digipeat a %s, payload 0x%08X", pt, payload_crc);
    p->filter_data = i; // Avoid the need to re-parse the digipeater list
    return true;
  }
  return false;
}

static bool aprsdigi_consumer(connection *conn, packet_parse *p) {
  // Need some way to keep digipeating on a very busy link from chewing
  // up too many buffers
  //  if (conn->pending_packets >= conn->capabilities.maxframe) {  // change this
  //    LOG_ERR("Too too many digipeater frames, dropping");
  //    return false;
  //  }
  LOG_DBG("Performing APDS digipeating");
  if (p->path.digipeaters == 0) {
    LOG_ERR(" No digipeaters in path, this should not happen");
    return false;
  }
  if (p->filter_data >= p->path.digipeaters) {
    LOG_ERR(" My digipeater index is too high, this should not happen");
    return false;
  }
  int digi = p->filter_data;
  plaintext_callsign pt;
  decode_callsign(pt, &p->path.digi[digi]);
  LOG_DBG(" Can digipeat this for %s", pt);
  // Decrement the SSID.  The filter has assured us that it's
  // nonzero and within legal limits.  We could just subtract 2
  // from the .ssid byte if we wanted to be tricky.
  int ssid = (p->path.digi[digi].ssid >> 1) & 0xF;
  p->path.digi[digi].ssid =
    (p->path.digi[digi].ssid & 0xE1) | ((ssid - 1) << 1);
  // If the encoded SSID was 1 and is now 0, set the has-been-repeated bit.
  if (ssid == 1) {
    p->path.digi[digi].ssid |= SSID_BYTE_C_H_BIT;
  }
  // Move this digi, and any following ones upwards in the
  // list.  The filter has assured us that there's room for
  // one more.
  for (int i = p->path.digipeaters; i > digi; i--) {
    p->path.digi[i] = p->path.digi[i-1];
  }
  p->path.digipeaters ++;
  // We've moved the WIDE up to the next position, so we can
  // replace the working one with our callsign, and set the
  // has-been-repeated bit.
  p->path.digi[digi] = tnc_state.mycall;
  p->path.digi[digi].ssid |= SSID_BYTE_C_H_BIT;
  // OK, the finished packet should be good-to-go.
  ax25buffer *buffer = ax25buffer_alloc(AX25_FRAME,
					p->payload_length + MAX_PACKET_OVERHEAD);
  if (!buffer) {
    LOG_ERR("Can't alloc ax25buffer for %d bytes, dropping digipeat frame",
	    p->payload_length + MAX_PACKET_OVERHEAD);
    return false;
  }
  LOG_DBG("Digipeat packet, payload length %d", p->payload_length);
  prepare_packet_header(buffer, &p->path, p->frame_role,
			p->two_byte_control,
			p->control, p->pid);
  if (p->payload && p->payload_length > 0) {
    ax25buffer_put_bytes(buffer, p->payload, p->payload_length);
  }
  // I believe that changing the following policy would be Bad.
  // Bad as in "huge mobs of enraged villagers attack the mad
  // scientist's castle armed with torches, pitchforks, and
  // overpowered trebuchets hurling large quantities of
  // high-velocity dog yummies."
  buffer->fx25 = false; // Don't worsen the Tragedy of the Commons
  layer2_transmit(buffer);
  tnc_state.stats.aprs_digipeated++;
  return true;
}

static void aprsdigi_configure(connection *conn) {
}

// We can't receive more data to digipeater, if we have as many (or more)
// unacknowledged digipeater packets in the queue as we can send in a single
// burst.
static bool aprsdigi_can_receive(connection *conn) {
  return conn->pending_packets < conn->capabilities.maxframe;
};




// Appends data to an ax25buffer under construction, either from the
// sharebuffer or from another ax25buffer.
static void append_packet_data(connection *conn, ax25buffer *buffer, uint32_t index) {
  if (conn->pending_output[index].buffer) {
    ax25buffer_put_bytes(buffer, ax25buffer_get_data(conn->pending_output[index].buffer),
			 ax25buffer_byte_count(conn->pending_output[index].buffer));
  } else {
    // This is slower than is possible.  Code which peeks
    // directly into the sharebuffer, and handles the wraparound
    // at end-of-buffer, could do this in at most two
    // ax25buffer_put_bytes() calls.  However, given the speed
    // of the link, the increased efficiency probably isn't worth
    // the trouble.
    for (int offset = 0; offset < conn->pending_output[index].length; offset++) {
      uint8_t byte;
      int offset_wrapped = (conn->pending_output[index].sharebuffer_offset + offset) %
	sharebuffer_get_size(&console_sharebuffer);
      sharebuffer_get_data_range(&console_sharebuffer,
				 offset_wrapped, &byte, 1);
      ax25buffer_put_byte(buffer, byte);
    }
  }
}

static void tick_connection_timers(connection *conn) {
  // T1 is our basic "keep the protocol moving" timer, which will
  // trigger retries of one sort of another when it expires.
  // It's hard to strike a static balance with this timer.  Short
  // values are better for point-to-point connections on quiet
  // channels, as one can recover faster from packet loss.  On busy
  // channels (e.g. everybody hitting on a busy BBS) a larger value
  // works better, as it takes longer to get acknowledgements from the
  // BBS and too small a value leads to excessive queries and retries.
  // So, this hacky code will make the timer "run more slowly" when
  // the TNC is detecting activity on the air.  This is somewhat akin
  // to the effect of P-persistence during transmission.
  //
  // Setting the carrier-effect-on-t1 to 4 will make T1 count down
  // at quarter-speed when carrier is detected.
  static uint32_t iterations = 0;
  iterations++;
  if (board_report_carrier_state() == CARRIER_OFF ||
      (tnc_state.carrier_effect_on_t1 == 0 ||
       (iterations % tnc_state.carrier_effect_on_t1 == 0))) {
    tick_conn_timer(&conn->t1);
  }
  tick_conn_timer(&conn->t2);
  tick_conn_timer(&conn->t3);
}

// Scan up through the receive window, checking the deferred-input
// list for missing packets.  Send an SREJ response for each.
// The first one will be FINAL as it serves as an implicit
// RR, the others will not.
static void send_srej_sequence(connection *conn) {
  bool first = true;
  int32_t missing = conn->v_r;
  LOG_INF("SREJ is needed");
  for (int i = 1; i < conn->capabilities.filter_window; i++) {
    int32_t probe = sequence_mod(conn, conn->v_r + i);
    LOG_DBG(" Probe for %d", probe);
    if (NULL != find_deferred_input(conn, probe)) {
      LOG_DBG("  Found it!");
      while (missing >= 0 && missing != probe) {
	LOG_INF("   Sending SREJ for %d now", missing);
	enqueue_packet(&conn->path, conn,
		       FRAME_IS_RESPONSE,
		       conn->capabilities.extended_sequence,
		       true,
		       encode_s_control(missing, first, S_FRAME_SREJ,
					conn->capabilities.extended_sequence),
		       AX25_NO_PID, NULL, 0, fx25_policy(conn));
	missing = sequence_mod(conn, missing + 1);
	first = false;
      }
      missing = -1;
    } else {
      LOG_DBG("  Not found.");
      if (missing == -1) {
	LOG_DBG("    Missing-packet sequence starts here");
	missing = probe;
      }
    }
  }
  conn->srej_sent = true;
  conn->send_rej = false;
  conn->rej_sent = false;
  stop_conn_timer(&conn->t2);
  // Turn off the "we were polled for RR/RNR" since our
  // first SREJ is a final response.
  conn->polled_for_rr_or_rnr = false;
}

static void handle_outbound_state(connection *conn) {
  // Update our windows prior to sending any RR/RNR/REJ.
  conn->n_s = conn->v_s;
  conn->n_r = conn->v_r;
  // What we're going to send, if anything.
  bool send_now = false;
  bool send_final = false;
  uint8_t frame_type = S_FRAME_RR;
  bool start_t1 = false;
  const char *reason = "Normal";
  const char *log_type = "RR";
  enum command_or_response c_r = FRAME_IS_COMMAND;
  do {
    // If the data path to the consumeer (e.g. UART ring buffer) is
    // too full to accept another full-sized frame, we'll want to send
    // RNR.  For the console, this should happen only very rarely, since the host
    // interface is typically much faster than the over-the-air data
    // rate.
    bool congested = !conn->can_receive(conn);
    if (congested) {
      reason = "Receive data path congested";
    }
    // For testing, simulate occasional buffer-full states of varying
    // lengths.
    if (!congested && tnc_state.not_ready_simulation != 0) {
      static int32_t last_packet_count = 0;
      static int32_t not_ready_timer = 0;
      if (conn->received_packets != last_packet_count &&
	  not_ready_timer <= 0 &&
	  (board_get_random32() & 0xFF) < tnc_state.not_ready_simulation) {
	reason = "Simulated buffer-full";
	not_ready_timer = 2 + board_get_random32() % tnc_state.not_ready_simulation;
      }
      not_ready_timer --;
      last_packet_count = conn->received_packets;
      if (not_ready_timer > 0) {
	congested = true;
      }
    }

    // If input is congested due to buffer-space issues (either
    // real or simulated) we should send an RNR if we have not
    // done so already.
    if (congested) {
      conn->send_rnr = true;
      frame_type = S_FRAME_RNR;
      send_now = !conn->rnr_sent;
      conn->rnr_sent = true;
      log_type = "RNR";
    } else {
      if (conn->rnr_sent) {
	// otherwise, send RR or REJ to clear the RNR state.
	conn->send_rnr = false;
	conn->rnr_sent = false;
	send_now = true; // will send RR or REJ, depending...
	reason = "Buffer OK";
	continue;
      }
    }

    // If we need to send one or more SREJ, do so now.
    if (conn->send_srej && !conn->srej_sent) {
      send_srej_sequence(conn);
      continue;
    }

    // If we received a RR/RNR/REJ command with the poll bit set, we must send
    // a response frame with the final bit set, and are to do this "at our
    // earliest opportunity".
    if (conn->polled_for_rr_or_rnr && !conn->send_srej) {
      stop_conn_timer(&conn->t2);
      send_now = true;
      send_final = true;
      c_r = FRAME_IS_RESPONSE;
      conn->polled_for_rr_or_rnr = false;
      reason = "Polled";
      continue;
    }

    // If we lost a packet and aren't RNR, must send a REJ.
    if (!conn->send_rnr && conn->send_rej && !conn->rej_sent) {
      frame_type = S_FRAME_REJ;
      send_now = true;
      conn->rej_sent = true;
      conn->send_rej = false;
      start_t1 = true;
      reason = "Wrong sequence";
      log_type = "REJ";
      continue;
    }

    // If we've received an I-frame with the P bit set ("This is the
    // last one of a burst, please acknowledge") send RR/RNR/REJ now
    // rather than waiting for T2 to expire.  Used to skip the
    // sending if we were about to send some I-frames (which will
    // serve as an implicit acknowledgement) but MJF-1270 and perhaps
    // other TNC-2 clones don't recognize our I-frame as a response
    // to their poll, and re-send the last frame(s) anyhow.
    if (conn->i_frame_poll_received) {
      conn->i_frame_poll_received = false;
      send_now = true;
      send_final = true;
      reason = "I-frame P flag";
      continue;
    }

    // OK, with all of the above, we know whether we need to send something
    // based on the current connection state, what it is, and whether the
    // final bit is to be set.
    if (send_now) {
      continue;
    }

    // Yet another reason to send something - the "We received an
    // inbound I-frame and should acknowledge it" T2 timer has expired.
    if (check_conn_timer(&conn->t2)) {
      stop_conn_timer(&conn->t2);
      send_now = true;
      reason = "T2 ack";
      continue;
    }

    // If T3 expires, we want to fire off a keep-alive.
    if (check_conn_timer(&conn->t3)) {
      stop_conn_timer(&conn->t3);
      send_now = true;
      send_final = true;
      reason = "T3 keepalive";
      conn->retries_used = 0;
      start_t1 = true;
      continue;
    }

  } while (false);
  if (send_now) {
    LOG_INF("%s, send %s %s, N_R %d P/F %d", reason, log_type,
	    frame_roles[c_r], conn->n_r, send_final);
    enqueue_packet(&conn->path, conn,
		   c_r,
		   conn->capabilities.extended_sequence,
		   start_t1,
		   encode_s_control(conn->n_r, send_final, frame_type,
				    conn->capabilities.extended_sequence),
		   AX25_NO_PID, NULL, 0, fx25_policy(conn));
    stop_conn_timer(&conn->t2);
  }
}

static void handle_disconnection(connection *conn) {
  // If we've been asked to disconnect, start doing so.
  if (conn->disconnect) {
    conn->disconnect = false;
    switch (conn->state) {
    case CONN_DISCONNECTED:
      break;
    case CONN_CONNECTING:
    case CONN_NEGOTIATING:
    case CONN_CONNECTED:
      conn->state = CONN_DISCONNECTING;
      start_conn_timer(&conn->t1);
      conn->retries = conn->capabilities.retries;
      enqueue_packet(&conn->path, conn, FRAME_IS_COMMAND,
		     false, false,
		     encode_u_control(U_FRAME_DISC, true),
		     AX25_NO_PID, NULL, 0, fx25_policy(conn));
      break;
    case CONN_DISCONNECTING:
    case CONN_CLEANUP:
    case CONN_UNPROTO:
      break;
    }
  }
  // If we've successfully disconnected, clean up any residue
  // of the connection.
  if (conn->state == CONN_CLEANUP) {
    LOG_INF("Doing post-connection cleanup");
    while (release_oldest_l3_packet(conn)) {};
    reinit_connection(conn);
  }
}

static void handle_t1_expiration(connection *conn) {
  // We may need to do any of a number of things when T1 expires.
  if (check_conn_timer(&conn->t1)) {
    LOG_DBG("T1 timed out");
    stop_conn_timer(&conn->t1);
    conn->retries_used ++;
    // Handle TNCs which ignore SABME - they should FRMR, but some
    // just drop the unrecognized U-frame and ignore it.  We'll
    // use up the first half of our retry limit on SABME and then
    // switch to SABM.
    if (conn->retries_used >= conn->retries / 2 &&
	conn->state == CONN_CONNECTING &&
	conn->attempting_sabme) {
      LOG_INF("No response to SABME, trying SABM instead");
      conn->attempting_sabme = false;
    }
    if (conn->retries_used >= conn->retries) {
      if (conn->state == CONN_NEGOTIATING) {
	LOG_INF("Peer never responded to XID - punting");
	conn->capabilities.supports_srej = false;
	conn->state = CONN_CONNECTED;
      } else if (conn->state == CONN_DISCONNECTING) {
	conn->state = CONN_CLEANUP;
      } else {
	if (conn->endpoint->is_console) {
	  LOG_INF("Retry limit exceeded");
	  console_printline("*** Retries exceeded, disconnecting");
	}
	conn->state = CONN_DISCONNECTING;
      }
    }
    switch (conn->state) {
    case CONN_DISCONNECTED:
      break;
    case CONN_CONNECTING:
      LOG_INF(" Resend %s", conn->attempting_sabme ? "SABME" : "SABM");
      enqueue_packet(&conn->path, conn, FRAME_IS_COMMAND,
		     false, true,
		     encode_u_control(conn->attempting_sabme ? U_FRAME_SABME : U_FRAME_SABM, true),
		     AX25_NO_PID, NULL, 0, fx25_policy(conn));
      break;
    case CONN_NEGOTIATING:
      LOG_INF("No response to XID, resending");
      send_XID_frame(conn, true);
      start_conn_timer(&conn->t1);
      break;
    case CONN_CONNECTED:
      if (conn->send_srej && conn->srej_sent) {
	LOG_INF("Will try SREJ again");
	conn->srej_sent = false;  // output-state routine will retry
	stop_conn_timer(&conn->t2);
	break;
      }
      LOG_INF("Send RR to poll, N_R %d", conn->n_r);
      stop_conn_timer(&conn->t2);
      enqueue_packet(&conn->path, conn,
		     FRAME_IS_COMMAND,
		     conn->capabilities.extended_sequence,
		     true,
		     encode_s_control(conn->n_r, true, S_FRAME_RR,
				      conn->capabilities.extended_sequence),
		     AX25_NO_PID, NULL, 0, fx25_policy(conn));
      break;
    case CONN_DISCONNECTING:
      enqueue_packet(&conn->path, conn, FRAME_IS_COMMAND,
		     false, true,
		     encode_u_control(U_FRAME_DISC, true),
		     AX25_NO_PID, NULL, 0, fx25_policy(conn));
      break;
    case CONN_CLEANUP:
    case CONN_UNPROTO:
      break;
    }
  }
}

// As a side effect, we update the we_use_fx25 flag in
// the connection each time we make a policy decision.
bool fx25_policy(connection *conn) {
  bool policy = false;
  switch (conn->fx25_connection_mode) {
  case FX25_CONNECTION_OFF:
  default:
    break;
  case FX25_CONNECTION_RESPONSIVE:
    policy = conn->peer_uses_fx25;
    break;
  case FX25_CONNECTION_OPPORTUNISTIC:
    policy = conn->peer_uses_fx25 | !conn->peer_uses_non_fx25;
    break;
  case FX25_CONNECTION_ON:
    policy = true;
  }
  conn->we_use_fx25 = policy;
  return policy;
}

static void send_i_frames(connection *conn) {
  // If we're connected, and the peer is not RNR, check to see if
  // we have any I-frames to send.
  if (conn->state == CONN_CONNECTED && conn->peer_ready) {
    // If we've hit our retry limit, give up
    if (conn->retries_used >= conn->retries) {
      console_printline("*** Retry limit exceeded");
      conn->disconnect = true;
      return;
    }
    // During normal operation, we can send up to as many frames
    // as the window will allow.  If we've received a REJ, only send the
    // packet which was rejected (and only once per REJ).
    int to_send = 0;
    int send_limit;
    if (conn->rej_received) {
      send_limit = 1;
    } else {
      send_limit = conn->capabilities.maxframe;
    }
    if (send_limit > conn->pending_packets) {
      send_limit = conn->pending_packets;
    }
    for (int i = 0; i < send_limit; i++) {
      if (!conn->pending_output[i].sent_once) {
	to_send++;
      }
    }
    int sent = 0;
    conn->n_r = conn->v_r;
    conn->n_s = conn->v_s;
    LOG_DBG("Looking for packets to send, %d allowed", to_send);
    for (int i = 0;
	 i < send_limit && sent < to_send &&
	   !ax25buffer_congested(conn->pending_output[i].length);
	 i++) {
      int seq = sequence_mod(conn, conn->n_s + i);
      if (conn->pending_output[i].sent_once) {
	LOG_INF("     I-frame %d N_S %d already sent", i, seq);
      } else {
	bool last = tnc_state.poll_on_last && (sent == to_send - 1);
	LOG_INF("Send I-frame %d N_S %d P %d N_R %d sbo %d len %d", i,
		seq, last, conn->n_r,
		conn->pending_output[i].sharebuffer_offset,
		conn->pending_output[i].length);
	ax25buffer *b = ax25buffer_alloc(AX25_FRAME, conn->pending_output[i].length +
					 MAX_PACKET_OVERHEAD);
	if (!b) {
	  LOG_ERR("Could not allocate ax25buffer");
	  break;
	}
	// AX.25 v2.0 and later say we should mark all frames as either
	// command or response.  Dire Wolf agrees, and flags our I-frames
	// as problematic if we do not do so.
	prepare_packet_header(b, &conn->path, FRAME_IS_COMMAND,
			      conn->capabilities.extended_sequence,
			      encode_i_control(conn->n_r, last,
					       seq,
					       conn->capabilities.extended_sequence),
			      AX25_P_TEXT);
	append_packet_data(conn, b, i);
	ax25buffer_set_callback(b, conn, start_t1_timer, (void *) i);
	b->fx25 = fx25_policy(conn);
	b->start_t1 = true;
	LOG_INF("Marking index %d n_s %d as having been sent", i, sequence_mod(conn, conn->n_s + i));
	conn->pending_output[i].sent_once = true;
	layer2_transmit(b);
	// Sending an I-frame keeps the peer up to date, so we don't need
	// to send an RR unless we're explicitly polled for one.
	stop_conn_timer(&conn->t2);
	sent++;
      }
    }
  }
}

static void send_ui_frames(connection *conn) {
  // In UNPROTO mode, simply fire-and-forget any pending packets
  // as UI frames.
  if (conn->state == CONN_UNPROTO) {
    while (conn->pending_packets > 0 &&
	   !ax25buffer_congested(conn->pending_output[0].length)) {
      LOG_INF("Send U-frame, sbo %d len %d",
	      conn->pending_output[0].sharebuffer_offset,
	      conn->pending_output[0].length);
      ax25buffer *b = ax25buffer_alloc(AX25_FRAME, conn->pending_output[0].length +
				       MAX_PACKET_OVERHEAD);
      if (!b) {
	LOG_ERR("Could not allocate ax25buffer");
	break;
      }
      prepare_packet_header(b, &conn->path, FRAME_IS_NEITHER,
			    false,
			    encode_u_control(U_FRAME_UI, false),
			    AX25_P_TEXT);
      append_packet_data(conn, b, 0);
      b->fx25 = fx25_policy(conn);
      layer2_accept_outgoing(b);
      release_oldest_l3_packet(conn);
    }
  }
}

// Assumes mutex is already held
void release_deferred_input(connection *conn) {
  ax25buffer *buffer, *safe;
  LOG_DBG("Flushing deferred-input queue");
  SYS_DLIST_FOR_EACH_CONTAINER_SAFE(&conn->deferred_input, buffer, safe, node) {
    ax25buffer_free(buffer);
  }
  conn->n_s_needed = -1;
  conn->srej_sent = false;
  conn->send_srej = false;
}

// Assumes mutex is already held.
ax25buffer *find_deferred_input(connection *conn, uint32_t seq) {
  ax25buffer *buffer;
  SYS_DLIST_FOR_EACH_CONTAINER(&conn->deferred_input, buffer, node) {
    if (buffer->opaque_data == seq) {
      return buffer;
    }
  }
  return NULL;
}

// Currently, all of the capabilities we manage are defaulted to
// the same values, as set in the current TNC state.  The size
// of our receive window is set to half of the sequence range
// (to improve stale-packet filtering) or 16 frames (to limit
// memory use during SREJ recovery).

void configure_default_capabilities(connection *conn, bool extended_sequence) {
  conn->capabilities.maxframe = tnc_state.maxframe;
  conn->capabilities.paclen = tnc_state.paclen;
  conn->capabilities.t1 = tnc_state.t1 * 1000; // milliseconds
  conn->capabilities.retries = tnc_state.retries;
  conn->capabilities.supports_srej = false;
  conn->capabilities.extended_sequence = extended_sequence;
  conn->capabilities.sequence_mask = extended_sequence ? 0x7F : 0x07;
  conn->capabilities.filter_window = extended_sequence ? 16 : 4;
}

void reset_connection(connection *conn, bool preserve_fx25_info) {
  // We come here when starting a new connection, or if an
  // irrecoverable frame error of some sort has been detected by us,
  // or by the peer (i.e. FRMR received).  In all of these cases, we
  // are to reset our link state variables to zero.  We will also mark
  // any pending I-frames as not-yet-sent, and retransmit them.
  k_mutex_lock(&conn->mutex, K_FOREVER);
  conn->v_s = conn->v_r = 0;
  release_deferred_input(conn);
  conn->retries_used = 0;
  conn->retries = tnc_state.retries;
  conn->fx25_connection_mode = tnc_state.fx25_connection_mode;
  if (!preserve_fx25_info) {
    conn->we_use_fx25 = false;
    conn->peer_uses_fx25 = false;
    conn->peer_uses_non_fx25 = false;
  }
  stop_conn_timer(&conn->t1);
  stop_conn_timer(&conn->t2);
  stop_conn_timer(&conn->t3);
  init_conn_timer(&conn->t1, tnc_state.t1, 10);
  init_conn_timer(&conn->t2, tnc_state.t2, 1);
  init_conn_timer(&conn->t3, tnc_state.t3, 100);

  relist_l3_packets(conn);
  k_mutex_unlock(&conn->mutex);
}

void reinit_connection(connection *conn) {
  // We come here after we've finished using a connection.
  // Reset its matching parameters to the defaults for its endpoint,
  // and mark it as no longer alive.
  conn->attempting_sabme = false;
  conn->match_mycall = conn->endpoint->match_mycall_default;
  conn->match_endpoint_call = conn->endpoint->match_endpoint_call_default;
  conn->match_peer = conn->endpoint->match_peer_default;
  conn->configure = conn->endpoint->configure_default;
  conn->filter = conn->endpoint->filter_default;
  conn->consumer = conn->endpoint->consumer_default;
  conn->can_receive = conn->endpoint->can_receive_default;
  conn->state = CONN_DISCONNECTED;
  conn->live = false;
  configure_default_capabilities(conn, false);
};

// Need digi support at some point.

bool start_console_connection(connection *conn, encoded_callsign *dest,
			      encoded_callsign *digis, int num_digis) {
  if (conn->state != CONN_DISCONNECTED) {
    console_printline("Already connected - disconnect first");
    return false;
  }
  if (num_digis > MAX_DIGIS) {
    console_printline("Too many digipeaters");
    return false;
  }
  k_mutex_lock(&conn->mutex, K_FOREVER);
  reset_connection(conn, false);
  conn->filter = NULL;
  conn->consumer = console_consumer;
  conn->path.dest = *dest;
  conn->path.src = tnc_state.mycall;
  conn->path.digipeaters = num_digis;
  memcpy(conn->path.digi, digis, num_digis * sizeof(*digis));
  conn->match_mycall = true;
  conn->match_endpoint_call = false;
  conn->match_peer = true;
  conn->pid = AX25_P_TEXT;
  conn->state = CONN_CONNECTING;
  conn->live = true;
  conn->attempting_sabme = conn->capabilities.extended_sequence;
  // Sanity-check capabilities limits
  if (conn->capabilities.extended_sequence) {
    if (conn->capabilities.maxframe > 127) {
      conn->capabilities.maxframe = 127;
    }
  } else {
    if (conn->capabilities.maxframe > 7) {
      conn->capabilities.maxframe = 7;
    }
  }
  if (conn->capabilities.paclen > MAX_IFRAME_PAYLOAD) {
    conn->capabilities.paclen = MAX_IFRAME_PAYLOAD;
  }
  conn->retries = conn->capabilities.retries;
  LOG_INF("Sending %s", conn->attempting_sabme ? "SABME" : "SABM");
  enqueue_packet(&conn->path, conn, true,
		 false, true,
		 encode_u_control(conn->attempting_sabme ? U_FRAME_SABME : U_FRAME_SABM, true),
		 AX25_NO_PID, NULL, 0, fx25_policy(conn));
  console_printline("Connecting...");
  k_mutex_unlock(&conn->mutex);
  return true;
}

bool start_unproto_connection(connection *conn, encoded_callsign *dest) {
  if (conn->state != CONN_DISCONNECTED) {
    console_printline("Already connected - disconnect first");
    return false;
  }
  k_mutex_lock(&conn->mutex, K_FOREVER);
  reset_connection(conn, false);
  conn->filter = NULL;
  conn->consumer = unproto_consumer;
  conn->path.dest = *dest;
  conn->path.src = tnc_state.mycall;
  conn->path.src.ssid |= SSID_BYTE_ADDRESS_EXTENSION_BIT;
  conn->path.digipeaters = 0;
  conn->match_mycall = false;
  conn->match_endpoint_call = false;
  conn->match_peer = false;
  conn->pid = AX25_P_TEXT;
  conn->state = CONN_UNPROTO;
  conn->live = true;
  k_mutex_unlock(&conn->mutex);
  return true;
}

bool start_inbound_connection(connection *conn, full_address_path *path,
			      bool extended_sequence) {
  k_mutex_lock(&conn->mutex, K_FOREVER);
  // initialize timer structures
  reset_connection(conn, true);
  configure_default_capabilities(conn, extended_sequence);
  conn->path = *path;
  conn->peer_ready = true;
  conn->pid = AX25_P_TEXT;
  conn->match_mycall = conn->endpoint->match_mycall_default;
  conn->match_endpoint_call = conn->endpoint->match_endpoint_call_default;
  conn->match_peer = conn->endpoint->match_peer_default;
  conn->configure = conn->endpoint->configure_default;
  conn->can_receive = conn->endpoint->can_receive_default;
  conn->consumer = conn->endpoint->consumer_default;
  conn->filter = conn->endpoint->filter_default;
  conn->state = CONN_CONNECTED;
  conn->live = true;
  k_mutex_unlock(&conn->mutex);
  enqueue_packet(path, conn, FRAME_IS_RESPONSE,
		 false, false,
		 encode_u_control(U_FRAME_UA, true),
		 AX25_NO_PID, NULL, 0, fx25_policy(conn));
  if (conn->endpoint->is_console) {
    plaintext_callsign caller;
    decode_callsign(caller, &path->dest);
    console_printstr("*** CONNECTED to ");
    console_printline(caller);
  }
  return true;
}

void stop_connection(connection *conn) {
  if (conn->state == CONN_DISCONNECTED) {
    console_printline("Not connected");
    conn->live = false;
    return;
  }
  k_mutex_lock(&conn->mutex, K_FOREVER);
  conn->disconnect = true;
  k_mutex_unlock(&conn->mutex);
}

bool room_for_conn_packet(connection *conn) {
  return conn->pending_packets < CONNECTION_PACKET_COUNT;
}

bool have_i_frames_to_send(connection *conn) {
  for (int i = 0; i < conn->pending_packets; i++) {
    if (!conn->pending_output[i].sent_once) {
      return true;
    }
  }
  return false;
}

bool append_l3_packet(connection *conn, ax25buffer *buffer, uint32_t sharebuffer_offset, uint32_t length) {
  k_mutex_lock(&conn->mutex, K_FOREVER);
  bool result = false;
  if (conn->pending_packets < CONNECTION_PACKET_COUNT) {
    conn->pending_output[conn->pending_packets].buffer = buffer;
    if (buffer) {
      LOG_DBG("Adding L3 B-packet %d with %d-byte payload", conn->pending_packets,
	      ax25buffer_byte_count(buffer));
      conn->pending_output[conn->pending_packets].sharebuffer_offset = -1;
      conn->pending_output[conn->pending_packets].length = ax25buffer_byte_count(buffer);
      conn->pending_output[conn->pending_packets].claimed = false;
    } else {
      conn->pending_output[conn->pending_packets].length = length;
      LOG_DBG("Adding L3 S-packet %d with %d-byte payload", conn->pending_packets, length);
      conn->pending_output[conn->pending_packets].sharebuffer_offset =
	sharebuffer_offset;
      conn->pending_output[conn->pending_packets].length = length;
      conn->pending_output[conn->pending_packets].claimed = true;
    }
    conn->pending_output[conn->pending_packets].valid = true;
    conn->pending_output[conn->pending_packets].sent_once = false;
    conn->pending_packets ++;
    conn->queued_packets ++;
    LOG_DBG("There are now %d packets pending, %d queued, %d ack'ed", conn->pending_packets,
	    conn->queued_packets, conn->sent_packets);
    result = true;
  }
  k_mutex_unlock(&conn->mutex);
  return result;
}

// Initialize timer (set duration, clear other fields).
void init_conn_timer(struct conn_timer *t, uint32_t duration,
		     uint32_t multiplier) {
  t->duration = duration * multiplier;
  t->ticks_remaining = 0;
  t->expired = false;
  LOG_DBG("Initialize timer %s, period %d ticks", t->name, t->duration);
}

// Clear "expired" flag, set ticks_remaining to duration
void start_conn_timer(struct conn_timer *t) {
  if (t->ticks_remaining == 0) {
    LOG_DBG("Timer %s had expired", t->name);
  }
  t->expired = false;
  t->ticks_remaining = t->duration;
  LOG_DBG("Start %s, %d ticks", t->name, t->ticks_remaining);
}

void start_conn_timer_for(struct conn_timer *t, uint32_t new_duration) {
  t->duration = new_duration;
  start_conn_timer(t);
}

void stop_conn_timer(struct conn_timer *t) {
  if (t->ticks_remaining != 0 || t->expired) {
    t->expired = false;
    t->ticks_remaining = 0;
    LOG_DBG("Timer %s stopped", t->name);
  } else {
    LOG_DBG("Timer %s was already stopped", t->name);
  }
}

// If timer is running, decrement count.  If count reaches
// zero, set "expired" flag.  Return "expired" flag.
bool tick_conn_timer(struct conn_timer *t) {
  if (t->ticks_remaining > 0) {
    t->ticks_remaining --;
    if (!t->ticks_remaining) {
      t->expired = true;
      LOG_DBG("Timer %s expired", t->name);
    }
  }
  return t->expired;
}

struct connection_endpoint console_endpoint = {
  .consumer_default = console_consumer,
  .configure_default = console_configure,
  .can_receive_default = console_can_receive,
  .match_mycall_default = true,
  .match_endpoint_call_default = false,
  .match_peer_default = true,
  .is_console = true
};

struct connection_endpoint echo_endpoint = {
  .consumer_default = echo_consumer,
  .configure_default = echo_configure,
  .can_receive_default = echo_can_receive,
  .match_mycall_default = false,
  .match_endpoint_call_default = true,
  .match_peer_default = true,
  .is_console = false
};

struct connection_endpoint digipeater_endpoint = {
  .consumer_default = digipeater_consumer,
  .configure_default = digipeater_configure,
  .filter_default = digipeater_filter,
  .can_receive_default = digipeater_can_receive,
  .match_mycall_default = false,
  .match_endpoint_call_default = false,
  .match_peer_default = false,
  .is_console = false
};

struct connection_endpoint aprsdigi_endpoint = {
  .consumer_default = aprsdigi_consumer,
  .configure_default = aprsdigi_configure,
  .filter_default = aprsdigi_filter,
  .can_receive_default = aprsdigi_can_receive,
  .match_mycall_default = false,
  .match_endpoint_call_default = false,
  .match_peer_default = false,
  .is_console = false
};

union {
  struct {
    connection console_connection;
    connection echo_connection;
    connection digipeater_connection;
    connection aprsdigi_connection;
  };
  connection all[1]; // actually a flexible array.
} all_connections = {
  .console_connection.endpoint = &console_endpoint,
  .console_connection.t1 = {.name = "T1"},
  .console_connection.t2 = {.name = "T2"},
  .console_connection.t3 = {.name = "T3"},
  .echo_connection.endpoint = &echo_endpoint,
  .echo_connection.t1 = {.name = "T1"},
  .echo_connection.t2 = {.name = "T2"},
  .echo_connection.t3 = {.name = "T3"},
  .digipeater_connection.endpoint = &digipeater_endpoint,
  .aprsdigi_connection.endpoint = &aprsdigi_endpoint
};

connection * const console_connection = &all_connections.console_connection;

bool some_connection_has_traffic(void) {
  for (int i = 0; i < sizeof(all_connections) / sizeof(connection); i++) {
    if (all_connections.all[i].live &&
	all_connections.all[i].pending_packets > 0) {
      return true;
    }
  }
  return false;
}

// Refactor this mess!

connection *match_packet_to_active_connection(ax25buffer *buffer, packet_parse *p) {
  for (int i = 0; i < sizeof(all_connections) / sizeof(connection); i++) {
    // If there's a special filter present for a connection, use its
    // judgement instead of looking at the matching variables.
    LOG_DBG("Look for active connection %d", i);
    if (all_connections.all[i].filter) {
      bool matches = (*all_connections.all[i].filter)(&all_connections.all[i], p);
      if (matches) {
	LOG_DBG("Special connection-filter match %d!", i);
	return &all_connections.all[i];
      }
      LOG_DBG("Special connection-filter did not match");
      continue;
    }
    if (!all_connections.all[i].live) {
      continue;
    }
    if (all_connections.all[i].match_mycall &&
	!same_callsign(&tnc_state.mycall, (encoded_callsign *)buffer->data)) {
      continue;
    }
    if (all_connections.all[i].match_endpoint_call &&
	!same_callsign(&all_connections.all[i].endpoint->callsign, (encoded_callsign *)buffer->data)) {
      continue;
    }
    if (all_connections.all[i].match_peer &&
	!same_callsign(&all_connections.all[i].path.dest, &p->path.src)) {
      continue;
    }
    LOG_DBG(" Matched to active connection %d", i);
    return &all_connections.all[i];
  }
  return NULL;
}

// Match an incoming packet-buffer with the first free matching connection
connection *match_packet_to_free_connection(ax25buffer *buffer, packet_parse *p) {
  for (int i = 0; i < sizeof(all_connections) / sizeof(connection); i++) {
    LOG_DBG("Look for free connection %d", i);
    // For now, don't do active match against anything with a custom
    // filter.
    if (all_connections.all[i].filter) {
      LOG_DBG(" Has a filter, skip it");
      continue;
    }
    if (all_connections.all[i].live) {
      LOG_DBG(" Is busy, skip it");
      continue;
    }
    if (all_connections.all[i].match_mycall &&
	!same_callsign(&tnc_state.mycall, (encoded_callsign *)buffer->data)) {
      LOG_DBG(" Callsign missmatch, skip it");
      continue;
    }
    if (all_connections.all[i].match_endpoint_call &&
	!same_callsign(&all_connections.all[i].endpoint->callsign, (encoded_callsign *)buffer->data)) {
      LOG_DBG(" Endpoint callsign missmatch, skip it");
      continue;
    }
    LOG_DBG(" Matched to free connection %d", i);
    return &all_connections.all[i];
  }
  return NULL;
}

// Match an incoming packet-buffer with a connection (live or not)
connection *match_packet_to_some_connection(ax25buffer *buffer, packet_parse *p) {
  for (int i = 0; i < sizeof(all_connections) / sizeof(connection); i++) {
    LOG_DBG("Look for some connection %d", i);
    if (all_connections.all[i].filter) {
      LOG_DBG(" Has a filter, skip it");
      continue;
    }
    if (all_connections.all[i].match_mycall &&
	!same_callsign(&tnc_state.mycall, (encoded_callsign *)buffer->data)) {
      LOG_DBG(" Callsign missmatch, skip it");
      continue;
    }
    if (all_connections.all[i].match_endpoint_call &&
	!same_callsign(&all_connections.all[i].endpoint->callsign, (encoded_callsign *)buffer->data)) {
      LOG_DBG(" Endpoint callsign missmatch, skip it");
      continue;
    }
    return &all_connections.all[i];
  }
  return NULL;
}

// Find an available connection for an endpoint
connection *find_free_connection_for_endpoint(connection_endpoint *ep) {
  for (int i = 0; i < sizeof(all_connections) / sizeof(connection); i++) {
    if (all_connections.all[i].endpoint == ep &&
	!all_connections.all[i].live) {
      return &all_connections.all[i];
    }
  }
  return NULL;
}

bool sequence_in_window(connection *conn, int32_t n_s) {
  int32_t delta = n_s - (int32_t) conn->v_r;
  int32_t modulus = conn->capabilities.sequence_mask + 1;
  if (delta < 0) {
    delta += modulus;
  }
  if (delta < conn->capabilities.filter_window) {
    return true;
  }
  return false;
}


void init_connections(void) {
  for (int i = 0; i < sizeof(all_connections) / sizeof(connection); i++) {
    connection *conn = &all_connections.all[i];
    k_mutex_init(&conn->mutex);
    sys_dlist_init(&conn->deferred_input);;
    reinit_connection(conn); // initialize it properly
  }
  echo_endpoint.callsign = tnc_state.echoback_call;
}

int connection_thread(void) {
  LOG_INF("Connection thread started");
  for (int i = 0; i < sizeof(all_connections) / sizeof(connection); i++) {
    connection *conn = &all_connections.all[i];
    k_mutex_lock(&conn->mutex, K_FOREVER);
    init_conn_timer(&conn->t1, tnc_state.t1, 10);
    init_conn_timer(&conn->t2, tnc_state.t2, 1);
    init_conn_timer(&conn->t3, tnc_state.t3, 100);
  }
  while (1) {
    for (int i = 0; i < sizeof(all_connections) / sizeof(connection); i++) {
      connection *conn = &all_connections.all[i];
      k_mutex_unlock(&conn->mutex);
    }
    k_msleep(100); // Could use semaphore-with-timeout someday
    for (int i = 0; i < sizeof(all_connections) / sizeof(connection); i++) {
      connection *conn = &all_connections.all[i];
      k_mutex_lock(&conn->mutex, K_FOREVER);
      if (conn->state == CONN_DISCONNECTED) {
	continue;
      }
      tick_connection_timers(conn);
      // Don't make decisions about what to send, or enqueue any packets,
      // when the channel is busy, for a couple of reasons:
      //
      // (1) If the peer is sending frames to us, it's best if we
      // wait until they all arrive before we decide what to send.
      // Some TNCs send multiple N_R/N_S updates (in RR, RNR, REJ,
      // or I-frames) and we can make the best decision about what to
      // send (or to not send, if RNR) if we wait until all of these
      // have arrived.
      //
      // (2) We can't send now, anyhow, because the channel is busy,
      // so the additional delay involved in making a decision later
      // is no worse than 100 milliseconds.
      //
      // (3) We don't want to start the T1 retransmission timer
      // before we actually (can) transmit.
      if (board_report_carrier_state() != CARRIER_OFF) {
	continue;
      }
      handle_outbound_state(conn);  // Sending RR, RNR, REJ as necessary
      handle_disconnection(conn);   // Tearing down and cleaning up
      handle_t1_expiration(conn);   // Retries
      send_i_frames(conn);          // Outbound connection data
      send_ui_frames(conn);         // Outbound UNPROTO
    }
  }
}

K_THREAD_DEFINE(connection_tid, CONNECTION_STACK_SIZE,
                connection_thread, NULL, NULL, NULL,
                CONNECTION_PRIORITY, 0, 500);
