#include <stdint.h>

#include "xoshiro128.h"

uint32_t xoshiro128(void);

static uint32_t s[4];

void xoshiro128_init(uint32_t a, uint32_t b, uint32_t c, uint32_t d) {
  s[0] = a;
  s[1] = b;
  s[2] = c;
  s[3] = d | 1;
}

static inline uint32_t rotl(const uint32_t x, int k) {
  return (x << k) | (x >> (32 - k));
}

uint32_t xoshiro128(void) {
  const uint32_t result = rotl(s[0] + s[3], 7) + s[0];

  const uint32_t t = s[1] << 9;

  s[2] ^= s[0];
  s[3] ^= s[1];
  s[1] ^= s[2];
  s[0] ^= s[3];

  s[2] ^= t;

  s[3] = rotl(s[3], 11);

  return result;
}
