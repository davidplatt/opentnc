#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>

#include "board.h"
#include "leds.h"

// This set of arcane gibberish will iterate through all of the VLED
// nodes.  It will define a gpio_dt_spec for each one (naming it
// according to the "struct-field" property - e.g. a node with
// struct-field = "foo-bar" will be have a C spec created,
// named foo_bar_led.  If the node has a gpios property, it will
// be used to initialize the gpio_dt_spec;  if not, the gpio_dt_spec
// will be given a dummy initialization in which the .port field
// contains NULL;

#define NULL_PORT {.port = NULL}

#define INIT_LED_GPIO(node) const struct gpio_dt_spec NODE_STRUCT_NAME(node) = \
    GPIO_DT_SPEC_GET_OR(node, gpios, NULL_PORT);

DT_N_S_vleds_FOREACH_CHILD(INIT_LED_GPIO)

struct leds leds;

// Another iteration is used to create an initializer function, which
// configures the GPIO pin for any VLED which has one.

#define RUN_LED_INITIALIZER_2(node, structure) \
  extern const struct gpio_dt_spec structure; \
  if (DT_NODE_HAS_PROP(node, gpios)) gpio_pin_configure_dt(&structure, GPIO_OUTPUT_ACTIVE);

#define RUN_LED_INITIALIZER(node) RUN_LED_INITIALIZER_2(node, NODE_STRUCT_NAME(node))

void init_leds(void) {
  DT_N_S_vleds_FOREACH_CHILD(RUN_LED_INITIALIZER);
}

// Yet another iteration is used to create a periodic-update
// function, which will count down the remaining hang time for
// any active VLED which has a hang-time specified, and will
// reset the VLED to zero when the remaining hang time hits zero.
// Any VLED which has a non-zero period will be set to 1, whenever
// the tick count mod the period is zero.

#define RUN_LED_UPDATER_2(node, field, setter, ticks)			    \
  if (VLED_HANG_TIME(node) != 0 && leds.field.remaining_hang > 0) { \
    if (0 == --leds.field.remaining_hang) { \
      setter(0);				\
    }                                           \
  }                                             \
  if (VLED_PERIOD(node) != 0 && 0 == ticks % VLED_PERIOD(node)) { \
    setter(1);				\
  }

#define RUN_LED_UPDATER(node) RUN_LED_UPDATER_2(node, DT_STRING_TOKEN(node, struct_field), NODE_SETTER_NAME(node), ticks)

void update_leds(uint32_t ticks) {
  if (tnc_state.lamp_test_time != 0) {
    if (--tnc_state.lamp_test_time == 0) {
      restore_leds();
    } else {
      set_all_led_gpios_to(1);
    }
  } else {
    DT_N_S_vleds_FOREACH_CHILD(RUN_LED_UPDATER);
  }
}

// Yet more are used to provide "set-all-pins" and "reset" functions,
// for use by the lamp-test features.

#define RUN_LED_GPIO_SETTER(node, structure, val)			\
  if (DT_NODE_HAS_PROP(node, gpios)) gpio_pin_set_dt(&structure, val != 0);

#define RUN_LED_SETTER(node) RUN_LED_GPIO_SETTER(node, NODE_STRUCT_NAME(node), value)

void set_all_led_gpios_to(uint32_t value) {
  DT_N_S_vleds_FOREACH_CHILD(RUN_LED_SETTER);
}

#define RUN_LED_RESTORER_2(node, setter, structure, field) \
  if (DT_NODE_HAS_PROP(node, gpios)) gpio_pin_set_dt(&structure, leds.field.value != 0);

#define RUN_LED_RESTORER(node) RUN_LED_RESTORER_2(node, NODE_SETTER_NAME(node), NODE_STRUCT_NAME(node), DT_STRING_TOKEN(node, struct_field))

void restore_leds(void) {
  DT_N_S_vleds_FOREACH_CHILD(RUN_LED_RESTORER);
}



