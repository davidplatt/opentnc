/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

/*
  This file contains portions of the OpenTNC code which are
  specific to the STM32F411 (or, to be pedantic, to this chip
  as used on the WeAct Black Pill 3.x board, which runs at a
  system clock speed of 96 MHz).
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/flash.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/crc.h>
#include <zephyr/sys/ring_buffer.h>

#include <stm32_ll_adc.h>
#include <stm32_ll_bus.h>
#include <stm32_ll_dma.h>
#include <stm32_ll_gpio.h>
#include <stm32_ll_spi.h>
#include <stm32_ll_tim.h>

#include <zephyr/usb/usb_device.h>
#include <zephyr/logging/log.h>

#include "board.h"
#include "demod.h"
#include "globals.h"
#include "hdlc.h"
#include "leds.h"
#include "mod.h"
#include "kiss.h"
#include "xoshiro128.h"

#include "noiseshaping_stm32f411.h"

LOG_MODULE_REGISTER(stm32f411, LOG_LEVEL_INF);

#define DMA1_NODE DT_ALIAS(dma1)
#define DMA2_NODE DT_ALIAS(dma2)
#define UART_NODE DT_ALIAS(uart)
#define SPI_NODE DT_ALIAS(spi)
#define ADC_NODE DT_ALIAS(radio)
#define FLASH_NODE DT_ALIAS(flash)

#if DT_NODE_HAS_STATUS(DMA1_NODE, okay)
#else
#error "DMA1 is not okay"
#endif

#if DT_NODE_HAS_STATUS(DMA2_NODE, okay)
#else
#error "DMA2 is not okay"
#endif

#define HEARTBEAT_STACK_SIZE	2048
static struct k_thread blink_thread;
static K_THREAD_STACK_ARRAY_DEFINE(tstack, 1, HEARTBEAT_STACK_SIZE);

#define TRANSCEIVER_STACK_SIZE	2048
static struct k_thread transceiver_thread;
static K_THREAD_STACK_ARRAY_DEFINE(transceiver_stack, 1, TRANSCEIVER_STACK_SIZE);

#define ADC_DEVICE_NAME         "radio" /* fix this */
#define ADC_RESOLUTION		12
#define ADC_GAIN		ADC_GAIN_1
#define ADC_REFERENCE		ADC_REF_INTERNAL
#define ADC_ACQUISITION_TIME	ADC_ACQ_TIME_DEFAULT
#define ADC_1ST_CHANNEL_ID      1 // A1

#define SHOW_DELTA_SIGMA_CLOCK 0

static ADC_TypeDef *ll_adc = NULL;

const struct device *cdc_dev = DEVICE_DT_GET(UART_NODE);
const struct device *flash_dev = DEVICE_DT_GET(FLASH_NODE);

const struct gpio_dt_spec ptt = GPIO_DT_SPEC_GET(ZEPHYR_USER_NODE, ptt_gpios);
const struct gpio_dt_spec hdlc = GPIO_DT_SPEC_GET(ZEPHYR_USER_NODE, hdlc_direct_gpios);

const struct device *adc;

int adc_overruns;
int dma_interrupts;

K_SEM_DEFINE(sampler_sem, 0, 2);

struct sample_sets sample_sets;

bool sample_set_0_ready = false;
bool sample_set_1_ready = false;

uint32_t scaled_sine_cycles_per_output_sample;
uint32_t scaled_sine_cycles;

int phase_index = 0;

#define MODULATOR_BUFFER_BYTES (1 << CONFIG_OUTPUT_BUFFER_BYTES_LOGARITHM)

uint8_t modulator_buffers[MODULATOR_BUFFER_BYTES * 2] __attribute__((aligned(4)));

uint8_t *modulator_buffer_0;
uint8_t *modulator_buffer_1;

int modulator_buffer_bytes_active;
int adc_buffer_bytes_active;

// For now, run at 4x 38k
#define OUTPUT_SAMPLES_PER_BAUD (SAMPLES_PER_BAUD * 4)
#define OUTPUT_SAMPLES_PER_SECOND (OUTPUT_SAMPLES_PER_BAUD * BAUDRATE)

// Don't ask me why the IRQs are mapped two per stream, with every
// second one being zero.
#define DMA1_Stream7_IRQn (DT_PROP_BY_IDX(DMA1_NODE, interrupts, 14))
#define DMA2_Stream0_IRQn (DT_PROP_BY_IDX(DMA2_NODE, interrupts, 0))
#define DMA2_Stream2_IRQn (DT_PROP_BY_IDX(DMA2_NODE, interrupts, 4))
#define ADC1_IRQn (DT_PROP_BY_IDX(ADC_NODE, interrupts, 0))

void board_heartbeat(uint32_t ticks)
{
  uint32_t time_then = k_uptime_get();
  uint64_t old_samples_processed = samples_processed;
  if (overruns) {
    overruns = 0;
    LOG_INF("Sampler overrun");
  }
  if (LL_DMA_IsActiveFlag_HT0(DMA2)) {
    LOG_DBG("DMA first half full");
  }
  if (LL_DMA_IsActiveFlag_TC0(DMA2)) {
    LOG_DBG("DMA second half full");
  }
  uint32_t time_now = k_uptime_get();
  uint64_t samples_now = samples_processed;
  time_then = time_now;
  old_samples_processed = samples_now;
}

static void sampler_isr(void *arg)
{
  LOG_DBG("DMA2 ISR");
  dma_interrupts++;
  if (LL_DMA_IsActiveFlag_TE0(DMA2)) {
    LOG_ERR("DMA transfer error\n");
  }
  /* Half Transfer means that the first half has been filled */
  if (LL_DMA_IsActiveFlag_HT0(DMA2)) {
    LOG_DBG("First half full");
    if (sample_set_0_ready) overruns++;
    sample_set_0_ready = true;
    LL_DMA_ClearFlag_HT0(DMA2);
    k_sem_give(&sampler_sem);
  }
  /* Transfer Complete means that the second half has been filled */
  if (LL_DMA_IsActiveFlag_TC0(DMA2)) {
    LOG_DBG("Second half full");
    if (sample_set_1_ready) overruns++;
    sample_set_1_ready = true;
    LL_DMA_ClearFlag_TC0(DMA2);
    k_sem_give(&sampler_sem);
  }
}

// ADC overruns require resetting the overrun status.  We could, but
// don't bother to reset the DMA engine here - we just accept that
// we've dropped one or more samples.  Should probably change this
// at some point to detect pathologically-bad dropping, assume that
// some thing has gone bad with the DMA engine, and abort and
// clear and reset the engine before proceeding.
static void adc_isr(void *arg) {
  if (LL_ADC_IsActiveFlag_OVR(ADC1)) {
    adc_overruns++;
    LOG_DBG("ADC overrun");
    LL_ADC_ClearFlag_OVR(ADC1);
  }
}

int dma_swap1 = 0, dma_swap2 = 0;

static void spi_dma_isr(void *arg)
{
  LOG_DBG("DMA2 ISR for SPI");
  gpio_pin_toggle_dt(&tx_busy_led);
  dma_interrupts++;
  if (LL_DMA_IsActiveFlag_TE2(DMA2)) {
    LOG_ERR("DMA transfer error\n");
  }
  /* Transfer Complete means that the double buffer has flipped */
  if (LL_DMA_IsActiveFlag_TC2(DMA2)) {
    const uint8_t *next_buffer = noiseshaping.idle;
    if (HDLC_Output_Avail()) {
      bool data_bit = HDLC_Get_Bit();
      board_set_hdlc(data_bit);
      if (!tnc_state.adc_loopback) {
	set_data_leds(data_bit);
      }
      if (tnc_state.hdlc_loopback) {
	ConsumeNRZI(&hdlc_context_flat, data_bit);
      }
      // Point into the low- or high-tone delta-sigma stream,
      // starting at the offset corresponding to the current
      // phase index.
      next_buffer = noiseshaping.tones[data_bit].phases[phase_index].samples;
      // Bump the phase index (wrapping around).
      phase_index = (phase_index + noiseshaping.tones[data_bit].phase_increment) %
	noiseshaping.phases;
    }
    LOG_DBG("Double-buffer flip\n");
    // If the Current Target bit is 0, DMA is currently working in the
    // first memory block, and we can provide a new address for the
    // second block.  If Current Target is 1, the opposite relationship
    // holds.
    if (LL_DMA_GetCurrentTargetMem(DMA2, LL_DMA_STREAM_2) == 0) {
      LL_DMA_SetMemory1Address(DMA2, LL_DMA_STREAM_2, (uint32_t) next_buffer);
      dma_swap2++;
    } else {
      LL_DMA_SetMemoryAddress(DMA2, LL_DMA_STREAM_2, (uint32_t) next_buffer);
      dma_swap1++;
    }
    LL_DMA_ClearFlag_TC2(DMA2);
  }
}

// Re-fill one modulator buffer with the next few baud worth of modulation
// data.
static void TIME_CRITICAL(refill_modulator_buffer)(uint8_t *buffer, uint32_t size) {
  set_tx_busy_led(1);
  int offset = 0;
  static bool data_bit = false;
  bool new_data_bit_avail = false;
  static uint32_t phase = 0;
  LOG_DBG("Refill modulator buffer with %d bytes", size);
  while (offset < size) {
    if (HDLC_Output_Avail()) {
      LOG_DBG("Take HDLC bit");
      data_bit = HDLC_Get_Bit();
      new_data_bit_avail = true;
      board_set_hdlc(data_bit);
      if (!tnc_state.adc_loopback) {
	set_data_leds(data_bit);
      }
      if (tnc_state.hdlc_loopback) {
	ConsumeNRZI(&hdlc_context_flat, data_bit);
      }
    } else {
      LOG_DBG("HDLC starved");
      new_data_bit_avail = false;
    }
    // Now, we must load the modulator data for the next baud
    // into whichever buffer was just completed.
    switch (tnc_state.modulator_transmitting) {
    case PWM_MODULATOR:
      for (int i = 0 ; i < OUTPUT_SAMPLES_PER_BAUD; i++) {
	assert(offset < size); // just to be safe...
	/* Increment the accumulated phase angle by 1/32 baud */
	scaled_sine_cycles = (scaled_sine_cycles + scaled_sine_cycles_per_output_sample) & 0xFFFF;
	/* Find the index into the sine table */
	uint32_t sine_index = (scaled_sine_cycles * PWM_ENTRIES) >> 16;
	*(uint32_t *)(buffer + offset) = pwmtable[sine_index];
	offset += sizeof(uint32_t);
      }
      break;
    case G3RUH_PWM_MODULATOR:
      // Send 1-bits if we don't get new data, to try to force
      // an abort.
      if (!new_data_bit_avail) {
	data_bit = 1;
      }
      // If we're in waveform-test mode, override the HDLC encoder and
      // the G3RUH data scrambler.  Simply send out a 1-bit (positive
      // pulse) every 32 baud, so we can see the waveform.
      bool out;
      if (tnc_state.waveform_test) {
	out = (G3RUH_modulator_state.scrambler >> 31) & 1;
	G3RUH_modulator_state.scrambler = G3RUH_modulator_state.scrambler << 1;
	// Handle both the initial all-zero state, and the once-in-32 overflow.
	if (G3RUH_modulator_state.scrambler == 0) {
	  G3RUH_modulator_state.scrambler = 1;
	}
      } else {
	// Implement the G3RUH data scrambler.
	out = 1 & (data_bit ^ (G3RUH_modulator_state.scrambler >> 11) ^ (G3RUH_modulator_state.scrambler >> 16));
	G3RUH_modulator_state.scrambler = (G3RUH_modulator_state.scrambler << 1) | out;
      }
      G3RUH_modulator_state.transmit_bit_history = ((G3RUH_modulator_state.transmit_bit_history << 1) | out) &
	G3RUH_modulator_state.transmit_bit_mask;
      // Every sample going to the PWM is four bytes long.
      int32_t bytes_per_burst = G3RUH_modulator_cache.samples_per_baud * sizeof(int32_t);
      LOG_DBG("G3RUH bit pattern 0x%04x to offset %d, %d bytes",
	      G3RUH_modulator_state.transmit_bit_history, offset, bytes_per_burst);
      memcpy(buffer + offset,
	     &G3RUH_modulator_cache.bursts[G3RUH_modulator_state.transmit_bit_history],
	     bytes_per_burst);
      offset += bytes_per_burst;
      break;
    default:
      LOG_ERR("Oops, bad modulator in callback");
      break;
    }
  }
  LOG_DBG("Phase: enter 0x%04X leave 0x%04X", phase, scaled_sine_cycles);
  phase = scaled_sine_cycles;
  set_tx_busy_led(0);
}

static void pwm_dma_isr(void *arg)
{
  dma_interrupts++;
  bool update_tc = false;
  bool update_ht = false;
  if (LL_DMA_IsActiveFlag_TC7(DMA1)) {
    LL_DMA_ClearFlag_TC7(DMA1);
    update_tc = true;
  }
  if (LL_DMA_IsActiveFlag_HT7(DMA1)) {
    LL_DMA_ClearFlag_HT7(DMA1);
    update_ht = true;
  }
  LOG_DBG("DMA1 ISR for PWM, tc %d, ht %d", update_tc, update_ht);
  LOG_DBG("Stream7   CR 0x%08X", DMA1_Stream7->CR);
  if (update_tc || update_ht) {
    // If the Current Target bit is 0, DMA is currently working in the
    // first memory block, and we can provide new data for the second
    // block.  If Current Target is 1, the opposite relationship
    // holds.
    //    if (LL_DMA_GetCurrentTargetMem(DMA1, LL_DMA_STREAM_7) == 1) {
    if (update_ht) {
      LOG_DBG("First half drained");
      refill_modulator_buffer(modulator_buffer_0, modulator_buffer_bytes_active);
    } else if (update_tc) {
      LOG_DBG("Second half drained");
      refill_modulator_buffer(modulator_buffer_1, modulator_buffer_bytes_active);
    }
    //    LL_DMA_ClearFlag_TC7(DMA1);
  }
  if (LL_DMA_IsActiveFlag_FE7(DMA1)) {
    LOG_DBG("PWM-DMA FIFO error");
    LL_DMA_ClearFlag_FE7(DMA1);
  }
}

static const struct adc_channel_cfg m_1st_channel_cfg = {
	.gain             = ADC_GAIN,
	.reference        = ADC_REFERENCE,
	.acquisition_time = ADC_ACQUISITION_TIME,
	.channel_id       = ADC_1ST_CHANNEL_ID,
#if defined(CONFIG_ADC_CONFIGURABLE_INPUTS)
	.input_positive   = ADC_1ST_CHANNEL_INPUT,
#endif
};

static const struct device *init_adc(void)
{
	const struct device *adc_dev = DEVICE_DT_GET(ADC_NODE);
	int ret;

	if (!adc_dev) {
	  LOG_ERR("Can't find ADC %s", ADC_DEVICE_NAME);
	  return NULL;
	}

	if (!device_is_ready(adc_dev)) {
                LOG_ERR("ADC device not ready");
        }

	ret = adc_channel_setup(adc_dev, &m_1st_channel_cfg);
	if (ret != 0)
	  LOG_ERR("Setting up ADC channel failed with code %d", ret);

	return adc_dev;
}

/* TIM2 does all the heavy-lifting timing work for both reception
 * (demodulation) and PWM transmission (modulation).  It ticks over at
 * a rate of 38400 times/second.  During reception, Channel 2 is used
 * to generate an internal signal which clocks ADC1, sampling the
 * analog input pin.  During transmission, the counter update event
 * interrupt is enabled, entering a fast interrupt handler which
 * updates the PCM duty cycle (the sinewave modulator) and clocks HDLC
 * bits into the modulator at the proper rate (1200 bits/second).
 *
 * On the WeAct SM32F411 "black pill" board, the system clock is
 * at 96 MHz, which is conveniently 10,000 times the basic 9600 Hz HDLC
 * work rate, and 2500 times the oversampled 38400 Hz rate.
 */
static void Init_TIM2(void) {
  LOG_INF("Setting up TIM2");
  LL_TIM_DisableCounter(TIM2);
  LL_TIM_SetClockSource(TIM2, LL_TIM_CLOCKSOURCE_INTERNAL);
  LL_TIM_DisableIT_CC2(TIM2); /* Don't interrupt on timer compare */
  LL_TIM_SetPrescaler(TIM2, (100 - 1)); /* Input at 96 MHz, divide to 96 kHz*/
  LL_TIM_SetAutoReload(TIM2, (100 / OVERSAMPLING) - 1); /* Divide by 25, to 38400 Hz */
  /* TIM2 CH2 serves as the sampling timer for DAC1 */
  LL_TIM_OC_SetMode(TIM2, LL_TIM_CHANNEL_CH2, LL_TIM_OCMODE_PWM1);
  LL_TIM_OC_SetCompareCH2(TIM2, 15); // Muse be less than the auto-reload value
  LL_TIM_OC_EnablePreload(TIM2, LL_TIM_CHANNEL_CH2);
  LL_TIM_EnableARRPreload(TIM2);
  LL_TIM_CC_EnableChannel(TIM2, LL_TIM_CHANNEL_CH2);
  // Try using the trigger output instead of CH2 compare.
  LL_TIM_SetTriggerOutput(TIM2, LL_TIM_TRGO_UPDATE);
  /* The update event from TIM2 is used for the "next TX bit time" IRQ. */
  LL_TIM_SetUpdateSource(TIM2, LL_TIM_UPDATESOURCE_REGULAR);
  /* Enable DMA request on update (only need this for PWM modulator) */
  LOG_INF("Enabling DMAREQ on update for TIM2");
  LL_TIM_EnableDMAReq_UPDATE(TIM2);
  LL_TIM_EnableUpdateEvent(TIM2);
  LL_TIM_EnableCounter(TIM2);
}

// Set timer 2 to a specific rate, measured in hertz.  Used to
// switch between input-sampling rate (RX) and output-sample
// generation rate (TX).
void Set_TIM2_rate(uint32_t hz) {
  LOG_INF("Set TIM2 to %d Hz", hz);
  uint32_t prescaler = 1;
  uint32_t ticks_per_count = (sys_clock_hw_cycles_per_sec() + hz / 2) / hz;
  LOG_INF("Need %d ticks per count", ticks_per_count);
  while (ticks_per_count / prescaler > 0xFFFF) {
    prescaler ++;
  }
  ticks_per_count /= prescaler;
  LOG_INF(" Prescaler %d, auto-reload %d", prescaler, ticks_per_count);
  LL_TIM_SetPrescaler(TIM2, prescaler - 1);
  LL_TIM_SetAutoReload(TIM2, ticks_per_count - 1);
}

static void Init_TIM3(void) {
  LOG_INF("Setting up TIM3 for PWM");
  LL_TIM_DisableCounter(TIM3);
  LL_TIM_SetClockSource(TIM3, LL_TIM_CLOCKSOURCE_INTERNAL);
  LL_TIM_DisableIT_CC2(TIM3); /* Don't interrupt on timer compare */
  LL_TIM_SetPrescaler(TIM3, 0); /* Run timer clock at full speed*/
  LL_TIM_SetAutoReload(TIM3, PWM_PERIOD); /* Divide it down to the chosen PWM rate */
  LL_TIM_EnableARRPreload(TIM3);
  LL_TIM_OC_SetMode(TIM3, LL_TIM_CHANNEL_CH2, LL_TIM_OCMODE_PWM1);
  LL_TIM_OC_SetCompareCH2(TIM3, PWM_PERIOD / 2); /* ~50% duty cycle for now */
  LL_TIM_OC_EnablePreload(TIM3, LL_TIM_CHANNEL_CH2);
  LL_TIM_CC_EnableChannel(TIM3, LL_TIM_CHANNEL_CH2);
  LL_TIM_EnableAllOutputs(TIM3);
  LL_TIM_EnableUpdateEvent(TIM3);
  LL_TIM_EnableCounter(TIM3);
}

static void SetupPins(void) {
  LOG_INF("Here we can do pin setup");
}

static void EnableClocks(void) {
  LOG_DBG("Enabling clock for DMA1");
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1);
  LOG_DBG("Enabling clock for DMA2");
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA2);
  LOG_INF("Enabling clock for SPI");
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SPI1);
  LOG_INF("Enabling clock for TIM2");
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);
  LOG_INF("Enabling clock for TIM3");
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3);
  k_msleep(1);
}

// On the STM32F411, ADC1 is connected to DMA2, Channel 0, Stream 0 and Stream 4.
// This code uses Stream 0, Channel 0.

static void Setup_DMA_for_sampler(void) {
  LOG_DBG("DMA2 is 0x%p", DMA2);
  LOG_DBG("ADC1 is 0x%p", ADC1);
  LOG_DBG("Setting up DMA2 stream %d channel %d for ADC", LL_DMA_STREAM_0, LL_DMA_CHANNEL_0);
  LOG_DBG("Buffer is at %p", &sample_sets);
  LOG_DBG("Peripheral register is at %p", ((void *) &ADC1->DR));
  LL_DMA_DisableStream(DMA2, LL_DMA_STREAM_0);
  LL_DMA_SetPeriphAddress(DMA2, LL_DMA_STREAM_0, ((uint32_t) &ADC1->DR));
  // We don't necessarily want to use the full size of the sample buffer
  // area - at low sample rates it'll take too long to fill.  Since we're
  // using a single circular-buffer DMA, we will "point" the first sample
  // set at the start of the buffer, and the second sample set partway
  // into the buffer (at, or before, the halfway point).  Any unused space
  // at the end of the buffer won't be touched.
  uint32_t samples_per_buffer = adc_buffer_bytes_active / sizeof(input_sample);
  LOG_INF("Data length is 0x%x", samples_per_buffer * 2);
  sample_set_0 = sample_set_base;
  sample_set_1 = sample_set_base + samples_per_buffer;
  LOG_INF("Sample buffers are at 0x%p and 0x%p, total sample count %d", 
	  sample_set_0, sample_set_1, samples_per_buffer * 2);
  LL_DMA_SetMemoryAddress(DMA2, LL_DMA_STREAM_0, (uint32_t) sample_set_0);
  LL_DMA_SetDataLength(DMA2, LL_DMA_STREAM_0, samples_per_buffer * 2);
  LL_DMA_SetChannelSelection(DMA2, LL_DMA_STREAM_0, LL_DMA_CHANNEL_0);
  LL_DMA_SetStreamPriorityLevel(DMA2, LL_DMA_STREAM_0, DMA_PRIORITY_MEDIUM);
  LL_DMA_SetDataTransferDirection(DMA2, LL_DMA_STREAM_0, LL_DMA_DIRECTION_PERIPH_TO_MEMORY);
  LL_DMA_SetMode(DMA2, LL_DMA_STREAM_0, LL_DMA_MODE_CIRCULAR);
  LL_DMA_ConfigFifo(DMA2, LL_DMA_STREAM_0, LL_DMA_FIFOMODE_ENABLE,
		    LL_DMA_FIFOTHRESHOLD_1_2);
  LL_DMA_SetPeriphIncMode(DMA2, LL_DMA_STREAM_0, LL_DMA_PERIPH_NOINCREMENT);
  LL_DMA_SetMemoryIncMode(DMA2, LL_DMA_STREAM_0, LL_DMA_MEMORY_INCREMENT);
  LL_DMA_SetPeriphSize(DMA2, LL_DMA_STREAM_0, LL_DMA_PDATAALIGN_HALFWORD);
  LL_DMA_SetMemorySize(DMA2, LL_DMA_STREAM_0, LL_DMA_MDATAALIGN_HALFWORD);
  LL_DMA_SetMemoryBurstxfer(DMA2, LL_DMA_STREAM_0, LL_DMA_MBURST_INC4);
  LL_DMA_SetPeriphBurstxfer(DMA2, LL_DMA_STREAM_0, LL_DMA_PBURST_SINGLE);
  LL_DMA_EnableIT_TC(DMA2, LL_DMA_STREAM_0);
  LL_DMA_EnableIT_HT(DMA2, LL_DMA_STREAM_0);
  LOG_DBG("DMA setup done");
  LOG_DBG("DMA enabled");
}

// On the STM32F411, SPI1 TX is connected to DMA2, Channel 2 Stream 2
// and Channel 3 Stream 5.  This code uses Channel 2 Stream 2.
//
// The DMA to SPI uses double-buffer mode.  Initially it's set up
// with both buffer halves pointing to the idle pattern.  The first
// real modulated data will be picked up after the first "half-empty"
// ISR is entered.

static void Setup_DMA_for_SPI_modulator(void) {
  LOG_DBG("DMA2 is 0x%p", DMA2);
  LOG_DBG("Setting up DMA2 stream %d channel %d for SPI modulator", LL_DMA_STREAM_2, LL_DMA_CHANNEL_2);
  LL_DMA_DisableStream(DMA2, LL_DMA_STREAM_2);
  // Clear any left-over flags for this stream
  LL_DMA_ClearFlag_HT2(DMA2);
  LL_DMA_ClearFlag_TC2(DMA2);
  LL_DMA_ClearFlag_TE2(DMA2);
  LL_DMA_ClearFlag_DME2(DMA2);
  LL_DMA_ClearFlag_FE2(DMA2);
  LL_DMA_SetMode(DMA2, LL_DMA_STREAM_2, LL_DMA_MODE_CIRCULAR);
  LL_DMA_EnableDoubleBufferMode(DMA2, LL_DMA_STREAM_2);
  LL_DMA_SetPeriphAddress(DMA2, LL_DMA_STREAM_2, ((uint32_t) &SPI1->DR));
  LL_DMA_SetMemoryAddress(DMA2, LL_DMA_STREAM_2, (uint32_t) &noiseshaping.idle);
  LL_DMA_SetMemory1Address(DMA2, LL_DMA_STREAM_2, (uint32_t) &noiseshaping.idle);
  LL_DMA_SetDataLength(DMA2, LL_DMA_STREAM_2, noiseshaping.bytes_per_baud);
  LL_DMA_SetChannelSelection(DMA2, LL_DMA_STREAM_2, LL_DMA_CHANNEL_2);
  LL_DMA_SetStreamPriorityLevel(DMA2, LL_DMA_STREAM_2, DMA_PRIORITY_HIGH);
  LL_DMA_SetDataTransferDirection(DMA2, LL_DMA_STREAM_2, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
  LL_DMA_ConfigFifo(DMA2, LL_DMA_STREAM_2, LL_DMA_FIFOMODE_ENABLE,
		    LL_DMA_FIFOTHRESHOLD_1_2);
  LL_DMA_SetPeriphIncMode(DMA2, LL_DMA_STREAM_2, LL_DMA_PERIPH_NOINCREMENT);
  LL_DMA_SetMemoryIncMode(DMA2, LL_DMA_STREAM_2, LL_DMA_MEMORY_INCREMENT);
  LL_DMA_SetPeriphSize(DMA2, LL_DMA_STREAM_2, LL_DMA_PDATAALIGN_BYTE);
  LL_DMA_SetMemorySize(DMA2, LL_DMA_STREAM_2, LL_DMA_MDATAALIGN_BYTE);
  LL_DMA_SetMemoryBurstxfer(DMA2, LL_DMA_STREAM_2, LL_DMA_MBURST_INC4);
  LL_DMA_SetPeriphBurstxfer(DMA2, LL_DMA_STREAM_2, LL_DMA_PBURST_SINGLE);
  LL_DMA_EnableIT_TC(DMA2, LL_DMA_STREAM_2);
  LOG_DBG("DMA setup done");
}

// On the STM32F411, TIM2 update is connected to DMA1, Channel 3 Stream 1
// and Channel 3 Stream 7.  This code uses Channel 3 Stream 7.
//
// The DMA to the TIM3 PWM control uses circular-buffer mode.

static void Setup_DMA_for_PWM_modulator(void) {
  //  static TIM_Typedef *myTIM3 = (TIM_Typedef *) TIM3_BASE;
  LOG_INF("DMA1 is 0x%p", DMA1);
  LOG_INF("Setting up DMA1 stream %d channel 0x%08X for PWM modulator", LL_DMA_STREAM_7, LL_DMA_CHANNEL_3);
  LL_DMA_DisableStream(DMA1, LL_DMA_STREAM_7);
  // Clear any left-over flags for this stream
  LL_DMA_ClearFlag_HT2(DMA1);
  LL_DMA_ClearFlag_TC2(DMA1);
  LL_DMA_ClearFlag_TE2(DMA1);
  LL_DMA_ClearFlag_DME2(DMA1);
  LL_DMA_ClearFlag_FE2(DMA1);
  LL_DMA_SetMode(DMA1, LL_DMA_STREAM_7, LL_DMA_MODE_CIRCULAR);
  LL_DMA_SetPeriphAddress(DMA1, LL_DMA_STREAM_7, ((uint32_t) &TIM3->CCR2));
  LL_DMA_SetMemoryAddress(DMA1, LL_DMA_STREAM_7, (uint32_t) modulator_buffers);
  LL_DMA_SetDataLength(DMA1, LL_DMA_STREAM_7, 2 * modulator_buffer_bytes_active / sizeof(uint32_t));
  LL_DMA_SetChannelSelection(DMA1, LL_DMA_STREAM_7, LL_DMA_CHANNEL_3);
  LL_DMA_SetStreamPriorityLevel(DMA1, LL_DMA_STREAM_7, DMA_PRIORITY_HIGH);
  LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_STREAM_7, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
  LL_DMA_ConfigFifo(DMA1, LL_DMA_STREAM_7, LL_DMA_FIFOMODE_ENABLE,
		    LL_DMA_FIFOTHRESHOLD_1_2);
  LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_STREAM_7, LL_DMA_PERIPH_NOINCREMENT);
  LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_STREAM_7, LL_DMA_MEMORY_INCREMENT);
  LL_DMA_SetPeriphSize(DMA1, LL_DMA_STREAM_7, LL_DMA_PDATAALIGN_WORD);
  LL_DMA_SetMemorySize(DMA1, LL_DMA_STREAM_7, LL_DMA_MDATAALIGN_WORD);
  LL_DMA_SetMemoryBurstxfer(DMA1, LL_DMA_STREAM_7, LL_DMA_MBURST_SINGLE);
  LL_DMA_SetPeriphBurstxfer(DMA1, LL_DMA_STREAM_7, LL_DMA_PBURST_SINGLE);
  LOG_INF("DMA setup done");
  LL_DMA_EnableIT_HT(DMA1, LL_DMA_STREAM_7);
  LL_DMA_EnableIT_TC(DMA1, LL_DMA_STREAM_7);
  LOG_INF("DMA interrupts enabled");
}

#define ISOK(dev) \
  if (!gpio_is_ready_dt(&dev)) { \
    LOG_ERR("GPIO is not ready"); \
  } else { \
    int ret = gpio_pin_configure_dt(&dev, GPIO_OUTPUT_ACTIVE); \
    if (ret < 0) { \
      LOG_ERR("GPIO not configured");		\
    } \
  }

void board_init_default_tnc_state(void)
{
}

void board_init(void)
{
	int ret;

	ISOK(ptt);
	ISOK(hdlc);

	SetupPins();
	EnableClocks();

	// Turn off all of the LEDs
	set_heartbeat_led(0);
	set_data_1_led(0);
	set_data_0_led(0);
	set_hdlc_lock_led(0);
	set_good_packet_led(0);
	set_tx_busy_led(0);
	set_rx_busy_led(0);
	set_ptt_led(0);
	set_clip_warning_led(0);

	gpio_pin_set_dt(&ptt, 0);

        if (!device_is_ready(cdc_dev)) {
                LOG_ERR("CDC ACM device not ready");
        }

	ret = usb_enable(NULL);
	if (ret != 0) {
		LOG_ERR("Failed to enable USB");
		//		return;
	}

	LOG_INF("Start heartbeat");

	k_thread_create(&blink_thread,
	  tstack[0], HEARTBEAT_STACK_SIZE,
	  (k_thread_entry_t)common_heartbeat,
	  NULL, NULL, NULL,
	  HEARTBEAT_THREAD_PRIORITY, 0, K_NO_WAIT);

	uart_irq_callback_set(cdc_dev, CDC_interrupt_handler);

	/* Enable rx interrupts */
	uart_irq_rx_enable(cdc_dev);

#define MY_DEV_PRIO 2

	LOG_INF("Connecting DMA2 Stream 0 IRQ %d for ADC", DMA2_Stream0_IRQn);
	IRQ_CONNECT(DMA2_Stream0_IRQn, MY_DEV_PRIO, sampler_isr, NULL, 0);

	LOG_INF("Connecting DMA2 Stream 2 IRQ %d for SPI modulator", DMA2_Stream2_IRQn);
	IRQ_CONNECT(DMA2_Stream2_IRQn, MY_DEV_PRIO, spi_dma_isr, NULL, 0);

	LOG_INF("Connecting DMA1 Stream 7 IRQ %d for PWM modulator", DMA1_Stream7_IRQn);
	IRQ_CONNECT(DMA1_Stream7_IRQn, MY_DEV_PRIO, pwm_dma_isr, NULL, 0);

	LOG_INF("Start transceiver thread");

	k_thread_create(&transceiver_thread,
			transceiver_stack[0], TRANSCEIVER_STACK_SIZE,
			(k_thread_entry_t)board_transceiver,
			NULL, NULL, NULL,
			TRANSCEIVER_THREAD_PRIORITY, 0, K_NO_WAIT);
	
	board_set_tx_freq(1200); /* set to a reference frequency */

	Init_TIM2();
	Init_TIM3();

	LOG_INF("Setting up DMA");
	
	adc = init_adc();
	if (!adc) {
	  LOG_ERR("ADC not set up properly");
	}

	LOG_INF("Hooking up ADC to IRQ %d", ADC1_IRQn);
	IRQ_DIRECT_CONNECT(ADC1_IRQn, MY_DEV_PRIO, adc_isr, 0);

	/* The Zephyr ADC APIs only support ADC conversion triggered by
	 * software.  Override this to set triggering from TIM2.
	 */
	{
		struct adc_stm32_cfg {
		  ADC_TypeDef *base;
		};
		const struct adc_stm32_cfg *config = adc->config;
		ll_adc = (ADC_TypeDef *)config->base;
		LOG_INF("Override ADC 0x%p configuration", ll_adc);
		LL_ADC_Disable(ll_adc);
		k_msleep(10);
		LL_ADC_SetChannelSamplingTime(ll_adc, LL_ADC_CHANNEL_1, LL_ADC_SAMPLINGTIME_144CYCLES);
		LL_ADC_DisableIT_JEOS(ll_adc); /* Don't interrupt on end of conversion */
		LL_ADC_EnableIT_OVR(ll_adc); /* Interrupt on overrun so we can reset it */
		LL_ADC_REG_SetDMATransfer(ll_adc, LL_ADC_REG_DMA_TRANSFER_UNLIMITED);
		// When the ADC isn't being used, it's set for
		// "software start" (which we don't do, so it doesn't
		// trigger).  When the ADC is being used during
		// reception, the trigger will be set to TIM2.
		LL_ADC_REG_SetTriggerSource(ll_adc, LL_ADC_REG_TRIG_EXT_TIM2_TRGO);
		LL_ADC_REG_StartConversionExtTrig(ll_adc, LL_ADC_REG_TRIG_EXT_RISING);
		// Disable sequencer scanning - force conversion of
		// only 1 channel.  Set channel 1 as the one to
		// convert.
		LL_ADC_REG_SetSequencerLength(ll_adc, LL_ADC_REG_SEQ_SCAN_DISABLE);
		LL_ADC_REG_SetSequencerRanks(ll_adc, LL_ADC_REG_RANK_1, LL_ADC_CHANNEL_1);
		k_msleep(10);
		LL_ADC_ClearFlag_OVR(ADC1);
		LOG_INF("Back from override");
	}

	LOG_INF("Enabling DMA2 Stream0 IRQ%d", DMA2_Stream0_IRQn);
	irq_enable(DMA2_Stream0_IRQn);
	LOG_INF("Enabling DMA2 Stream2 IRQ%d", DMA2_Stream2_IRQn);
	irq_enable(DMA2_Stream2_IRQn);
	LOG_INF("DMA2 IRQs enabled");
	LOG_INF("Enabling DMA1 Stream7 IRQ%d", DMA1_Stream7_IRQn);
	irq_enable(DMA1_Stream7_IRQn);
	LOG_INF("DMA1 IRQ enabled");
	k_msleep(100);
	LOG_INF("Running...");
}

void Set_ADC_sampling_speed(bool fast) {
  if (fast) {
    LL_ADC_SetChannelSamplingTime(ll_adc, LL_ADC_CHANNEL_1, LL_ADC_SAMPLINGTIME_15CYCLES);
    LOG_INF("ADC set to 15-cycle sampling");
  } else {
    LL_ADC_SetChannelSamplingTime(ll_adc, LL_ADC_CHANNEL_1, LL_ADC_SAMPLINGTIME_144CYCLES);
    LOG_INF("ADC set to 144-cycle sampling");
  }
}

/* We want a pseudo-random number generator which is seeded in a way
 * which nearly guaranteed a different sequence on each boot-up.
 * Since the STM32F103 doesn't have a full-fledged random entropy source
 * of its own, we'll fake it by seeding the PRNG the first time we
 * use it (when doing P-persistence for the first packet we're about
 * to transmit) using the CPU cycle counter as part of the seed.  For
 * the rest of the seed we'll use the most-recently-digitized samples
 * from the transceiver.
 */
uint32_t board_get_random32(void) {
  static bool random_initialized = false;
  if (!random_initialized) {
    xoshiro128_init(sample_sets.set_0[0], sample_sets.set_0[10],
		    sample_sets.set_0[20], k_cycle_get_32());
    /* Whiten the seed bits */
    xoshiro128();
    xoshiro128();
    xoshiro128();
    xoshiro128();
    random_initialized = true;
  }
  return xoshiro128();
}

void board_enable_rx(void) {
  LOG_DBG("Enabling RX");
  Setup_DMA_for_sampler();
  LL_DMA_EnableStream(DMA2, LL_DMA_STREAM_0);
  // Enable timer triggering of the ADC
  LL_ADC_REG_SetTriggerSource(ll_adc, LL_ADC_REG_TRIG_EXT_TIM2_TRGO);
  // Enable ADC
  LL_ADC_Enable(ll_adc);
}

void board_disable_rx(void){
  LOG_DBG("Disabling RX");
  LL_ADC_Disable(ll_adc);
  // Disable timer-based triggering of the ADC
  LL_ADC_REG_SetTriggerSource(ll_adc, LL_ADC_REG_TRIG_SOFTWARE);
  LL_DMA_DisableStream(DMA2, LL_DMA_STREAM_0);
}

// Crudely fill the transmit buffers with half-scale.
static void FillModulatorBuffers(void) {
  int32_t offset = 0;
  while (offset < MODULATOR_BUFFER_BYTES) {
    *(uint32_t *)&modulator_buffer_0[offset] = 0x7F;
    *(uint32_t *)&modulator_buffer_1[offset] = 0x7F;
    offset += sizeof(uint32_t);
  }
}

#if 0
static void dump_stuff() {
  LOG_INF("DMA1      at 0x%08X", (unsigned int) DMA1);
  LOG_INF("DMA1 LISR at 0x%08X", DMA1->LISR);
  LOG_INF("DMA1 HISR at 0x%08X", DMA1->HISR);
  LOG_INF("DMA1      at 0x%08X", (unsigned int) DMA1);
  LOG_INF("Stream7   at 0x%08X", (unsigned int) DMA1_Stream7);
  LOG_INF("Stream7   CR 0x%08X", DMA1_Stream7->CR);
  LOG_INF("Stream7   CR 0x%08X", DMA1_Stream7->CR);
  LOG_INF("Stream7 NDTR 0x%08X", DMA1_Stream7->NDTR);
  LOG_INF("Stream7  PAR 0x%08X", DMA1_Stream7->PAR);
  LOG_INF("Stream7 M0AR 0x%08X", DMA1_Stream7->M0AR);
  LOG_INF("Stream7 M1AR 0x%08X", DMA1_Stream7->M1AR);
  LOG_INF("Stream7 NDTR 0x%08X", DMA1_Stream7->NDTR);
  LOG_INF("Stream7  FCR 0x%08X", DMA1_Stream7->FCR);
  LOG_INF("TIM2    DIER 0x%08X", TIM2->DIER);
  LOG_INF("TIM2     CNT 0x%08X", TIM2->CNT);
  LOG_INF("TIM2     ARR 0x%08X", TIM2->ARR);
}
#endif

void board_enable_tx(void){
  LOG_INF("Enabling TX, modulator %d", tnc_state.modulator);
  board_set_tx_freq(1200);
  scaled_sine_cycles = 0;
  tnc_state.modulator_transmitting = tnc_state.modulator;
  switch (tnc_state.modulator_transmitting) {
  case PWM_MODULATOR:
  case G3RUH_PWM_MODULATOR:
  default:
    // Configure the pin for the counter-compare output.
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_7, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_0_7(GPIOA, LL_GPIO_PIN_7, LL_GPIO_AF_2);  // Alternate Function 2 - PWM output from TIM2 counter
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_7, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_7, LL_GPIO_OUTPUT_PUSHPULL);
    modulator_buffer_bytes_active = MODULATOR_BUFFER_BYTES;  // for now
    modulator_buffer_0 = modulator_buffers;
    modulator_buffer_1 = modulator_buffers + modulator_buffer_bytes_active;
    FillModulatorBuffers();
    Setup_DMA_for_PWM_modulator();
    if (tnc_state.modulator_transmitting == PWM_MODULATOR) {
      Set_TIM2_rate(OUTPUT_SAMPLES_PER_SECOND);
    } else {
      Set_TIM2_rate(BAUDRATE_G3RUH * G3RUH_modulator_cache.samples_per_baud);
    }
    LOG_INF("Enabling TIM2 update");
    LL_TIM_EnableIT_UPDATE(TIM2);
    LOG_INF("Enabling DMA1 stream 7");
    LL_DMA_EnableStream(DMA1, LL_DMA_STREAM_7);
    break;
  case DELTA_SIGMA_MODULATOR:
    // Enable the SPI MOSI function.  Optionally, also enable the
    // SPI clock (allows checking the clock rate reliably during
    // board bringup).
    LOG_DBG("Switch PA7 to MOSI");
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_7, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_0_7(GPIOA, LL_GPIO_PIN_7, LL_GPIO_AF_5);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_7, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_7, LL_GPIO_OUTPUT_PUSHPULL);
#if SHOW_DELTA_SIGMA_CLOCK
    LOG_DBG("Switch PA5 to SCK");
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_5, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_0_7(GPIOA, LL_GPIO_PIN_5, LL_GPIO_AF_5);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_5, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_5, LL_GPIO_OUTPUT_PUSHPULL);
#endif
    // Configure SPI appropriately
    LOG_DBG("Configure SPI");
    LL_SPI_Disable(SPI1);
    LL_SPI_SetMode(SPI1, LL_SPI_MODE_MASTER);
    LL_SPI_SetStandard(SPI1, LL_SPI_PROTOCOL_TI); // Doesn't matter much, simplifies setup
    LL_SPI_SetBaudRatePrescaler(SPI1, LL_SPI_BAUDRATEPRESCALER_DIV64); // 1.5 Mbit/second
    LL_SPI_SetTransferDirection(SPI1, LL_SPI_HALF_DUPLEX_TX);
    LL_SPI_SetDataWidth(SPI1, LL_SPI_DATAWIDTH_8BIT);
    LL_SPI_DisableCRC(SPI1);
    LL_SPI_SetNSSMode(SPI1, LL_SPI_NSS_SOFT);
    LL_SPI_SetTransferBitOrder(SPI1, LL_SPI_MSB_FIRST);
    // Reset and then set up the DMA channel for SPI, pointing it initially to the IDLE data.
    LOG_DBG("Setup DMA");
    Setup_DMA_for_SPI_modulator();
    LL_DMA_EnableStream(DMA2, LL_DMA_STREAM_2);
    // Release the Kraken
    phase_index = 0;
    LOG_DBG("Enable SPI");
    LL_SPI_EnableDMAReq_TX(SPI1);
    LL_SPI_Enable(SPI1);
    LOG_DBG("SPI enabled");
    break;
  }
}

void board_disable_tx(void){
  LOG_DBG("Disabling TX");
  switch (tnc_state.modulator_transmitting) {
  case PWM_MODULATOR:
  default:
    LL_DMA_DisableStream(DMA1, LL_DMA_STREAM_7);
    LL_TIM_DisableIT_UPDATE(TIM2);
    irq_disable(TIM2_IRQn);
    LL_TIM_OC_SetCompareCH2(TIM3, 128);
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_7, LL_GPIO_MODE_INPUT);
    break;
  case DELTA_SIGMA_MODULATOR:
    // Disable SPI
    LL_SPI_Disable(SPI1);
    LL_SPI_DisableDMAReq_TX(SPI1);
    // Abort DMA and wait for it to finish
    LL_DMA_DisableStream(DMA2, LL_DMA_STREAM_0);
    k_msleep(1); // Do better!
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_7, LL_GPIO_MODE_INPUT);
#if SHOW_DELTA_SIGMA_CLOCK
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_5, LL_GPIO_MODE_INPUT);
#endif
    // Clear any DMA errors
    break;
  }
  set_tx_busy_led(0);
}

void board_set_tx_freq(uint32_t hz) {
  scaled_sine_cycles_per_output_sample = (hz << 16) / OUTPUT_SAMPLES_PER_SECOND;
}

static const uint32_t parameter_offset =
  DT_REG_ADDR_BY_IDX(DT_ALIAS(flashblock), 0);
static const uint32_t parameter_total_size =
  DT_REG_SIZE_BY_IDX(DT_ALIAS(flashblock), 0);
static const uint32_t parameter_chunk_size =
  DT_PROP(ZEPHYR_USER_NODE, parameter_flash_chunk_size);

static bool scan_for_parameters(struct saved_parameters *latest,
				int32_t *free) {
  bool result = false;
  uint32_t most_recent_revision = 0;
  *free = -1;
  LOG_INF("Looking for saved parameters in flash");
  for (int offset = parameter_offset;
       offset < parameter_offset + parameter_total_size;
       offset += parameter_chunk_size) {
    LOG_DBG("Probing at offset %d", offset);
    // To allow for some amount of reverse compatibility (older
    // firmware reading larger parameter blocks saved by newer
    // firmware) we'll read more than we "understand".
    struct {
      struct saved_parameters sp;
      uint8_t padding[128];
    } buffer;
    LOG_DBG("Reading from flash, offset %d", offset);
    memset(&buffer, 0, sizeof(buffer));
    int err = flash_read(flash_dev, offset, &buffer, sizeof(buffer));
    if (err) {
      LOG_ERR("Flash read at %d returned %d", offset, err);
      return result;
    }
    LOG_DBG("Found magic 0x%08x size %d version %d revision %d checksum 0x%04x",
	    buffer.sp.magic, buffer.sp.sizeof_me, buffer.sp.version, buffer.sp.revision,
	    buffer.sp.checksum);
    uint32_t checksum = buffer.sp.checksum;
    buffer.sp.checksum = 0xFFFFFFFF;
    if (buffer.sp.magic == SAVED_PARAMETER_MAGIC &&
	buffer.sp.sizeof_me > 5 * sizeof(uint32_t) &&
	buffer.sp.sizeof_me <= sizeof(buffer) &&
	buffer.sp.revision > most_recent_revision &&
	checksum == crc32_ieee((uint8_t *) &buffer.sp,
			       buffer.sp.sizeof_me)) {
      LOG_DBG("Found saved parameter revision %d at offset %d",
	      buffer.sp.revision, offset);
      if (latest) {
	*latest = buffer.sp;
      }
      most_recent_revision = buffer.sp.revision;
      result = true;
    } else if (*free < 0) {
      bool is_free = true;
      const uint8_t *possible = (uint8_t *) &buffer;
      for (int i = 0; is_free && i < sizeof(buffer); i++) {
	if (0xFF != *(possible + i)) {
	  is_free = false;
	  LOG_DBG(" Non-free byte 0x%02X at sub-offset %d\n",
		  *(possible + i), i);
	}
      }
      if (is_free) {
	LOG_DBG("Found a free block at %d", offset);
	*free = offset;
      }
    }
  }
  return result;
}

bool board_load_saved_parameters(void) {
  struct saved_parameters most_recent;
  int32_t free;
  bool result = false;
  memset(&most_recent, 0, sizeof(most_recent)); // Keep the compiler happy
  bool found = scan_for_parameters(&most_recent, &free);
  if (found) {
    LOG_INF("Loading saved parameter, version %d revision %d",
	    most_recent.version, most_recent.revision);
    load_saved_parameters(&most_recent);
    result = true;
  }
  return result;
}

bool board_save_parameters(void) {
  struct saved_parameters sp;
  memset(&sp, 0, sizeof(sp)); // Keep the compiler happy
  int32_t free;
  int32_t revision;
  bool result = false;
  bool found = scan_for_parameters(&sp, &free);
  if (found) {
    revision = sp.revision + 1;
    LOG_INF("Next revision is %d", revision);
  } else {
    revision = 1;
    LOG_INF("No saved parameters;  this revision is 1");
  }
  prepare_to_save_parameters(&sp, revision);
  LOG_INF("Saving magic 0x%08x size %d revision %d checksum 0x%08x",
	  sp.magic, sp.sizeof_me, sp.revision, sp.checksum);
  if (free >= 0) {
    LOG_DBG("It looks like a free block at offset %d", free);
  } else {
    LOG_INF("No flash blocks free, need to erase, offset %d size %d",
	    parameter_offset, parameter_total_size);
    if (flash_erase(flash_dev, parameter_offset,
		   parameter_total_size) != 0) {
      LOG_ERR("Flash erase failed!");
      return false;
    }
    LOG_DBG("Flash region erased");
    found = scan_for_parameters(NULL, &free);
    if (free >= 0) {
      LOG_DBG("Rescan found a free block at %d", free);
    } else {
      LOG_ERR("Rescan found no free block!");
      return false;
    }
  }
  LOG_DBG("Writing block at offset %d", free);
  int write_result = flash_write(flash_dev, free, &sp, sizeof(sp));
  if (write_result == 0) {
    LOG_DBG("Flash write appears to have succeeded");
    result = true;
  } else {
    LOG_ERR("Flash write returned %d", write_result);
  }
  return result;
}

// Not yet implemented
void board_enter_bootloader(void) {
}

void board_configure_modulator(struct modulator_state *state,
			       struct modulator_cache *cache) {
  if (cache->bursts_are_in_float_format) {
    LOG_DBG("Remapping modulator data");
    for (int pattern = 0; pattern < 1 << G3RUH_SPAN_IN_BAUDS; pattern++) {
      for (int index = 0; index < G3RUH_SAMPLES_PER_BAUD; index++) {
	int32_t new = (cache->bursts[pattern].samples[index].float_format + 1.0f) * 127;
	cache->bursts[pattern].samples[index].int_format = (new << 16) | new;
      }
      cache->bursts_are_in_float_format = false;
    }
  }
  size_t buffer_size = MODULATOR_BUFFER_BYTES;
  int32_t sample_rate, bytes_per_sample;
  switch (tnc_state.modulator) {
  case PWM_MODULATOR:
  default:
    sample_rate = OUTPUT_SAMPLES_PER_SECOND;
    bytes_per_sample = sizeof(int32_t);
    break;
  case DELTA_SIGMA_MODULATOR:
    sample_rate = BAUDRATE;
    bytes_per_sample = noiseshaping.bytes_per_baud;
    break;
  case G3RUH_PWM_MODULATOR:
    sample_rate = BAUDRATE_G3RUH * G3RUH_modulator_cache.samples_per_baud;
    bytes_per_sample = sizeof(int32_t);
    break;
  }
  LOG_INF("Computing output buffer size - max possible is %d", buffer_size);
  LOG_INF("  Computed %d samples/second, %d byte per sample", sample_rate, bytes_per_sample);
  int32_t hz;
  adjust_buffer_size(sample_rate, bytes_per_sample, &buffer_size, NULL, &hz);
  LOG_INF("  Computed output buffer size %d, %d hz", buffer_size, hz);
  // Save this for actual TX
  modulator_buffer_bytes_active = buffer_size;
}
