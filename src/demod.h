/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_DEMOD
# define _H_DEMOD

#include <stdint.h>
#include <stdbool.h>

#include "board.h"
#include "hdlc.h"

// The G3RUH demodulator deals with time in terms of fractional
// samples... 24 bit integer, 8-bit fraction.  Converting back
// to integer always rounds down (backwards in time).  The
// same data type can be used for general fixed-point scaling
// in the demodulator.

typedef int32_t demod_fraction;

#define INT32_TO_SCALED(i) (i << 8)
#define FLOAT_TO_SCALED(f) ((int32_t) (f * 256.0f))
#define SCALED_TO_INT32(s) (s / 256)
#define SCALED_TO_FLOAT(s) (((float) s) / 256.0f)

#define IIR_FILTER(current, new, damping) (((current) * ((damping) - 1) + (new)) / (damping))

#define JITTER_BUCKETS 32

// Private state for a demodulator.  Currently optimized
// for the G3RUH demodulator.
struct demodulator_state {
  // This demodulator maintains its own idea of the signal mean,
  demod_fraction mean;
  // One baud may not be an integral number of samples.  We maintain
  // both a nominal value, and a value which has been adjusted to track
  // the actual incoming signal.
  demod_fraction samples_per_baud;
  demod_fraction adjusted_samples_per_baud;
  // We look at the waveform several times per baud, to detect
  // zero crossings.  We do _not_ look at every pair of samples;
  // this would be too expensive.
  demod_fraction samples_per_zero_crossing_check;
  demod_fraction previous_look_time;
  demod_fraction next_look_time;
  input_sample previous_look_value;
  bool previous_look_value_valid;
  // When do we expect the next zero crossing to occur, based
  // on when we saw the last one?
  demod_fraction next_zero_crossing_expected;
  // How many zero crossings have been detected during the current
  // baud cycle (between samplings)?
  int32_t zero_crossings;
  // How many zero crossings have been missed (not present) since the
  // last one we actually saw?
  int32_t zero_crossings_missed;
  // What's the average rate of zero crossings?  It should converge
  // to roughly 0.5 during packet reception.
  demod_fraction zero_crossing_rate;
  // What are the upper and lower limits of zero-crossing rate which
  // we will consider as being consistent to being locked on a
  // signal?
  demod_fraction lower_zero_crossing_rate;
  demod_fraction upper_zero_crossing_rate;
  // When do we expect to next sample the waveform (one-half baud
  // time after the zero crossing)
  demod_fraction next_sample_time;
  // How much mismatch between the expected and observed zero-
  // crossing times will we tolerate, before we decide that
  // this indicates a possible loss of lock and not just jitter?
  demod_fraction zero_crossing_jitter_limit;
  // How much do we shift the "next expected zero crossing time"
  // based on whether our most recent expectation was ahead or
  // behind reality?  (That is, how fast does the DPLL hunt for
  // a lock?).  With roughly 50 samples per baud, and roughly
  // one zero crossing per two baud on average, a value of 1.0
  // allows us to track up to 1% frequency error.  Smaller
  // values reduce the DPLL's capture range, and reduce sampling-
  // time jitter proportionally.
  demod_fraction zero_crossing_shift_rate;
  // What's our long-term time mismatch?  It should converge to very
  // close to zero if our DPLL clock, and the sender's clock are
  // well matched.  This is updated on each zero-crossing.
  demod_fraction average_mismatch;
  // What's our long-term average jitter? This is updated on each zero-crossing.
  demod_fraction average_jitter;
  // Data for histogramming jitter
  int32_t jitter[JITTER_BUCKETS];
  // What's the long-term amplitude variation (AM) in the center-of-
  // symbol sampling?
  int32_t average_sampling_value;
  int32_t average_sampling_AM;
  // Descrambler shift register
  uint32_t descrambler;
  // Post-descrambler bit counts
  int output_bit_count[2];
  // Demodulator lock state
  bool pll_locked;
  // Fast-lock counters and modulus.
  int32_t fast_lock_countdown;
  int32_t fast_lock_period;
};


void InitDemodulator(void);  // Once, at startup
void ReInitDemodulator(void); // Once at the beginning of each RX operation
void Demodulate(input_sample *samples, uint32_t sample_count);
void Demodulator_Statistics(void);
void G3RUH_Bit_Counts(void);

#endif
