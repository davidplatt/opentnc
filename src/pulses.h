/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_PULSES
#define _H_PULSES

#include <stdint.h>

struct pulse {
  const char *name;
  uint32_t bauds;
  uint32_t samples_per_baud;
  float samples[]; // array size is .bauds * samples_per_baud
};

#endif
