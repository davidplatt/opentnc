/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stdio.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/ring_buffer.h>

#include <zephyr/logging/log.h>

#include "board.h"
#include "demod.h"
#include "globals.h"
#include "kiss.h"
#include "xoshiro128.h"

struct rx_push_state rx_push;
struct rx_pull_state rx_pull;
struct tx_push_state tx_push;

sys_dlist_t rx_packets = SYS_DLIST_STATIC_INIT(&rx_packets);
sys_dlist_t tx_packets = SYS_DLIST_STATIC_INIT(&tx_packets);
sys_dlist_t kiss_packets = SYS_DLIST_STATIC_INIT(&kiss_packets);

#ifdef CONFIG_HOST_INPUTBUFFER_SIZE
# define HOST_INPUTBUFFER_SIZE CONFIG_HOST_INPUTBUFFER_SIZE
#else
# define HOST_INPUTBUFFER_SIZE 128
#endif

RING_BUF_DECLARE(host_inputbuffer, HOST_INPUTBUFFER_SIZE);

#ifdef CONFIG_HOST_OUTPUTBUFFER_SIZE
# define HOST_OUTPUTBUFFER_SIZE CONFIG_HOST_OUTPUTBUFFER_SIZE
#else
# define HOST_OUTPUTBUFFER_SIZE 1024
#endif

RING_BUF_DECLARE(host_outputbuffer, HOST_OUTPUTBUFFER_SIZE);

struct sharebuffer console_sharebuffer = {
  .buffer_size = SHAREBUFFER_SIZE,
};

encoded_callsign nocall;
encoded_callsign wide1;
encoded_callsign wide2;

int32_t demod_samples_processed;

/* Default values for txdelay/p/slot_time/txtail are set to match
 * those recommended by SCC-ARES-RACES.ORG for use in the Santa Clara
 * County ARES/RACES BBS network.
 */

struct tnc_state tnc_state;

const struct tnc_state default_tnc_state =
  {
    .kiss_mode = false,
    .kiss_auto = false,
    .echo = true,
    .auto_newline = true,
    .bkondel = true,
    .monitor = false,
    .hdlc_loopback = false,
    .adc_loopback = false,
    .poll_on_last = true,
    .conok = false,
    .conmode_is_transparent = false,
    .cpactime = false,
    .digipeating = false,
    .sendpac = 0x0D, // carriage return
    .canline = 0x18, // control-X
    .command = 0x03, // control-C
    .delete = 0x08, // control-H, backslace
    .paclen = 256,
    .maxframe = 2,
    .pactime = 10, // == 100 milliseconds
    .pactime_mode = PACTIME_AFTER,
    .audio_carrier_threshold = 200, // this may need tuning
    .carrier_detect_mode = CARRIER_DETECT_HDLC,
    .t1 = 6, // 6 seconds
    .t2 = 5, // .5 seconds (100-millisecond units)
    .t3 = 30, // 5 minutes (10-second units)
    .carrier_effect_on_t1 = 5,
    .outpost_delay = 0,
    .radio_baud_rate = RADIO_BAUD_1200,
    .ax25_version = AX25_VERSION_20,
    .p = 63,
    .slot_time = 10,
    .modulator = PWM_MODULATOR, // DELTA_SIGMA_MODULATOR,
    .demodulator = BIQUAD_FILTER_DEMODULATOR,
    .retries = 9,
    .initial_syncs = 10,
    .txdelay = 40,
    .drop_rx = 0,
    .drop_tx = 0,
    .operating_mode = OPERATING_MODE_NORMAL,
    .aprs_digi_mode = APRS_DIGI_MODE_OFF,
    .ekiss_mode = EKISS_OFF,
    .pulse_shape = PULSE_SINC,
    .damask_mode = DAMASK_ROUND_ROBIN,
    .damask_short_quiet_time = 5, // 100-millisecond increments
    .damask_long_quiet_time = 10, // 100-millisecond increments
    .fx25_encoding = FX25_ENCODING_AUTO,
    .fx25_connection_mode = FX25_CONNECTION_OPPORTUNISTIC,
    .zero_crossing_threshold = 4,
    .zero_crossing_hysteresis = 32,
    .lock_threshold = 32,
    .minimum_zeros = 10,
    .minimum_aborts = 2,
    .txtail = 0,
    .full_duplex = 0,
    .txeq_mode = TX_EQ_FLAT,
    .txeq_peak_frequency = 5000,
    .txeq_peak_gain_centibels = 20, // 2.0 dB
};

