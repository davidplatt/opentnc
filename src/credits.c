/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stddef.h>

const char * const credits[] =
  { "Credits:",
    "  Command shell: memfault (https://github.com/memfault/interrupt)",
    "  libcorrect: Brian Armstrong (https://github.com/DavidCPlatt/libcorrect)",
    NULL
  };
