/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/ring_buffer.h>

#include "ax25.h"
#include "board.h"
#include "connection.h"
#include "console.h"
#include "converse.h"
#include "demod.h"
#include "globals.h"
#include "hdlc.h"
#include "kiss.h"
#include "packetutil.h"
#include "sharebuffer.h"

LOG_MODULE_REGISTER(converse, LOG_LEVEL_INF);

static struct {
  bool transparent;
  bool has_connection;
  bool try_sending;
  bool final_sendpac;
  bool sendpac_queued;
  bool transmit_on_timer;
  uint32_t count;
  int32_t transmit_timer_initial;
  int32_t transmit_timer;
  int32_t quiet_time;
  int32_t quiet_time_threshold;
  int32_t command_characters_queued;
} converse_state;

static void pass_through_queued_command_chars(void) {
  uint8_t c = tnc_state.command;
  while (converse_state.command_characters_queued > 0) {
    LOG_DBG("Push one queued COMMAND char");
    sharebuffer_put_byte(&console_sharebuffer, c);
    converse_state.count ++;
    converse_state.command_characters_queued --;
  }
}

// Called when we're in "quiet time" (no new characters arrived) in
// transparent mode.  Returns "true" if a transparent-mode escape
// sequence has been detected (time delay of > 1 second, exactly
// three COMMAND characters in a row without delay, and then another
// time delay of > 1 second).
static bool transparent_mode_escape(void) {
  if (converse_state.quiet_time < converse_state.quiet_time_threshold) {
    return false;
  }
  if (converse_state.command_characters_queued == 3) {
    console_printline("");
    return true;
  }
  pass_through_queued_command_chars();
  return false;
}

// Returns true if converse is to exit back to the console.
static bool process_one_character(void) {
  uint8_t c;
  (void) ring_buf_get(&host_inputbuffer, &c, sizeof(c));
  board_check_input_flow_control();
  if (c == tnc_state.command) { // control-C, usually.
    if (!converse_state.transparent) {
      sharebuffer_flush_unclaimed(&console_sharebuffer);
      return true;
    }
    // For transparent mode, the command characters are to be passed
    // through transparently unless a delay-based (Heatherington-like)
    // escape sequence is used.  If we see one or more command
    // characters appearing after some "quiet time", just count them
    // up.
    if (converse_state.command_characters_queued > 0) {
      converse_state.command_characters_queued ++;
      LOG_DBG("Another COMMAND in transparent mode");
      return false;
    }
    if (converse_state.quiet_time > converse_state.quiet_time_threshold) {
      LOG_DBG("First COMMAND in transparent mode");
      converse_state.command_characters_queued = 1;
      converse_state.quiet_time = 0;
      return false;
    }
    sharebuffer_put_byte(&console_sharebuffer, c);
    converse_state.count++;
    return false;
  }
  converse_state.quiet_time = 0;
  if (converse_state.transparent) {
    pass_through_queued_command_chars();
    converse_state.final_sendpac = false;
    console_printchar(c);
    sharebuffer_put_byte(&console_sharebuffer, c);
    converse_state.count++;
    return false;
  }
  if (c == tnc_state.sendpac) {
    console_printline("");
    sharebuffer_put_byte(&console_sharebuffer, c);
    converse_state.count++;
    if (converse_state.transmit_on_timer &&
	tnc_state.pactime_mode == PACTIME_SENDPAC) {
      converse_state.transmit_timer = converse_state.transmit_timer_initial;
      converse_state.sendpac_queued = true;
      LOG_DBG("SENDPAC in PACTIME_MODE, start timer at %d", converse_state.transmit_timer);
    } else {
      converse_state.try_sending = true;
      converse_state.final_sendpac = true;
    }
  } else if (c == tnc_state.delete) { // backspace or DEL, usually
    if (converse_state.count > 0) {
      if (tnc_state.bkondel) {
	console_printstr("\010 \010"); // backspace over last
      } else {
	console_printstr("\\");
      }
      sharebuffer_pop_from_tail(&console_sharebuffer, 1);
      converse_state.count--;
    }
  } else if (c == tnc_state.canline) { // control-X or control-U
    console_printline("\\");
    if (converse_state.count > 0) {
      sharebuffer_pop_from_tail(&console_sharebuffer, converse_state.count);
      converse_state.count = 0;
    }
  } else {
    converse_state.final_sendpac = false;
    console_printchar(c);
    sharebuffer_put_byte(&console_sharebuffer, c);
    converse_state.count++;
    if (converse_state.transmit_on_timer && tnc_state.pactime > 0 &&
	tnc_state.pactime_mode == PACTIME_AFTER) {
      converse_state.transmit_timer = converse_state.transmit_timer_initial;
      LOG_DBG("Restart PACTIME timer");
    }
  }
  return false;
}

bool converse(bool connected_mode, bool transparent) {
  LOG_INF("Entering converse");
  uint32_t start = sharebuffer_get_tail(&console_sharebuffer);
  converse_state.transparent = transparent;
  converse_state.count = 0;
  converse_state.has_connection = false;
  converse_state.try_sending = false;
  converse_state.final_sendpac = false;
  converse_state.sendpac_queued = false;
  converse_state.transmit_on_timer = tnc_state.cpactime && tnc_state.pactime != 0;
  converse_state.transmit_timer_initial = abs(tnc_state.pactime) * 10; // scale from 100-ms units to 10-ms units
  converse_state.transmit_timer = 0;
  converse_state.quiet_time = 0;
  converse_state.quiet_time_threshold = 100;
  converse_state.command_characters_queued = 0;
  while (true) {
    if (console_connection->state == CONN_CONNECTED) {
      converse_state.has_connection = true;
    }
    if (converse_state.has_connection && console_connection->state != CONN_CONNECTED) {
      sharebuffer_flush_unclaimed(&console_sharebuffer);
      LOG_INF("Connection lost");
      return true;
    }
    if (!converse_state.has_connection && console_connection->state == CONN_DISCONNECTED) {
      sharebuffer_flush_unclaimed(&console_sharebuffer);
      LOG_INF("Connection failed");
      return true;
    }
    bool reading_allowed = false;
    if (ring_buf_is_empty(&host_inputbuffer)) {
      k_msleep(10);
      converse_state.quiet_time ++;
      // Handle the special Heatherington-like delay-escape-delay
      // protocol needed when in transparent mode.
      if (converse_state.transparent && transparent_mode_escape()) {
	LOG_INF("Transparent-mode escape");
	return true;
      }
      if (converse_state.transmit_on_timer && converse_state.transmit_timer > 0 &&
	  converse_state.count > 0) {
	converse_state.transmit_timer--;
	if (converse_state.transmit_timer == 0) {
	  converse_state.try_sending = true;
	  LOG_DBG("PACTIME expired, try sending");
	}
      }
    } else if (sharebuffer_get_space_avail(&console_sharebuffer) > 4) {
      reading_allowed = true;
    } else {
      k_msleep(10);
      if (!converse_state.transparent) {
	uint8_t c;
	(void) ring_buf_peek(&host_inputbuffer, &c, sizeof(c));
	if (c == tnc_state.command) {
	  reading_allowed = true;
	}
      }
    }
    if (reading_allowed) {
      if (process_one_character()) {
	return true;
      }
    }
    if (converse_state.count >= console_connection->capabilities.paclen) {
      LOG_DBG("Packet length limit, try sending");
      converse_state.try_sending = true;
    }
    // We may need to persist at trying to send, if the last character
    // from the console was the send-packet control (e.g. RETURN). We
    // can't count on being able to flush out the buffer entirely the
    // first time through here.
    if (converse_state.final_sendpac && converse_state.count > 0) {
      LOG_DBG("SENDPAC remains, try sending");
      converse_state.try_sending = true;
    }
    if (converse_state.try_sending) {
      if (connected_mode && !converse_state.has_connection) {
	sharebuffer_flush_unclaimed(&console_sharebuffer);
	start = sharebuffer_get_tail(&console_sharebuffer);
	converse_state.count = 0;
      } else if (room_for_conn_packet(console_connection)) {
	int to_send = converse_state.count;
	if (to_send > console_connection->capabilities.paclen) {
	  to_send = console_connection->capabilities.paclen;
	}
	LOG_DBG("Claiming range: start %d count %d", start, to_send);
	uint32_t new_start = sharebuffer_claim_range(&console_sharebuffer, start, to_send);
	LOG_DBG("Appending packet");
	bool sent = append_l3_packet(console_connection, NULL, start, to_send);
	if (!sent) {
	  console_printline("Message not sent, no space?");  // should never happen
	}
	start = new_start;
	converse_state.count -= to_send;
	LOG_DBG("Resuming at %d", start);
	converse_state.transmit_timer = converse_state.transmit_timer_initial;
	converse_state.try_sending = false;
	// If we deferred sending packets after a SENDPAC, make sure we flush
	// everything out.
	converse_state.final_sendpac = converse_state.sendpac_queued;
	converse_state.sendpac_queued = false;
      }
    }
  }
}
