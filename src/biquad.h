/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stdint.h>
#include "fp32.h"

typedef struct biquad_f {
  float a1, a2, b0, b1, b2;
  float normalization_gain;
  float x1, x2, y1, y2;
  fp32_t a1_fp, a2_fp, b0_fp, b1_fp, b2_fp;
  fp32_t normalization_gain_fp, residue_fp;
  fp32_t x1_fp, x2_fp, y1_fp, y2_fp;
} biquad_f;

void dump_biquad(biquad_f *bq);

void compute_bandpass_biquad_f(biquad_f *bq, float Fs, float F0, float Q);

void compute_lowpass_biquad_f(biquad_f *bq, float Fs, float F0, float Q);

void reset_biquad_f(biquad_f *bq);

float iterate_biquad_f(biquad_f *bq, float x);

fp32_t iterate_biquad_fp(biquad_f *bq, fp32_t x);

