/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_CONSOLE
# define _H_CONSOLE

void console_printbytes(uint8_t *bytes, size_t len);
void console_printchar(char c);
void console_printstr(const char *s);
void console_printline(const char *s);  // appends return-linefeed

#endif // _H_CONSOLE
