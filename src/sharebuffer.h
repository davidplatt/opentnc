/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_SHAREBUFFER
#define _H_SHAREBUFFER

#include <stdint.h>
#include <stdbool.h>

#include <zephyr/kernel.h>

// A sharebuffer is a fairly straightforward circular buffer
// with some API extensions allowing its contents to be
// "pointed to" (conceptually) by external data structures
// such as packet I-frames.

#ifdef CONFIG_SHAREBUFFER_SIZE
# define SHAREBUFFER_SIZE CONFIG_SHAREBUFFER_SIZE
#else
# define SHAREBUFFER_SIZE 4096
#endif

struct sharebuffer {
  struct k_mutex mutex;
  uint32_t head;
  uint32_t tail;  // if head == tail, buffer is empty.
  uint32_t claimed; // Bytes between head and claimed are pointed to externally and cannot be flushed
  uint32_t buffer_size;
  uint8_t buffer[SHAREBUFFER_SIZE];
};

void sharebuffer_init(struct sharebuffer *sb);

uint32_t sharebuffer_get_tail(struct sharebuffer *sb);
uint32_t sharebuffer_get_head(struct sharebuffer *sb);
uint32_t sharebuffer_get_space_avail(struct sharebuffer *sb);
uint32_t sharebuffer_get_size(struct sharebuffer *sb);

// Functions to append data (always at the tail).  These may
// be called from an ISR, so mutex locking is unavailable...
// any threads which wish to call these must lock out
// applicable interrupts.
void sharebuffer_put_byte(struct sharebuffer *sb, uint8_t byte);
void sharebuffer_put_bytes(struct sharebuffer *sb, const uint8_t *bytes, uint32_t count);

// Move the tail backwards towards the head, "un-putting" one or
// more bytes.  The tail cannot be moved back further than the
// end of the claimed area.
void sharebuffer_pop_from_tail(struct sharebuffer *sb, uint32_t count);

uint32_t sharebuffer_get_data_avail(struct sharebuffer *sb);
void sharebuffer_get_unclaimed_data_avail(struct sharebuffer *sb, uint32_t *offset, uint32_t *count);

// Grab and consume the byte at the head
uint8_t sharebuffer_get_byte(struct sharebuffer *sb);

// Get data from a given offset, automatically wrapping around the end
// if necessary.
void sharebuffer_get_data_range(struct sharebuffer *sb, uint32_t offset, uint8_t *bytes, uint32_t count);

// Claim and unclaim bytes within the buffer.  Bytes must be claimed from the
// head forward, and released in the same order.  Bytes which have been
// claimed are immune to buffer flushing (e.g. from a control-C with data
// waiting to be sent).  Unclaiming a range (which must be at the head)
// moves the head, thus freeing the space for re-use.
//
// sharebuffer_claim_range returns the index of the first unclaimed byte.

uint32_t sharebuffer_claim_range(struct sharebuffer *sb, uint32_t offset, uint32_t count);
void sharebuffer_unclaim_range(struct sharebuffer *sb, uint32_t offset, uint32_t count);

// Flush all unclaimed data from the sharebuffer - move the tail back towards
// the head as far as possible.
void sharebuffer_flush_unclaimed(struct sharebuffer *sb);

#endif // ifdef _H_SHAREBUFFER
