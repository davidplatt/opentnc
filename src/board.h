/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_BOARD
#define _H_BOARD

#include <stdint.h>
#include <stdbool.h>
#include <zephyr/device.h>
#include <zephyr/kernel.h>

#include "globals.h"

// Provide a board-specific wrapper macro which can be used to tag
// routines as being time-critical.  For the RPi Pico, this causes
// them to be relocated into RAM at runtime.  No effect on other
// platforms.

#ifdef CONFIG_SOC_RP2040
# define TIME_CRITICAL(x) __noinline __attribute__((section(".time_critical"))) x
#else
# define TIME_CRITICAL(x) x
#endif

#define ZEPHYR_USER_NODE DT_PATH(zephyr_user)

extern const struct gpio_dt_spec heartbeat_led;
extern const struct gpio_dt_spec data_1_led;
extern const struct gpio_dt_spec hdlc_lock_led;
extern const struct gpio_dt_spec data_0_led;
extern const struct gpio_dt_spec good_packet_led;
extern const struct gpio_dt_spec tx_busy_led;
extern const struct gpio_dt_spec clip_warning_led;
extern const struct gpio_dt_spec rx_busy_led;
extern const struct gpio_dt_spec ptt_led;
extern const struct gpio_dt_spec ptt;
extern const struct gpio_dt_spec hdlc;

extern const int32_t sinetable[];
extern const int32_t sinetable_count;
extern uint32_t cycles_per_sine_step;
extern uint32_t sine_offset;

extern struct k_sem sampler_sem;

extern struct k_mutex work_area_mutex;
extern uint8_t work_area[CONFIG_WORK_AREA_SIZE];

#define BAUDRATE 1200
#define BAUDRATE_G3RUH 9600
#define LOW_TONE 1200
#define HIGH_TONE 2200
#define SLICES_PER_BIT 8
#define OVERSAMPLING 4
#define SAMPLES_PER_BAUD (SLICES_PER_BIT * OVERSAMPLING)
#define SAMPLES_PER_SECOND (SAMPLES_PER_BAUD * BAUDRATE)
#define SAMPLE_COUNT (1 << (CONFIG_ADC_BUFFER_BYTES_LOGARITHM - 1))

/* 1000 msec = 1 sec */
#define SLEEP_TIME_MS   1000
#define SLEEP_TIME_SHORT 100

// Thread priorities.  In the standard Zephyr configuration, cooperative
// threads ("real-time") are from -3 to -1, and preemptable threads
// are from 0 to 14.  Lower numbers mean higher priority, of course.

// In this design, cooperative priority are used for the transceiver
// thread, which executes the end-of-DMA service routines to handle
// incoming audio samples promptly and (on some boards) generate the
// PWM or noise-shaped waveform for transmission.  Low-level packet
// management (layer 2 and 3) runs in high-priority preemptable
// threads.  Higher-level packet management (including the current-
// packet-to-HDLC encoder) runs a preemptable thread of moderate
// priority.  The heartbeat thread is least critical and runs at a
// low priority.

#define TRANSCEIVER_THREAD_PRIORITY K_PRIO_COOP(2)
#define LAYER2_THREAD_PRIORITY K_PRIO_PREEMPT(1)
#define LAYER3_THREAD_PRIORITY K_PRIO_PREEMPT(1)
#define PACKET_THREAD_PRIORITY K_PRIO_PREEMPT(3)
#define HEARTBEAT_THREAD_PRIORITY K_PRIO_PREEMPT(5)

typedef int16_t input_sample;

// This exists to allow data-setup code to find out the size of
// one complete sample buffer.  Some architectures e.g. RP2040
// prefer or require having the buffer start on an address which
// is zero mod its size, in order to allow round-robin DMA.

struct input_sample_set {
  input_sample samples[SAMPLE_COUNT];
};

// A contiguous range of memory, large enough to hold two
// sample sets, is statically allocated by the board-specific
// code, aligned as the architecture requires.

extern struct sample_sets {
  input_sample set_0[SAMPLE_COUNT];
  input_sample set_1[SAMPLE_COUNT];
} sample_sets;

extern bool sample_set_0_ready;
extern bool sample_set_1_ready;

extern input_sample *sample_set_base;
extern input_sample *sample_set_0;
extern input_sample *sample_set_1;

extern uint32_t cycles_before;
extern int goodPacketTime;
extern uint64_t samples_processed;
extern uint32_t overruns;

extern const struct device *cdc_dev;


// Functions common to the Blue Pill (F103) and Black Pill (F411)
// boards.

void board_init(void);

// Virtual-LED values for LED_GOOD_PACKET.  The standard GPIO
// VLED interface treats these values alike and will blink the
// LED if present.  The WS2812 bindings for the VLEDs can map
// these values to different colors, if desired.

enum good_packet_led {
  GOOD_PACKET = 1,
  GOOD_PACKET_WITH_FX25 = 2
};

// Virtual-LED values for the "connected" LED.

enum conn_packet_led {
  LED_DISCONNECTED = 0,
  LED_CONNECTING = 1,
  LED_CONNECTED = 2,
  LED_CONNECTED_FX25 = 3,
  LED_DISCONNECTING = 4,
  LED_KISS_MODE = 5
};

// Virtual LED values for the PTT LED
enum ptt_led {
  PTT_OFF = 0,
  PTT_ON = 1,
  PTT_HDLC = 2,
  PTT_RAW = 3
};

// Virtual LED values for the carrier LED
enum carrier_led {
  CARRIER_OFF = 0,
  CARRIER_HDLC_1200 = 1,
  CARRIER_HDLC_9600 = 2,
  CARRIER_AUDIO = 3,
  CARRIER_SQUELCH_OPEN = 4
};

void board_init_default_tnc_state(void);

enum carrier_led board_report_carrier_state(void);

void generic_push_to_radio(void);

void board_push_to_radio(void);

void board_set_hdlc(uint32_t bit);

void board_set_tx_freq(uint32_t hz);

uint32_t board_get_random32(void);

bool board_detecting_carrier(void);

void board_set_hardware(uint8_t *kiss_command_payload, uint32_t len);

bool board_load_saved_parameters(void);
bool board_save_parameters(void);

// The "common" functions perform any general setup required
// for the operation, and then call the board_() equivalents.
void common_enable_rx(void);
void common_enable_tx(void);
void common_disable_rx(void);
void common_disable_tx(void);

void board_enable_rx(void);
void board_disable_rx(void);
void board_enable_tx(void);
void board_disable_tx(void);
void board_enable_ptt(void);
void board_disable_ptt(void);

void board_kick_host_tx(void);

void board_enter_bootloader(void);

size_t board_get_host_output_buffer_avail();

// Check to see whether it's appropriate to unthrottle input
// from the host.  If so, do so.
void board_check_input_flow_control(void);

// Returns true if the "UART" output to the host has a
// backlog, and we can't be certain that we can process one
// maximum-sized input frame without having to drop it.
bool board_check_output_flow_control(void);

void CDC_interrupt_handler(const struct device *dev, void *user_data);

// The board-specific file allocates stack space for the heartbeat
// thread, and starts it up at common_heartbeat().  common_heartbeat()
// runs at approximately a 60-Hz rate, performing those aspects of
// board keep-alive which are architecturally board-independent
// (e.g. handling the period and hang-time of virtual LEDs) and then
// calling the board-specific heartbeat.

void common_heartbeat(void *arg1, void *arg2, void *arg3);
void board_heartbeat(uint32_t ticks);

void board_transceiver(void *arg1, void *arg2, void *arg3);
void board_modulator(void); // optional

void hexdump(void *p, char *name, size_t len);

// Some demodulators e.g. G3RUH will require some board-specific
// configuration setup, since their operation depends on specifics
// of the ADC sample rate and perhaps other things as well.
// This function will be called each time RX starts up.
struct demodulator_state;
void board_configure_demodulator(struct demodulator_state *state);

// Likewise, for modulators.
struct modulator_state;
struct modulator_cache;
void common_configure_modulator(void);
void board_configure_modulator(struct modulator_state *state,
			       struct modulator_cache *cache);

// Quiesce and de-quiesce the transceiver.  Used when it's
// necessary to shift the transmit or receive hardware between
// incompatible modes.
void quiesce_transceiver(void);
void unquiesce_transceiver(void);

// Utility function to convert a floating-point number in
// the range of [0,1] into a best-approximation rational
// number, given an upper limit on the numerator and
// denominator values.
struct rational {
  uint32_t numerator;
  uint32_t denominator;
};

struct rational rationalize(float f, uint32_t limit);

// Transfers parameters back and forth between the global
// tnc_state, and a saved_parameters block.

// Parameters which can be saved in flash.
// Magic number remains constant.
// Version number starts at 1, increases by 1 with each new
// structure member added (always at the end, but before the
// FCS.  Never remove any obsolete member!)
// The checksum is computed over the body of the structure,
// with the checksum field having been initialized to 0xFFFFFFFF.
// The sizeof_me field reflects the size of the structure (and
// hence the amount of data checksummed).

#define SAVED_PARAMETER_MAGIC 0xD1917564U

struct saved_parameters {
  uint32_t checksum;  // IEEE 802.3 CRC32
  uint32_t magic;
  uint32_t sizeof_me;
  uint32_t version; // Version number of the struct
  uint32_t revision;
  // Version 1 entries start here
  encoded_callsign mycall;
  encoded_callsign unproto_call;
  bool bkondel;
  bool conok;
  bool conmode_is_transparent;
  bool cpactime;
  bool kiss_auto;
  int32_t sendpac;
  int32_t canline;
  int32_t command;
  int32_t delete;
  int32_t paclen;
  int32_t maxframe;
  int32_t pactime;
  enum pactime_mode pactime_mode;
  int32_t t1; // 1-second increments
  int32_t t2; // 100-millisecond increments
  int32_t t3; // 10-second increments
  int32_t carrier_effect_on_t1;
  int32_t p;
  int32_t slot_time;
  int32_t modulator;
  int32_t demodulator;
  int32_t retries;
  int32_t initial_syncs;
  int32_t txdelay;
  enum damask_mode damask_mode;
  int32_t damask_short_quiet_time; // 100-millisecond increments
  int32_t damask_long_quiet_time; // 100-millisecond increments
  enum fx25_encoding fx25_encoding;
  // Version 1 entries end here
  enum fx25_connection_mode fx25_connection_mode;
  enum carrier_detect_mode carrier_detect_mode;
  int32_t audio_carrier_threshold;
  int32_t txtail;
  enum operating_mode operating_mode;
  // Version 2 entries end here
  encoded_callsign echoback_call;
  bool digipeating;
  enum aprs_digipeating_mode aprs_digi_mode;
  // Version 3 entries end here
  enum ekiss_mode ekiss_mode;
  enum radio_baud_rate radio_baud_rate;
  enum pulse_shape pulse_shape;
};

void load_saved_parameters(struct saved_parameters *sp);

void prepare_to_save_parameters(struct saved_parameters *sp, int32_t revision);

#endif
