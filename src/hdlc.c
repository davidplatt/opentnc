/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

/*
  Low-level HDLC logic - encoder with zero-stuffing, decoder with
  zero-stripping, clock synchronization, flag recognition, inbound
  and outbound FCS.
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <stdint.h>
#include <string.h>

#include <zephyr/logging/log.h>

#include "board.h"
#include "console.h"
#include "damask.h"
#include "globals.h"
#include "hdlc.h"
#include "kiss.h"
#include "layer2.h"

LOG_MODULE_REGISTER(hdlc, LOG_LEVEL_INF);

enum bitEvaluatorModes {
  edgeComparison = 0,       /* use result of rising / falling edge */
  midpointComparison = 1,   /* use single sample at mid-bit */
  votingComparison = 2      /* majority vote of 4 samples around mid-bit time */
};

/*
  Action table for the bit slicer.  This table is indexed by the
  four most recently-received cells (most recently received
  in the LSB).

  It's used for two purposes.  The entries tagged with EDGE_PRESENT
  (HHLL and LLHH) indicate a clean transition between states;
  this synchronizes the bit slicer, with the cell data containing
  the samples for sub-bits 0 1 2 3.

  At sub-bit time 5 (nominally right in the middle of the bit time)
  we decide whether we're most likely looking at a high tone or
  a low tone, based on the other tags in the table.  We make
  the actual decision based on the CHOOSE_HIGH flag (which must
  be the LSB in the table entries as we use it as a 1-bit mask).

  The remaining flags indicate how "certain" we are of our
  choice.  Our fast-path code doesn't actually look at these
  flags, but we *could* save the locations of uncertain bits
  in a packet, and try fiddling with the least-certain ones
  and recalculating the FCS if the original packet fails
  the FCS syndrome calculation.  "For future implementation".

  UNANIMOUS means HHHH or LLLL - nice clean signal.

  MAJORITY means three of high and one of low, or vice versa.

  BIAS means HLLH or LHHL;  we can infer that the sub-bit
  before these four was of the same sense as the two in the
  middle (otherwise we would have seen an EDGE_PRESENT
  condition and resynchronized) so we vote that way.

  CHATTER means HLHL or LHLH - we can't infer anything about
  the previous sub-bit from this, so it's really a crap-shoot.

*/

#define CHOOSE_HIGH   0x01
#define UNANIMOUS     0x02
#define MAJORITY      0x04
#define BIAS          0x08
#define CHATTER       0x10
#define EDGE_PRESENT  0x20


static const uint32_t slicerTable[16] =
  {
    UNANIMOUS,            /* LLLL */
    MAJORITY,             /* LLLH */
    MAJORITY,             /* LLHL */
    EDGE_PRESENT+CHOOSE_HIGH, /* LLHH */
    MAJORITY,             /* LHLL */
    CHATTER,              /* LHLH */
    CHOOSE_HIGH+BIAS,     /* LHHL with a H before it */
    CHOOSE_HIGH+MAJORITY, /* LHHH with a H before it */
    MAJORITY,             /* HLLL with a L before it */
    BIAS,                 /* HLLH with a L before it */
    CHOOSE_HIGH+CHATTER,  /* HLHL */
    CHOOSE_HIGH+MAJORITY, /* HLHH */
    EDGE_PRESENT,         /* HHLL */
    CHOOSE_HIGH+MAJORITY, /* HHLH */
    CHOOSE_HIGH+MAJORITY, /* HHHL */
    CHOOSE_HIGH+UNANIMOUS /* HHHH */
  };

struct hdlc_rcv hdlc_context_flat;
struct hdlc_rcv hdlc_context_highboost;
struct hdlc_rcv hdlc_context_highcut;

void InitHDLC(struct hdlc_rcv *context) {
  memset(context, 0, sizeof(*context));
  context->bitCaptureTime = 5;    /* sub-bit time on which to sample */
  context->maxLockConfidence = 256;
  context->transitionTimeout = 8 * SLICES_PER_BIT;  /* eight bit times @ 8x oversampling */
  context->bitEvaluatorMode = votingComparison;
  context->r_s_enabled = true;
  context->priority = FRAME_PRIORITY_HDLC;
}

bool Flag(struct hdlc_rcv *context) {
  bool result = false;
  LOG_DBG("%lld: FLAG, FCS is 0x%04X", context->timestamp, context->fcs);
  if (context->fcsGood) {
    if (context->loopback && tnc_state.kiss_mode) {
      LOG_DBG("Drop loopback packet in KISS mode");
    } else {
      LOG_DBG("Accept packet");
      // Get a buffer large enough for the frame, minus the FCS.
      ax25buffer *b = ax25buffer_alloc(AX25_FRAME, context->bytes_received - 2);
      if (b) {
	b->priority = context->priority;
	b->time_start = context->time_start;
	b->time_end = context->timestamp;
	b->fx25 = context->report_fx25;
	b->loopback = context->loopback;
	ax25buffer_put_bytes(b, context->bytes, context->bytes_received - 2); /* back out the FCS */
	kiss_maybe_append_crc16(b);  // Set the command byte appropriately
	damask_examine_incoming(b); // Snoop on it
	//	hexdump(b->data, "HDLC frame", ax25buffer_byte_count(b));
	layer2_accept_incoming(b);
      } else {
	LOG_ERR("Can't alloc ax25buffer for %d bytes, dropping frame",
		context->bytes_received - 2 + 2);
      }
    }
    result = true;
  }
  context->fcsGood = false;
  context->dataBits = 0;
  context->fcs = 0xFFFF;
  context->fcsfast = 0xFFFF;
  context->bytes_received = 0;
  context->time_start = context->timestamp;
  return result;
}

void Abort(struct hdlc_rcv *context) {
  LOG_DBG("#  %lld: ABORT/idle\n", context->timestamp);
  context->dataBits = 0;
}

const uint16_t hdlc_fcstab[256] = {
  0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
  0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
  0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
  0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
  0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
  0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
  0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
  0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
  0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
  0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
  0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
  0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
  0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
  0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
  0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
  0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
  0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
  0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
  0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
  0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
  0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
  0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
  0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
  0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
  0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
  0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
  0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
  0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
  0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
  0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
  0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
  0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};

static void TIME_CRITICAL(FCS) (struct hdlc_rcv *context, int theBit) {
  int shifted = context->fcs >> 15;
  context->fcs = context->fcs << 1;
  if (theBit != shifted) {
    context->fcs ^= 0x1021;
  }
}

void TIME_CRITICAL(TakeRawBit)(struct hdlc_rcv *context, int theBit) {
  if (context->r_s_capturing) {
    context->r_s_raw_bits[context->raw_bits_received / 8] =
      ((context->r_s_raw_bits[context->raw_bits_received / 8]) >> 1) |
      (theBit & 1) << 7;
    context->raw_bits_received++;
    if (context->raw_bits_received >= context->raw_bits_needed) {
      ax25buffer *b = ax25buffer_alloc(AX25_RAW, 255);  // Always give a full-sized R-S
      if (b) {
	ax25buffer_put_bytes(b, context->r_s_raw_bits, context->raw_bits_received / 8);
	b->time_start = context->fx25_time_start;
	b->time_end = context->timestamp;
	b->fx25_code = context->fx25_code;
	b->priority = FRAME_PRIORITY_FX25;
	b->loopback = context->loopback;
	LOG_DBG("Appending R-S frame of %d bytes to rx_packet_candidates list, delta-T %d",
		ax25buffer_byte_count(b), (int) (b->time_end - b->time_start));
	//	hexdump(b->data, "R-S code block", ax25buffer_byte_count(b));
	layer2_accept_incoming(b);
      } else {
	LOG_ERR("Can't alloc ax25buffer, dropping FX.25 frame");
      }
      context->r_s_capturing = false;
    }
  } else if (context->r_s_enabled) {
    context->correlation_tag = (context->correlation_tag >> 1) |
      (((uint64_t) theBit) << 63);
    bool found_tag = false;
    int code = 0;
    for (code = 0; !found_tag && code < fx25_code_count; code++) {
      union {
	uint64_t correlation_tag;
	uint8_t correlation_tag_bytes[8];
      } compare;
      compare.correlation_tag = context->correlation_tag ^
	fx25_codes[code].correlation_tag;
      int diff =
	mismatching_bits[compare.correlation_tag_bytes[0]] +
	mismatching_bits[compare.correlation_tag_bytes[1]] +
	mismatching_bits[compare.correlation_tag_bytes[2]] +
	mismatching_bits[compare.correlation_tag_bytes[3]] +
	mismatching_bits[compare.correlation_tag_bytes[4]] +
	mismatching_bits[compare.correlation_tag_bytes[5]] +
	mismatching_bits[compare.correlation_tag_bytes[6]] +
	mismatching_bits[compare.correlation_tag_bytes[7]];
      if (diff < 8) { // FIXME - this is arbitrary
	found_tag = true;
	break;
      }
#if 0
      if ((board_get_random32() / 13) % 1000 == 0) {
	found_tag = true;
	break;
      }
#endif
    }
    if (found_tag) {
      LOG_DBG("FX.25 hit, code %d", code);
      memset(context->r_s_raw_bits, 0, sizeof(context->r_s_raw_bits));
      // In the real world, we will need to match the correlation tag against
      // our table, and figure out the number of bytes of data in the
      // codeblock.
      int bytes_in_codeblock = fx25_codes[code].codeblock_bytes;
      context->fx25_time_start = context->timestamp;
      context->raw_bits_received = 0;
      context->raw_bits_needed = bytes_in_codeblock * 8;
      LOG_DBG("Codeblock is %d bytes, need %d raw bits", bytes_in_codeblock,
	      context->raw_bits_needed);
      context->r_s_capturing = true;
      context->fx25_code = code;
      // Tell the Layer 2 candidate logic to wait until after this
      // entire R-S block is received and handed off, before it looks
      // at any HDLC-frame candidates.  May make this optional at some
      // point.
      tnc_state.hdlc_hold_until = context->timestamp +
	(bytes_in_codeblock + 5) * 8 * SLICES_PER_BIT; // Maybe add a fudge factor?
    }
  }
}

void TIME_CRITICAL(TakeDataBit)(struct hdlc_rcv *context, int theBit) {
  context->outputPattern = (context->outputPattern >> 1) | (theBit << 7);
  context->dataBits ++;
  if (context->dataBits == 8) {
    context->dataBits = 0;
    context->fcsfast = (context->fcsfast >> 8) ^
      hdlc_fcstab[0xFF & (context->outputPattern ^ context->fcsfast)];
    LOG_DBG("#  %lld: DATA byte 0x%02X, FCS is 0x%04X (inv 0x%04X), FCS2 is 0x%04X (inv 0x%04X)",
	    context->timestamp, context->outputPattern,
	    context->fcs, context->fcs ^ 0xFFFF, context->fcsfast, context->fcsfast ^ 0xFFFF);
    if (context->bytes_received < HDLC_MAX_PACKET_LENGTH) {
      context->bytes[context->bytes_received++] = context->outputPattern;
    }
    if (context->fcsfast == 0xF0B8) {
      LOG_DBG("# %lld:  ** GOOD FCS **", context->timestamp);
      context->fcsGood = 1;
    } else {
      context->fcsGood = 0;
    }
  }
}

/* Perform HDLC zero-unstuffing */
bool TIME_CRITICAL(ConsumeDataBit)(struct hdlc_rcv *context, int dataBit) {
  bool result = false;
  if (context->self_timed) {
    context->timestamp += 8;  // 8 sample clocks per bit
  }
  if (dataBit == 0) {
    switch (context->oneBits) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
      FCS(context, 0);
      TakeDataBit(context, 0);
      break;
    case 5:
      /* Do nothing, it's a zero-stuffed bit */
      break;
    case 6:
      result = Flag(context);
      break;
    default:
      break;
    }
    context->oneBits = 0;
  } else {
    context->oneBits++;
    if (context->oneBits <= 5) {
      FCS(context, 1);
      TakeDataBit(context, 1);
    } else if (context->oneBits == 7) {
      Abort(context);
    }
  }
  return result;
}

/* Turn NRZI-encoded data back into baseband, and pass it along to
 * the raw-capture and HDLC-unstuffing logic.
 */
bool TIME_CRITICAL(ConsumeNRZI)(struct hdlc_rcv *context, int nrziBit) {
  int dataBit = nrziBit ^ context->previousBit ^ 1;
  LOG_DBG("C %d -> %d", nrziBit, dataBit);
  context->previousBit = nrziBit;
  context->timestamp = demod_samples_processed;
  TakeRawBit(context, dataBit);
  return ConsumeDataBit(context, dataBit);
}


void TIME_CRITICAL(SubmitByte)(struct hdlc_rcv *context, int i) {
  int count;
  for (count=0; count < 8; count++) {
    FCS(context, i&1);
    TakeDataBit(context, i&1);
    i = i >> 1;
  }
}

bool TIME_CRITICAL(ConsumeSubbit)(struct hdlc_rcv *context, uint32_t outputBit) {
    /*
      Perform edge detection.  We consider a "clean edge" to consist
      of the transition between two samples of high-tone and two
      of low-tone, or vice versa... only on such clean transitions
      will we "lock" to the phase of the incoming signal.  We bump
      up our "lock confidence" if we find that we're re-locking
      to the same clock phase (plus or minus one sample).  We
      lose clock confidence if we're locking to a different clock
      phase or if we see what appear to be noisy transitions
      in the signal.

      TBD: if we're in the middle of handling a packet and find that
      we've suddenly re-locked to an edge that's very much in the wrong
      place, we might want to abort parsing of the incoming packet?
      Less chance of accepting a bad packet that way, and (maybe) little
      chance of falsely rejecting a packet we would have been able to
      handle anyhow?
    */
    uint8_t edgeCapture;
    bool result = false;
    context->bitPattern = ((context->bitPattern << 1) & 0xE) | outputBit;
    if (slicerTable[context->bitPattern] & EDGE_PRESENT) {
      edgeCapture = slicerTable[context->bitPattern];
      // Re-study this...
      if (context->bitPhase <= 2 &&
	  context->lock_confidence < context->maxLockConfidence) {
	context->lock_confidence ++;
      } else if (context->lock_confidence > context->minLockConfidence) {
	context->lock_confidence --;
      }
      context->bitPhase = 1;
      context->transitionTime = 0;
    } else {
      edgeCapture = 0;
      context->transitionTime ++;
      if (context->lock_confidence > context->minLockConfidence) {
	if (context->transitionTime > context->transitionTimeout ||
	    (slicerTable[context->bitPattern] & CHATTER)) {
	  context->lock_confidence --;
	}
      }
    }
    /*
      If we're at the right point in the bit cycle,
      choose a high or low and output it to the NRZI
      decoder.
    */
    if (context->bitPhase == context->bitCaptureTime) {
      switch (context->bitEvaluatorMode) {
      case edgeComparison:
	context->thisDatabit = edgeCapture & CHOOSE_HIGH;
	break;
      case midpointComparison:
	context->thisDatabit = outputBit;
	break;
      case votingComparison:
	context->thisDatabit = slicerTable[context->bitPattern] & CHOOSE_HIGH;
	break;
      }
      LOG_DBG("Demod consume");
      result = ConsumeNRZI(context, context->thisDatabit);
    }
    context->bitPhase = (context->bitPhase + 1) % 8;
    return result;
}

bool HDLCLocked(struct hdlc_rcv *context) {
  return context->lock_confidence > tnc_state.lock_threshold;
}

/* The HDLC encoder maintains a circular buffer of encoded HDLC
 * data, stored in 32-bit words.  Within any given word, encoded
 * bits are stored starting from the LSB position;  during transmission
 * the word can simply be right-shifted after each bit is sent.
 *
 * We can hold 32 bits of encoded HDLC in each word in the ring.  At
 * 9600 bits/second (pre-encoding) we consume up to 360 words per
 * second (assuming worst-case zero stuffing).  Since the board's
 * main thread (which calls the encoder) runs at somewhat below
 * 100 iterations per second, we'll need to fill about 4 words per
 * iteration.  At lower speeds, 8 entries total was sufficient, but
 * should expand this to 16 to provide some timing slack in case the
 * main thread is preempted for a while.
 */

#define HDLC_RING_SIZE 16
struct {
  uint32_t bitring[HDLC_RING_SIZE];
  bool bitring_word_filled[HDLC_RING_SIZE];
  uint32_t encode_word;
  uint32_t encode_bit;
  uint32_t encode_bits_free;
  uint32_t transmit_word;
  uint32_t transmit_bits_ready;
  uint32_t ones;
  bool previous_nrzi;
  uint32_t bits_read_out;
  uint16_t computed_fcs;
} hdlc_encode;

static ax25buffer *raw_out;

void HDLC_Dump_Raw_Out(void) {
  if (raw_out) {
    hexdump(raw_out->data, "Raw frame data transmitted", raw_out->bits_written / 8 + 1);
  }
}

void HDLC_Reset_Capture(void) {
  if (raw_out) {
    ax25buffer_reset(raw_out);
  }
}

void HDLC_Reset_Encoder(void) {
  memset(&hdlc_encode, 0, sizeof(hdlc_encode));
  hdlc_encode.encode_bits_free = 32;  /* Ready to encode into word 0 */
  hdlc_encode.transmit_bits_ready = 32;  /* Ready to transmit when it's full */
  hdlc_encode.computed_fcs = 0xFFFF;
  if (!raw_out) {
    raw_out = ax25buffer_alloc(AX25_RAW, 512);
  }
  if (raw_out) {
    ax25buffer_reset(raw_out);
  }
}

void HDLC_Reset_FCS(void) {
  LOG_DBG("Reset FCS");
  hdlc_encode.computed_fcs = 0xFFFF;
}

uint16_t HDLC_Get_FCS(void) {
  return hdlc_encode.computed_fcs;
}

/* As a simplifying hack, only agree to accept more data to be encoded if
 * there are at least 16 bits free in the current word _or_ if the next word
 * is available.  This guarantees that we will always be able to encode a full
 * byte of input, even if worst-case zero-stuffing must be performed.
 */
bool HDLC_Ready(void) {
  if (hdlc_encode.encode_bits_free > 16) {
    return true;
  }
  uint32_t next_word = (hdlc_encode.encode_word + 1) % HDLC_RING_SIZE;
  if (!hdlc_encode.bitring_word_filled[next_word]) {
    return true;
  }
  return false;
}

bool HDLC_Busy(void) {
  if (hdlc_encode.encode_bits_free != 32 ||
      hdlc_encode.bitring_word_filled[hdlc_encode.transmit_word]) {
    return true;
  } else {
    return false;
  }
}

static void HDLC_Word_Done(void) {
  LOG_DBG("Done filling word %d 0x%08X", hdlc_encode.encode_word, hdlc_encode.bitring[hdlc_encode.encode_word]);
  hdlc_encode.bitring_word_filled[hdlc_encode.encode_word] = true;
  hdlc_encode.encode_word = (hdlc_encode.encode_word + 1) % HDLC_RING_SIZE;
  hdlc_encode.encode_bit = 0;
  hdlc_encode.encode_bits_free = 32;
}

void HDLC_Set_NRZI_History(bool previous) {
  hdlc_encode.previous_nrzi = previous;
}

void TIME_CRITICAL(HDLC_Append_Bit)(int bit) {
  if (raw_out) {
    ax25buffer_put_bit(raw_out, bit&1);
  }
  if ((bit & ~1) != 0) {
    LOG_ERR("Bad append bit 0x%x", bit);
  }
  if (hdlc_encode.bitring_word_filled[hdlc_encode.encode_word]) {
    LOG_DBG("encoder overrun");
  }
  bool nrzi_bit = hdlc_encode.previous_nrzi ^ bit ^ 1; // Transition on 0
  LOG_DBG("E %d -> %d, %d/%d", bit, nrzi_bit, hdlc_encode.encode_word, hdlc_encode.encode_bit);
  hdlc_encode.previous_nrzi = nrzi_bit;
  hdlc_encode.bitring[hdlc_encode.encode_word] |=
    nrzi_bit << hdlc_encode.encode_bit;
  ++hdlc_encode.encode_bit;
  --hdlc_encode.encode_bits_free;
  if (bit) {
    ++hdlc_encode.ones;
  } else {
    hdlc_encode.ones = 0;
  }
  if (hdlc_encode.encode_bits_free == 0) {
    HDLC_Word_Done();
  }
}

void TIME_CRITICAL(HDLC_Encode_Sync)(void) {
  HDLC_Append_Bit(0);
  HDLC_Append_Bit(1);
  HDLC_Append_Bit(1);
  HDLC_Append_Bit(1);
  HDLC_Append_Bit(1);
  HDLC_Append_Bit(1);
  HDLC_Append_Bit(1);
  HDLC_Append_Bit(0);
}

bool TIME_CRITICAL(HDLC_Append_Raw_Byte)(uint8_t byte, int reps) {
  for (int rep = 0; rep < reps; rep++) {
    if (!HDLC_Ready()) {
      return false;
    }
    uint8_t b = byte;
    for (int i = 0; i < 8; i++) {
      HDLC_Append_Bit(b & 1);
      b >>= 1;
    }
  }
  return true;
}

void TIME_CRITICAL(HDLC_Encode_Byte)(uint8_t byte) {
  uint8_t byte_in = byte;
  hdlc_encode.computed_fcs = (hdlc_encode.computed_fcs >> 8) ^
    hdlc_fcstab[0xFF & (byte ^ hdlc_encode.computed_fcs)];
  for (int i = 0; i < 8; i++) {
    HDLC_Append_Bit(byte & 1);
    byte >>= 1;
    if (hdlc_encode.ones == 5) {
      HDLC_Append_Bit(0);
    }
  }
  LOG_DBG("HDLC encode byte 0x%02X, FCS 0x%04X", byte_in, hdlc_encode.computed_fcs);
}

void TIME_CRITICAL(HDLC_Encode_FCS_Byte)(uint8_t byte) {
  for (int i = 0; i < 8; i++) {
    HDLC_Append_Bit((byte & 0x80) ? 1 : 0);
    if (hdlc_encode.ones == 5) {
      HDLC_Append_Bit(0);
    }
    byte <<= 1;
  }
}

uint32_t TIME_CRITICAL(HDLC_Encode_Bytes)(uint8_t *buffer, uint32_t len) {
  for (int i = 0; i < len; i++) {
    if (!HDLC_Ready()) {
      return i;
    }
    HDLC_Encode_Byte(buffer[i]);
  }
  return len;
}

// Send at least 8 unchanging bits (and up to 40) to
// signal end of / aborted frame.
void TIME_CRITICAL(HDLC_Encode_Abort)(void) {
  for (int i = 0; i < 8; i++) {
    HDLC_Append_Bit(1);
  }
  // This is broken, need to re-think it
  //  while (hdlc_encode.encode_bits_free != 0) {
  //    HDLC_Append_Bit(1);
  //  }
}

void HDLC_Flush(void) {
  if (hdlc_encode.encode_bit != 0) {
    HDLC_Word_Done();
  }
}

bool TIME_CRITICAL(HDLC_Output_Avail)(void) {
  return hdlc_encode.bitring_word_filled[hdlc_encode.transmit_word];
}

bool TIME_CRITICAL(HDLC_Get_Bit)(void) {
  if (!hdlc_encode.bitring_word_filled[hdlc_encode.transmit_word]) {
    LOG_ERR("Word %d not filled!", hdlc_encode.transmit_word);
    return true;
  }
  if (hdlc_encode.transmit_bits_ready == 32) {
    LOG_DBG("R %d 0x%08X", hdlc_encode.transmit_word,
	    hdlc_encode.bitring[hdlc_encode.transmit_word]);
  }
  LOG_DBG("G %d/%d", hdlc_encode.transmit_word, hdlc_encode.transmit_bits_ready);
  bool output_bit = hdlc_encode.bitring[hdlc_encode.transmit_word] & 1;
  hdlc_encode.bitring[hdlc_encode.transmit_word] >>= 1;
  --hdlc_encode.transmit_bits_ready;
  if (hdlc_encode.transmit_bits_ready == 0) {
    LOG_DBG("Done xmitting word %d", hdlc_encode.transmit_word);
    hdlc_encode.bitring[hdlc_encode.transmit_word] = 0;
    hdlc_encode.bitring_word_filled[hdlc_encode.transmit_word] = false;
    hdlc_encode.transmit_bits_ready = 32;
    hdlc_encode.transmit_word = (hdlc_encode.transmit_word + 1) % HDLC_RING_SIZE;
  }
  return output_bit;
}

/* Dump encoder status
*/
void HDLC_Status(void) {
  LOG_INF("Filled %d %d %d %d", hdlc_encode.bitring_word_filled[0], hdlc_encode.bitring_word_filled[1],
	  hdlc_encode.bitring_word_filled[2], hdlc_encode.bitring_word_filled[3]);
  LOG_INF("Encoding to word %d, %d bits avail", hdlc_encode.encode_word, hdlc_encode.encode_bits_free);
  LOG_INF("Transmit word %d, %d bits ready", hdlc_encode.transmit_word, hdlc_encode.transmit_bits_ready);
  LOG_INF("Bits read out %d", hdlc_encode.bits_read_out);
}
