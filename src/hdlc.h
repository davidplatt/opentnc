/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_HDLC
#define _H_HDLC

#include <stdint.h>
#include <stdbool.h>

#include "ax25.h"
#include "fx25.h"

#define HDLC_MAX_PACKET_LENGTH (MAX_IFRAME_PAYLOAD + MAX_PACKET_OVERHEAD)

#define HDLC_FLAG 0x7E

enum frame_priority {
  FRAME_PRIORITY_HDLC = 0,
  FRAME_PRIORITY_FX25 = 1,
  FRAME_PRIORITY_CORRECTED_HDLC = 2
};

extern const uint16_t hdlc_fcstab[256];

struct hdlc_rcv {
  int64_t timestamp;
  int64_t time_start;
  int64_t fx25_time_start;
  int64_t time_end;
  uint64_t correlation_tag;
  int dataBits;
  uint16_t fcs;
  uint16_t fcsfast;
  int fcsGood;
  int oneBits;
  int previousBit;
  int bitPhase, bitPattern;
  int thisDatabit, previousDatabit;
  int outputPattern;
  int bitEvaluatorMode;
  int bitCaptureTime;
  uint32_t lock_confidence;
  uint32_t lock_threshold;
  int time;
  int minLockConfidence;
  int maxLockConfidence;
  int transitionTimeout;  /* eight bit times @ 8x oversampling */
  int transitionTime;
  int bytes_received;
  int raw_bits_needed;
  int raw_bits_received;
  bool loopback;
  bool self_timed;
  bool r_s_enabled;
  bool r_s_capturing;
  bool report_fx25;
  int fx25_code;
  enum frame_priority priority;
  uint8_t bytes[HDLC_MAX_PACKET_LENGTH];
  uint8_t r_s_raw_bits[256];
};

// Three HDLC contexts are provided, one for an audio path
// with a flat frequency response, and two for audio paths
// which apply a high-boost or high-cut equalization before
// discrimination. Currently, only the Biquad demodulator makes
// use of the latter two.

extern struct hdlc_rcv hdlc_context_flat;
extern struct hdlc_rcv hdlc_context_highboost;
extern struct hdlc_rcv hdlc_context_highcut;

void InitHDLC(struct hdlc_rcv *context);

// The following functions return TRUE if the HDLC decoder has
// just declared success (FLAG seen, good FCS) and submitted
// a packet to the packet ring.

bool ConsumeSubbit(struct hdlc_rcv *context, uint32_t outputBit);
bool ConsumeNRZI(struct hdlc_rcv *context, int nrziBit);
bool ConsumeDataBit(struct hdlc_rcv *context, int dataBit);

static inline void HDLC_SetLock(struct hdlc_rcv *context, bool isLocked) {
  if (isLocked) {
    context->lock_confidence = context->maxLockConfidence;
  } else {
    context->lock_confidence = 0;
  }
}

static inline void HDLC_AdjustLock(struct hdlc_rcv *context, bool jitter) {
  if (jitter) {
    if (context->lock_confidence > context->minLockConfidence) {
      context->lock_confidence --;
    }
  } else {
    if (context->lock_confidence < context->maxLockConfidence) {
      context->lock_confidence ++;
    }
  }
}

bool HDLCLocked(struct hdlc_rcv *context);

void HDLC_Dump_Raw_Out(void);
void HDLC_Reset_Capture(void);

void HDLC_Reset_Encoder(void);

/* Returns true if the HDLC encoder can currently accept at least one
 * byte of new data (or a SYNC or ABORT).
 */
bool HDLC_Ready(void);

/* Returns true if the HDLC encoder has bits available for the
 * consumer.
 */
bool HDLC_Output_Avail(void);

/* Returns true if the HDLC encoder currently holds any un-sent
 * encoded bits.
 */
bool HDLC_Busy(void);

/*
 * Sets the previous-bit NRZI encoder history.  Can be used
 * to ensure that the NRZI stream starts out in a known
 * (low or high) state.
 */

void HDLC_Set_NRZI_History(bool previous);

/*
 * Appends one bit to the HDLC stream (zero-bit stuffing is
 * not performed.
 */
void HDLC_Append_Bit(int bit);

/*
 * Appends one byte to the HDLC stream, LSB-first, one or more times
 * (zero-bit stuffing is not performed.  Returns false if the HDLC
 * system wasn't ready for more data and the byte was not appended
 * as many times as requested.
 */
bool HDLC_Append_Raw_Byte(uint8_t byte, int reps);

/* Encodes an HDLC Sync (zero-bit insertion is not performed).  Caller must
 * check HDLC_Ready() first.
 */
void HDLC_Encode_Sync(void);

/* Encodes a byte of data (LSB first, doing zero-bit insertion as
 * necessary) and updates the FCS. Caller must check HDLC_Ready()
 * first
 */
void HDLC_Encode_Byte(uint8_t byte);

/* Encodes a byte of the FCS (MSB first, doing one-bit insertion as
 *  necessary).  Caller must check HDLC_Ready() first
 */
void HDLC_Encode_FCS_Byte(uint8_t byte);

/* Tries to encode one or more bytes of data (doing one-bit insertion
 * as necessary). Returns the number of bytes encoded.  Caller need
 *  not check HDLC_Ready() first, but must be prepared to receive a 0
 * in return.
 */
uint32_t HDLC_Encode_Bytes(uint8_t *buffer, uint32_t len);

/* Encodes an HDLC Abort (one-bit insertion is not performed).  Caller must
 * check HDLC_Ready() first.
 */
void HDLC_Encode_Abort(void);

/* Resets the FCS at the beginning of a new frame.
 */
void HDLC_Reset_FCS(void);

/* Returns the 16-bit FCS computed since the encoder was last reset.
 */
uint16_t HDLC_Get_FCS(void);

/* Tells the HDLC encoder that encoding is done for now and that all
 * encoded data may be transmitted.
 */
void HDLC_Flush(void);

/* Returns the next bit of encoded HDLC for transmission.  Caller must
 * check HDLC_Busy() first - if not busy this routine will return zero.
 */
bool HDLC_Get_Bit(void);

/* Dump encoder status
*/
void HDLC_Status(void);

#endif
