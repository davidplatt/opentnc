/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_CONNECTION
# define _H_CONNECTION

#include <zephyr/kernel.h>

#include "ax25.h"
#include "ax25buffer.h"
#include "fx25.h"

enum connection_state {
  CONN_DISCONNECTED,
  CONN_CONNECTING,
  CONN_NEGOTIATING,
  CONN_CONNECTED,
  CONN_DISCONNECTING,
  CONN_CLEANUP,
  CONN_UNPROTO
};

struct conn_timer {
  const char *name;
  uint32_t duration;
  uint32_t ticks_remaining;
  bool expired;
};

#define CONNECTION_PACKET_COUNT 8

#define APRS_CRC_PACKET_COUNT 10   // number of packet CRCs we remember
#define APRS_CRC_PACKET_TIMEOUT 30 // seconds

struct pending_packet {
  int32_t sharebuffer_offset;
  uint32_t length;
  ax25buffer *buffer;
  bool valid;
  bool claimed;
  bool sent_once;
};

// forward ref
typedef struct connection_endpoint connection_endpoint;
typedef struct connection connection;

// Determine whether a connection matches the packet
// in question.  If present in a connection/endpoint, the
// packet filter overrides the other matching criteria.
typedef bool (*packet_filter)(connection *conn, packet_parse *p);

// Packet consumer returns false if the packet data could not
// be accepted (e.g. buffer overrun).
typedef bool (*packet_consumer)(connection *conn, packet_parse *p);

// Connection config routine can override default values on a
// connection during startup.
typedef void (*connection_configure)(connection *conn);

// Determine whether a connection is able to receive additional
// packets at this time.  Used to decide when to send RR or RNR.
typedef bool (*can_receive)(connection *conn);

struct connection_endpoint {
  encoded_callsign callsign;  // Relevant iff match_endpoint_call is true
  packet_filter filter_default;
  packet_consumer consumer_default;
  connection_configure configure_default;
  can_receive can_receive_default;
  bool match_mycall_default;
  bool match_endpoint_call_default;
  bool match_peer_default;
  bool is_console;
};

// Peer capabilities, based on information in the XID frame.
struct peer_capabilities {
  int32_t maxframe;      // Transmit window size.
  int32_t window;        // Our announced receive window limit
  int32_t filter_window; // Window size for "this is really stale!" filtering
  int32_t paclen;  // maximum I-frame size, bytes
  int32_t t1;
  int32_t retries;
  bool supports_srej;
  bool extended_sequence;
  uint32_t sequence_mask; // either 0x7 or 0x7F depending on extended_sequence
};

typedef struct connection {
  connection_endpoint *endpoint;
  struct k_mutex mutex;
  enum connection_state state;
  full_address_path path;
  struct conn_timer t1, t2, t3;
  bool match_mycall;
  bool match_endpoint_call;
  bool match_peer;
  bool peer_ready;
  bool live;  // Have gotten first packet after the SABM/UA exchange
  bool i_frame_poll_received;
  bool polled_for_rr_or_rnr;
  bool poll_for_rr_or_rnr_sent;
  bool send_final_bit;
  bool send_rej; // Wrong packet arrived, we must send a REJ
  bool rej_sent; // We've sent a REJ once.  Not yet cleared.
  bool send_srej; // We're doing selective rejects to recover a single lost packet
  bool srej_sent; // We've launched one, cannot launch another
  bool send_rnr; // We have insufficient room to receive
  bool rnr_sent; // We told the peer we're busy...
  bool rej_received;
  bool disconnect;
  bool attempting_sabme;
  bool we_use_fx25; // Apply fx.25 to outbound packet
  bool peer_uses_fx25; // We've seen fx.25 from the peer
  bool peer_uses_non_fx25; // We've seen a non-fx.25 packet from the peer
  uint32_t v_s, v_r, n_s, n_r;
  int32_t n_s_needed; // for sending SREJ; -1 = none
  uint32_t retries;
  uint32_t retries_used;
  struct pending_packet pending_output[CONNECTION_PACKET_COUNT];
  sys_dlist_t deferred_input;
  uint32_t pending_packets;
  uint32_t queued_packets;
  uint32_t sent_packets;
  uint32_t received_packets;
  enum ax25_protocol pid;
  enum fx25_connection_mode fx25_connection_mode;
  struct peer_capabilities capabilities;
  packet_filter filter;
  packet_consumer consumer;
  connection_configure configure;
  can_receive can_receive;
} connection;

// Need digi support at some point.

// Match an incoming packet-buffer with the first relevant active connection,
// based on information in the endpoint.  Matches the callsign (mycall or
// endpoint-specific);  returns only active connections.
connection *match_packet_to_active_connection(ax25buffer *buffer, packet_parse *p);

// Match an incoming packet-buffer with the first free matching connection.
// Matches the callsign (mycall or endpoint-specific);  returns only
// connection structures which are not active.
connection *match_packet_to_free_connection(ax25buffer *buffer, packet_parse *p);

// Match the incoming packet-buffer with the first connection (alive
// or not) which matches the callsign.
connection *match_packet_to_some_connection(ax25buffer *buffer, packet_parse *p);

// Find an available connection for an endpoint
connection *find_free_connection_for_endpoint(connection_endpoint *ep);

// See if any connection has traffic
bool some_connection_has_traffic(void);

void init_connections(void);

bool start_console_connection(connection *conn, encoded_callsign *dest,
			      encoded_callsign *digis, int num_digis);
bool start_unproto_connection(connection *conn, encoded_callsign *dest);
bool start_inbound_connection(connection *conn, full_address_path *path, bool extended_sequence);
void stop_connection(connection *conn);
void reset_connection(connection *conn, bool preserve_fx25_info);
void reinit_connection(connection *conn);

// See if there's room for another connection packet.
bool room_for_conn_packet(connection *conn);

// Flush any I-frames which have been queued up during SREJ.
void release_deferred_input(connection *conn);

// Find a deferred I-frame buffer for a given sequence number,
// if it exists.
ax25buffer *find_deferred_input(connection *conn, uint32_t seq);

// Callback routine, hooked to buffers which want the T1
// timer started/restarted when their transmission is
// complete.
void start_t1_timer(ax25buffer *buffer);

// Check to see if we have any I-frames elegible for immediate
// transmission.
bool have_i_frames_to_send(connection *conn);

// Create a new outbound packet for the connection, given either an
// ax25buffer containing private data, or a claimed sharebuffer range
// (offset and packet length).  Returns false on failure (no packets available).
bool append_l3_packet(connection *conn, ax25buffer *buffer, uint32_t sharebuffer_offset, uint32_t length);

// Clear the "sent already" flag in any pending outbound packets,
// so that they can be resent (after a REJ or other loss).
void relist_l3_packets(connection *conn);

// Release the oldest outbound packet in the connection
// buffer, and return true.  If the connection buffer was
// empty, return false.
bool release_oldest_l3_packet(connection *conn);

// Decide whether to use FX.25 on an outbound packet on this
// connection.
bool fx25_policy(connection *conn);

// Configure the default capability set for a new connection,
// based on whether it is (SABME) or is not (SABM) one which
// uses extended sequence numbers.
void configure_default_capabilities(connection *conn, bool extended_sequence);

// Determine whether a packet's sender sequence number is inside
// our defined receiver window.  This can be used to detect
// badly-out-of-sequence frames, such as old frames we've
// already acknowledged or duplicates from overlapping SREJ
// retransmission.  We can drop these silently, rather
// than having to reset the connection.
bool sequence_in_window(connection *conn, int32_t n_s);

// Mask a sequence number after adding or subtracting.
static inline int32_t sequence_mod(connection *conn, int32_t n) {
  return (n & conn->capabilities.sequence_mask);
}

// The L3 timer functions require that the caller be holding
// a mutex which protects the timer in question.  Typically
// this will be the connection.mutex

// Initialize timer (set duration, clear other fields).  The
// user-visible duration is sometimes given in 100-millisecond units
// (multipler 1), sometimes in seconds (multiplier 10), and sometimes
// in 10-second increments (multiplier 100).
void init_conn_timer(struct conn_timer *t, uint32_t duration,
		     uint32_t multiplier);

// Clear "expired" flag, set ticks_remaining to duration
void start_conn_timer(struct conn_timer *t);

// Clear "expired" flag, set ticks_remaining and duration to the new_duration
// (always given in 100-millisecond ticks).
void start_conn_timer_for(struct conn_timer *t, uint32_t new_duration);

// Clear "expired" flag and ticks_remaining
void stop_conn_timer(struct conn_timer *t);

// If timer is running, decrement count.  If count reaches
// zero, set "expired" flag.  Return "expired" flag.
bool tick_conn_timer(struct conn_timer *t);

// Check for timer expiry
static bool inline check_conn_timer(struct conn_timer *t) {
  return t->expired;
}


#endif  // _H_CONNECTION
