/*
 * Copyright (c) 2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

/*
  This file contains portions of the OpenTNC code which are
  specific to the Raspberry Pi Pico.
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/dma.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/flash.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/crc.h>
#include <zephyr/sys/ring_buffer.h>

#include <zephyr/usb/usb_device.h>
#include <zephyr/logging/log.h>

// low-level RP2040 and RPI stuff, much of which isn't directly supported by Zephyr yet.
#include <hardware/adc.h>
#include <hardware/dma.h>
#include <hardware/irq.h>
#include <hardware/pio.h>
#include <hardware/pwm.h>

#include <pico/bootrom.h>

#include "board.h"
#include "board_rp2040.h"
#include "demod.h"
#include "globals.h"
#include "hdlc.h"
#include "kiss.h"
#include "leds.h"
#include "mod.h"
#include "pulses.h"
#include "ws2812.h"
#include "xoshiro128.h"

#include "board_rpi_pico_bitsender.h"
#include "board_rpi_pico_ws2812.h"
#include "noiseshaping_rpi_pico.h"

LOG_MODULE_REGISTER(rpi_pico, LOG_LEVEL_INF);

#define FLASH_NODE DT_ALIAS(flash)

const struct device *flash_dev = DEVICE_DT_GET(FLASH_NODE);

#define UART_NODE DT_ALIAS(uart)
const struct device *cdc_dev = DEVICE_DT_GET(UART_NODE);

#define HEARTBEAT_STACK_SIZE	2048
static struct k_thread blink_thread;
static K_THREAD_STACK_ARRAY_DEFINE(tstack, 1, HEARTBEAT_STACK_SIZE);

#define TRANSCEIVER_STACK_SIZE	2048
static struct k_thread transceiver_thread;
static K_THREAD_STACK_ARRAY_DEFINE(transceiver_stack, 1, TRANSCEIVER_STACK_SIZE);


#define DMA_NODE DT_ALIAS(dma)

const struct gpio_dt_spec ptt = GPIO_DT_SPEC_GET(ZEPHYR_USER_NODE, ptt_gpios);
const struct gpio_dt_spec hdlc = GPIO_DT_SPEC_GET(ZEPHYR_USER_NODE, hdlc_direct_gpios);
const struct gpio_dt_spec rx_data_in = GPIO_DT_SPEC_GET(ZEPHYR_USER_NODE, rx_data_in_gpios);
const struct gpio_dt_spec tx_data_out = GPIO_DT_SPEC_GET(ZEPHYR_USER_NODE, tx_data_out_gpios);
const struct gpio_dt_spec squelch_input = GPIO_DT_SPEC_GET(ZEPHYR_USER_NODE, squelch_input_gpios);

static const struct device *adc;
static const struct device *dma = DEVICE_DT_GET(DMA_NODE);

#define ADC_NODE DT_ALIAS(radio)

#define ADC_DEVICE_NAME         "radio" /* fix this */
#define ADC_RESOLUTION		12
#define ADC_GAIN		ADC_GAIN_1
#define ADC_REFERENCE		ADC_REF_INTERNAL
#define ADC_ACQUISITION_TIME	ADC_ACQ_TIME_DEFAULT
#define ADC_1ST_CHANNEL_ID      DT_PROP(ZEPHYR_USER_NODE, adc_input_selector)

#define SHOW_DELTA_SIGMA_CLOCK 0

K_SEM_DEFINE(sampler_sem, 0, 2);

// Align the array (and each sample set) on a boundary which is 0 mod the sample-set size
// so that RP2040 DMA channels can operate in circular-ring mode if desired.
struct sample_sets sample_sets __attribute__((aligned(sizeof(struct input_sample_set))));

_Static_assert(sizeof(sample_sets.set_0) == (1 << CONFIG_ADC_BUFFER_BYTES_LOGARITHM), "Sample array size doesn't match CONFIG_ADC_BUFFER_BYTES_LOGARITHM");

// Structures needed for the DMA-based modulators (PWM and noise-shaping)

#if DT_NODE_HAS_PROP(ZEPHYR_USER_NODE, ws2812_gpios) && NUM_WS2812 > 0

#define HAS_WS2812_SUPPORT

const struct gpio_dt_spec ws2812 = GPIO_DT_SPEC_GET(ZEPHYR_USER_NODE, ws2812_gpios);

static int ws2812_dma_channel;
static uint32_t ws2812_pio_sm;
static uint32_t ws2812_pio_offset;

static dma_channel_config ws2812_dma_cfg;

static void ws2812_callback(const struct device *dev, void *user_data, uint32_t channel, int status)
{
  LOG_DBG("WS2812 DMA callback");
}

static void Setup_DMA_for_WS2812(void) {
  uint32_t mask = 0x00FFFFFF;
  ws2812_dma_channel = dma_request_channel(dma, &mask);
  ws2812_pio_sm = 1;
  pio_sm_claim(pio0, ws2812_pio_sm);
  ws2812_pio_offset = pio_add_program(pio0, &ws2812_program);
  LOG_DBG("WS2812 PIO program loaded at offset %d for sm %d for DMA channel %d",
	  ws2812_pio_offset, ws2812_pio_sm, ws2812_dma_channel);

  ws2812_program_init(pio0, ws2812_pio_sm, ws2812_pio_offset, ws2812.pin, 800000.0f, false);

  struct dma_config dma_cfg;
  struct dma_block_config block_cfg;
  /*
    Use the Zephyr DMA API to do the driver setup.
  */
  memset(&dma_cfg, 0, sizeof(dma_cfg));
  memset(&block_cfg, 0, sizeof(block_cfg));
  dma_cfg.head_block = &block_cfg;
  dma_cfg.channel_direction = MEMORY_TO_PERIPHERAL;
  dma_cfg.block_count = 1;
  dma_cfg.source_data_size = sizeof(uint32_t);
  dma_cfg.dest_data_size = sizeof(uint32_t);
  dma_cfg.dma_callback = ws2812_callback;
  block_cfg.source_address = (uint32_t) &ws2812_leds;
  block_cfg.source_addr_adj = DMA_ADDR_ADJ_INCREMENT;
  block_cfg.dest_address = (uint32_t) &pio0->txf[ws2812_pio_sm];
  block_cfg.dest_addr_adj = DMA_ADDR_ADJ_NO_CHANGE;

  dma_config(dma, ws2812_dma_channel, &dma_cfg);

  // Set up again, specifying the things Zephyr cannot.

  dma_channel_config ws2812_channel_config = dma_channel_get_default_config(ws2812_dma_channel);

  channel_config_set_read_increment(&ws2812_channel_config, true);
  channel_config_set_write_increment(&ws2812_channel_config, false);
  channel_config_set_transfer_data_size(&ws2812_channel_config, DMA_SIZE_32);
  channel_config_set_dreq(&ws2812_dma_cfg, pio_get_dreq(pio0, ws2812_pio_sm, true));
  dma_channel_set_config(ws2812_dma_channel, &ws2812_channel_config, false);
  dma_channel_set_read_addr(ws2812_dma_channel, (void *) &ws2812_leds, false);
  dma_channel_set_write_addr(ws2812_dma_channel, &pio0->txf[ws2812_pio_sm], false);
}

static void ws2812_updater(void) {
  static struct ws2812_led last_ws2812_leds[NUM_WS2812];
  encode_leds_to_ws2812();
  if (memcmp(ws2812_leds, last_ws2812_leds, sizeof(last_ws2812_leds)) == 0) {
    return;
  }
  memcpy(last_ws2812_leds, ws2812_leds, sizeof(last_ws2812_leds));
  pio_sm_set_enabled(pio0, ws2812_pio_sm, false);
  pio_sm_clear_fifos(pio0, ws2812_pio_sm);
  dma_channel_set_read_addr(ws2812_dma_channel, (void *) &ws2812_leds, false);
  dma_channel_set_write_addr(ws2812_dma_channel, &pio0->txf[ws2812_pio_sm], false);
  dma_channel_set_trans_count(ws2812_dma_channel, sizeof(ws2812_leds) / sizeof(uint32_t), false);
  dma_channel_start(ws2812_dma_channel);
  pio_sm_restart(pio0, ws2812_pio_sm);
  pio_sm_set_enabled(pio0, ws2812_pio_sm, true);
}

#endif

void board_heartbeat(uint32_t ticks) {
  uint32_t time_then = k_uptime_get();
  uint64_t old_samples_processed = samples_processed;
  if (overruns) {
    LOG_DBG("Sampler had %d overrun(s)", overruns);
  }
  overruns = 0;
  uint32_t time_now = k_uptime_get();
  uint64_t samples_now = samples_processed;
  time_then = time_now;
  old_samples_processed = samples_now;
#ifdef HAS_WS2812_SUPPORT
  ws2812_updater();
#endif
}

static const struct adc_channel_cfg m_1st_channel_cfg = {
	.gain             = ADC_GAIN,
	.reference        = ADC_REFERENCE,
	.acquisition_time = ADC_ACQUISITION_TIME,
	.channel_id       = ADC_1ST_CHANNEL_ID,
#if defined(CONFIG_ADC_CONFIGURABLE_INPUTS)
	.input_positive   = ADC_1ST_CHANNEL_INPUT,
#endif
};

static const struct device *init_adc(void)
{
	const struct device *adc_dev = DEVICE_DT_GET(ADC_NODE);
	int ret;

	if (!adc_dev) {
	  LOG_ERR("Can't find ADC %s", ADC_DEVICE_NAME);
	  return NULL;
	}

	if (!device_is_ready(adc_dev)) {
                LOG_ERR("ADC device not ready");
        }

	ret = adc_channel_setup(adc_dev, &m_1st_channel_cfg);
	if (ret != 0)
	  LOG_ERR("Setting up ADC channel with Zephyr failed with code %d", ret);

	/* The Zephyr ADC APIs only support synchronous, polled
	 * sampling.  Use the HAL to reconfigure the ADC to enable a
	 * properly-paced free-running sampling, with the FIFO
	 * enabled.
	 */
	LOG_DBG("Setting up ADC for paced, FIFO operation");
	/* No ADC IRQ needed... the DMA channel does that. */
	adc_irq_set_enabled(false);
	/* ADC clock runs at 48 MHz, and requires 96 cycles to perform
	   a complete conversion (maximum rate of 500,000 conversions
	   per second).  Set the ADC clock divisor to slow down the
	   clock to result in 38400 conversions per second.
	*/
	adc_set_clkdiv((48000000.0f / 38400) - 1);
	/* Make sure GPIO is configured properly */
	adc_gpio_init(rx_data_in.pin);
	/* Enable the FIFO */
	adc_fifo_setup(true /* enable */, true /* use DREQ */, 1 /* DREQ threshold */,
		       true /* flag bad conversions */, 0 /* no byte shift */);
	LOG_DBG("ADC setup complete");

	// Tell the system about bad-sample flags.
	tnc_state.board_bad_ADC_sample_flag = 0x8000;  // bit 15 will be set.

	return adc_dev;
}
static void SetupPins(void) {
  LOG_DBG("Here we can do pin setup");
}

// The ADC DMA completion callback signals the transceiver thread to handle
// data in whichever of the two buffers has been filled.  The driver
// will have disabled the IRQ for the channel, and (since the channel
// has been put into ring-buffer mode) the write address will have been
// set back to the beginning, so all that's necessary to allow further
// processing of the buffer is to re-enable the IRQ.

static void TIME_CRITICAL(sampler_callback)(const struct device *dev, void *user_data, uint32_t channel, int status)
{
  LOG_DBG("sampler DMA0 callback");
  dma_interrupts++;
  if (channel == adc_dma_channel_0) {
    LOG_DBG("First half full");
    if (sample_set_0_ready) overruns++;
    sample_set_0_ready = true;
    k_sem_give(&sampler_sem);
  } else if (channel == adc_dma_channel_1) {
    LOG_DBG("Second half full");
    if (sample_set_1_ready) overruns++;
    sample_set_1_ready = true;
    k_sem_give(&sampler_sem);
  }
  dma_channel_set_irq0_enabled(channel, true);
  // Poll the carrier-detect input here.
  int value;
  value = gpio_pin_get_dt(&squelch_input);
  tnc_state.squelch_open_detect = (value == 1);
}

static void Initialize_ADC_DMA(int channel, int other_channel, input_sample  *buffer, int32_t sample_count) {
  struct dma_config dma_cfg;
  struct dma_block_config block_cfg;
  /*
    Use the Zephyr DMA API to set up the basics of the channel.  The configuration
    doesn't have to be entirely correct, as it will never actually be used... it
    just has to be good enough to pass muster with the driver, so that the driver
    recognizes the channel and can pass the proper IRQ to our callback routine.
  */
  memset(&dma_cfg, 0, sizeof(dma_cfg));
  memset(&block_cfg, 0, sizeof(block_cfg));
  dma_cfg.head_block = &block_cfg;
  dma_cfg.channel_direction = PERIPHERAL_TO_MEMORY;
  dma_cfg.block_count = 1;
  dma_cfg.source_data_size = sizeof(input_sample);
  dma_cfg.dest_data_size = sizeof(input_sample);
  dma_cfg.dma_callback = sampler_callback;
  block_cfg.source_address = (uint32_t) &adc_hw->fifo;
  block_cfg.source_addr_adj = DMA_ADDR_ADJ_NO_CHANGE;
  block_cfg.dest_address = (uint32_t) buffer;
  block_cfg.dest_addr_adj = DMA_ADDR_ADJ_INCREMENT;
  block_cfg.block_size = sample_count;  // check this - number of transfers, or byte count???

  dma_config(dma, channel, &dma_cfg);
}

static void Configure_ADC_DMA(dma_channel_config *config, int channel, int other_channel, 
			      input_sample  *buffer, int32_t sample_count,
			      int32_t ring_bits) {
  /*
    Zephyr set up the basic of the DMA channel configuration, but we need to
    use features that Zephyr doesn't support (channel chaining, and automatic
    ring address wrapping).
  */
  channel_config_set_read_increment(config, false);
  channel_config_set_write_increment(config, true);
  channel_config_set_dreq(config, DREQ_ADC);
  channel_config_set_transfer_data_size(config, DMA_SIZE_16);
  channel_config_set_chain_to(config, other_channel);
  channel_config_set_ring(config, true, ring_bits);
  dma_channel_set_config(channel, config, false);
  dma_channel_set_read_addr(channel, &adc_hw->fifo, false);
  dma_channel_set_write_addr(channel, buffer, false);
  dma_channel_set_trans_count(channel, sample_count, false);
  dma_channel_set_irq0_enabled(channel, true);
}

static void Setup_DMA_for_sampler(void) {
  uint32_t filter = 0x00FFFFFF;  // Accept only channels bound to IRQ 0 by the driver
  adc_dma_channel_0 = dma_request_channel(dma, &filter);
  adc_dma_channel_1 = dma_request_channel(dma, &filter);
  LOG_DBG("ADC will used DMA channels %d and %d", adc_dma_channel_0, adc_dma_channel_1);
  Initialize_ADC_DMA(adc_dma_channel_0, adc_dma_channel_1, sample_sets.set_0, SAMPLE_COUNT);
  Initialize_ADC_DMA(adc_dma_channel_1, adc_dma_channel_0, sample_sets.set_1, SAMPLE_COUNT);
  adc_dma_cfg_0 = dma_channel_get_default_config(adc_dma_channel_0);
  adc_dma_cfg_1 = dma_channel_get_default_config(adc_dma_channel_1);
  LOG_DBG("Initial ADC DMA setup complete");
}

/* DMA-based modulator support */

// Re-fill one modulator buffer with the next few baud worth of modulation
// data.
static void TIME_CRITICAL(refill_modulator_buffer)(uint8_t *buffer) {
  set_tx_busy_led(1);
  int offset = 0;
  static bool data_bit = false;
  bool new_data_bit_avail = false;
  LOG_DBG("Refill modulator buffer with %d bytes", modulator_buffer_bytes_active);
  while (offset < modulator_buffer_bytes_active) {
    if (HDLC_Output_Avail()) {
      LOG_DBG("Take HDLC bit");
      data_bit = HDLC_Get_Bit();
      new_data_bit_avail = true;
      board_set_hdlc(data_bit);
      if (!tnc_state.adc_loopback) {
	set_data_leds(data_bit);
      }
      if (tnc_state.hdlc_loopback) {
	ConsumeNRZI(&hdlc_context_flat, data_bit);
      }
    } else {
      LOG_DBG("HDLC starved");
      new_data_bit_avail = false;
    }
    // Now, we must load the modulator data for the next baud
    // into whichever buffer was just completed.
    switch (tnc_state.modulator_transmitting) {
    case PWM_MODULATOR:
      for (int i = 0 ; i < OUTPUT_SAMPLES_PER_BAUD; i++) {
	assert(offset < modulator_buffer_bytes_active); // just to be safe...
	/* Increment the accumulated phase angle by 1/32 baud */
	scaled_sine_cycles += scaled_sine_cycles_per_output_sample;
	/* Find the index into the sine table */
	uint32_t sine_index = ((scaled_sine_cycles & 0xFFFF) * PWM_ENTRIES) >> 16;
	/* Put the sine value in both halfwords, so that it'll affect whichever
	   PWM channel the pin is assigned to.
	*/
	*(uint32_t *)(buffer + offset) = (pwmtable[sine_index] << 16) | pwmtable[sine_index];
	offset += sizeof(uint32_t);
#if 0
	if (tnc_state.adc_loopback) {
	  static int adc_loopback_index = 0;
	  /* The following depends on the fact that the two sample sets are
	   * contiguous in memory.  Also, the scaling is crude - would be
	   * better to auto-scale to about 1/2 max amplitude based on the
	   * PWM period / high-time ratio.  TODO - clean this up.
	   */
	  sample_sets.set_0[adc_loopback_index] = 0x800 + (pwmtable[sine_index] << 2);
	  adc_loopback_index = (adc_loopback_index + 1) % (SAMPLE_COUNT * 2);
	  if (adc_loopback_index == SAMPLE_COUNT) {
	    sample_set_0_ready = true;
	    k_sem_give(&sampler_sem);
	  } else if (adc_loopback_index == 0) {
	    sample_set_1_ready = true;
	    k_sem_give(&sampler_sem);
	  }
	}
#endif
      }
      break;
    case DELTA_SIGMA_MODULATOR:
      /* The delta-sigma modulator always DMAs out a full baud's worth of sample
	 data at a time.  It's copied from either the idle stream, or the stream
	 associated with the current tone and offset.
      */
      if (new_data_bit_avail) {
	memcpy(buffer + offset,
	       noiseshaping.tones[data_bit].phases[phase_index].samples,
	       noiseshaping.bytes_per_baud);
	// Bump the phase index (wrapping around).
	phase_index = (phase_index + noiseshaping.tones[data_bit].phase_increment) %
	  noiseshaping.phases;
      } else {
	memcpy(buffer + offset, noiseshaping.idle, noiseshaping.bytes_per_baud);
      }
      offset += noiseshaping.bytes_per_baud;
      break;
    case G3RUH_PWM_MODULATOR:
      // Send 1-bits if we don't get new data, to try to force
      // an abort.
      if (!new_data_bit_avail) {
	data_bit = 1;
      }
      // If we're in waveform-test mode, override the HDLC encoder and
      // the G3RUH data scrambler.  Simply send out a 1-bit (positive
      // pulse) every 32 baud, so we can see the waveform.
      bool out;
      if (tnc_state.waveform_test) {
	out = (G3RUH_modulator_state.scrambler >> 31) & 1;
	G3RUH_modulator_state.scrambler = G3RUH_modulator_state.scrambler << 1;
	// Handle both the initial all-zero state, and the once-in-32 overflow.
	if (G3RUH_modulator_state.scrambler == 0) {
	  G3RUH_modulator_state.scrambler = 1;
	}
      } else {
	// Implement the G3RUH data scrambler.
	out = 1 & (data_bit ^ (G3RUH_modulator_state.scrambler >> 11) ^ (G3RUH_modulator_state.scrambler >> 16));
	G3RUH_modulator_state.scrambler = (G3RUH_modulator_state.scrambler << 1) | out;
      }
      G3RUH_modulator_state.transmit_bit_history = ((G3RUH_modulator_state.transmit_bit_history << 1) | out) &
	G3RUH_modulator_state.transmit_bit_mask;
      // Every sample going to the PWM is four bytes long.
      int32_t bytes_per_burst = G3RUH_modulator_cache.samples_per_baud * sizeof(int32_t);
      LOG_DBG("G3RUH bit pattern 0x%04x to offset %d, %d bytes",
	      G3RUH_modulator_state.transmit_bit_history, offset, bytes_per_burst);
      memcpy(buffer + offset,
	     &G3RUH_modulator_cache.bursts[G3RUH_modulator_state.transmit_bit_history],
	     bytes_per_burst);
      offset += bytes_per_burst;
      break;
    default:
      LOG_ERR("Oops, bad modulator in callback");
      break;
    }
  }
  set_tx_busy_led(0);
}

// Called from the "transceiver" thread.
void TIME_CRITICAL(board_modulator)(void)
{
  if (modulator_buffer_0_empty) {
    refill_modulator_buffer(modulator_buffer_0);
    modulator_buffer_0_empty = false;
  }
  if (modulator_buffer_1_empty) {
    refill_modulator_buffer(modulator_buffer_1);
    modulator_buffer_1_empty = false;
  }
}

static void TIME_CRITICAL(modulator_callback)(const struct device *dev, void *user_data, uint32_t channel, int status)
{
  dma_interrupts++;
  if (status) {
    LOG_ERR("DMA channel %d status %d", channel, status);
  }
  if (channel == modulator_dma_channel_0) {
    modulator_buffer_0_empty = true;
    k_sem_give(&sampler_sem);
  } else if (channel == modulator_dma_channel_1) {
    modulator_buffer_1_empty = true;
    k_sem_give(&sampler_sem);
  } else {
    LOG_ERR("IRQ callback on unknown modulator DMA channel");
    return;
  }
  dma_channel_set_irq0_enabled(channel, true);
}

static void Configure_modulator_DMA(dma_channel_config *config, int channel, int other_channel, uint8_t *buffer) {
  struct dma_config dma_cfg;
  struct dma_block_config block_cfg;
  /*
    Use the Zephyr DMA API to set up the basics of the channel.  The configuration
    doesn't have to be entirely correct, as it will never actually be used... it
    just has to be good enough to pass muster with the driver, so that the driver
    recognizes the channel and can pass the proper IRQ to our callback routine.
  */
  memset(&dma_cfg, 0, sizeof(dma_cfg));
  memset(&block_cfg, 0, sizeof(block_cfg));
  dma_cfg.head_block = &block_cfg;
  dma_cfg.channel_direction = MEMORY_TO_PERIPHERAL;
  dma_cfg.block_count = 1;
  dma_cfg.source_data_size = sizeof(uint32_t);
  dma_cfg.dest_data_size = sizeof(uint32_t);
  dma_cfg.dma_callback = modulator_callback;
  block_cfg.source_address = (uint32_t) buffer;
  block_cfg.source_addr_adj = DMA_ADDR_ADJ_INCREMENT;
  block_cfg.dest_address = (uint32_t) buffer; // will be filled in by modulator startup
  block_cfg.dest_addr_adj = DMA_ADDR_ADJ_NO_CHANGE;
  block_cfg.block_size = MODULATOR_BUFFER_BYTES;

  dma_config(dma, channel, &dma_cfg);

  channel_config_set_read_increment(config, true);
  channel_config_set_write_increment(config, false);
  channel_config_set_transfer_data_size(config, DMA_SIZE_32); // updated by modulator startup
  channel_config_set_chain_to(config, other_channel);
  channel_config_set_ring(config, false, CONFIG_OUTPUT_BUFFER_BYTES_LOGARITHM);  // write==false, so ring the read side
  dma_channel_set_config(channel, config, false);
  dma_channel_set_read_addr(channel, buffer, false);
  dma_channel_set_write_addr(channel, buffer, false);
  dma_channel_set_trans_count(channel, MODULATOR_BUFFER_BYTES / sizeof(uint32_t), false);
  LOG_DBG("Modulator DMA channel %d set up, chains to channel %d", channel, other_channel);
}

static void Setup_DMA_for_modulator(void) {
  uint32_t filter = 0x00FFFFFF;  // Accept only channels bound to IRQ 0
  modulator_dma_channel_0 = dma_request_channel(dma, &filter);
  modulator_dma_channel_1 = dma_request_channel(dma, &filter);
  LOG_DBG("Modulator will use DMA channels %d and %d", modulator_dma_channel_0, modulator_dma_channel_1);
  modulator_dma_cfg_0 = dma_channel_get_default_config(modulator_dma_channel_0);
  modulator_dma_cfg_1 = dma_channel_get_default_config(modulator_dma_channel_1);
  Configure_modulator_DMA(&modulator_dma_cfg_0, modulator_dma_channel_0, modulator_dma_channel_1, modulator_buffer_0);
  Configure_modulator_DMA(&modulator_dma_cfg_1, modulator_dma_channel_1, modulator_dma_channel_0, modulator_buffer_1);
  LOG_DBG("Modulator DMA setup done");
  modulator_dma_timer = dma_claim_unused_timer(true);
  LOG_DBG("Modulator will use DMA timer %d", modulator_dma_timer);
  dma_timer_set_fraction(modulator_dma_timer, 1, sys_clock_hw_cycles_per_sec() / SAMPLES_PER_SECOND);
  LOG_DBG("DMA timer multiplier set to %d / %d", SAMPLES_PER_SECOND, sys_clock_hw_cycles_per_sec());
  pio_sm = 0;
  pio_offset = pio_add_program(pio0, &bitsender_program);
  LOG_DBG("Bitsender PIO program loaded at offset %d for sm %d", pio_offset, pio_sm);
  bitsender_program_init(pio0, pio_sm, pio_offset, tx_data_out.pin, noiseshaping.bitclock_divisor);
}

#define SETUP(ptr, node, pin, flags) \
  ptr = device_get_binding(node); \
  if (ptr == NULL) return; \
  ret = gpio_pin_configure(ptr, pin, GPIO_OUTPUT_ACTIVE | flags); \
  if (ret < 0) return; \
  gpio_pin_set(ptr, pin, 0);

#define ISOK(dev, mode)		 \
  if (!gpio_is_ready_dt(&dev)) { \
    LOG_ERR("GPIO is not ready"); \
  } else { \
    int ret = gpio_pin_configure_dt(&dev, mode); \
    if (ret < 0) { \
      LOG_ERR("GPIO not configured");		\
   } else LOG_DBG("Pin configured for mode %d", mode);	\
  }

void board_init_default_tnc_state(void)
{
}

void board_init(void)
{
	int ret;

	ISOK(ptt, GPIO_OUTPUT_ACTIVE);
	ISOK(hdlc, GPIO_OUTPUT_ACTIVE);
	ISOK(squelch_input, GPIO_INPUT);

	SetupPins();

	// Turn off all of the LEDs
	set_heartbeat_led(0);
	set_data_1_led(0);
	set_data_0_led(0);
	set_hdlc_lock_led(0);
	set_good_packet_led(0);
	set_tx_busy_led(0);
	set_rx_busy_led(0);
	set_ptt_led(PTT_OFF);

	gpio_pin_set_dt(&ptt, 0);

        if (!device_is_ready(cdc_dev)) {
                LOG_ERR("CDC ACM device not ready");
        }

	ret = usb_enable(NULL);
	if (ret != 0) {
		LOG_ERR("Failed to enable USB");
		//		return;
	}

	LOG_DBG("Start heartbeat");

	k_thread_create(&blink_thread,
	  tstack[0], HEARTBEAT_STACK_SIZE,
	  (k_thread_entry_t)common_heartbeat,
	  NULL, NULL, NULL,
	  HEARTBEAT_THREAD_PRIORITY, 0, K_NO_WAIT);

	uart_irq_callback_set(cdc_dev, CDC_interrupt_handler);

	/* Enable rx interrupts */
	uart_irq_rx_enable(cdc_dev);

	LOG_DBG("Start transceiver thread");

	k_thread_create(&transceiver_thread,
			transceiver_stack[0], TRANSCEIVER_STACK_SIZE,
			(k_thread_entry_t)board_transceiver,
			NULL, NULL, NULL,
			TRANSCEIVER_THREAD_PRIORITY, 0, K_NO_WAIT);
	
	board_set_tx_freq(LOW_TONE); /* set to a reference frequency */

	LOG_DBG("Setting up DMA");

	Setup_DMA_for_sampler();
	Setup_DMA_for_modulator();
	
	adc = init_adc();
	if (!adc) {
	  LOG_ERR("ADC not set up properly");
	}

#ifdef HAS_WS2812_SUPPORT
	LOG_DBG("Setting up DMA and PIO for WS2812");
	Setup_DMA_for_WS2812();
#endif

	k_msleep(100);
	LOG_DBG("Running...");
}

/* We want a pseudo-random number generator which is seeded in a way
 * which nearly guaranteed a different sequence on each boot-up.
 * Since some platforms don't have a full-fledged random entropy
 * source of their own, we'll fake it by seeding the PRNG the first
 * time we use it (when doing P-persistence for the first packet we're
 * about to transmit) using the CPU cycle counter as part of the seed.
 * For the rest of the seed we'll use the most-recently-digitized
 * samples from the transceiver.
 */
uint32_t board_get_random32(void) {
  static bool random_initialized = false;
  if (!random_initialized) {
    xoshiro128_init(sample_sets.set_0[0], sample_sets.set_0[10],
		    sample_sets.set_0[20], k_cycle_get_32());
    /* Whiten the seed bits */
    xoshiro128();
    xoshiro128();
    xoshiro128();
    xoshiro128();
    random_initialized = true;
  }
  return xoshiro128();
}

static void cleanup_my_dma_channel(unsigned int channel) {
  // disable the channel on IRQ0
  dma_channel_set_irq0_enabled(channel, false);
  // abort the channel
  dma_channel_abort(channel);
  // Wait long enough that the DMA timer has time to churn once.
  k_sleep(K_MSEC(1));
  // clear the spurious IRQ (if there was one)
  dma_channel_acknowledge_irq0(channel);
}

void board_enable_rx(void) {
  LOG_DBG("Sample count is %d, sample ring bits is %d", adc_buffer_bytes_active / sizeof(input_sample), 
	  adc_buffer_ring_bits);
  Configure_ADC_DMA(&adc_dma_cfg_0, adc_dma_channel_0, adc_dma_channel_1, sample_sets.set_0,
		    adc_buffer_bytes_active / sizeof(input_sample), adc_buffer_ring_bits);
  Configure_ADC_DMA(&adc_dma_cfg_1, adc_dma_channel_1, adc_dma_channel_0, sample_sets.set_1,
		    adc_buffer_bytes_active / sizeof(input_sample), adc_buffer_ring_bits);
  LOG_DBG("ADC DMA configuration done");
  // Trigger channel 0.  It won't start seeing any incoming data until the ADC is
  // put into free-running mode by the enable-RX logic.
  LOG_DBG("Starting DMA channel %d", adc_dma_channel_0);
  dma_channel_start(adc_dma_channel_0);
  LOG_DBG("ADC DMA started");
  LOG_DBG("Starting ADC");
  adc_run(true);
}

void board_disable_rx(void){
  LOG_DBG("Stopping ADC");
  adc_run(false);
  k_msleep(1);
  LOG_DBG("Unchaining ADC DMA channels");
  channel_config_set_chain_to(&adc_dma_cfg_0, adc_dma_channel_0);
  channel_config_set_chain_to(&adc_dma_cfg_1, adc_dma_channel_1);
  dma_channel_set_config(adc_dma_channel_0, &adc_dma_cfg_0, false);
  dma_channel_set_config(adc_dma_channel_1, &adc_dma_cfg_1, false);
  LOG_DBG("Shutting down ADC DMA channels");
  cleanup_my_dma_channel(adc_dma_channel_0);
  cleanup_my_dma_channel(adc_dma_channel_1);
  LOG_DBG("Flushing ADC FIFO");
  adc_fifo_drain();
}

void board_enable_tx(void){
  LOG_DBG("Enabling TX, modulator %d", tnc_state.modulator);
  set_data_1_led(0);
  set_data_0_led(0);
  board_set_tx_freq(LOW_TONE);
  scaled_sine_cycles = 0;
  samples_sent = 0;
  tnc_state.modulator_transmitting = tnc_state.modulator;
  tnc_state.audio_peak = 0;
  tnc_state.squelch_open_detect = false;
  switch (tnc_state.modulator_transmitting) {
  case PWM_MODULATOR:
  case G3RUH_PWM_MODULATOR:
  default:
    LOG_DBG("Setting data pin to PWM, and enabling");
    gpio_pin_configure_dt(&tx_data_out, GPIO_OUTPUT_ACTIVE);
    gpio_set_function(tx_data_out.pin, GPIO_FUNC_PWM);
    tx_data_slice = pwm_gpio_to_slice_num(tx_data_out.pin);
    tx_data_channel = pwm_gpio_to_channel(tx_data_out.pin);
    pwm_config config = pwm_get_default_config();
    pwm_config_set_wrap(&config, PWM_PERIOD);
    pwm_config_set_clkdiv_int(&config, 1);
    LOG_DBG("Configuring PWM, pin %d, slice %d chan %d", tx_data_out.pin, tx_data_slice, tx_data_channel);
    pwm_init(tx_data_slice, &config, false);
    LOG_DBG("PWM period is %d", PWM_PERIOD);
    /* Set the GPIO channel to half-scale. */
    pwm_set_chan_level(tx_data_slice, tx_data_channel, PWM_PERIOD / 2);
    pwm_set_enabled(tx_data_slice, true);
    LOG_DBG("PWM enabled");
    struct rational dma_timer;
    if (tnc_state.modulator_transmitting == PWM_MODULATOR) {
      dma_timer = rationalize(OUTPUT_SAMPLES_PER_SECOND / (float) sys_clock_hw_cycles_per_sec(),
			      0xFFFF);
    } else {
      dma_timer = rationalize((BAUDRATE_G3RUH * G3RUH_modulator_cache.samples_per_baud) /
			      (float) sys_clock_hw_cycles_per_sec(),
			      0xFFFF);
    }
    LOG_DBG("Adjusting modulator DMA timer to %d/%d cycles", dma_timer.numerator, dma_timer.denominator);
    dma_timer_set_fraction(modulator_dma_timer, dma_timer.numerator, dma_timer.denominator);
    /* Fill the modulator DMA buffers with the same half-scale value, in
       the format required by the register.
    */
    LOG_DBG("Filling buffer");
    {
      int offset = 0;
      while (offset < sizeof(modulator_buffer_0)) {
	*(uint32_t *)(modulator_buffer_0 + offset) = (PWM_PERIOD / 2) | (PWM_PERIOD / 2);
	offset += sizeof(uint32_t);
      }
      memcpy(modulator_buffer_1, modulator_buffer_0, sizeof(modulator_buffer_1));
    }
    /* Identify the hardware register-part assigned to this PWM slice and channel.
    */
    uint32_t *cc = (uint32_t *) &pwm_hw->slice[tx_data_slice].cc;
    LOG_DBG("Configuring DMA channels.  Write register is 0x%X", (uint32_t) cc);
    channel_config_set_transfer_data_size(&modulator_dma_cfg_0, DMA_SIZE_32);
    channel_config_set_transfer_data_size(&modulator_dma_cfg_1, DMA_SIZE_32);
    channel_config_set_dreq(&modulator_dma_cfg_0, dma_get_timer_dreq(modulator_dma_timer));
    channel_config_set_dreq(&modulator_dma_cfg_1, dma_get_timer_dreq(modulator_dma_timer));
    // Change the ring size to match the subset of the DMA buffer size we've
    // decided to use.
    LOG_DBG("  Setting ring bits to %d, buffer byte count is %d",
	    modulator_buffer_ring_bits, modulator_buffer_bytes_active);
    channel_config_set_ring(&modulator_dma_cfg_0, false, modulator_buffer_ring_bits);
    channel_config_set_ring(&modulator_dma_cfg_1, false, modulator_buffer_ring_bits);
    // The two channels are chained together only when running.  It's necessary
    // to un-chain them when stopping them, so they'll stop properly.
    channel_config_set_chain_to(&modulator_dma_cfg_0, modulator_dma_channel_1);
    channel_config_set_chain_to(&modulator_dma_cfg_1, modulator_dma_channel_0);
    dma_channel_set_config(modulator_dma_channel_0, &modulator_dma_cfg_0, false);
    dma_channel_set_config(modulator_dma_channel_1, &modulator_dma_cfg_1, false);
    dma_channel_set_read_addr(modulator_dma_channel_0, modulator_buffer_0, false);
    dma_channel_set_read_addr(modulator_dma_channel_1, modulator_buffer_1, false);
    dma_channel_set_write_addr(modulator_dma_channel_0, cc, false);
    dma_channel_set_write_addr(modulator_dma_channel_1, cc, false);
    dma_channel_set_trans_count(modulator_dma_channel_0, modulator_buffer_bytes_active / sizeof(uint32_t), false);
    dma_channel_set_trans_count(modulator_dma_channel_1, modulator_buffer_bytes_active / sizeof(uint32_t), false);
    dma_channel_set_irq0_enabled(modulator_dma_channel_0, true);
    dma_channel_set_irq0_enabled(modulator_dma_channel_1, true);
    dma_channel_start(modulator_dma_channel_0);
    bool busy_0 = dma_channel_is_busy(modulator_dma_channel_0);
    bool busy_1 = dma_channel_is_busy(modulator_dma_channel_1);
    LOG_DBG("PWM modulator start");
    LOG_DBG("PWM DMA channel busy status: %d %d", busy_0, busy_1);
    break;
  case DELTA_SIGMA_MODULATOR:
    LOG_DBG("Setting data pin to PIO, and enabling");
    gpio_pin_configure_dt(&tx_data_out, GPIO_OUTPUT_ACTIVE);
    gpio_set_function(tx_data_out.pin, GPIO_FUNC_PIO0);
    phase_index = 0;
    /* Fill the modulator DMA buffers with an averaging pattern */
    memset(modulator_buffer_0, 0x55, sizeof(modulator_buffer_0));
    memset(modulator_buffer_1, 0x55, sizeof(modulator_buffer_1));
    channel_config_set_transfer_data_size(&modulator_dma_cfg_0, DMA_SIZE_32);
    channel_config_set_transfer_data_size(&modulator_dma_cfg_1, DMA_SIZE_32);
    channel_config_set_dreq(&modulator_dma_cfg_0, pio_get_dreq(pio0, pio_sm, true));
    channel_config_set_dreq(&modulator_dma_cfg_1, pio_get_dreq(pio0, pio_sm, true));
    channel_config_set_chain_to(&modulator_dma_cfg_0, modulator_dma_channel_1);
    channel_config_set_chain_to(&modulator_dma_cfg_1, modulator_dma_channel_0);
    channel_config_set_ring(&modulator_dma_cfg_0, false, modulator_buffer_ring_bits);
    channel_config_set_ring(&modulator_dma_cfg_1, false, modulator_buffer_ring_bits);
    dma_channel_set_config(modulator_dma_channel_0, &modulator_dma_cfg_0, false);
    dma_channel_set_config(modulator_dma_channel_1, &modulator_dma_cfg_1, false);
    dma_channel_set_read_addr(modulator_dma_channel_0, modulator_buffer_0, false);
    dma_channel_set_read_addr(modulator_dma_channel_1, modulator_buffer_1, false);
    dma_channel_set_write_addr(modulator_dma_channel_0, &pio0->txf[pio_sm], false);
    dma_channel_set_write_addr(modulator_dma_channel_1, &pio0->txf[pio_sm], false);
    dma_channel_set_trans_count(modulator_dma_channel_0, modulator_buffer_bytes_active / sizeof(uint32_t), false);
    dma_channel_set_trans_count(modulator_dma_channel_1, modulator_buffer_bytes_active / sizeof(uint32_t), false);
    dma_channel_set_irq0_enabled(modulator_dma_channel_0, true);
    dma_channel_set_irq0_enabled(modulator_dma_channel_1, true);
    dma_channel_start(modulator_dma_channel_0);
    // "Release the Kraken!"
    pio_sm_set_enabled(pio0, pio_sm, true);
    LOG_DBG("Delta-sigma modulator start");
    break;
  }
}

void board_disable_tx(void){
  LOG_DBG("Disabling TX");
  switch (tnc_state.modulator_transmitting) {
  case PWM_MODULATOR:
  case G3RUH_PWM_MODULATOR:
  default:
    // Un-chain them
    channel_config_set_chain_to(&modulator_dma_cfg_0, modulator_dma_channel_0);
    channel_config_set_chain_to(&modulator_dma_cfg_1, modulator_dma_channel_1);
    dma_channel_set_config(modulator_dma_channel_0, &modulator_dma_cfg_0, false);
    dma_channel_set_config(modulator_dma_channel_1, &modulator_dma_cfg_1, false);
    cleanup_my_dma_channel(modulator_dma_channel_0);
    cleanup_my_dma_channel(modulator_dma_channel_1);
    if (tx_data_slice >= 0) {
      pwm_set_chan_level(tx_data_slice, tx_data_channel, PWM_PERIOD / 2);
    }
    break;
  case DELTA_SIGMA_MODULATOR:
    pio_sm_set_enabled(pio0, pio_sm, false);
    // Un-chain them
    channel_config_set_chain_to(&modulator_dma_cfg_0, modulator_dma_channel_0);
    channel_config_set_chain_to(&modulator_dma_cfg_1, modulator_dma_channel_1);
    dma_channel_set_config(modulator_dma_channel_0, &modulator_dma_cfg_0, false);
    dma_channel_set_config(modulator_dma_channel_1, &modulator_dma_cfg_1, false);
    cleanup_my_dma_channel(modulator_dma_channel_0);  // abort, disable IRQ
    cleanup_my_dma_channel(modulator_dma_channel_1);  // abort, disable IRQ
    pio_sm_clear_fifos(pio0, pio_sm);
    break;
  }
  // Switch the TX pin back to normal GPIO mode, so it remains at a constant
  // value when we're not transmitting.  No sense pushing anything into the
  // low-pass filter (and maybe making some RF noise) when we don't need to.
  gpio_set_function(tx_data_out.pin, GPIO_FUNC_SIO);
  set_tx_busy_led(0);
}

void board_set_tx_freq(uint32_t hz) {
  scaled_sine_cycles_per_output_sample = (hz << 16) / OUTPUT_SAMPLES_PER_SECOND;
  LOG_DBG("Scaled sine cycles per output sample: 0x%08X", scaled_sine_cycles_per_output_sample);
}

static const uint32_t parameter_total_size =
  DT_PROP(ZEPHYR_USER_NODE, parameter_flash_region_size);
static const uint32_t parameter_offset =
  DT_REG_SIZE_BY_IDX(DT_ALIAS(flashblock), 0) -
  DT_PROP(ZEPHYR_USER_NODE, parameter_flash_region_size);
static const uint32_t parameter_chunk_size =
  DT_PROP(ZEPHYR_USER_NODE, parameter_flash_chunk_size);

static bool scan_for_parameters(struct saved_parameters *latest,
				int32_t *free) {
  bool result = false;
  int32_t most_recent_revision = -1;
  int32_t found_free = -1;
  LOG_DBG("Looking for saved parameters in flash, offset %d total_size %d chunk_size %d",
	  parameter_offset, parameter_total_size, parameter_chunk_size);
  for (int offset = parameter_offset;
       offset < parameter_offset + parameter_total_size;
       offset += parameter_chunk_size) {
    // To allow for some amount of reverse compatibility (older
    // firmware reading larger parameter blocks saved by newer
    // firmware) we'll read more than we "understand".
    struct {
      struct saved_parameters sp;
      uint8_t padding[128];
    } buffer;
    LOG_DBG("Reading from flash, offset %d", offset);
    memset(&buffer, 0, sizeof(buffer));
    int err = flash_read(flash_dev, offset, &buffer, sizeof(buffer));
    if (err) {
      LOG_ERR("Flash read at %d returned %d", offset, err);
      return result;
    }
    uint32_t checksum = buffer.sp.checksum;
    uint32_t computed_checksum = 0xDEADF00D;
    bool size_ok = false;
    if (buffer.sp.sizeof_me > 5 * sizeof(uint32_t) &&
	buffer.sp.sizeof_me <= sizeof(buffer)) {
      size_ok = true;
      buffer.sp.checksum = 0xFFFFFFFF;
      computed_checksum = crc32_ieee((uint8_t *) &buffer.sp,
				    buffer.sp.sizeof_me);
    }
    LOG_DBG("Found magic 0x%08x size %d version %d revision %d (ref %d) checksum 0x%04x computed 0x%04x size_ok %s",
	    buffer.sp.magic, buffer.sp.sizeof_me, buffer.sp.version, buffer.sp.revision,
	    most_recent_revision,
	    checksum, computed_checksum,
	    size_ok ? "Y" : "N");
    if (buffer.sp.magic == SAVED_PARAMETER_MAGIC &&
	size_ok &&
	(int32_t) buffer.sp.revision > most_recent_revision &&
	checksum == computed_checksum) {
      LOG_DBG("Found saved parameter revision %d at offset %d",
	      buffer.sp.revision, offset);
      if (latest) {
	*latest = buffer.sp;
      }
      most_recent_revision = buffer.sp.revision;
      result = true;
    } else if (found_free < 0) {
      bool is_free = true;
      const uint8_t *possible = (uint8_t *) &buffer;
      for (int i = 0; is_free && i < sizeof(buffer); i++) {
	if (0xFF != *(possible + i)) {
	  is_free = false;
	  LOG_DBG(" Non-free byte 0x%02X at sub-offset %d\n",
		  *(possible + i), i);
	}
      }
      if (is_free) {
	LOG_DBG("Found a free block at %d", offset);
	if (free) {
	  *free = offset;
	}
	found_free = offset;
      }
    }
  }
  LOG_DBG("Scan complete");
  return result;
}

// This is shared between board_load_saved_parameters() and
// board_save_parameters.  It would be proper to secure it
// via a mutex.

static struct saved_parameters sp;

K_MUTEX_DEFINE(sp_mutex);

bool board_load_saved_parameters(void) {
  int32_t free;
  bool result = false;
  k_mutex_lock(&sp_mutex, K_FOREVER);
  memset(&sp, 0, sizeof(sp)); // Keep the compiler happy
  bool found = scan_for_parameters(&sp, &free);
  if (found) {
    LOG_DBG("Loading saved parameter, version %d revision %d",
	    sp.version, sp.revision);
    load_saved_parameters(&sp);
    result = true;
  }
  k_mutex_unlock(&sp_mutex);
  return result;
}

bool board_save_parameters(void) {
  k_mutex_lock(&sp_mutex, K_FOREVER);
  memset(&sp, 0, sizeof(sp)); // Keep the compiler happy
  int32_t free;
  int32_t revision;
  bool result = false;
  bool found = scan_for_parameters(&sp, &free);
  if (found) {
    revision = sp.revision + 1;
    LOG_DBG("Next revision is %d", revision);
  } else {
    revision = 1;
    LOG_DBG("No saved parameters;  this revision is 1");
  }
  prepare_to_save_parameters(&sp, revision);
  LOG_DBG("Saving magic 0x%08x size %d revision %d checksum 0x%08x",
	  sp.magic, sp.sizeof_me, sp.revision, sp.checksum);
  if (free >= 0) {
    LOG_DBG("It looks like a free block at offset %d", free);
  } else {
    LOG_DBG("No flash blocks free, need to erase, offset %d size %d",
	    parameter_offset, parameter_total_size);
    if (flash_erase(flash_dev, parameter_offset,
		   parameter_total_size) != 0) {
      LOG_ERR("Flash erase failed!");
      k_mutex_unlock(&sp_mutex);
      return false;
    }
    LOG_DBG("Flash region erased");
    found = scan_for_parameters(NULL, &free);
    if (free >= 0) {
      LOG_DBG("Rescan found a free block at %d", free);
    } else {
      LOG_ERR("Rescan found no free block!");
      k_mutex_unlock(&sp_mutex);
      return false;
    }
  }
  LOG_DBG("Writing block at offset %d", free);
  int write_result = flash_write(flash_dev, free, &sp, sizeof(sp));
  if (write_result == 0) {
    LOG_DBG("Flash write appears to have succeeded");
    result = true;
  } else {
    LOG_DBG("Flash write returned %d", write_result);
  }
  k_mutex_unlock(&sp_mutex);
  return result;
}

void board_enter_bootloader(void) {
  void *entry = rom_func_lookup(rom_table_code('U', 'B'));
  if (!entry) {
    LOG_ERR("Can't find USB bootloader entry");
    return;
  }
  void (*callable)(uint32_t gpio_activity_pin_mask, uint32_t disable_interface_mask) =
    (void (*)(uint32_t, uint32_t)) entry;
  (*callable)(0, 0);
  LOG_ERR("Oops, bootloader entry failed!");
  return;
}

void board_configure_modulator(struct modulator_state *state,
			       struct modulator_cache *cache) {
  if (cache->bursts_are_in_float_format) {
    LOG_DBG("Remapping modulator data");
    for (int pattern = 0; pattern < 1 << G3RUH_SPAN_IN_BAUDS; pattern++) {
      for (int index = 0; index < G3RUH_SAMPLES_PER_BAUD; index++) {
	int32_t new = (cache->bursts[pattern].samples[index].float_format + 1.0f) * 127;
	cache->bursts[pattern].samples[index].int_format = (new << 16) | new;
      }
      cache->bursts_are_in_float_format = false;
    }
  }
  size_t buffer_size = MODULATOR_BUFFER_BYTES;
  int32_t ring_bits = CONFIG_OUTPUT_BUFFER_BYTES_LOGARITHM;
  int32_t sample_rate, bytes_per_sample;
  switch (tnc_state.modulator) {
  case PWM_MODULATOR:
  default:
    sample_rate = OUTPUT_SAMPLES_PER_SECOND;
    bytes_per_sample = sizeof(int32_t);
    break;
  case DELTA_SIGMA_MODULATOR:
    sample_rate = BAUDRATE;
    bytes_per_sample = noiseshaping.bytes_per_baud;
    break;
  case G3RUH_PWM_MODULATOR:
    sample_rate = BAUDRATE_G3RUH * G3RUH_modulator_cache.samples_per_baud;
    bytes_per_sample = sizeof(int32_t);
    break;
  }
  LOG_DBG("Computing output buffer size - max possible is %d, ring bits %d", buffer_size, ring_bits);
  LOG_DBG("  Computed %d samples/second, %d byte per sample", sample_rate, bytes_per_sample);
  int32_t hz;
  adjust_buffer_size(sample_rate, bytes_per_sample, &buffer_size, &ring_bits, &hz);
  LOG_DBG("  Computed output buffer size %d, ring bits %d, %d hz", buffer_size, ring_bits, hz);
  // Save this for actual TX
  modulator_buffer_bytes_active = buffer_size;
  modulator_buffer_ring_bits = ring_bits;
}
