The shell code used in this project is adapted from the "complex firmware shell"
example code, available at:

   https://github.com/memfault/interrupt.git

The Interrupt content was released under the terms of the Creative
Commons Attribution-ShareAlike 4.0 International license (see
https://github.com/memfault/interrupt?tab=License-1-ov-file#readme)
and this license applies to the derived version of the complex
firmware shell incorporated into the OpenTNC project.
