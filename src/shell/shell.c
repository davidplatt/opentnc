
#include "shell.h"

#include <ctype.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <strings.h>

#include <zephyr/kernel.h>
#include "../globals.h"

#define SHELL_RX_BUFFER_SIZE (256)
#define SHELL_MAX_ARGS (16)
#define SHELL_PROMPT "cmd:"

#define SHELL_FOR_EACH_COMMAND(command) \
  for (const sShellCommand *command = g_shell_commands; \
    command < &g_shell_commands[g_num_shell_commands]; \
    ++command)

static struct ShellContext {
  int (*send_char)(char c);
  size_t rx_size;
  char rx_buffer[SHELL_RX_BUFFER_SIZE];
} s_shell;

static bool prv_booted(void) {
  return s_shell.send_char != NULL;
}

static void prv_send_char(char c) {
  if (!prv_booted()) {
    return;
  }
  s_shell.send_char(c);
}

static void prv_echo(char c) {
  if ('\n' == c) {
    prv_send_char('\r');
    prv_send_char('\n');
  } else if (c == tnc_state.delete) {
    if (tnc_state.bkondel) {
      prv_send_char('\b');
      prv_send_char(' ');
      prv_send_char('\b');
    } else {
      prv_send_char('\\');
    }
  } else {
    prv_send_char(c);
  }
}

static char prv_last_char(void) {
  return s_shell.rx_buffer[s_shell.rx_size - 1];
}

static bool prv_is_rx_buffer_full(void) {
  return s_shell.rx_size >= SHELL_RX_BUFFER_SIZE;
}

static void prv_reset_rx_buffer(void) {
  memset(s_shell.rx_buffer, 0, sizeof(s_shell.rx_buffer));
  s_shell.rx_size = 0;
}

static void prv_echo_str(const char *str) {
  for (const char *c = str; *c != '\0'; ++c) {
    prv_echo(*c);
  }
}

static void prv_send_prompt(void) {
  // Optionally, delay for a while before sending the prompt, to
  // allow the TNC-to-host buffer to drain away to the host and for
  // all of what we've sent to have been read by Outpost.  Outpost
  // (some versions at least) won't recognize the prompt if it
  // appears at the end of the data from a long read... it has to
  // arrive stand-alone.
  k_msleep(tnc_state.outpost_delay);
  prv_echo_str(SHELL_PROMPT);
}

static const sShellCommand *prv_find_command(const char *name) {
  SHELL_FOR_EACH_COMMAND(command) {
    const char *cmd = command->command;
    // Commands whose names are prefixed with "!" are treated specially;
    // they're accepted as usual, but "help" doesn't show them.  Commands
    // whose name starts with '*" are accepted and shown only in expert
    // operating mode.
    if (*cmd == '!') {
      cmd++;
    } else if (*cmd == '*') {
      if (tnc_state.operating_mode == OPERATING_MODE_EXPERT) {
	cmd++;
      } else {
	continue;
      }
    }
    // Accept full command, case-insensitive.
    if (strcasecmp(cmd, name) == 0) {
      return command;
    }
    // Accept partial command, abbreviated no more than the command
    // specification allows - the first part in upper case must match,
    // the second part in lower case may be omitted or truncated.  For
    // example, a command defined at "CONNect" will match "connect",
    // "connec", or "conn", but not "con".
    int partial_len = strlen(name);
    if (partial_len < strlen(cmd) &&
	strncasecmp(cmd, name, partial_len) == 0 &&
	islower(cmd[partial_len])) {
      return command;
    }
  }
  return NULL;
}

static void prv_process(void) {
  if (prv_last_char() != '\n' && !prv_is_rx_buffer_full()) {
    return;
  }

  const char *argv[SHELL_MAX_ARGS] = {0};
  int argc = 0;

  char *next_arg = NULL;
  for (size_t i = 0; i < s_shell.rx_size && argc < SHELL_MAX_ARGS; ++i) {
    char *const c = &s_shell.rx_buffer[i];
    if (*c == ' ' || *c == '\n' || i == s_shell.rx_size - 1) {
      *c = '\0';
      if (next_arg) {
        argv[argc++] = next_arg;
        next_arg = NULL;
      }
    } else if (!next_arg) {
      next_arg = c;
    }
  }

  if (s_shell.rx_size == SHELL_RX_BUFFER_SIZE) {
    prv_echo('\n');
  }

  if (argc >= 1) {
    k_msleep(tnc_state.outpost_delay);
    const sShellCommand *command = prv_find_command(argv[0]);
    if (!command) {
      prv_echo_str("Unknown command: ");
      prv_echo_str(argv[0]);
      prv_echo('\n');
      prv_echo_str("Type 'help' to list all commands\n");
    } else {
      const char *cmd = command->command;
      if (*cmd == '*' || *cmd == '!') {
	cmd++;
      }
      argv[0] = cmd;
      command->handler(argc, argv);
    }
  }
  prv_reset_rx_buffer();
  prv_send_prompt();
}

void shell_boot(const sShellImpl *impl) {
  s_shell.send_char = impl->send_char;
  prv_reset_rx_buffer();
  prv_echo_str("\r\n" SHELL_PROMPT);
}

void shell_receive_char(char c) {
  if (c == '\r' || prv_is_rx_buffer_full() || !prv_booted()) {
    return;
  }

  if (c == tnc_state.delete) {
    if (s_shell.rx_size > 0) {
      s_shell.rx_buffer[--s_shell.rx_size] = '\0';
      prv_echo(c);
    }
    return;
  } else if (c == tnc_state.canline) {
    prv_echo_str("\\\r\n");
    prv_reset_rx_buffer();
    prv_send_prompt();
    return;
  }

  prv_echo(c);

  s_shell.rx_buffer[s_shell.rx_size++] = c;

  prv_process();
}

void shell_put_line(const char *str) {
  prv_echo_str(str);
  prv_echo('\n');
}

void shell_put_string(const char *str) {
  prv_echo_str(str);
}

int shell_help_handler(int argc, const char *argv[]) {
  if (argc == 2) {
    const sShellCommand *command = prv_find_command(argv[1]);
    if (command) {
      // If "help foo" is entered, call the handler with "foo"
      // with an illegal argc which will cause it to output
      // its "Syntax:" information.
      char cmd_buf[128];
      strncpy(cmd_buf, command->command, sizeof(cmd_buf));
      const char *cmd = (const char *) cmd_buf;
      if (*cmd == '!' || *cmd == '*') {
	cmd++;
      }
      (*command->handler)(-1, &cmd);
      prv_echo_str(command->help);
      prv_echo('\n');
      return 0;
    }
  }
  SHELL_FOR_EACH_COMMAND(command) {
    if (command->command[0] != '!') {
      const char *cmd = command->command;
      if (*cmd == '*') {
	if (tnc_state.operating_mode != OPERATING_MODE_EXPERT) {
	  continue;
	}
	cmd++;
      }
      prv_echo_str(cmd);
      prv_echo_str(": ");
      prv_echo_str(command->help);
      prv_echo('\n');
    }
  }
  return 0;
}
