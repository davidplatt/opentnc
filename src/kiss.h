/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

/* Special character encodings for KISS link (based on SLIP) */

#define KISS_FEND  0xC0
#define KISS_FESC  0xDB
#define KISS_TFEND 0xDC
#define KISS_TFESC 0xDD

/* KISS command code values */

#define KISS_CMD_MASK    0x0F
#define KISS_FLEX_MASK   0x20
#define KISS_SMACK_MASK  0x80

#define KISS_DATA_FRAME  0
#define KISS_TXDELAY     1
#define KISS_P           2
#define KISS_SLOTTIME    3
#define KISS_TXTAIL      4
#define KISS_FULLDUPLEX  5
#define KISS_SETHARDWARE 6

#define KISS_RETURN      0xFF

/* Accepts one or more bytes from host UART driver, performs KISS
 * framing and decoding, and submits to the TX ring buffer.  This
 * routine may run at IRQ level, hence may not block and should defer
 * lengthy processing to a work queue.
 */
void kiss_decode_bytes_from_host(uint8_t *buffer, uint32_t len);

/* Finds the next bytes of packet data, performs KISS framing
 * and encoding, and writes the framed/encoded bytes into the
 * supplied buffer.  Returns the number of bytes of data written; 0 means
 * "no more packet data available at this time."  buffer_len must be
 * at least 2 in order to guarantee that KISS escape-coding can be done.
 */
int32_t kiss_encode_bytes_to_host(uint8_t *buffer, uint32_t buffer_len);

/* Conditionally, request addition of a CRC16 to the current packet
 * under construction.  This will be done iff the KISS layer has
 * determined that the host supports (and wants to use) SMACK CRC
 * packets.  This just flags the buffer appropriately;  the CRC is
 * actually calculated and appended during transmission to the host.
 */
void kiss_maybe_append_crc16(ax25buffer *b);

/*
 * Read any pending data in the host inputbuffer, and parse it as KISS
 * frames.
 */
void kiss_read_from_host(void);

/*
 * Run the KISS encapsulator on packets in the receive ring, and tell
 * the board layer to push bytes towards the host.
 */
void kiss_push_to_host(void);

/*
 * Add a KISS salutation to the input ring buffer (an UNPROTO packet)
 * announcing KISS startup and giving exit instructions.
 */
void hello_kiss(void);
