/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_BOARD_RP2040
#define _H_BOARD_RP2040

#include <stdint.h>
#include <stdbool.h>
#include <zephyr/device.h>
#include <zephyr/kernel.h>

#include "board.h"
#include "globals.h"

// Use a higher sample rate for 1200-baud PWM in order to smooth out the waveform.
#define OUTPUT_SAMPLES_PER_BAUD 128
#define OUTPUT_SAMPLES_PER_SECOND (OUTPUT_SAMPLES_PER_BAUD * BAUDRATE)

#define MODULATOR_BUFFER_BYTES (1 << CONFIG_OUTPUT_BUFFER_BYTES_LOGARITHM)
#define MODULATOR_RING_BITS 9

extern int adc_dma_channel_0;
extern int adc_dma_channel_1;

extern dma_channel_config adc_dma_cfg_0;
extern dma_channel_config adc_dma_cfg_1;

extern int adc_overruns;
extern int dma_interrupts;

extern int tx_data_slice;
extern int tx_data_channel;

extern bool sample_set_0_ready;
extern bool sample_set_1_ready;

extern int phase_index;

extern uint8_t modulator_buffer_0[MODULATOR_BUFFER_BYTES];
extern uint8_t modulator_buffer_1[MODULATOR_BUFFER_BYTES];

extern int modulator_dma_channel_0;
extern int modulator_dma_channel_1;

extern dma_channel_config modulator_dma_cfg_0;
extern dma_channel_config modulator_dma_cfg_1;

extern int modulator_buffer_bytes_active;
extern int modulator_buffer_ring_bits;
extern int adc_buffer_bytes_active;
extern int adc_buffer_ring_bits;

extern dma_channel_config modulator_dma_cfg_0;
extern dma_channel_config modulator_dma_cfg_1;

extern int modulator_dma_timer;

extern bool modulator_buffer_0_empty;
extern bool modulator_buffer_1_empty;

extern uint32_t scaled_sine_cycles_per_output_sample;
extern uint32_t scaled_sine_cycles;
extern uint32_t samples_sent;

extern uint32_t pio_sm;
extern uint32_t pio_offset;

#endif

