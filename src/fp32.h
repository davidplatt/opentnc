/*
  Definitions for fixed-point numeric representation, using
  1 sign bit, 15 bits integer, 16 bits fractional.  A 64-bit
  format is provided to allow for multiplication - it has
  2 sign bits, 30 bits integer, 32 bits fractional.
*/


#ifndef _H_FP32
# define _H_FP32

#include <stdint.h>

typedef int32_t fp32_t;
typedef int64_t fp64_t;

#define force_inline static inline __attribute__((always_inline))

force_inline fp32_t int32_to_fp32(uint32_t i) { return ((fp32_t) ((i) << 16)); }

force_inline fp32_t fp32_to_int32(fp32_t f) { return (int32_t) ((f) >> 16); } // doesn't round

force_inline fp32_t float_to_fp32(float f) { return ((fp32_t) ((f) * 65536.0f)); }

force_inline float fp32_to_float(fp32_t fp) { return (((float) (fp)) / 65536.0f); }

force_inline fp64_t fp32_x_fp32(fp32_t a, fp32_t b) { return ((fp64_t) (a)) * b; }

force_inline fp32_t fp64_to_fp32(fp64_t a) { return (fp32_t) ((a) >> 16); }

force_inline fp32_t fp64_residue(fp64_t a) { return (fp32_t) ((a) & 0x0000FFFF); }

#endif
