/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_CONVERSE
# define _H_CONVERSE

#include <zephyr/kernel.h>

#include "ax25.h"

// The "converse" function enters interactive conversation mode
// (either connection-based, or unconnected/unproto).  It returns to
// the console when it wants to (e.g. when an active connection
// disconnects, or when the operator hits control-C).
//
// Transparent operation is meaningful/implemented only for
// connected mode.

bool converse(bool connected_mode, bool transparent);

#endif  // _H_CONVERSE
