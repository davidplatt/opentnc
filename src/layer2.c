/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

/*
  This file contains the code which manages incoming frames
  from the HDLC layer.  In normal operation, they'll all be
  packets after HDLC frame detection, zero-unstuffing, and
  FCS validation, and are simply passed along to the next-
  higher layer.  If FX.25 is in use, some frame buffers
  will contain FX.25 code blocks (complete HDLC frames with
  Reed-Solomon correction codes appended).  These will be
  R-S corrected, and then run through HDLC decoding which
  (if successful) will result in the validated payload being
  recirculated to this layer.
*/

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/byteorder.h>

#include "ax25.h"
#include "ax25buffer.h"
#include "board.h"
#include "console.h"
#include "globals.h"
#include "hdlc.h"
#include "layer2.h"
#include "leds.h"
#include "packetutil.h"

#include "correct.h"

#define LAYER2_STACK_SIZE 2000

LOG_MODULE_REGISTER(layer2, LOG_LEVEL_INF);

struct hdlc_rcv hdlc_context_fx25;

K_SEM_DEFINE(layer2_sem, 0, 1);
K_SEM_DEFINE(layer3_incoming_sem, 0, 1);

sys_dlist_t rx_packet_candidates = SYS_DLIST_STATIC_INIT(&rx_packet_candidates);
sys_dlist_t tx_packet_candidates = SYS_DLIST_STATIC_INIT(&tx_packet_candidates);

void layer2_init(void) {
  // The HDLC decoder used to post-process FX.25 frames is
  // given the highest priority, so that frames it outputs
  // are handled before any at-the-same-time frames which
  // came from the radio-level HDLC decoders.  It's
  // configured as "self-timed", so the timestamps used on
  // the frames it outputs are managed locally and are not
  // based on the global demod-samples count.
  InitHDLC(&hdlc_context_fx25);
  hdlc_context_fx25.priority = FRAME_PRIORITY_CORRECTED_HDLC;
  hdlc_context_fx25.self_timed = true;
  hdlc_context_fx25.report_fx25 = true;
}

// This function will attempt to run Reed-Solomon decoding
// on the contents of a raw bit buffer, using the Reed-Solomon
// parameters indicated by the correlation tag found during the
// bitstream scan.  If the decoding succeeds ("correct data" or
// "damaged data, successfully corrected") the resulting data
// is passed to an HDLC decoder for parsing;  any HDLC frames
// found therein will be queued for final processing.  If the
// decoding fails, no HDLC decoding is performed.
static void correct_and_parse(ax25buffer *raw) {
  uint8_t payload[255]; // maximum length of the codeblock.
  ssize_t encoded_len;
  tnc_state.stats.fx25_codeblocks_seen++;
  // Optionally, corrupt some symbols in the data before trying
  // to decode, so we can see how well the Reed-Solomon logic
  // works.
  if (tnc_state.smash_fx25_rx != 0) {
    int corrupted = 0;
    for (int index = 0; index < ax25buffer_byte_count(raw); index++) {
      uint32_t rand = board_get_random32();
      if ((rand / 13) % tnc_state.smash_fx25_rx == 0) {
	uint8_t damage = rand / 19;
	raw->data[index] ^= damage;
	corrupted++;
      }
    }
    if (corrupted != 0) {
      LOG_INF("Corrupted %d bytes of FX.25 codeblock", corrupted);
    }
  }
  encoded_len = fx25_decode(raw->fx25_code, raw->data, payload);
  if (encoded_len < 0) {
    LOG_INF("Failed Reed-Solomon (%d)", encoded_len);
    return;
  }
  LOG_DBG("Good FX.25 block of %d bytes", encoded_len);
  tnc_state.stats.fx25_codeblocks_accepted++;
  // Base the times for any HDLC frame found within this codeblock
  // on the start time of the codeblock, not on the realtime
  // sample counter.
  hdlc_context_fx25.timestamp = raw->time_start;
  for (int i = 0; i < encoded_len; i++) {
    uint8_t b = payload[i];
    for (int bit = 0; bit < 8; bit++) {
      ConsumeDataBit(&hdlc_context_fx25, b & 1);
      b = b >> 1;
    }
  }
}

// Process incoming frames - perform FX.25 error correction if
// necessary, and filter out duplicate frames coming from
// demodulators running in parallel.
static void layer2_handle_incoming(void) {
  // The FX.25 frame detector may tell us to defer processing of any
  // frames, until after the FX.25 codeblock has been completely
  // received and the raw frame has been passed to us.  This will
  // help reduce the chance that a bad HDLC frame will sneak through
  // (one chance in 2^16).
  if (tnc_state.hdlc_hold_until > demod_samples_processed) {
    return;
  }
  while (true) {
    // Find the highest-priority candidate packet.
    int best_priority = -1;
    ax25buffer *b = NULL;
    ax25buffer *candidate;
    SYS_DLIST_FOR_EACH_CONTAINER(&rx_packet_candidates, candidate, node) {
      if (candidate->priority > best_priority) {
	best_priority = candidate->priority;
	b = candidate;
      }
    }
    if (!b) {
      break;
    }
    LOG_DBG("Look at frame %p priority %d start %lld end %lld processed_through %lld", b, b->priority,
	    b->time_start, b->time_end, tnc_state.hdlc_processed_through);
    if (b->time_start < tnc_state.hdlc_processed_through) {
      LOG_DBG("Replicate frame at time [%lld - %lld], drop", b->time_start,
	      b->time_end);
      ax25buffer_free(b);
      tnc_state.stats.frames_filtered++;
      continue;
    }
    switch (b->type) {
    case AX25_FRAME:
      LOG_DBG("Normal frame at time [%lld - %lld]", b->time_start,
	      b->time_end);
      if (!b->loopback) {
	set_good_packet_led(b->fx25 ? GOOD_PACKET_WITH_FX25 : GOOD_PACKET);
      }
      tnc_state.hdlc_processed_through = b->time_end;
      if (tnc_state.drop_rx != 0 &&
	       (board_get_random32() / 13) % tnc_state.drop_rx == 0) {
	LOG_INF("Deliberately simulating packet loss, dropping RX packet");
      } else {
	ax25buffer_append_to_dlist(b, &rx_packets);
	k_sem_give(&layer3_incoming_sem);
	tnc_state.stats.frames_accepted++;
      }
      break;
    case AX25_RAW:
      LOG_DBG("Raw frame at time [%lld - %lld]", b->time_start,
	      b->time_end);
      // Don't need to set the LED here.  If the frame is valid (with or without
      // error correction) the payload will come back through the AX25_FRAME
      // path above.
      tnc_state.stats.frames_accepted++;
      correct_and_parse(b);
      ax25buffer_free(b);
      break;
    default:
      LOG_ERR("Bad frame");
      break;
    }
  }
}

// This function attempts to perform FX.25 encoding on a
// frame.  Based on the FX.25 mode and the frame payload
// size it will attempt to find a suitable FX.25 codeblock
// format.  A raw frame buffer will be allocated, the incoming
// frame will be formatted into valid HDLC (syncs added, zero
// stuffing performed), the HDLC data will be encoded using
// Reed-Solomon, and the correlation tag prepended.
//
// If the encoding is successful, this routine frees the
// incoming buffer, and returns a pointer to the newly-
// created raw buffer.  If the encoding cannot be performed
// (no suitable codeblock format, no memory, etc.) the
// original data-frame buffer is returned.

static ax25buffer *encode_buffer(ax25buffer *buffer, enum fx25_encoding fx25_encoding) {
  size_t payload_size = ax25buffer_byte_count(buffer);
  // Assume worst-case message i.e. all 1-bits, with maximum amount of
  // HDLC zero-stuffing overhead required, for the payload and the
  // FCS.  Add two bytes for two syncs and round up a bit just to be
  // safe.
  LOG_DBG("FX.25 encode, for a frame with %d bytes of payload", payload_size);
  size_t encoded_size = ((payload_size + 2) * 6 / 5) + 2 + 4;
  // Search for a suitable encoding
  LOG_DBG("Search for FX.25 encoding, %d bytes worst-case HDLC, mode %d",
	  encoded_size, (int) fx25_encoding);
  int code_index = fx25_select_encoding(encoded_size, fx25_encoding);
  if (code_index < 0) {
    LOG_DBG("  No encoding found");
    return buffer;
  }
  int codeblock_bytes = fx25_codes[code_index].codeblock_bytes;
  int data_bytes = fx25_codes[code_index].data_bytes;
  LOG_DBG("Found encoding %d, codeblock %d data %d parity %d",
	  code_index, codeblock_bytes, data_bytes, codeblock_bytes - data_bytes);
  // Leave room at the beginning for the 8-byte correlation tag
  ax25buffer *encoded = ax25buffer_alloc(AX25_RAW, codeblock_bytes + 8);
  if (!encoded) {
    LOG_ERR("Can't allocate FX.25 raw buffer");
    return buffer;
  }
  // Put the correlation tag at the beginning, without zero-stuffing.
  uint8_t tag[8];
  sys_put_le64(fx25_codes[code_index].correlation_tag, tag);
  ax25buffer_put_bytes(encoded, tag, sizeof(tag));
  LOG_DBG("Before HDLC encoding, dest is %d bits", encoded->bits_written);
  // Put in an HDLC FLAG.
  ax25buffer_put_byte(encoded, HDLC_FLAG);
  // HDLC-encode the frame payload - do the zero stuffing as required
  // but not NRZI.
  int ones = 0;
  int32_t databyte;
  uint16_t computed_fcs = 0xFFFF;
  ax25buffer_reset_read(buffer);
  while ((databyte = ax25buffer_get_byte(buffer)) >= 0) {
    computed_fcs = (computed_fcs >> 8) ^
      hdlc_fcstab[0xFF & (databyte ^ computed_fcs)];
    for (int bitnum = 0; bitnum < 8; bitnum++) {
      ax25buffer_put_bit(encoded, databyte & 1);
      if (databyte & 1) {
	ones++;
	if (ones == 5) {
	  ax25buffer_put_bit(encoded, 0);
	  ones = 0;
	}
      } else {
	ones = 0;
      }
      databyte = databyte >> 1;
    }
  }
  LOG_DBG("After payload encoding, dest is %d bits", encoded->bits_written);
  // Append the FCS
  computed_fcs ^= 0xFFFF;
  LOG_DBG("Frame FCS to push is 0x%04X", computed_fcs);
  for (int bitnum = 0; bitnum < 16; bitnum++) {
    ax25buffer_put_bit(encoded, computed_fcs & 1);
    if (computed_fcs & 1) {
      ones++;
      if (ones == 5) {
	ax25buffer_put_bit(encoded, 0);
	ones = 0;
      }
    } else {
      ones = 0;
    }
    computed_fcs = computed_fcs >> 1;
  }
  // Need to fill the remainder of the data_bytes range with HDLC FLAGs
  // (as many as will fit, plus possibly an LSB-truncated flag to fill
  // up the last byte).
  int must_fill_to = (data_bytes + 8) * 8;
  LOG_DBG("Must fill out to %d bits", must_fill_to);
  // Stuff in whole FLAGs, without zero stuffing
  while ((must_fill_to - encoded->bits_written) >= 8) {
    ax25buffer_put_byte(encoded, HDLC_FLAG);
  }
  LOG_DBG("After full HDLC FLAGs, dest is %d bits", encoded->bits_written);
  // If necessary, stuff in a partial flag, truncating the LSBs
  int residue = must_fill_to - encoded->bits_written;
  LOG_DBG(" Residue of %d bits", residue);
  databyte = HDLC_FLAG >> (8 - residue);
  while (residue > 0) {
    ax25buffer_put_bit(encoded, databyte & 1);
    databyte = databyte >> 1;
    residue --;
  }
  LOG_DBG("After final HDLC FLAGs, dest is %d bits", encoded->bits_written);
  if (encoded->bits_written != 8 * (data_bytes + 8)) {
    LOG_ERR("Bits-written should be %d", 8 * (data_bytes + 8));
    ax25buffer_free(encoded);
    return buffer;
  }
  // At this point we should have a complete databytes-long
  // part of the codeblock, starting at offset 8.  Run a
  // Reed-Solomon encoding.  The encoder will write the parity
  // bytes after the end of the data bytes.
  ssize_t total =
    fx25_encode(code_index, &encoded->data[sizeof(uint64_t)], data_bytes);
  if (total != codeblock_bytes) {
    LOG_ERR("FX.25 encode returned %d, expected %d", total, codeblock_bytes);
    ax25buffer_free(encoded);
    return buffer;
  }
  // Fix up the encoded-buffer count
  encoded->bits_written = 8 * (sizeof(uint64_t) + codeblock_bytes);
  LOG_DBG("Successful FX.25 encode");
  //  hexdump(encoded->data, "Raw frame data", encoded->bits_written / 8);
  ax25buffer_free(buffer);
  tnc_state.stats.fx25_codeblocks_sent++;
  // Optionally, corrupt some symbols in the data after encoding
  // so we can see how well the recipient's Reed-Solomon logic works.
  if (tnc_state.smash_fx25_tx != 0) {
    int corrupted = 0;
    for (int index = 0; index < ax25buffer_byte_count(encoded); index++) {
      uint32_t rand = board_get_random32();
      if ((rand / 13) % tnc_state.smash_fx25_tx == 0) {
	uint8_t damage = rand / 19;
	encoded->data[index] ^= damage;
	corrupted++;
      }
    }
    if (corrupted != 0) {
      LOG_INF("Corrupted %d bytes of FX.25 codeblock", corrupted);
    }
  }
  return encoded;
}

// Process outgoing frames - perform FX.25 encoding if
// requested and not disabled
static void layer2_handle_outgoing(void) {
  ax25buffer *buffer, *safe;
  SYS_DLIST_FOR_EACH_CONTAINER_SAFE(&tx_packet_candidates, buffer, safe, node) {
    if (tnc_state.monitor) {
      char line[256];
      DecodePacketFromBuffer('>', buffer, line, sizeof(line), buffer->extended_sequence);
      console_printline(line);
    }
    if (!buffer->fx25 || tnc_state.fx25_encoding == FX25_ENCODING_OFF) {
      ax25buffer_append_to_dlist(buffer, &tx_packets);
    } else {
      buffer = encode_buffer(buffer, tnc_state.fx25_encoding);
      ax25buffer_append_to_dlist(buffer, &tx_packets);
    }
  }
}

int layer2_thread(void)
{
  LOG_INF("Layer 2 receiver thread started");
  layer2_init();
  while (1) {
    k_sem_take(&layer2_sem, K_FOREVER);
    layer2_handle_incoming();
    layer2_handle_outgoing();
  }
  return 0;
}

void layer2_accept_incoming(ax25buffer *b) {
  LOG_DBG("Appending frame to rx_packet_candidates list");
  ax25buffer_append_to_dlist(b, &rx_packet_candidates);
  k_sem_give(&layer2_sem);
}

void layer2_accept_outgoing(ax25buffer *b) {
  LOG_DBG("Appending frame to tx_packet_candidates list");
  ax25buffer_append_to_dlist(b, &tx_packet_candidates);
  k_sem_give(&layer2_sem);
}

void layer2_transmit(ax25buffer *b) {
  if (tnc_state.kiss_mode && tnc_state.ekiss_mode != EKISS_PARTIAL) {
    ax25buffer *clone = ax25buffer_clone(b);
    if (clone) {
      ax25buffer_append_to_dlist(clone, &kiss_packets);
    }
  }
  LOG_DBG("Appending frame to tx_packet_candidates list");
  ax25buffer_append_to_dlist(b, &tx_packet_candidates);
  k_sem_give(&layer2_sem);
}

bool layer2_has_traffic(void) {
  if (sys_dlist_is_empty(&tx_packet_candidates) &&
      sys_dlist_is_empty(&tx_packets)) {
    return false;
  }
  return true;
}


K_THREAD_DEFINE(layer2_tid, LAYER2_STACK_SIZE,
                layer2_thread, NULL, NULL, NULL,
                LAYER2_THREAD_PRIORITY, 0, 500);

