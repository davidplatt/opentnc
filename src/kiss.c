/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stdio.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/ring_buffer.h>
#include <zephyr/sys/crc.h>

#include <zephyr/logging/log.h>

#include "ax25.h"
#include "ax25buffer.h"
#include "board.h"
#include "damask.h"
#include "globals.h"
#include "kiss.h"
#include "packetutil.h"

LOG_MODULE_REGISTER(kiss, LOG_LEVEL_INF);

static bool smack_enabled = false;

static uint8_t send_buffer[HDLC_MAX_PACKET_LENGTH];
static uint32_t send_length = 0;
static uint8_t send_command;

static void kiss_command_handler(void) {
  if (rx_pull.command_len == 0) {
    return; /* should not happen. */
  }
  if (rx_pull.command[0] == 0xFF) {
    tnc_state.kiss_mode = false;  // We've been told to kiss off.  How very rude.
    return;
  }
  switch (rx_pull.command[0] & 0x0F) {
  case KISS_TXDELAY:
    if (rx_pull.command_len >= 1) {
      tnc_state.txdelay = rx_pull.command[1];
    }
    LOG_INF("txdelay now %d", tnc_state.txdelay);
    break;
  case KISS_P:
    if (rx_pull.command_len >= 1) {
      tnc_state.p = rx_pull.command[1];
    }
    LOG_INF("p now %d", tnc_state.p);
    break;
  case KISS_SLOTTIME:
    if (rx_pull.command_len >= 1) {
      tnc_state.slot_time = rx_pull.command[1];
    }
    LOG_INF("slottime now %d", tnc_state.slot_time);
    break;
  case KISS_TXTAIL:
    if (rx_pull.command_len >= 1) {
      tnc_state.txtail = rx_pull.command[1];
    }
    LOG_INF("txtail now %d", tnc_state.txtail);
    break;
  case KISS_FULLDUPLEX:
    if (rx_pull.command_len >= 1) {
      tnc_state.full_duplex = rx_pull.command[1];
    }
    LOG_INF("fullduplex now %d", tnc_state.full_duplex);
    break;
  case KISS_SETHARDWARE:
    board_set_hardware(rx_pull.command + 1, rx_pull.command_len - 1);
    break;
  default:
    break;
  }
}

void kiss_decode_bytes_from_host(uint8_t *buffer, uint32_t len) {
  for (int32_t i = 0; i < len; i++) {
    uint8_t byte = buffer[i];
    if (byte == KISS_FEND) { /* Boundary between frames */
      LOG_DBG("FEND");
      if (rx_pull.data_frame) {
	if (rx_pull.flex_crc) {
	  LOG_DBG("FLEX data frame; strip checksum");
	  send_length -= 2;
	} else if (rx_pull.smack_crc ) {
	  LOG_DBG("SMACK data frame; validate checksum");
	  if (rx_pull.crc == 0x0000) {
	    LOG_DBG(" Good checksum");
	    send_length -= 2;
	    if (!smack_enabled) {
	      LOG_DBG("Enabling transmission of SMACK checksum");
	      smack_enabled = true;
	    }
	  } else {
	    LOG_DBG("Bad SMACK checksum, drop packet");
	    send_length = 0;
	  }
	} else {
	  LOG_DBG("Data frame accepted");
	}
	if (send_length > 0) {
	  ax25buffer *buffer = ax25buffer_alloc(AX25_FRAME, send_length);
	  if (buffer) {
	    ax25buffer_put_bytes(buffer, send_buffer, send_length);
	    ax25buffer_set_command(buffer, send_command);
	    damask_accept_outgoing(buffer);
	  } else {
	    LOG_ERR("Can't allocate ax25buffer of %d bytes, dropping frame", send_length);
	  }
	  send_length = 0;
	}
	rx_pull.data_frame = rx_pull.escaped = rx_pull.flex_crc =
	  rx_pull.smack_crc = false;
      } else if (rx_pull.command_frame) {
	kiss_command_handler();
	rx_pull.command_frame = rx_pull.escaped = rx_pull.flex_crc =
	  rx_pull.smack_crc = false;
      }
      continue;
    }
    if (byte == KISS_FESC) {
      rx_pull.escaped = true;
      continue;
    }
    if (rx_pull.escaped) {
      if (byte == KISS_TFEND) {
	byte = KISS_FEND;
      } else if (byte == KISS_TFESC) {
	byte = KISS_FESC;
      }
      rx_pull.escaped = false;
    }
    if (rx_pull.data_frame) {
      send_buffer[send_length++] = byte;
      rx_pull.crc = crc16_reflect(0xA001, rx_pull.crc, &byte, 1);
    } else if (rx_pull.command_frame) {
      if (rx_pull.command_len < COMMAND_LIMIT) {
	rx_pull.command[rx_pull.command_len++] = byte;
      }
    } else if ((byte & 0x0F) == KISS_DATA_FRAME) {
      rx_pull.data_frame = true;
      LOG_DBG("It's a data frame");
      send_command = byte;
      if (byte & KISS_SMACK_MASK) {
	LOG_DBG("Data frame with 0x80 bit - SMACK!");
	rx_pull.smack_crc = true;
	rx_pull.crc = crc16_reflect(0xA001, 0, &byte, 1);
      } else if (byte & KISS_FLEX_MASK) {
	LOG_DBG("Data frame with 0x20 bit - FLEX!");
	rx_pull.flex_crc = true;
      }
    } else {
      LOG_DBG("It's a command frame");
      rx_pull.command_frame = true;
      rx_pull.command[0] = byte;
      rx_pull.command_len = 1;
    }
  }
}

int32_t kiss_encode_bytes_to_host(uint8_t *buffer, uint32_t buffer_len) {
  uint32_t offset = 0;
  ax25buffer *b = rx_push.current;
  while (buffer_len >= 2) {
    if (!rx_push.working) {
      break;
    }
    if (rx_push.send_first_byte) {
      buffer[offset++] = rx_push.first_byte;
      buffer_len --;
      rx_push.send_first_byte = false;
    }
    if (rx_push.send_second_byte) {
      buffer[offset++] = rx_push.second_byte;
      buffer_len --;
      rx_push.send_second_byte = false;
    }
    if (rx_push.final) {
      rx_push.done = true;
      break;
    }
    int32_t byte = ax25buffer_get_byte(b);
    if (byte < 0) {
      LOG_DBG("No more packet data");
      if (rx_push.append_checksum) {
	LOG_DBG("Must append CRC");
	byte = rx_push.crc & 0xFF;
	rx_push.checksum_high = rx_push.crc >> 8;
	rx_push.append_checksum = false;
	rx_push.checksumming_high = true;
	LOG_DBG("Send low byte of CRC");
      } else if (rx_push.checksumming_high) {
	rx_push.checksumming_high = false;
	byte = rx_push.checksum_high;
	LOG_DBG("Send high byte of CRC");
      } else {	
	LOG_DBG("Send final FEND");
	rx_push.final = true;
	rx_push.first_byte = KISS_FEND;
	rx_push.send_first_byte = true;
	continue; // Bypass the escaping mechanism for the final FEND
      }
    }
    // CRC and then escape out (if necessary) the data bytes.  Running
    // the CRC algorithm on the two checksum bytes is unnecessary but
    // won't hurt anything.
    uint8_t really_a_byte = byte;
    rx_push.crc = crc16_reflect(0xA001, rx_push.crc, &really_a_byte, 1);
    LOG_DBG("Update CRC, byte 0x%02x, CRC 0x%04X", really_a_byte, rx_push.crc);
    switch (byte) {
    case KISS_FEND:
      rx_push.first_byte = KISS_FESC;
      rx_push.second_byte = KISS_TFEND;
      rx_push.send_first_byte = true;
      break;
    case KISS_FESC:
      rx_push.first_byte = KISS_FESC;
      rx_push.second_byte = KISS_TFESC;
      rx_push.send_first_byte = true;
      break;
    default:
      rx_push.second_byte = byte;
    }
    rx_push.send_second_byte = true;
  }
  return offset;
}

/* Conditionally, set the "SMACK CRC is in use" bit in the packet
 * command byte.  This will be done iff the KISS layer has determined
 * that the host supports (and wants to use) SMACK CRC packets.
 */
void kiss_maybe_append_crc16(ax25buffer *b) {
  if (smack_enabled) {
    ax25buffer_set_command(b, KISS_DATA_FRAME | KISS_SMACK_MASK);
  } else {
    ax25buffer_set_command(b, KISS_DATA_FRAME);
  }
}

void kiss_read_from_host(void) {
  uint8_t rx_buffer[32];
  int rx_avail;
  while (true) {
    board_check_input_flow_control();
    rx_avail = ring_buf_get(&host_inputbuffer, rx_buffer, sizeof(rx_buffer));
    if (rx_avail <= 0) {
      break;
    }
    kiss_decode_bytes_from_host(rx_buffer, rx_avail);
  }
}

void kiss_push_to_host(void) {
  // Handle completion of the possible completion of the
  // encoding of a packet.
  if (rx_push.done) {
    rx_push.done = rx_push.final = rx_push.working = false;
    if (rx_push.current) {
      ax25buffer_free(rx_push.current);
      rx_push.current = NULL;
    }
  }
  // If we're working on a packet, try to encode and push more
  // of it to the host ring-buffer
  if (rx_push.working) {
    uint8_t push_buffer[32];
    size_t avail = ring_buf_space_get(&host_outputbuffer);
    if (avail == 0) {
      return;
    }
    if (avail > sizeof(push_buffer)) {
      avail = sizeof(push_buffer);
    }
    int32_t to_send = kiss_encode_bytes_to_host(push_buffer,
						sizeof push_buffer);
    if (to_send > 0) {
      ring_buf_put(&host_outputbuffer, push_buffer, to_send);
      board_kick_host_tx();
    }
    return;
  }
  // If we're idle, see if there's another packet to encode
  // and push
  if (!rx_push.current) {
    rx_push.current = ax25buffer_take_head(&kiss_packets);
    if (!rx_push.current) {
      return;
    }
    LOG_DBG("KISS-push taking buffer %p", rx_push.current);
  }
  rx_push.first_byte = KISS_FEND;
  uint8_t command_byte = ax25buffer_get_command(rx_push.current); // KISS_DATA_FRAME, possibly with KISS_SMACK_MASK added
  rx_push.second_byte = command_byte;
  rx_push.send_first_byte = true;
  rx_push.send_second_byte = true;
  rx_push.working = true;
  rx_push.final = false;
  rx_push.done = false;
  rx_push.checksumming_high = false;
  if (command_byte == (KISS_DATA_FRAME | KISS_SMACK_MASK)) {
    rx_push.append_checksum = true;
    rx_push.crc = crc16_reflect(0xA001, 0, &command_byte, 1);
    LOG_DBG("Initialize CRC, command byte 0x%02x, CRC 0x%04X", command_byte, rx_push.crc);
  } else {
    rx_push.append_checksum = false;
  }
}

// Rather crudely stuff an UNPROTO packet into the receive packet list.
void hello_kiss(void) {
  encoded_callsign enc;
  const unsigned char *salutation = "Entering KISS AUTO mode.  Send 0xC0 xFF 0xC0 to exit.";
  ax25buffer *b = ax25buffer_alloc(AX25_FRAME, strlen(salutation) + 32);
  if (b) {
    ax25buffer_put_byte(b, KISS_DATA_FRAME);
    encode_callsign(&enc, "host");
    ax25buffer_put_bytes(b, (uint8_t *) &enc, sizeof(enc));
    encode_callsign(&enc, "OpenTNC");
    enc.ssid |= SSID_BYTE_ADDRESS_EXTENSION_BIT;
    ax25buffer_put_bytes(b, (uint8_t *) &enc, sizeof(enc));
    ax25buffer_put_byte(b, encode_u_control(U_FRAME_UI, false));
    ax25buffer_put_byte(b, AX25_P_TEXT);
    ax25buffer_put_bytes(b, salutation, strlen(salutation));
    ax25buffer_append_to_dlist(b, &rx_packets);
  }
}

