/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_AX25
# define _H_AX25

#include "ax25buffer.h"

#ifdef CONFIG_MAX_IFRAME_PAYLOAD
# define MAX_IFRAME_PAYLOAD CONFIG_MAX_IFRAME_PAYLOAD
#else
# define MAX_IFRAME_PAYLOAD 256
#endif

// The U-frame MMMMM bits are split, so the enum must be
// diced apart before being inserted, and pulled out in
// pieces before being compared/used.

enum U_frame_mmmmm {
  U_FRAME_UI = 0,
  U_FRAME_DM = 3,
  U_FRAME_SABM = 7,
  U_FRAME_DISC = 8,
  U_FRAME_UA = 12,
  U_FRAME_SABME = 15,
  U_FRAME_FRMR = 17,
  U_FRAME_XID = 23,
  U_FRAME_TEST = 28,
};

enum S_frame_ss {
  S_FRAME_RR = 0,
  S_FRAME_RNR = 1,
  S_FRAME_REJ = 2,
  S_FRAME_SREJ = 3,
};

enum XID_identifiers { // as used by AX.25
  XID_FI = 0x82, // general-purpose XID information
  XID_GI = 0x80, // parameter negotiation
};

enum XID_frame_PI {
  PI_CLASS_OF_PROCEDURES = 2,
  PI_HDLC_OPTIONAL_FUNCTIONS = 3,
  PI_I_FIELD_LENGTH_TX = 5,
  PI_I_FIELD_LENGTH_RX = 6,
  PI_I_WINDOW_SIZE_TX = 7,
  PI_I_WINDOW_SIZE_RX = 8,
  PI_ACK_TIMER = 9,
  PI_RETRYS = 10,
};

// HDLC optional functions are spread across 3 bytes
// in the parameter.
enum HDLC_optional_functions {
  HDLC_BYTE0_2_REJ_supported =     0x02,
  HDLC_BYTE0_3A_SREJ_supported =   0x04,
  HDLC_BYTE0_7B_extended_address = 0x80,
  HDLC_BYTE1_10A_modulo_8 =        0x04,
  HDLC_BYTE1_10B_modulo_128 =      0x08,
  HDLC_BYTE1_12_TEST_supported =   0x20,
  HDLC_BYTE1_14A_16_bit_FCS =      0x80,
  HDLC_BYTE2_14A_32_bit_FCS =      0x01,
  HDLC_BYTE2_15A_synchronous_TX =  0x02,
};

typedef struct encoded_callsign {
  uint8_t base_callsign[6];
  uint8_t ssid;
} encoded_callsign;

#define SSID_BYTE_C_H_BIT               0x80
#define SSID_BYTE_RR_BITS               0x60
#define SSID_BYTE_SSID_BITS             0x1E
#define SSID_BYTE_ADDRESS_EXTENSION_BIT 0x01
#define MAX_DIGIS 6

// Maximum packet overhead size - accounts for path, control byte(s), and PID
#define MAX_PACKET_OVERHEAD ((MAX_DIGIS + 2) * sizeof(encoded_callsign) + 3)

enum command_or_response {
  FRAME_IS_NEITHER = 0,
  FRAME_IS_RESPONSE = 1,
  FRAME_IS_COMMAND = 2
};

#define FRAME_ROLES {"legacy", "response", "command"}

typedef struct full_address_path {
  uint32_t digipeaters;
  encoded_callsign dest;
  encoded_callsign src;
  encoded_callsign digi[MAX_DIGIS];
} full_address_path;

typedef struct packet_parse {
  full_address_path path;
  bool two_byte_control;
  uint16_t control; // may be 8 or 16 bits
  int16_t pid; // must keep it as signed, so that AX25_NO_PID works!
  enum command_or_response frame_role;
  bool fx25;
  uint8_t *payload;
  uint32_t payload_length;
  uint64_t filter_data; // Private data, passed between connection filter and connection consumer
} packet_parse;

enum ax25_protocol {
  AX25_NO_PID = -1,
  AX25_P_TEXT = 0xF0,
};

// Two bits in the SSID are reserved and subject to
// interpretation by different networks.  They should
// normally be set to 1.  Some TNCs use them to distinguish
// between legacy and extended sequence number sizes (which
// cannot otherwise be done by looking at an HDLC packet),
// and DAMA communities use one to indicate "Hey, client,
// your transmit timing is under control of a DAMA master,
// do not transmit unless polled."  These are unofficial
// (and somewhat conflicting) uses.

enum ssid_reserved_bits {
  SSID_RESERVED_40 = 0x40,
  SSID_RESERVED_20 = 0x20,
  SSID_RESERVED_MASK = 0x60,
  SSID_DAMA = SSID_RESERVED_20,              // DAMA protocol if zero, say the Linux utils
  SSID_EXTENDED_SEQUENCE = SSID_RESERVED_40  // extended sequence if zero, say the Linux utils
};

static inline uint32_t get_reserved_callsign_bits(encoded_callsign *enc) {
  return (enc->ssid & SSID_RESERVED_MASK) >> 5;
}

static inline void set_reserved_callsign_bits(encoded_callsign *enc, uint32_t bits) {
  enc->ssid = (enc->ssid & ~SSID_RESERVED_MASK) | ((bits << 5) & SSID_RESERVED_MASK);
}

// This typedef holds a callsign in plaintext ASCII - six primary
// characters, optional "-x" or "-xx" for an SSID if non-zero, and
// a terminating null.
typedef char plaintext_callsign[10];

// Returns true if the callsign is valid, false otherwise.
bool encode_callsign(encoded_callsign *enc, const char *plaintext);

void decode_callsign(plaintext_callsign plaintext, const encoded_callsign *enc);

// Compare the ASCII part of two well-formed encoded callsigns, ignoring
// the SSID and other irrelevant bits.
bool same_base_callsign(const encoded_callsign *call1, const encoded_callsign *call2);

// Compare the ASCII and SSID parts of two well-formed encoded callsigns, ignoring
// the irrelevant bits.
bool same_callsign(const encoded_callsign *call1, const encoded_callsign *call2);

// Invert the path of an incoming packet - swap source and destination,
// reverse any digitpeaters, and set the final (only) address extension bit.
// The C bit from the incoming packet's destination address is preserved
// in the outgoing packet's source address, and vice versa, thus
// maintaining the command/response convention in AX.25 2.0.
void invert_path(full_address_path *out, const full_address_path *in);

// Parse a packet in memory, filling out a packet_parse structure.  The
// contents of the packet_parse are tied to the lifetime and contents of
// the memory buffer!  Returns true if the packet seems valid.
bool parse_packet(packet_parse *parsed, uint8_t *packet, uint32_t length, bool extended_sequence);

// General schmoos to send a packet, given a full path, information byte,
// and optionally some payload.  A PID of -1 means "none, do not include".
//
// enqueue_packet does the whole job, and then commits the packet.
// prepare_packet_header queues up the header (and PID if relevant) but
// no payload, and does not commit the packet.

struct connection;  // forward ref

void enqueue_packet(const full_address_path *path,
		    struct connection *conn, // may be null
		    enum command_or_response frame_role,
		    bool two_byte_control,
		    bool should_start_t1,
		    uint16_t control,
		    int16_t pid,
		    uint8_t *payload,
		    size_t payload_length,
		    bool fx25);

void prepare_packet_header(ax25buffer *buffer,
			   const full_address_path *path,
			   enum command_or_response frame_role,
			   bool two_byte_control,
			   uint16_t control,
			   int16_t pid);

// Split up the five MMMMM bits in a U frame control byte, and combine them
// with the P/F bit and the two required must-be-1 bits to create a valid
// U control byte.  Inline this because the compiler will be able to do
// all the constant math, in almost all cases.
static uint8_t inline encode_u_control(enum U_frame_mmmmm mmmmm, bool p_f) {
  uint8_t result =
    ((mmmmm & 0x1C) << 3) |
    (p_f << 4) |
    ((mmmmm & 0x3) << 2) |
    0x03;
  return result;
}

static uint16_t inline encode_s_control(uint8_t n_r, bool p_f, enum S_frame_ss ss, bool two_byte_control) {
  uint16_t result;
  if (two_byte_control) {
    result = ((n_r & 0x7F) << 9) |
      ((p_f ? 1 : 0) << 8) |
      (ss << 2) |
      0x01;
  } else {
    result = ((n_r & 0x7) << 5) |
      ((p_f ? 1 : 0) << 4) |
      (ss << 2) |
      0x01;
  }
  return result;
}

static uint16_t inline encode_i_control(uint8_t n_r, bool p_f, uint8_t n_s, bool two_byte_control) {
  uint16_t result;
  if (two_byte_control) {
    result = ((n_r & 0x7F) << 9) |
      ((p_f ? 1 : 0) << 8) |
      ((n_s & 0x7F) << 1);
  } else {
    result = ((n_r & 0x7) << 5) |
      ((p_f ? 1 : 0) << 4) |
      ((n_s & 0x7) << 1);
  }
  return result;
}

#endif // _H_AX25
