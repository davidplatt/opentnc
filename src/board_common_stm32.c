/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stdio.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/ring_buffer.h>

#include <stm32_ll_adc.h>
#include <stm32_ll_bus.h>
#include <stm32_ll_dma.h>
#include <stm32_ll_gpio.h>
#include <stm32_ll_tim.h>

#include <zephyr/usb/usb_device.h>
#include <zephyr/logging/log.h>

#include "board.h"
#include "demod.h"
#include "globals.h"
#include "hdlc.h"
#include "kiss.h"
#include "leds.h"
#include "mod.h"
#include "xoshiro128.h"

LOG_MODULE_REGISTER(common_stm32f, LOG_LEVEL_INF);

// Stuff currently in board_stm32f411.c

extern int adc_buffer_bytes_active;
void Set_TIM2_rate(uint32_t hz);
void Set_ADC_sampling_speed(bool fast);

void board_enable_ptt(void){
  LOG_DBG("Enabling PTT");
  gpio_pin_set_dt(&ptt, 1);
  set_ptt_led(PTT_ON);
  tnc_state.ptt = true;
}

void board_disable_ptt(void){
  LOG_DBG("Disabling PTT");
  gpio_pin_set_dt(&ptt, 0);
  set_ptt_led(PTT_OFF);
  tnc_state.ptt = false;
}

void board_set_hdlc(uint32_t bit) {
  gpio_pin_set_dt(&hdlc, bit);
  board_set_tx_freq(bit ? HIGH_TONE : LOW_TONE);
}

void board_push_to_radio(void) {
  generic_push_to_radio();
}

void CDC_interrupt_handler(const struct device *dev, void *user_data)
{
  ARG_UNUSED(user_data);
  static int tx_avail = 0;
  static int tx_offset = 0;
  static uint8_t tx_buffer[64];
  uint8_t rx_buffer[64];
  int transferred;

  /* Here we receive data from the CDC "UART" and put it into the
   * input ring buffer. It'll be read by a thread running either the
   * KISS parser, or the console/converse logic, depending on the
   * mode.
   */
  set_serial_led(true);
  while (!tnc_state.input_throttled && uart_irq_update(dev) && uart_irq_rx_ready(dev)) {
    transferred = uart_fifo_read(dev, rx_buffer, sizeof(rx_buffer));
    if (ring_buf_space_get(&host_inputbuffer) < transferred) {
      LOG_ERR("UART input overrun!");
    } else {
      int space_before = ring_buf_space_get(&host_inputbuffer);
      int ret = ring_buf_put(&host_inputbuffer, rx_buffer, transferred);
      int space_after =  ring_buf_space_get(&host_inputbuffer);
      LOG_DBG("Transfer: sp_b %d trans %d sp_a %d ret %d", space_before,
	      transferred, space_after, ret);
    }
    if (ring_buf_space_get(&host_inputbuffer) < sizeof(rx_buffer)) {
      LOG_DBG("UART input flow control asserted");
      uart_irq_rx_disable(dev); // Flow-control the host
      tnc_state.input_throttled = true;
    }
  }
  /* Here we push data from the ring buffer to the CDC "UART" in
   * efficient chunks.
   */
  while (uart_irq_update(dev) && uart_irq_tx_ready(dev)) {
    if (tx_avail == 0) {
      LOG_DBG("Look for outputbuffer data");
      tx_avail = ring_buf_get(&host_outputbuffer, tx_buffer, sizeof(tx_buffer));
      LOG_DBG("Got %d bytes", tx_avail);
      tx_offset = 0;
      if (tx_avail == 0) {
	uart_irq_tx_disable(dev);
	break;
      }
    }
    transferred = uart_fifo_fill(dev, &tx_buffer[tx_offset], tx_avail);
    tx_avail -= transferred;
    tx_offset += transferred;
  }
}

void board_check_input_flow_control(void) {
  if (tnc_state.input_throttled &&
      ring_buf_space_get(&host_inputbuffer) >
      ring_buf_capacity_get(&host_inputbuffer) / 2) {
    tnc_state.input_throttled = false;
    uart_irq_rx_enable(cdc_dev);
  }
}

bool board_check_output_flow_control(void) {
  return (ring_buf_space_get(&host_outputbuffer) < HDLC_MAX_PACKET_LENGTH);
}

void board_kick_host_tx(void) {
  uart_irq_tx_enable(cdc_dev);
}

/* This routine preprocesses a batch of raw samples, prior to passing
 * them off to the demodulator.
 */

void handle_samples(input_sample *samples, uint32_t count) {
  static int32_t limit = 0x1000;
  static int headroom = 0x100;
  int i, j;
  LOG_DBG("Handle %d samples at %p", count, samples);
  gpio_pin_set_dt(&rx_busy_led, 1);
  // The G3RUH demodulator is special - it needs to see all of the
  // incoming raw samples without filtering (which we can't afford
  // to do anyhow at a rate in excess of 1 megasample per second!).
  // Clipping detection, and tracking-the-mean will be done in
  // the demodulator.
  if (tnc_state.demodulator == G3RUH_DEMODULATOR) {
    j = count;
  } else {
    for (i = 0, j = 0; i < count; i += OVERSAMPLING, j ++) {
#if OVERSAMPLING == 1
      input_sample in = samples[i];
#elif OVERSAMPLING == 2
      input_sample in = (samples[i] + samples[i+1]) / 2;
#elif OVERSAMPLING == 4
      input_sample in = (samples[i] + samples[i+1] + samples[i+2] +
			 samples[i+3]) / 4;
#else
#error Bad oversampling ratio
#endif
      samples[j] = in;
      /* Report near-clipping */
      if (in < headroom || in > limit - headroom) {
	gpio_pin_set_dt(&clip_warning_led, 1);
      }
    }
  }
  Demodulate(samples, j);
  gpio_pin_set_dt(&rx_busy_led, 0);
  samples_processed += j;
}

/* The transceiver runs whenever the DMA ISR tells it that
 * new samples are ready.
 */
void board_transceiver(void *arg1, void *arg2, void *arg3) {
  while (1) {
    while (!sample_set_0_ready && !sample_set_1_ready) {
      k_sem_take(&sampler_sem, K_FOREVER);
    }
    LOG_DBG("Samples!");
    if (sample_set_0_ready) {
      handle_samples(sample_set_0, adc_buffer_bytes_active / sizeof(input_sample));
      sample_set_0_ready = false;
    }
    if (sample_set_1_ready) {
      handle_samples(sample_set_1, adc_buffer_bytes_active / sizeof(input_sample));
      sample_set_1_ready = false;
    }
  }
}

// FIXME later!
void board_configure_demodulator(struct demodulator_state *state) {
  LOG_INF("Configuring demodulator");
  memset(state, 0, sizeof(*state));
  // All of the demodulators other than G3RUH run at 1200 baud,
  // 32x oversampled, hence we want to set the ADC clock divisor
  // so that we get 38400 samples per second.
  int32_t samples_per_second, bytes_per_sample;
  bool fast_sampling;
  if (tnc_state.demodulator != G3RUH_DEMODULATOR) {
    LOG_INF("Configure sampler for legacy rate");
    samples_per_second = SAMPLES_PER_SECOND;
    bytes_per_sample = 2;
    fast_sampling = false;
  } else {
    // The G3RUH demodulator wants all the samples as fast as they
    // can be delivered (500 kilosamples/second).  It knows how to
    // pick out the ones it wants, and ignores the rest.  So, set
    // the ADC clock divisor to 0, to request continuous conversion.
    LOG_INF("Configure sampler for full speed");
    samples_per_second = 500000;
    bytes_per_sample = 2;
    fast_sampling = true;
  }
  Set_TIM2_rate(samples_per_second);
  Set_ADC_sampling_speed(fast_sampling);
  // Compute the buffer sizes to use - the biggest power-of-2
  // which will complete within the latency bounds implied
  // by the service frequency.
  adc_buffer_bytes_active = 1 << CONFIG_ADC_BUFFER_BYTES_LOGARITHM;
  int32_t hz;
  LOG_INF("Computing ADC buffer size (max %d)", adc_buffer_bytes_active);
  adjust_buffer_size(samples_per_second, bytes_per_sample,
		     &adc_buffer_bytes_active, NULL, &hz);
  LOG_INF(" Computed ADC buffer size %d, %d hz",
	  adc_buffer_bytes_active, hz);
  // Set a mean value to the middle of the "12-bit" ADC range.
  state->mean = INT32_TO_SCALED(1 < 11);
  // 500 kHz conversion rate (controlled by TIM2).
  state->samples_per_baud = state->adjusted_samples_per_baud =
    INT32_TO_SCALED(500000) / 9600;
  // For now, just decide to do 7 zero-crossing scans per baud.  Since
  // the demodulator will use linear interpolation to try to figure
  // out the actual time of zero crossing, and since the curve should
  // be close-to-linear when it crosses the axis, there's not a huge
  // amount to be gained by doing a lot of shorter "looks".  Maybe
  // make this KConfig-urable later.
  state->samples_per_zero_crossing_check = state->samples_per_baud / 7;
  // There's a fair amount of jitter present in the zero-crossing times
  // for a G3RUH signal sent using a sinc() pulse prototype, due to the
  // significant amount of interference between the shoulders of the
  // individual pulses.  Measurements show an average jitter of
  // as high as 4.86 samples, which is just under 1/10 baud.  For now,
  // gonna say that an average jitter of more than 1/5 of the baud time
  // is Bad.
  state->zero_crossing_jitter_limit = state->samples_per_baud / 5;
  // The system should converge to approximately 0.5 zero crossings per
  // baud when sent a properly-scrambled signal.  We set some upper and
  // lower limits to the average rate which we consider reasonable for
  // a "locked" condition.  Too high (much above 1) probably means we're
  // receiving noise.  Too low probably means that we're receiving
  // silence.  The rates are scaled by 100 (i.e. as a percentage)
  // in order to avoid IIR filter truncation.
  state->lower_zero_crossing_rate = INT32_TO_SCALED(20);
  state->upper_zero_crossing_rate = INT32_TO_SCALED(120);
  // Drift the PLL by 1/5 sample time each time we correct.  This
  // should allow our 50-state DPLL to work about the same as
  // the 256-state G3RUH design - a maximum capture range of
  // 0.4%, 4 parts per thousand, 4000 parts per million - well
  // in excess of the worst-case expected error of the crystals
  // at both ends of the connection.
  state->zero_crossing_shift_rate = INT32_TO_SCALED(1) / 5;
  // Initialize the "next time" values to something reasonable...
  // pretend that we started up just at a good zero crossing.
  // It's a lie, but keeps us out of trouble.
  state->previous_look_time = -1;
  state->next_look_time = state->samples_per_zero_crossing_check;
  state->next_sample_time = state->samples_per_baud / 2;
  state->next_zero_crossing_expected = state->samples_per_baud;
  // Set the fast-lock period to allow a fast-lock attempt every
  // 10 baud.
  state->fast_lock_period = 10;
}
