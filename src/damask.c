/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>

#include "ax25.h"
#include "damask.h"
#include "globals.h"
#include "layer2.h"

LOG_MODULE_REGISTER(damask, LOG_LEVEL_INF);

#define NUM_DAMASK_CLIENTS 8

static sys_dlist_t client_list = SYS_DLIST_STATIC_INIT(&client_list);

static damask_client clients[NUM_DAMASK_CLIENTS];

static sys_dlist_t nonclient_packets = SYS_DLIST_STATIC_INIT(&nonclient_packets);

int64_t damask_last_time;

static bool damask_fx25_policy(bool client_uses_fx25, bool client_uses_non_fx25) {
  bool policy = false;
  switch (tnc_state.fx25_connection_mode) {
  case FX25_CONNECTION_OFF:
  default:
    break;
  case FX25_CONNECTION_RESPONSIVE:
    policy = client_uses_fx25;
    break;
  case FX25_CONNECTION_OPPORTUNISTIC:
    policy = client_uses_fx25 | !client_uses_non_fx25;
    break;
  case FX25_CONNECTION_ON:
    policy = true;
  }
  return policy;
}

void damask_init(void) {
  LOG_INF("Initializing client list");
  for (int i = 0; i < NUM_DAMASK_CLIENTS; i++) {
    sys_dnode_init(&clients[i].node);
    sys_dlist_init(&clients[i].outbound_buffers);
    sys_dlist_append(&client_list, &clients[i].node);
  }
  damask_last_time = k_uptime_get();
}

static void transmit_client_packets(damask_client *client) {
  ax25buffer *buffer, *safe;
  SYS_DLIST_FOR_EACH_CONTAINER_SAFE(&client->outbound_buffers, buffer, safe,
				    node) {
    LOG_DBG("Transmit client %p frame %p", client, buffer);
    buffer->fx25 = damask_fx25_policy(client->uses_fx25, client->uses_non_fx25);
    layer2_accept_outgoing(buffer);
  }
}

void damask_examine_incoming(ax25buffer *buffer) {
  LOG_DBG("Looking at incoming frame %p", buffer);
  packet_parse p;
  if (!parse_packet(&p, buffer->data, buffer->bits_written / 8, false)) {
    LOG_ERR("Packet did not parse");
    return;
  }
  LOG_DBG("Packet parsed OK");
  plaintext_callsign plaintext;
  decode_callsign(plaintext, &p.path.src);
  damask_client *try, *client = NULL;
  SYS_DLIST_FOR_EACH_CONTAINER(&client_list, try, node) {
    if (same_callsign(&p.path.dest, &try->src) &&
	same_callsign(&p.path.src, &try->dest)) {
      client = try;
      LOG_DBG("Found an existing client %s at %p", plaintext, client);
      break;
    }
  }
  if (!client) {
    sys_dnode_t *tail = sys_dlist_peek_tail(&client_list);
    sys_dlist_remove(tail);
    sys_dlist_prepend(&client_list, tail);
    client = SYS_DLIST_CONTAINER(tail, client, node);
    LOG_DBG("Setting up new client %s at %p", plaintext, client);
    transmit_client_packets(client);  // Don't lose any!
    memcpy(&client->src, &p.path.dest, sizeof client->src);
    memcpy(&client->dest, &p.path.src, sizeof client->dest);
    client->uses_fx25 = client->uses_non_fx25 = false;
    client->last_state = S_FRAME_RR;
    client->last_n_s = client->last_n_r = -1; // unknown
  }
  client->priority = 1;
  client->time_since_traffic = 0;
  if (buffer->fx25) {
    client->uses_fx25 = true;
  } else {
    client->uses_non_fx25 = true;
  }
}

void damask_accept_outgoing(ax25buffer *buffer) {
  LOG_DBG("Looking at outgoing frame %p", buffer);
  packet_parse p;
  if (!parse_packet(&p, buffer->data, buffer->bits_written / 8, false)) {
    LOG_ERR("Packet did not parse");
    return;
  }
  LOG_DBG("Packet parsed OK");
  damask_client *client = NULL, *try;
  SYS_DLIST_FOR_EACH_CONTAINER(&client_list, try, node) {
    if (same_callsign(&p.path.src, &try->src) &&
	same_callsign(&p.path.dest, &try->dest)) {
      client = try;
      LOG_DBG("Found an existing client at %p", client);
      break;
    }
  }
  if (client) {
    sys_dlist_prepend(&client_list, &buffer->node);
    ax25buffer_append_to_dlist(buffer, &client->outbound_buffers);
  } else {
    ax25buffer_append_to_dlist(buffer, &nonclient_packets);
  }
}

static void schedule_damask_off(void) {
  ax25buffer *buffer, *safe;
  for (int i = 0; i < NUM_DAMASK_CLIENTS; i++) {
    transmit_client_packets(&clients[i]);
  }
  SYS_DLIST_FOR_EACH_CONTAINER_SAFE(&nonclient_packets, buffer, safe, node) {
    LOG_DBG("Transmit nonclient frame %p", buffer);
    buffer->fx25 = damask_fx25_policy(false, false);
    layer2_accept_outgoing(buffer);
  }
}

static enum {
  DAMASK_IDLING,
  DAMASK_TRANSMIT_WAIT,
  DAMASK_TRANSMITTING,
  DAMASK_QUIET_TIME
} damask_state = DAMASK_IDLING;

static int32_t damask_timeout = 0;
static int32_t damask_rotor;

#define PTT_TIMEOUT 20000 // 20 seconds
#define END_PTT_TIMEOUT 20000 // same

static void schedule_damask_round_robin(bool by_priority, bool do_polling) {
  int64_t time_now = k_uptime_get();
  int32_t elapsed = time_now - damask_last_time;
  damask_last_time = time_now;
  damask_timeout -= elapsed;
  switch (damask_state) {
  case DAMASK_TRANSMIT_WAIT:  // Wait for us to assert PTT
    if (damask_timeout > 0 && !tnc_state.ptt) {
      return;
    }
    damask_state = DAMASK_TRANSMITTING;
    damask_timeout = END_PTT_TIMEOUT;
    LOG_DBG("Waiting for PTT release");
    return;
  case DAMASK_TRANSMITTING:  // Wait for us to release PTT
    if (damask_timeout > 0 && tnc_state.ptt) {
      return;
    }
    damask_state = DAMASK_QUIET_TIME;
    damask_timeout = tnc_state.damask_short_quiet_time * 100;
    LOG_DBG("Starting quiet time");
    return;
  case DAMASK_QUIET_TIME:  // Give clients time to reply
    if (damask_timeout > 0 && tnc_state.ptt) {
      return;
    }
    damask_state = DAMASK_IDLING;
    LOG_DBG("OK to schedule");
    break;
  case DAMASK_IDLING:  // We can go ahead and queue up something to send
    break;
  }

  // Each time we try to schedule, we'll check all of the known
  // clients, and the nonclient list, starting right after the
  // one we found last time.

  sys_dlist_t *chosen = NULL;
  bool use_fx25 = false;
  for (int probe = 0; !chosen && probe <= NUM_DAMASK_CLIENTS; probe++) {
    damask_rotor = (damask_rotor + 1) % (NUM_DAMASK_CLIENTS + 1);
    if (damask_rotor == NUM_DAMASK_CLIENTS) {
      if (!sys_dlist_is_empty(&nonclient_packets)) {
	LOG_INF("Sending to non-clients");
	chosen = &nonclient_packets;
	use_fx25 = damask_fx25_policy(false, false);
      }
    } else {
      if (!sys_dlist_is_empty(&clients[damask_rotor].outbound_buffers)) {
	plaintext_callsign plaintext;
	decode_callsign(plaintext, &clients[damask_rotor].dest);
	LOG_INF("Sending to %s", plaintext);
	use_fx25 = damask_fx25_policy(clients[damask_rotor].uses_fx25, clients[damask_rotor].uses_non_fx25);
	chosen = &clients[damask_rotor].outbound_buffers;
      }
    }
  }
  if (!chosen) {
    return;
  }
  ax25buffer *buffer, *safe;
  SYS_DLIST_FOR_EACH_CONTAINER_SAFE(chosen, buffer, safe, node) {
    buffer->fx25 = use_fx25;
    layer2_accept_outgoing(buffer);
  }
  damask_state = DAMASK_TRANSMIT_WAIT;
  damask_timeout = PTT_TIMEOUT;
  LOG_DBG("Waiting for PTT");
}

void damask_run(void) {
  switch (tnc_state.damask_mode) {
  case DAMASK_OFF:
    schedule_damask_off();
    break;
  case DAMASK_ROUND_ROBIN:
    schedule_damask_round_robin(false, false);
    break;
  case DAMASK_BY_PRIORITY:
    schedule_damask_round_robin(true, false);
    break;
  case DAMASK_KEEPALIVE:
    schedule_damask_round_robin(true, true);
    break;
  case DAMASK_FORCE_DAMA:
    schedule_damask_round_robin(true, true);
    break;
  }
}

bool damask_has_traffic(void) {
  for (int probe = 0; probe < NUM_DAMASK_CLIENTS; probe++) {
    if (!sys_dlist_is_empty(&clients[probe].outbound_buffers)) {
      return true;
    }
  }
  if (!sys_dlist_is_empty(&nonclient_packets)) {
    return true;
  }
  return false;
}
