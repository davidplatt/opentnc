/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stdio.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/ring_buffer.h>

#include <stm32_ll_adc.h>
#include <stm32_ll_bus.h>
#include <stm32_ll_dma.h>
#include <stm32_ll_gpio.h>
#include <stm32_ll_tim.h>

#include <zephyr/usb/usb_device.h>
#include <zephyr/logging/log.h>

#include "board.h"
#include "demod.h"
#include "globals.h"
#include "hdlc.h"
#include "kiss.h"
#include "xoshiro128.h"

LOG_MODULE_REGISTER(stm32f103, LOG_LEVEL_INF);

#define LED0_NODE DT_ALIAS(led0)
#define LED1_NODE DT_ALIAS(led1)
#define LED2_NODE DT_ALIAS(led2)
#define LED3_NODE DT_ALIAS(led3)
#define LED4_NODE DT_ALIAS(led4)
#define LED5_NODE DT_ALIAS(led5)
#define LED6_NODE DT_ALIAS(led6)
#define LED7_NODE DT_ALIAS(led7)
#define LED8_NODE DT_ALIAS(led8)
#define PTT_NODE  DT_ALIAS(ptt)
#define HDLC_NODE DT_ALIAS(hdlc)
#define ADC_NODE DT_ALIAS(radio)

#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led0 devicetree alias is not defined"
#define LED0	""
#define PIN	0
#define FLAGS	0
#endif

#define RING_BUF_SIZE 1024
uint8_t ring_buffer[RING_BUF_SIZE];

struct ring_buf ringbuf;

#define STACK_SIZE	128
static struct k_thread blink_thread;
static K_THREAD_STACK_ARRAY_DEFINE(tstack, 1, STACK_SIZE);

#define TRANSCEIVER_STACK_SIZE	512
static struct k_thread transceiver_thread;
static K_THREAD_STACK_ARRAY_DEFINE(transceiver_stack, 1, TRANSCEIVER_STACK_SIZE);

#define ADC_DEVICE_NAME         "radio" /* fix this */
#define ADC_RESOLUTION		12
#define ADC_GAIN		ADC_GAIN_1
#define ADC_REFERENCE		ADC_REF_INTERNAL
#define ADC_ACQUISITION_TIME	ADC_ACQ_TIME_DEFAULT
#define ADC_1ST_CHANNEL_ID	0

static ADC_TypeDef *ll_adc = NULL;

const struct device *cdc_dev;

#define U_ID 0x1FF80050

const struct gpio_dt_spec led0 = GPIO_DT_SPEC_GET(LED0_NODE, gpios);
const struct gpio_dt_spec led1 = GPIO_DT_SPEC_GET(LED1_NODE, gpios);
const struct gpio_dt_spec led2 = GPIO_DT_SPEC_GET(LED2_NODE, gpios);
const struct gpio_dt_spec led3 = GPIO_DT_SPEC_GET(LED3_NODE, gpios);
const struct gpio_dt_spec led4 = GPIO_DT_SPEC_GET(LED4_NODE, gpios);
const struct gpio_dt_spec led5 = GPIO_DT_SPEC_GET(LED5_NODE, gpios);
const struct gpio_dt_spec led6 = GPIO_DT_SPEC_GET(LED6_NODE, gpios);
const struct gpio_dt_spec led7 = GPIO_DT_SPEC_GET(LED7_NODE, gpios);
const struct gpio_dt_spec led8 = GPIO_DT_SPEC_GET(LED8_NODE, gpios);
const struct gpio_dt_spec ptt = GPIO_DT_SPEC_GET(PTT_NODE, gpios);
const struct gpio_dt_spec hdlc = GPIO_DT_SPEC_GET(HDLC_NODE, gpios);

const struct device *adc;

K_SEM_DEFINE(sampler_sem, 0, 2);

struct sample_sets sample_sets;

bool sample_set_0_ready = false;
bool sample_set_1_ready = false;

// Funky sine-wave table, used by the PWM duty-cycle logic for this
// SOC only.  Need to revisit the math used for this.

const int32_t pwmtable[] = {
	86,  89,  92,  95,  98,  102, 105, 108, 111, 114, 117, 119, 122, 125,
	128, 131, 133, 136, 138, 141, 143, 145, 148, 150, 152, 154, 156, 158,
	159, 161, 162, 164, 165, 166, 167, 168, 169, 170, 171, 171, 172, 172,
	172, 172, 172, 172, 172, 172, 171, 171, 170, 169, 168, 167, 166, 165,
	164, 162, 161, 159, 158, 156, 154, 152, 150, 148, 145, 143, 141, 138,
	136, 133, 131, 128, 125, 122, 119, 117, 114, 111, 108, 105, 102, 98,
	95,  92,  89,  86,  83,  80,  77,  74,  70,  67,  64,  61,  58,  55,
	53,  50,  47,  44,  41,  39,  36,  34,  31,  29,  27,  24,  22,  20,
	18,  16,  14,  13,  11,  10,  8,   7,   6,   5,   4,   3,   2,   1,
	1,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   2,   3,   4,
	5,   6,   7,   8,   10,  11,  13,  14,  16,  18,  20,  22,  24,  27,
	29,  31,  34,  36,  39,  41,  44,  47,  50,  53,  55,  58,  61,  64,
	67,  70,  74,  77,  80,  83
};

#define PWMTABLE_COUNT (sizeof(pwmtable)/sizeof(pwmtable[0]))

const int32_t pwmtable_count = PWMTABLE_COUNT;

static void sampler_isr(void *arg)
{
  if (LL_DMA_IsActiveFlag_TE1(DMA1)) {
    LOG_ERR("DMA transfer error\n");
  }
  /* Half Transfer means that the first half has been filled */
  if (LL_DMA_IsActiveFlag_HT1(DMA1)) {
    if (sample_set_0_ready) overruns++;
    sample_set_0_ready = true;
    LL_DMA_ClearFlag_HT1(DMA1);
    k_sem_give(&sampler_sem);
  }
  /* Transfer Complete means that the second half has been filled */
  if (LL_DMA_IsActiveFlag_TC1(DMA1)) {
    if (sample_set_1_ready) overruns++;
    sample_set_1_ready = true;
    LL_DMA_ClearFlag_TC1(DMA1);
    k_sem_give(&sampler_sem);
  }
}

/* Sinewave-from-PWM ISR.  Compute the time which has passed
 * since the last time we were here, and use that time delta
 * to move along the sine-wave curve and adjust the PWM
 * duty cycle accordingly.
 *
 * This is implemented as a direct ISR in order to minimize
 * overhead and allow for a high ISR rate.
 */

#if 0
ISR_DIRECT_DECLARE(tim4_isr) {
  static int offset = 0;
  uint32_t cycles_now = k_cycle_get_32();
  uint32_t cycle_delta = cycles_now - cycles_before;
  uint32_t steps_to_increment = cycle_delta / cycles_per_sine_step;
  cycles_before += steps_to_increment * cycles_per_sine_step;
  offset = (offset + steps_to_increment) % pwmtable_count;
  LL_TIM_ClearFlag_UPDATE(TIM4);
  LL_TIM_ClearFlag_CC2(TIM4);
  LL_TIM_OC_SetCompareCH2(TIM3, pwmtable[offset]);
  sine_offset = offset;
  return 0; /* no need to reschedule */
}
#endif


/* TIM2 ISR, driven from the TIM2 update event.  This event/ISR is
 * enabled only when the transmitter is active, and is used to update
 * the PWM-to-sinewave modulator and periodically push the next HDLC
 * bit out to the modulator.
 */
ISR_DIRECT_DECLARE(tim2_isr) {
  static int32_t firings;
  gpio_pin_set_dt(&led5, 1);
  ++firings;
  /* TODO - higher-level code should set the firings modulus, so that
   * we can handle 9600 bit/second transmission as well.
   */
  if (firings % (8 * OVERSAMPLING) == 0) {
    if (HDLC_Output_Avail()) {
      bool data_bit = HDLC_Get_Bit();
      board_set_hdlc(data_bit);
      if (!tnc_state.adc_loopback) {
	set_data_leds(data_bit);
      }
      if (tnc_state.hdlc_loopback) {
	ConsumeNRZI(&hdlc_context_flat, data_bit);
      }
    }
  }
  /* update the PWM modulator.  This is done based on the actual
   * time lag since the last update, so that a delay in ISR
   * processing doesn't result in a decrease in the sinewave
   * frequency.
   */
  static int offset = 0;
  uint32_t cycles_now = k_cycle_get_32();
  uint32_t cycle_delta = cycles_now - cycles_before;
  uint32_t steps_to_increment = cycle_delta / cycles_per_sine_step;
  if (steps_to_increment > pwmtable_count) {
    steps_to_increment = 1;
    cycles_before = cycles_now;
  } else {
    cycles_before += steps_to_increment * cycles_per_sine_step;
  }
  offset = (offset + steps_to_increment) % pwmtable_count;
  LL_TIM_OC_SetCompareCH2(TIM3, pwmtable[offset]);
  sine_offset = offset;
  /* In ADC loopback mode, feed a scaled version of the PWM back
   * into the input sampler, as if it had just been digitized by
   * the ADC.
   */
  if (tnc_state.adc_loopback) {
    static int adc_loopback_index = 0;
    /* The following depends on the fact that the two sample sets are
     * contiguous in memory.  Also, the scaling is crude - would be
     * better to auto-scale to about 1/2 max amplitude based on the
     * PWM period / high-time ratio.  TODO - clean this up.
     */
    sample_sets.set_0[adc_loopback_index] = 0x800 + (pwmtable[offset] << 2);
    adc_loopback_index = (adc_loopback_index + 1) % (SAMPLE_COUNT * 2);
    if (adc_loopback_index == SAMPLE_COUNT) {
      sample_set_0_ready = true;
      k_sem_give(&sampler_sem);
    } else if (adc_loopback_index == 0) {
      sample_set_1_ready = true;
      k_sem_give(&sampler_sem);
    }
  }
  /* Done.  No need to reschedule. */
  LL_TIM_ClearFlag_UPDATE(TIM2);
  gpio_pin_set_dt(&led5, 0);
  return 0;
}

static const struct adc_channel_cfg m_1st_channel_cfg = {
	.gain             = ADC_GAIN,
	.reference        = ADC_REFERENCE,
	.acquisition_time = ADC_ACQUISITION_TIME,
	.channel_id       = ADC_1ST_CHANNEL_ID,
#if defined(CONFIG_ADC_CONFIGURABLE_INPUTS)
	.input_positive   = ADC_1ST_CHANNEL_INPUT,
#endif
};

static const struct device *init_adc(void)
{
	const struct device *adc_dev = DEVICE_DT_GET(ADC_NODE);
	int ret;

	if (!adc_dev) {
	  LOG_ERR("Can't find ADC %s", ADC_DEVICE_NAME);
	  return NULL;
	}

	ret = adc_channel_setup(adc_dev, &m_1st_channel_cfg);
	if (ret != 0)
	  LOG_ERR("Setting up ADC channel failed with code %d", ret);

	return adc_dev;
}

/* TIM2 does all the heavy-lifting timing work for both reception
 * (demodulation) and transmission (modulation).  It ticks over at a
 * rate of 38400 times/second.  During reception, Channel 2 is used to
 * generate an internal signal which clocks ADC1, sampling the analog
 * input pin.  During transmission, the counter update event interrupt
 * is enabled, entering a fast interrupt handler which updates the PCM
 * duty cycle (the sinewave modulator) and clocks HDLC bits into the
 * modulator at the proper rate (1200 bits/second).
 */
static void Init_TIM2(void) {
  LOG_INF("Setting up TIM2");
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);
  k_msleep(1);
  LL_TIM_DisableCounter(TIM2);
  LL_TIM_SetClockSource(TIM2, LL_TIM_CLOCKSOURCE_INTERNAL);
  LL_TIM_DisableIT_CC2(TIM2); /* Don't interrupt on timer compare */
  LL_TIM_SetPrescaler(TIM2, 15); /* Clock input at 72 MHz, divide by 16 to get 4.5 MHz*/
  LL_TIM_SetAutoReload(TIM2, (468 / OVERSAMPLING) - 1); /* divide by 468 to yield 9600 samples/second */
  /* TIM2 CH2 serves as the sampling timer for DAC1 */
  LL_TIM_OC_SetMode(TIM2, LL_TIM_CHANNEL_CH2, LL_TIM_OCMODE_PWM1);
  LL_TIM_OC_SetCompareCH2(TIM2, 100);
  LL_TIM_CC_EnableChannel(TIM2, LL_TIM_CHANNEL_CH2);
  /* The update event from TIM2 is used for the "next TX bit time" IRQ. */
  LL_TIM_EnableUpdateEvent(TIM2);
  LL_TIM_EnableCounter(TIM2);
}

static void Init_TIM3(void) {
  LOG_INF("Setting up TIM3 for PWM");
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3);
  k_msleep(1);
  LL_TIM_DisableCounter(TIM3);
  LL_TIM_SetClockSource(TIM3, LL_TIM_CLOCKSOURCE_INTERNAL);
  LL_TIM_DisableIT_CC2(TIM3); /* Don't interrupt on timer compare */
  LL_TIM_SetPrescaler(TIM3, 0); /* Clock input at 36 MHz, run at full speed*/
  LL_TIM_SetAutoReload(TIM3, 172); /* divide by 173, giving 208 kHz */
  LL_TIM_EnableARRPreload(TIM3);
  LL_TIM_OC_SetMode(TIM3, LL_TIM_CHANNEL_CH2, LL_TIM_OCMODE_PWM1);
  LL_TIM_OC_SetCompareCH2(TIM3, 86); /* ~50% duty cycle for now */
  LL_TIM_OC_EnablePreload(TIM3, LL_TIM_CHANNEL_CH2);
  LL_TIM_CC_EnableChannel(TIM3, LL_TIM_CHANNEL_CH2);
  LL_TIM_EnableAllOutputs(TIM3);
  LL_TIM_EnableUpdateEvent(TIM3);
  LL_TIM_EnableCounter(TIM3);
}

#if 0
static void Init_TIM4(void) {
  LOG_INF("Setting up TIM4");
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM4);
  k_msleep(1);
  LL_TIM_DisableCounter(TIM4);
  LL_TIM_SetClockSource(TIM4, LL_TIM_CLOCKSOURCE_INTERNAL);
  LL_TIM_DisableIT_CC2(TIM4); /* Don't interrupt on timer compare */
  LL_TIM_SetPrescaler(TIM4, 0); /* Clock input at 36 MHz (half 72) */
  LL_TIM_SetAutoReload(TIM4, 1800);  /* 20 kHz sinewave advance rate */
  LL_TIM_EnableARRPreload(TIM4);
  /* TIM4 update event provides the DMA service request for DMA1 Channel 7, updating TIM3. */
  LL_TIM_OC_SetCompareCH2(TIM4, 1);
  //  LL_TIM_OC_SetMode(TIM3, LL_TIM_CHANNEL_CH2, LL_TIM_OCMODE_FROZEN);
  LL_TIM_OC_EnablePreload(TIM4, LL_TIM_CHANNEL_CH2);
  LL_TIM_CC_EnableChannel(TIM4, LL_TIM_CHANNEL_CH2);
  // LL_TIM_EnableIT_CC2(TIM4);
  // LL_TIM_EnableDMAReq_CC2(TIM4);
  LL_TIM_EnableUpdateEvent(TIM4);
  LL_TIM_EnableIT_UPDATE(TIM4);
  // LL_TIM_EnableDMAReq_UPDATE(TIM4);
  cycles_before = k_cycle_get_32();
}
#endif

static void SetupPins(void) {
  LOG_INF("Setting up pin states");
  /* A7 is the PWM output from TIM3 CH2. */
  LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_7, LL_GPIO_MODE_ALTERNATE);
  LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_7, LL_GPIO_SPEED_FREQ_HIGH);
  LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_7, LL_GPIO_OUTPUT_PUSHPULL);
}

static void Init_DMA(void) {
  LOG_INF("DMA1 is 0x%p", DMA1);
  LOG_INF("ADC1 is 0x%p", ADC1);
  LOG_INF("Enabling clock for DMA1");
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1);
  LOG_INF("Setting up DMA for ADC");
  LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_1);
  LL_DMA_SetPeriphAddress(DMA1, LL_DMA_CHANNEL_1, ((uint32_t) &ADC1->DR));
  LL_DMA_SetMemoryAddress(DMA1, LL_DMA_CHANNEL_1, (uint32_t) &sample_sets);
  LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_1, LL_DMA_DIRECTION_PERIPH_TO_MEMORY);
  LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MODE_CIRCULAR);
  LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PERIPH_NOINCREMENT);
  LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MEMORY_INCREMENT);
  LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PDATAALIGN_HALFWORD);
  LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MDATAALIGN_HALFWORD);
  LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_1, SAMPLE_COUNT * 2);
  LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_1, DMA_PRIORITY_MEDIUM);
  LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_1);
  LL_DMA_EnableIT_HT(DMA1, LL_DMA_CHANNEL_1);
  LOG_INF("DMA setup done");
  //  LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_1);
  LOG_INF("Setting up DMA for PWM");
  LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_7);
  LL_DMA_SetPeriphAddress(DMA1, LL_DMA_CHANNEL_7, ((uint32_t) &TIM3->CCR2));
  LL_DMA_SetMemoryAddress(DMA1, LL_DMA_CHANNEL_7, (uint32_t) &pwmtable[0]);
  LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_7, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
  LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_7, LL_DMA_MODE_CIRCULAR);
  LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_7, LL_DMA_PERIPH_NOINCREMENT);
  LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_7, LL_DMA_MEMORY_INCREMENT);
  LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_7, LL_DMA_PDATAALIGN_WORD);
  LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_7, LL_DMA_MDATAALIGN_WORD);
  LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_7, pwmtable_count);
  LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_7, DMA_PRIORITY_LOW);
  LL_DMA_DisableIT_TC(DMA1, LL_DMA_CHANNEL_7);
  LL_DMA_DisableIT_HT(DMA1, LL_DMA_CHANNEL_7);
  LOG_INF("DMA setup done, enabling");
  LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_7);
  LOG_INF("DMA enabled");
}

#define SETUP(ptr, node, pin, flags) \
  ptr = device_get_binding(node); \
  if (ptr == NULL) return; \
  ret = gpio_pin_configure(ptr, pin, GPIO_OUTPUT_ACTIVE | flags); \
  if (ret < 0) return; \
  gpio_pin_set(ptr, pin, 0);

#define ISOK(dev) \
  if (!gpio_is_ready_dt(&dev)) { \
    LOG_ERR("GPIO is not ready"); \
  } else { \
    int ret = gpio_pin_configure_dt(&dev, GPIO_OUTPUT_ACTIVE); \
    if (ret < 0) { \
      LOG_ERR("GPIO not configured");		\
    } \
  }



void board_init(void)
{
	int ret;

	ISOK(led0);
	ISOK(led1);
	ISOK(led2);
	ISOK(led3);
	ISOK(led4);
	ISOK(led5);
	ISOK(led6);
	ISOK(led7);
	ISOK(led8);
	ISOK(ptt);
	ISOK(hdlc);

	cdc_dev = device_get_binding("CDC_ACM_0");
	if (!cdc_dev) {
		LOG_ERR("CDC ACM device not found");
		return;
	}

	ret = usb_enable(NULL);
	if (ret != 0) {
		LOG_ERR("Failed to enable USB");
		return;
	}

	adc = init_adc();
	if (!adc) {
	  LOG_ERR("ADC not set up properly");
	}

	ring_buf_init(&ringbuf, sizeof(ring_buffer), ring_buffer);

	LOG_INF("Start heartbeat");

	k_thread_create(&blink_thread,
	  tstack[0], STACK_SIZE,
	  (k_thread_entry_t)board_heartbeat,
	  NULL, NULL, NULL,
	  K_PRIO_PREEMPT(10), 0, K_NO_WAIT);

	uart_irq_callback_set(cdc_dev, CDC_interrupt_handler);

	/* Enable rx interrupts */
	uart_irq_rx_enable(cdc_dev);

	LOG_INF("Connecting DMA1 IRQ");
#define MY_DEV_PRIO 2
	IRQ_CONNECT(DMA1_Channel1_IRQn, MY_DEV_PRIO, sampler_isr, NULL, 0);
	irq_enable(DMA1_Channel1_IRQn);
	LOG_INF("DMA1 IRQ enabled");

	Init_DMA();

#if 1
	/* The Zephyr ADC APIs only support ADC conversion triggered by
	 * software.  Override this to set triggering from TIM2.
	 * Unfortunately the config structure within the STM32F1xx ADC
	 * driver is not public...
	 */
	{
		struct adc_stm32_cfg {
		  ADC_TypeDef *base;
		};
		const struct adc_stm32_cfg *config = adc->config;
		ll_adc = (ADC_TypeDef *)config->base;
		LOG_INF("Override ADC 0x%p configuration", ll_adc);
		LL_ADC_Disable(ll_adc);
		k_msleep(1);
		LL_ADC_SetChannelSamplingTime(ll_adc, LL_ADC_CHANNEL_0, LL_ADC_SAMPLINGTIME_239CYCLES_5);
		LL_ADC_DisableIT_EOS(ll_adc); /* Don't interrupt on end of conversion */
		LL_ADC_REG_SetDMATransfer(ll_adc, LL_ADC_REG_DMA_TRANSFER_UNLIMITED);
		LL_ADC_REG_SetTriggerSource(ll_adc, LL_ADC_REG_TRIG_EXT_TIM2_CH2);
		LL_ADC_REG_StartConversionExtTrig(ll_adc, LL_ADC_REG_TRIG_EXT_RISING);
		k_msleep(1);
		LL_ADC_Enable(ll_adc);
		LOG_INF("Back from override");
	}
#endif

	LOG_INF("Start transceiver thread");

	k_thread_create(&transceiver_thread,
			transceiver_stack[0], TRANSCEIVER_STACK_SIZE,
			(k_thread_entry_t)board_transceiver,
			NULL, NULL, NULL,
			K_PRIO_COOP(1), 0, K_NO_WAIT);
	
	SetupPins();

	board_set_tx_freq(1200); /* set to a reference frequency */

	LOG_INF("Hooking up TIM2");
	IRQ_DIRECT_CONNECT(TIM2_IRQn, MY_DEV_PRIO, tim2_isr, 0);

#if 0
	LOG_INF("Hooking up TIM4");
	IRQ_DIRECT_CONNECT(TIM4_IRQn, MY_DEV_PRIO, tim4_isr, 0);
#endif

	Init_TIM2();
	Init_TIM3();

#if 0
	Init_TIM4();

	LOG_INF("Enabling TIM4 IRQ");
	irq_enable(TIM4_IRQn);
	LOG_INF("TIM4 IRQ enabled");

#endif

	LOG_INF("Enabling TIM2 IRQ");
	irq_enable(TIM2_IRQn);
	LOG_INF("TIM2 IRQ enabled");

	LOG_INF("Running...");
}

/* We want a pseudo-random number generator which is seeded in a way
 * which nearly guaranteed a different sequence on each boot-up.
 * Since the STM32F103 doesn't have a full-fledged random entropy source
 * of its own, we'll fake it by seeding the PRNG the first time we
 * use it (when doing P-persistence for the first packet we're about
 * to transmit) using the CPU cycle counter as part of the seed.  For
 * the rest of the seed we'll use the most-recently-digitized samples
 * from the transceiver.
 */
uint32_t board_get_random32(void) {
  static bool random_initialized = false;
  if (!random_initialized) {
    xoshiro128_init(sample_sets.set_0[0], sample_sets.set_0[10],
		    sample_sets.set_0[20], k_cycle_get_32());
    /* Whiten the seed bits */
    xoshiro128();
    xoshiro128();
    xoshiro128();
    xoshiro128();
    random_initialized = true;
  }
  return xoshiro128();
}

void board_enable_rx(void) {
  LOG_INF("Enabling RX");
  LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_1);
}

void board_disable_rx(void){
  LOG_INF("Disabling RX");
  LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_1);
}

void board_enable_tx(void){
  LOG_DBG("Enabling TX");
  board_set_tx_freq(1200);
  cycles_before = k_cycle_get_32();
  //  LL_TIM_DisableCounter(TIM4);
  LL_TIM_EnableIT_UPDATE(TIM2);
}

void board_disable_tx(void){
  LOG_DBG("Disabling TX");
  LL_TIM_DisableIT_UPDATE(TIM2);
  //  LL_TIM_DisableCounter(TIM4);
  LL_TIM_OC_SetCompareCH2(TIM3, 86);
}

void board_set_tx_freq(uint32_t hz) {
  cycles_per_sine_step = sys_clock_hw_cycles_per_sec() / hz / pwmtable_count;
}
