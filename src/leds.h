#ifndef _H_LEDS
# define _H_LEDS

#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>

#include "board.h"

// This set of arcane gibberish will iterate twice through all of the
// VLED nodes.  It first lays out a data structure "struct leds" which
// contains a data field (int16_t) for each VLED.  It then creates a
// setter function for each VLED, which stores the incoming value into
// the proper field in "struct leds", and (if node has a gpios
// property) writes the bit to the pin.

#define MY_CONCAT2(foo, bar) foo ## bar
#define MY_MAKE_LED(foo) MY_CONCAT2(foo, _led)
#define NODE_STRUCT_NAME(node) MY_MAKE_LED(DT_STRING_TOKEN(node, struct_field))

#define MY_CONCAT3(foo, bar, baz) foo ## bar ## baz
#define MY_MAKE_SETTER(foo) MY_CONCAT3(set_, foo, _led)
#define NODE_SETTER_NAME(node) MY_MAKE_SETTER(DT_STRING_TOKEN(node, struct_field))

#define DEFINE_FIELD(node) struct { uint8_t value, remaining_hang; } DT_STRING_TOKEN(node, struct_field);

struct leds {
  DT_N_S_vleds_FOREACH_CHILD(DEFINE_FIELD)
};

extern struct leds leds;

#define VLED_HANG_TIME(node)  DT_PROP_OR(node, hang_time, 0)
#define VLED_PERIOD(node)  DT_PROP_OR(node, period, 0)

#define DECLARE_LED_SETTER_2(node, setter, structure, field) static inline void setter (int16_t val) { \
  extern const struct gpio_dt_spec structure; \
  if (DT_NODE_HAS_PROP(node, gpios)) gpio_pin_set_dt(&structure, val != 0); \
  leds.field.value = val; \
  if (VLED_HANG_TIME(node) != 0 && val != 0) { leds.field.remaining_hang = VLED_HANG_TIME(node); } \
}

#define DECLARE_LED_SETTER(node) DECLARE_LED_SETTER_2(node, NODE_SETTER_NAME(node), NODE_STRUCT_NAME(node), DT_STRING_TOKEN(node, struct_field))

DT_N_S_vleds_FOREACH_CHILD(DECLARE_LED_SETTER)

void init_leds();

void update_leds(uint32_t ticks);

void set_all_led_gpios_to(uint32_t value);

void restore_leds(void);

static inline void set_data_leds(int on) {
  set_data_1_led(on ? 1 : 0);
  set_data_0_led(on ? 0 : 1);
}

#endif
