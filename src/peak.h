/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stdint.h>

typedef struct peak_f {
  int32_t count;
  int32_t cursor;
  float *samples;
} peak_f;

#define DECLARE_PEAK_F(NAME, COUNT) \
  float NAME##samples[COUNT]; \
  peak_f NAME = {.count = COUNT, .cursor = 0, .samples = NAME##samples }

void reset_peak_f(peak_f *peak);

float update_peak_f(peak_f *peak, float new_sample);

