/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_LAYER2
#define _H_LAYER2

#include <stdint.h>
#include <stdbool.h>

#include "ax25buffer.h"

extern const char* frame_roles[];

extern struct k_sem layer3_incoming_sem;

void send_XID_frame(connection *conn, bool is_command);

void layer2_accept_incoming(ax25buffer *b);
void layer2_accept_outgoing(ax25buffer *b);

// Use layer2_transmit when originating a packet from within the TNC
// (e.g. for an outbound connection, or when digipeating).  It will
// call layer2_accept_outgoing() to queue the packet for transmission.
// If the TNC is operating in extended-KISS mode it will also send
// a clone of the packet to the host via the KISS interface, for
// monitoring purposes.
//
// This API is *not* used when the KISS interface receives packets from
// the host for transmission, as this would cause them to loop back
// to the host (annoying).
void layer2_transmit(ax25buffer *b);

bool layer2_has_traffic(void);

#endif  // _H_LAYER2

