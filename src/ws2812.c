#include <string.h>
#include <stdlib.h>

#include "board.h"
#include "leds.h"
#include "ws2812.h"

#if NUM_WS2812 > 0

// Always put the storage structure on a 4-byte boundary for
// DMA compatibility.

struct ws2812_led ws2812_leds[NUM_WS2812] __attribute__((aligned(4)));

#define CLIP(x) ((x) < 255 ? (x) : 255)

void map_led_to_ws2812(int value, const int16_t * mapping, size_t mapping_size) {
  size_t offset = 0;
  int limit = mapping_size / sizeof(*mapping);
  while (offset + 4 < limit) {
    int match = mapping[offset++];
    int ws2812_index = mapping[offset++];
    int red = mapping[offset++];
    int green = mapping[offset++];
    int blue = mapping[offset++];
    if (ws2812_index >= NUM_WS2812) {
      continue;
    }
    if (value != match && match >= 0) {
      continue;
    }
    if (match < 0) {
      red *= value;
      green *= value;
      blue *= value;
    }
    ws2812_leds[ws2812_index].red = CLIP(ws2812_leds[ws2812_index].red + red);
    ws2812_leds[ws2812_index].green = CLIP(ws2812_leds[ws2812_index].green + green);
    ws2812_leds[ws2812_index].blue = CLIP(ws2812_leds[ws2812_index].blue + blue);
    return;
  }
}

#define CALL_LED_MAPPER2(mapping, field) { static const int16_t map[] = mapping; \
    if (sizeof(map) > 0) map_led_to_ws2812(leds.field.value, map, sizeof(map)); \
  }

#define NODE_MAPPING(node)  DT_PROP_OR(node, ws2812_mappings, {})

#define CALL_LED_MAPPER(node) CALL_LED_MAPPER2(NODE_MAPPING(node), DT_STRING_TOKEN(node, struct_field))

static void ws2812_test(void) {
  int32_t phase = abs(tnc_state.lamp_test_time % 60);
  int32_t color = phase / 20;
  int32_t brightness = (phase % 20) / 4;
  for (int ws2812_index = 0; ws2812_index < NUM_WS2812; ws2812_index++) {
    switch (color) {
    case 0:
      ws2812_leds[ws2812_index].red = brightness;
      break;
    case 1:
      ws2812_leds[ws2812_index].green = brightness;
      break;
    case 2:
      ws2812_leds[ws2812_index].blue = brightness;
      break;
    }
  }
}

void encode_leds_to_ws2812(void) {
  memset(ws2812_leds, 0, sizeof(ws2812_leds));

  if (tnc_state.lamp_test_time != 0) {
    ws2812_test();
  } else {
    DT_N_S_vleds_FOREACH_CHILD(CALL_LED_MAPPER);
  }
}

#else

#warning NUM_WS2812 is zero

#endif





