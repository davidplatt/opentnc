/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>

#include <zephyr/logging/log.h>

#include "ax25buffer.h"
#include "board.h"
#include "demod.h"
#include "globals.h"
#include "hdlc.h"
#include "kiss.h"
#include "leds.h"
#include "mod.h"
#include "pulses.h"

LOG_MODULE_REGISTER(tnc_common, LOG_LEVEL_INF);

void generic_push_to_radio(void) {
  bool slowpace = false;
  if (!tx_push.working) {
    if (tnc_state.tx_inhibit) {
      return;
    }
    if (!ax25buffer_peek_head(&tx_packets)) {
      LOG_DBG("No push packets");
      return;
    }
    /* This implements P-persistence. */
    if (board_detecting_carrier()) {
      tx_push.slot_delay = 0;
      return;
    }
    if (tx_push.slot_delay > 0) {
      tx_push.slot_delay--;
      return;
    }
    uint8_t nearly_spherical_die = board_get_random32() & 0xFF;
    if (tnc_state.p < nearly_spherical_die) {
      tx_push.slot_delay = tnc_state.slot_time;
      return;
    }
    /* OK, we've decided it's our time to transmit. Grab the
     * first frame and unlink it.
     */
    tx_push.current = ax25buffer_take_head(&tx_packets);
    if (!tx_push.current) {
      LOG_ERR("ax25buffer_get_head failed");
      return; /* shouldn't happen. */
    }
    LOG_DBG("Found %s frame %p of %d bytes to send",
	    tx_push.current->type == AX25_RAW ? "raw" : "normal",
	    tx_push.current, tx_push.current->bits_written / 8);
    //    HDLC_Status();
    tx_push.working = true;
    /* TODO - calculate the delay burst based on the modulator baud
     * rate.  For now assume 1200 baud - 10 milliseconds per txdelay,
     * about 6 milliseconds per sync).  Enforce a configurable number
     * of zero bytes to ensure clock sync-up at the receiving end, and
     * follow by a bunch of flags to sync up.
     */
    tx_push.send_zeros = (tnc_state.txdelay * 10) / 6;
    if (tx_push.send_zeros < tnc_state.minimum_zeros) {
      tx_push.send_zeros = tnc_state.minimum_zeros;
    }
    tx_push.send_syncs = tnc_state.initial_syncs;

    /* txtail is deprecated, and defaults to zero, but we still support it.
     * We'll always send at least two aborts as a tail.
     */
    tx_push.send_aborts = (tnc_state.txtail * 10) / 6;
    if (tx_push.send_aborts < tnc_state.minimum_aborts) {
      tx_push.send_aborts = tnc_state.minimum_aborts;
    }
    if (tx_push.current->type == AX25_FRAME) {
      tx_push.need_fcs = true;
      tx_push.send_fcs_lsb = true;
      tx_push.send_fcs_msb = true;
      tx_push.send_final_sync = 2;
    } else {
      tx_push.need_fcs = false;
      tx_push.send_fcs_lsb = false;
      tx_push.send_fcs_msb = false;
      tx_push.send_final_sync = 0;
    }
    common_disable_rx();
    common_enable_tx();
    board_enable_ptt();
    tnc_state.hdlc_locked = false;
    HDLC_Reset_FCS();
  }
  if (!HDLC_Ready()) {
    LOG_DBG("HDLC not ready to encode");
    return;
  }
  while (tx_push.send_zeros > 0) {
    LOG_DBG("Send zero");
    HDLC_Encode_Byte(0);
    --tx_push.send_zeros;
    if (slowpace || !HDLC_Ready()) {
      return;
    }
  }
  while (tx_push.send_syncs > 0) {
    LOG_DBG("Send sync");
    HDLC_Reset_Capture();
    HDLC_Encode_Sync();
    --tx_push.send_syncs;
    HDLC_Reset_FCS();
    if (slowpace || !HDLC_Ready()) {
      return;
    }
  }

  if (tx_push.current) {
    switch (tx_push.current->type) {
    case AX25_FRAME:
      set_ptt_led(PTT_HDLC);
      int32_t byte;
      while ((byte = ax25buffer_get_byte(tx_push.current)) >= 0) {
	LOG_DBG("Encode/send byte 0x%02x", byte);
	HDLC_Encode_Byte(byte);
	if (slowpace || !HDLC_Ready()) {
	  return;
	}
      }
      break;
    case AX25_RAW:
      set_ptt_led(PTT_RAW);
      int bit;
      while ((bit = ax25buffer_get_bit(tx_push.current)) >= 0) {
	LOG_DBG("Encode/send bit %d", bit);
	HDLC_Append_Bit(bit);
	if (slowpace || !HDLC_Ready()) {
	  return;
	}
      }
      break;
    case AX25_PAYLOAD:
      LOG_ERR("Can't push AX25_PAYLOAD buffers to radio!");
      break;
    }
  }

  if (tx_push.need_fcs) {
    LOG_DBG("Need to append FCS");
    tx_push.need_fcs = false;
    tx_push.fcs = HDLC_Get_FCS() ^ 0xFFFF;
    if (tnc_state.drop_tx != 0 &&
	(board_get_random32() / 13) % tnc_state.drop_tx == 0) {
      LOG_INF("Deliberately corrupting TX FCS");
      tx_push.fcs ^= 0x5AA5;
    }
    LOG_DBG("Transmitted FCS 0x%04X\n", tx_push.fcs);
  }
  if (tx_push.send_fcs_lsb) {
    LOG_DBG("Encode/send FCS LSB 0x%02X", tx_push.fcs & 0xFF);
    HDLC_Encode_Byte(tx_push.fcs & 0xFF);
    tx_push.send_fcs_lsb = 0;
    if (slowpace || !HDLC_Ready()) {
      return;
    }
  }
  if (tx_push.send_fcs_msb) {
    LOG_DBG("Encode/send FCS MSB 0x%02X", (tx_push.fcs >> 8) & 0xFF);
    HDLC_Encode_Byte((tx_push.fcs >> 8) & 0xFF);
    tx_push.send_fcs_msb = 0;
    if (slowpace || !HDLC_Ready()) {
      return;
    }
  }

  // At this point we're done with this packet - call its callback
  // if there is one, and then release it.
  if (tx_push.current) {
    LOG_DBG("Done with packet");
    tnc_state.stats.frames_sent ++;
    if (tx_push.current->callback) {
      (*tx_push.current->callback)(tx_push.current);
    }
    ax25buffer_free(tx_push.current);
    tx_push.current = NULL;
  }

  // The "final" syncs will serve to end this frame, and
  // introduce the following frame if any.
  while (tx_push.send_final_sync) {
    LOG_DBG("Send final sync");
    HDLC_Encode_Sync();
    tx_push.send_final_sync --;
    if (slowpace || !HDLC_Ready()) {
      return;
    }
  }

  // If there's another available in the list, we can start
  // encoding it immediately - we've send one or more
  // syncs between frames so we're ready to go.

  tx_push.current = ax25buffer_take_head(&tx_packets);
  if (tx_push.current) {
    LOG_DBG("Send further %s frame %p, %d bytes",
	    tx_push.current->type == AX25_RAW ? "raw" : "normal",
	    tx_push.current,
	    tx_push.current->bits_written / 8);
    if (tx_push.current->type == AX25_FRAME) {
      tx_push.need_fcs = true;
      tx_push.send_fcs_lsb = true;
      tx_push.send_fcs_msb = true;
      tx_push.send_final_sync = 2;
    } else {
      tx_push.need_fcs = false;
      tx_push.send_fcs_lsb = false;
      tx_push.send_fcs_msb = false;
      tx_push.send_final_sync = 0;
    }
    HDLC_Reset_FCS();
    return; /* start encoding new frame next time */
  }

  // OK, we're really done.  Send a short burst of ABORT to
  // make it clear that we're QRT for now, then hang up.

  while (tx_push.send_aborts > 0) {
    LOG_DBG("Send abort");
    HDLC_Encode_Abort();
    --tx_push.send_aborts;
    if (slowpace || !HDLC_Ready()) {
      return;
    }
  }

  HDLC_Flush();

  // We'll keep transmitting until the low-level HDLC code
  // runs out of encoded bits.

  if (HDLC_Output_Avail()) {
    return;
  }

  tx_push.working = false;
  board_disable_ptt();
  common_disable_tx();
  if (!tnc_state.rx_inhibit) {
    common_enable_rx();
  }
}


void common_enable_rx(void) {
  LOG_DBG("In common_enable_rx");
  ReInitDemodulator();
  board_enable_rx();
  tnc_state.rx_running = true;
}

void common_enable_tx(void) {
  LOG_DBG("In common_enable_tx");
  common_configure_modulator();
  board_enable_tx();
  tnc_state.tx_running = true;
}

void common_disable_rx(void) {
  LOG_DBG("In common_disable_rx");
  board_disable_rx();
  tnc_state.rx_running = false;
}

void common_disable_tx(void) {
  LOG_DBG("In common_disable_tx");
  board_disable_tx();
  tnc_state.tx_running = false;
}

void quiesce_transceiver(void) {
  tnc_state.tx_inhibit = true;
  tnc_state.rx_inhibit = true;
  while (tnc_state.tx_running) {
    LOG_DBG("TX running during quiesce");
    k_msleep(100);
  }
  if (tnc_state.rx_running) {
    common_disable_rx();
  }
}

void unquiesce_transceiver(void) {
  tnc_state.rx_inhibit = false;
  LOG_DBG("Unquiesce: tx_running %d rx_running %d", tnc_state.tx_running, tnc_state.rx_running);
  if (!tnc_state.rx_running && !tnc_state.tx_running) {
    common_enable_rx();
  }
  tnc_state.tx_inhibit = false;
}

struct modulator_state G3RUH_modulator_state;
struct modulator_cache G3RUH_modulator_cache;

#define MAX_TAP_OFFSET 63

struct fir_filter_tap_set {
  uint32_t number_of_taps;
  struct {
    uint32_t offset;
    float coefficient;
  } taps[];
};

const struct fir_filter_tap_set flat_filter =
  {.number_of_taps = 1,
   .taps[0].offset = 0,
   .taps[0].coefficient = 1.0f,
  };

struct fir_filter_tap_set user_filter =
  {.number_of_taps = 3,
   .taps[0].offset = 0,
   .taps[0].coefficient = 0.0f,
   .taps[1].offset = 0,
   .taps[1].coefficient = 0.0f,
   .taps[2].offset = 0,
   .taps[2].coefficient = 0.0f
  };

static void setup_pulse_modulator(struct modulator_state *state,
				  struct modulator_cache *cache,
				  const struct pulse *pulse) {
  if (cache->valid &&
      cache->pulse_shape == tnc_state.pulse_shape &&
      cache->txeq_mode == tnc_state.txeq_mode &&
      cache->txeq_peak_frequency == tnc_state.txeq_peak_frequency &&
      cache->txeq_peak_gain_centibels == tnc_state.txeq_peak_gain_centibels) {
    LOG_DBG("G3RUH modulator using cached waveforms");
  }
  memset(cache, 0, sizeof(*cache));
  LOG_DBG("Pulse modulator setup");
  LOG_DBG("  Pulse shape: %s", pulse->name);
  LOG_DBG("  Bauds: %d", pulse->bauds);
  LOG_DBG("  Samples per baud: %d", pulse->samples_per_baud);
  // Select the FIR filter to use.  When "flat" has been specified,
  // it's trivial.  When a non-flat filter has been specified, we
  // synthesize a simple 2- or 3-tap filter based on the user's
  // indicated peak frequency and peak gain.
  const struct fir_filter_tap_set *filter;
  if (tnc_state.txeq_mode == TX_EQ_FLAT) {
    filter = &flat_filter;
  } else {
    // Gain is specified in dB
    float primary_coeff = pow(.1, (double) (tnc_state.txeq_peak_gain_centibels / 200.0f)); // 20 dB == 200 cB
    float secondary_coeff = 1.0f - primary_coeff;
    int32_t sample_delta = (G3RUH_SAMPLES_PER_BAUD * BAUDRATE_G3RUH) / (2 * tnc_state.txeq_peak_frequency);
    if (sample_delta > MAX_TAP_OFFSET / 2) {
      sample_delta = MAX_TAP_OFFSET / 2;
    }
    LOG_DBG("Peak frequency %d, setting tap offset to %d", tnc_state.txeq_peak_frequency, sample_delta);
    user_filter.taps[0].coefficient = primary_coeff;
    user_filter.taps[0].offset = sample_delta;
    switch (tnc_state.txeq_mode) {
    case TX_EQ_LEADING:
      user_filter.number_of_taps = 2;
      user_filter.taps[1].coefficient = -secondary_coeff;
      user_filter.taps[1].offset = 0;
      break;
    case TX_EQ_TRAILING:
      user_filter.number_of_taps = 2;
      user_filter.taps[1].coefficient = -secondary_coeff;
      user_filter.taps[1].offset = sample_delta * 2;
      break;
    case TX_EQ_NOSHIFT:
    default: // to keep the compiler happy
      user_filter.number_of_taps = 3;
      user_filter.taps[1].coefficient =
	user_filter.taps[2].coefficient = -secondary_coeff / 2.0f;
      user_filter.taps[1].offset = 0;
      user_filter.taps[2].offset = sample_delta * 2;
      break;
    }
    filter = (const struct fir_filter_tap_set *) &user_filter;
  }
  LOG_DBG("  FIR filter has %d taps", filter->number_of_taps);
  int32_t pulse_samples = pulse->bauds * pulse->samples_per_baud;
  int32_t filtered_pulse_samples = pulse_samples + MAX_TAP_OFFSET + 1;
  int32_t output_samples = filtered_pulse_samples;
  size_t space_needed = sizeof(float) * (filtered_pulse_samples + output_samples);
  if (space_needed > CONFIG_WORK_AREA_SIZE) {
    LOG_ERR("Pulse requires too many samples to composite!");
    return;
  }
  cache->samples_per_baud = pulse->samples_per_baud;
  state->transmit_bit_mask = 0xFF;  // 256 states, 8 bits
  k_mutex_lock(&work_area_mutex, K_FOREVER);
  // Grab two parts of the work area.  The first will hold the shaped
  // pulse (prototype pulse, convolved with the chosen filter).  The
  // second will hold the composite waveform we're creating.
  float *filtered_pulse = (float *) work_area;
  float *composite = filtered_pulse + filtered_pulse_samples;
  for (int i = 0; i < filtered_pulse_samples; i++) {
    filtered_pulse[i] = 0.0f;
  }
  for (int i = 0; i < output_samples; i++) {
    composite[i] = 0.0f;
  }
  // Construct the filtered pulse.
  for (int tap = 0; tap < filter->number_of_taps; tap++) {
    assert(filter->taps[tap].offset <= MAX_TAP_OFFSET);
    for (int i = 0; i < pulse_samples; i++) {
      filtered_pulse[i + filter->taps[tap].offset] +=
	pulse->samples[i] * filter->taps[tap].coefficient;
    }
  }
  // We're going to go through all 256 possible bit patterns, using
  // a maximal-length linear feedback shift register pattern to jump
  // from one index to the next in a way which lets us re-use the
  // waveform computations from the previous pattern.  During each
  // iteration we shift the previous waveform to the left by one
  // baud time, and then add or subtract in the chosen pulse waveform.
  // We must do this several times in sequence to generate the initial
  // waveform data for bit pattern 00000000 (extra iterations are
  // done to allow for the possible expansion of the pulse by the
  // filtering process), and can then do just one iteration for each
  // subsequent pattern.
  //
  // As we go, we'll maintain the peak value of any of the
  // composite waveforms.
  int32_t pattern = 0;
  int32_t iterations = 16; // special case, for pattern 0
  bool complete = false;
  int32_t did = 0;
  float waveform_peak = 0.0f;
  while (!complete) {
    for (int rep = 0; rep < iterations; rep++) {
      // Discard one baud's worth of samples from the head of the composite
      for (int ci = 0; ci < output_samples - pulse->samples_per_baud; ci++) {
	composite[ci] = composite[ci + pulse->samples_per_baud];
      }
      // Add the pulse into the composite data.  We "zero out" all
      // negative-going (zero-bit) pulses when we're in waveform-test
      // mode, so that the funny scrambler override in the modulator
      // just sends an occasional positive-going pulse with no
      // negative-going neighbors.
      for (int ci = 0; ci < output_samples; ci++) {
	if (pattern & 1) {
	  composite[ci] += filtered_pulse[ci];
	} else if (!tnc_state.waveform_test) {
	  composite[ci] -= filtered_pulse[ci];
	}
	if (fabsf(composite[ci]) > waveform_peak) {
	  waveform_peak = fabsf(composite[ci]);
	}
      }
    }
    LOG_DBG("Handling index %d  0x%02x", pattern, pattern);
    // Take one baud's worth of samples from the head of the
    // composite.
    for (int si = 0; si < pulse->samples_per_baud; si++) {
      cache->bursts[pattern].samples[si].float_format = composite[si];
    }
    did ++;
    if (pattern == 0) {
      pattern = 1;
      iterations = 1;
    } else {
      int32_t insert = 1 & ((pattern >> 7) ^ (pattern >> 5) ^ (pattern >> 4) ^ (pattern >> 3));
      pattern = ((pattern << 1) & 0xFE) | insert;
      if (pattern == 1) {
	complete = true;
      }
    }
  }
  // Rescale all of the sample bursts to within (-1.0, 1.0),
  // leaving a bit of headroom just to be safe.
  waveform_peak += .01f;
  for (int pattern = 0; pattern < 256; pattern++) {
    for (int si = 0; si < pulse->samples_per_baud; si++) {
      cache->bursts[pattern].samples[si].float_format /= waveform_peak;
    }
  }
  cache->bursts_are_in_float_format = true;
  // Keep track of the global configuration from which we built
  // the cache.
  cache->pulse_shape = tnc_state.pulse_shape;
  cache->txeq_mode = tnc_state.txeq_mode;
  cache->txeq_peak_frequency = tnc_state.txeq_peak_frequency;
  cache->txeq_peak_gain_centibels = tnc_state.txeq_peak_gain_centibels;
  cache->valid = true;
  LOG_DBG("Did %d values", did);
  k_mutex_unlock(&work_area_mutex);
}

void common_configure_modulator(void) {
  memset(&G3RUH_modulator_state, 0, sizeof G3RUH_modulator_state);
  const struct pulse *chosen_pulse;
  LOG_DBG("Pulse shape is %d", tnc_state.pulse_shape);
  switch (tnc_state.pulse_shape) {
  case PULSE_SINC:
  default:
    LOG_DBG("Chose sinc");
    chosen_pulse = &sinc;
    break;
  case PULSE_RAISED_COSINE_050:
    LOG_DBG("Chose raised-cosine 50");
    chosen_pulse = &raised_cosine_50;
    break;
  case PULSE_RAISED_COSINE_090:
    LOG_DBG("Chose raised-cosine 90");
    chosen_pulse = &raised_cosine_90;
    break;
  case PULSE_RAISED_COSINE_095:
    LOG_DBG("Chose raised-cosine 95");
    chosen_pulse = &raised_cosine_95;
    break;
  }
  setup_pulse_modulator(&G3RUH_modulator_state, &G3RUH_modulator_cache, chosen_pulse);
  board_configure_modulator(&G3RUH_modulator_state, &G3RUH_modulator_cache);
}

void adjust_buffer_size(int32_t samples_per_second,
			int32_t bytes_per_sample,
			size_t *buffer_size,
			int32_t *ring_bits,
			int32_t *hz) {
  int32_t bytes_per_second = samples_per_second * bytes_per_sample;
  int32_t byte_limit = bytes_per_second / CONFIG_BUFFER_COMPLETION_RATE;
  while (*buffer_size > byte_limit) {
    *buffer_size /= 2;
    if (ring_bits) {
      (*ring_bits) --;
    }
  }
  if (hz) {
    *hz = (bytes_per_second + *buffer_size / 2) / *buffer_size;
  }
}
