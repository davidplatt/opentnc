/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/crc.h>
#include <zephyr/sys/ring_buffer.h>

#include <zephyr/logging/log.h>

#include "ax25buffer.h"
#include "board.h"
#include "demod.h"
#include "globals.h"
#include "hdlc.h"
#include "kiss.h"
#include "packetutil.h"

LOG_MODULE_REGISTER(posix, LOG_LEVEL_INF);

#define STACK_SIZE	4096
static struct k_thread blink_thread;
static K_THREAD_STACK_ARRAY_DEFINE(tstack, 1, STACK_SIZE);

#define TRANSCEIVER_STACK_SIZE	4096
static struct k_thread transceiver_thread;
static K_THREAD_STACK_ARRAY_DEFINE(transceiver_stack, 1, TRANSCEIVER_STACK_SIZE);

#define UART_STACK_SIZE	4096
static struct k_thread uart_thread;
static K_THREAD_STACK_ARRAY_DEFINE(uart_stack, 1, UART_STACK_SIZE);

#define MODULATOR_STACK_SIZE	4096
static struct k_thread modulator_thread;
static K_THREAD_STACK_ARRAY_DEFINE(modulator_stack, 1, MODULATOR_STACK_SIZE);

const struct device *uart1 = DEVICE_DT_GET(DT_NODELABEL(uart1));

static bool tx_enabled = false;
static bool flush_loopback = false;

// Semaphore which is given when transmission of a KISS
// packet to the host should be started.
K_SEM_DEFINE(kiss_pusher_sem, 0, 1);

void board_enable_ptt(void){
  LOG_DBG("Enabling PTT");
}

void board_disable_ptt(void){
  LOG_DBG("Disabling PTT");
}

void board_set_hdlc(uint32_t bit) {
  board_set_tx_freq(bit ? HIGH_TONE : LOW_TONE);
}

void board_set_tx_freq(uint32_t hz) {
  cycles_per_sine_step = sys_clock_hw_cycles_per_sec() / hz / sinetable_count;
}

void board_push_to_radio(void) {
  generic_push_to_radio();
}

void board_kick_host_tx(void) {
  k_sem_give(&kiss_pusher_sem);
}

static void handle_samples(int16_t *samples, uint32_t count) {
  static int32_t limit = 0x1000;
  static int headroom = 0x100;
  static int32_t mean = 0x800;
  int i, j;
  for (i = 0, j = 0; i < count; i += OVERSAMPLING, j ++) {
#if OVERSAMPLING == 1
    int16_t in = samples[i];
#elif OVERSAMPLING == 2
    int16_t in = (samples[i] + samples[i+1]) / 2;
#elif OVERSAMPLING == 4
    int16_t in = (samples[i] + samples[i+1] + samples[i+2] +
		  samples[i+3]) / 4;
#else
#error Bad oversampling ratio
#endif
    samples[j] = in - mean;
    mean = ((mean * 255) + in) >> 8;
    /* Report near-clipping */
    if (in < headroom || in > limit - headroom) {
      // we would do something such as gpio_pin_set_dt(&led6, 0);
    }
  }
  Demodulate(samples, j);
  samples_processed += j;
}

#define SIM_SAMPLES 256
void board_transceiver(void *arg1, void *arg2, void *arg3) {
  LOG_INF("Started simulation transceiver");
  FILE *simulation = (FILE *) arg1;
  int16_t samples[SIM_SAMPLES];
  //  InitDemodulator();
  while (1) {
    static int iterations = 0;
    if (iterations++ == 1000) {
      //      k_sleep(K_MSEC(10));
      iterations = 0;
    }
    while (ax25buffer_congested(HDLC_MAX_PACKET_LENGTH)) {
      static bool notice = false;
      if (!notice) {
	LOG_INF("Got a packet, waiting for buffer space");
	notice = true;
      }
      k_sleep(K_MSEC(10));
    }
    size_t got = fread(samples, sizeof(samples[0]), SIM_SAMPLES, simulation);
    if (got == 0) {
      break;
    }
    // Incoming samples are assumed to be in S16 format at 38400 samples/second
    // and are scaled to full-range. Convert them to unsigned 12-bit format as
    // biased appropriately, as that's what the chip expects.
    for (int i = i; i < SIM_SAMPLES; i++) {
      samples[i] = 0x0800 + (((uint16_t) samples[i]) >> 4);
    }
    handle_samples(samples, got);
    LOG_DBG("Feeding samples");
  }
  LOG_INF("Simulation transceiver exits, end of data after %d samples", samples_processed);
}

void board_heartbeat(void *arg1, void *arg2, void *arg3)
{
  uint32_t time_then = k_uptime_get();
  uint64_t old_samples_processed = samples_processed;
  LOG_INF("Started board heartbeat thread");
  while (1) {
    k_msleep(SLEEP_TIME_SHORT);
    uint32_t time_now = k_uptime_get();
    uint64_t samples_now = samples_processed;
    time_then = time_now;
    old_samples_processed = samples_now;
  }
}

void posix_uart_thread(void *arg1, void *arg2, void *arg3)
{
  LOG_INF("POSIX uart push/pull thread started");
  const struct device *uart = (const struct device *)arg1;
  static uint8_t tx_buffer[8];
  uint8_t rx_buffer;

  // We must operate in polled mode, as the POSIX ptty uart doesn't
  // support interrupts.  We'll wake up when there's KISS output
  // to be pushed to the host, or every 20 milliseconds to check
  // for new incoming input KISS.
  while (true) {
    k_sem_take(&kiss_pusher_sem, K_MSEC(20));
    /* Here we receive KISS/SLIP frames from the pty "UART" and
     * submit them to the KISS decoder.
     */
    while (uart_poll_in(uart, &rx_buffer) == 0) {
      LOG_DBG("'%c' 0x%2x", rx_buffer, rx_buffer);
      kiss_decode_bytes_from_host(&rx_buffer, 1);
    }
    /* Here we ask the KISS encoder for framed/encoded packet data,
     * and push it out to the CDC "UART" in efficient chunks.  The
     * write call will block for as long as the UART is full.  This
     * will block processing of incoming KISS packets but this should
     * not be a problem in practice, I think (if it is I'll have to split
     * this into two threads).
     */
    int tx_avail = kiss_encode_bytes_to_host(tx_buffer, sizeof(tx_buffer));
    if (tx_avail > 0) {
      for (int i = 0; i < tx_avail; i++) {
	uart_poll_out(uart, tx_buffer[i]);
      }
      k_sem_give(&kiss_pusher_sem);
    }
  }
}

/* Modulator-push thread.  This is essentially a simulation of
 * what's done in the ISR2 timer routine in the STM32F103 board.
 * It pretends that it's running at 8x the bit rate (times
 * the oversampling rate).  arg1, if not NULL, points to a
 * FILE to which the sample values should be written.
 */
void posix_modulator_thread(void *arg1, void *arg2, void *arg3)
{
  FILE *target = (FILE *) arg1;
  static int32_t firings;
  bool flushed = true;
  LOG_INF("POSIX modulator thread started, oversampling %d", OVERSAMPLING);
  while (true) {
    while (!HDLC_Output_Avail()) {
      if (target && !flushed) {
	fflush(target);
	flushed = true;
      }
      if (tnc_state.adc_loopback && flush_loopback) {
	int16_t adc_zeros[SIM_SAMPLES];
	LOG_INF("Flushing sample loopback");
	flush_loopback = false;
	memset(adc_zeros, 0, sizeof(adc_zeros));
	for (int i = 0; i < 1024; i++) {
	  handle_samples(adc_zeros, SIM_SAMPLES);
	}
      }
      k_sleep(K_MSEC(20));
    }
    ++firings;
    /* TODO - higher-level code should set the firings modulus, so that
     * we can handle 9600 bit/second transmission as well.
     */
    if (firings % (8 * OVERSAMPLING) == 0) {
      if (HDLC_Output_Avail()) {
	bool data_bit = HDLC_Get_Bit();
	board_set_hdlc(data_bit);
	if (!tnc_state.adc_loopback) {
	  set_data_leds(data_bit);
	}
	if (tnc_state.hdlc_loopback) {
	  ConsumeNRZI(&hdlc_context_flat, data_bit);
	}
      } else {
	LOG_INF("Modulator underrun");
      }
    }
    /* Figure out the next sample value to output.  For now, fake
     * up the timing somewhat crudely.
     */
    static int offset = 0;
    uint32_t cycle_delta = sys_clock_hw_cycles_per_sec() / (1200 * 8 * OVERSAMPLING);
    uint32_t steps_to_increment = cycle_delta / cycles_per_sine_step;
    offset = (offset + steps_to_increment) % sinetable_count;
    int16_t sample = sinetable[offset];
    /* If we're writing to a file, do so.  These will be 16-bit
       host-byte-order unsigned integers (.u16, in "sox" terms).
    */
    if (target) {
      fwrite(&sample, sizeof(sample), 1, target);
      flushed = false;
    }
    /* In ADC loopback mode, feed the PWM sample back into the input
     * sampler, as if it had just been digitized by the ADC.
     */
    if (tnc_state.adc_loopback) {
      static int adc_loopback_index = 0;
      int16_t adc_loopback_samples[SIM_SAMPLES];
      adc_loopback_samples[adc_loopback_index] = sample + 0x1000; // offset to create unsigned
      adc_loopback_index++;
      if (adc_loopback_index == SIM_SAMPLES) {
	handle_samples(adc_loopback_samples, SIM_SAMPLES);
	adc_loopback_index = 0;
      }
    }
  }
}

void board_init(void)
{
	LOG_INF("In Posix board_init()");

	LOG_INF("Start heartbeat");

	k_thread_create(&blink_thread,
	  tstack[0], STACK_SIZE,
	  (k_thread_entry_t)board_heartbeat,
	  NULL, NULL, NULL,
	  K_PRIO_PREEMPT(10), 0, K_NO_WAIT);

	LOG_INF("Checking for uart1");

	if (device_is_ready(uart1)) {
	  LOG_INF("uart1 is ready, starting KISS I/O thread");
	  k_thread_create(&uart_thread,
			  uart_stack[0], UART_STACK_SIZE,
			  (k_thread_entry_t)posix_uart_thread,
			  (void *) uart1, NULL, NULL,
			  K_PRIO_PREEMPT(10), 0, K_NO_WAIT);
	} else {
	  LOG_INF("uart1 is not ready");
	}

	FILE *simulation = NULL;
	char *path = getenv("AUDIOIN");

	if (path) {
	  simulation = fopen(path, "rb");
	}

	if (simulation) {
	  k_thread_create(&transceiver_thread,
			  transceiver_stack[0], TRANSCEIVER_STACK_SIZE,
			  (k_thread_entry_t)board_transceiver,
			  (void *) simulation, NULL, NULL,
			  K_PRIO_COOP(1), 0, K_NO_WAIT);
	};

	FILE *target = NULL;
	path = getenv("AUDIOOUT");

	if (path) {
	  target = fopen(path, "wb");
	}

	/* This is a bit funky but OK for now */

	if (!simulation) {
	  LOG_INF("Enabling pseudo-analog loopback");
	  tnc_state.adc_loopback = true;
	}

	k_thread_create(&modulator_thread,
			modulator_stack[0], MODULATOR_STACK_SIZE,
			(k_thread_entry_t)posix_modulator_thread,
			(void *) target, NULL, NULL,
			K_PRIO_COOP(1), 0, K_NO_WAIT);

	LOG_INF("Enabling KISS mode");

	tnc_state.kiss_mode = true;
	
	LOG_INF("Running...");
}

uint32_t board_get_random32(void) {
  return random();
}

void board_enable_rx(void) {
  LOG_INF("Enabling RX");
}

void board_disable_rx(void){
  LOG_INF("Disabling RX");
}

void board_enable_tx(void){
  LOG_INF("Enabling TX");
  board_set_tx_freq(1200);
  cycles_before = k_cycle_get_32();
  tx_enabled = true;
}

void board_disable_tx(void){
  LOG_INF("Disabling TX");
  tx_enabled = false;
  flush_loopback = true;
}

bool board_load_saved_parameters(void) {
  return false;
}

bool board_save_parameters(void) {
  return false;
}
