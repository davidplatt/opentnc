/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

void ConsumeAndPrintPackets(void);
void DecodePacketFromBuffer(char prefix, ax25buffer *b, char *line, size_t line_size,
			    bool uses_extended_sequence);
void LogPacket(char prefix, ax25buffer *b);
