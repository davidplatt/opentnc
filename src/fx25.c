/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stdio.h>
#include <string.h>
#include <zephyr/kernel.h>

#include <zephyr/logging/log.h>

#include "board.h"
#include "fx25.h"
#include "globals.h"

#include "correct.h"

LOG_MODULE_REGISTER(fx25, LOG_LEVEL_INF);

// Facts gleaned from looking at Dire Wolf:
//
// John used KA9Q's GPL'ed code
// John looked at the behavior of the UZ7HO soundmodem
// Polynomial is 0x11D
// Index of root polynomial is 1
// Index of primitive element is 1
// Number of roots is 16, 32, or 64
// Accepts no more than 8 mismatch bits in the correlation tag
// Allows manual specification of number of check bits
// Allows auto specification - longer frames get more bits.

const struct fx25_code fx25_codes[] =
  {
    { .correlation_tag = 0xB74DB7DF8A532F3ELL,
      .codeblock_bytes = 255, .data_bytes = 239 },
    { .correlation_tag = 0x26FF60A600CC8FDELL,
      .codeblock_bytes = 144, .data_bytes = 128 },
    { .correlation_tag = 0xC7DC0508F3D9B09ELL,
      .codeblock_bytes =  80, .data_bytes =  64 },
    { .correlation_tag = 0x8F056EB4369660EELL,
      .codeblock_bytes =  48, .data_bytes =  32 },
    { .correlation_tag = 0x6E260B1AC5835FAELL,
      .codeblock_bytes = 255, .data_bytes = 223 },
    { .correlation_tag = 0xFF94DC634F1CFF4ELL,
      .codeblock_bytes = 160, .data_bytes = 128 },
    { .correlation_tag = 0x1EB7B9CDBC09C00ELL,
      .codeblock_bytes =  96, .data_bytes =  64 },
    { .correlation_tag = 0xDBF869BD2DBB1776LL,
      .codeblock_bytes =  64, .data_bytes =  32 },
    { .correlation_tag = 0x3ADB0C13DEAE2836LL,
      .codeblock_bytes = 255, .data_bytes = 191 },
    { .correlation_tag = 0xAB69DB6A543188D6LL,
      .codeblock_bytes = 192, .data_bytes = 128 },
    { .correlation_tag = 0x4A4ABEC4A724B796LL,
      .codeblock_bytes = 128, .data_bytes = 64 },
  };

const int fx25_code_count = sizeof(fx25_codes) / sizeof(fx25_codes[0]);

const uint8_t mismatching_bits[256] =
  { 0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3,
    4, 2, 3, 3, 4, 3, 4, 4, 5, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4,
    4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 1, 2, 2, 3, 2,
    3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5,
    4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4,
    5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3,
    3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2,
    3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6,
    4, 5, 5, 6, 5, 6, 6, 7, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5,
    6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 3, 4, 4, 5, 4, 5,
    5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6,
    7, 7, 8 };

ssize_t fx25_decode(int correlation_tag_index, uint8_t *input, uint8_t output[255]) {
  int32_t num_roots = fx25_codes[correlation_tag_index].codeblock_bytes -
    fx25_codes[correlation_tag_index].data_bytes;
  uint8_t temp[255];
  uint8_t corrected[255];
  memset(temp, 0, sizeof(temp));
  memcpy(temp, input, fx25_codes[correlation_tag_index].data_bytes);
  memcpy(temp + 255 - num_roots, input + fx25_codes[correlation_tag_index].data_bytes,
	 num_roots);
  LOG_DBG("Initializing a Reed-Solomon corrector with %d roots, %d data bytes", num_roots,
	  fx25_codes[correlation_tag_index].data_bytes);
  correct_reed_solomon *rs =
    correct_reed_solomon_create(correct_rs_primitive_polynomial_8_4_3_2_0,
				1, 1, num_roots);
  if (!rs) {
    LOG_ERR("Can't allocate Reed-Solomon corrector");
    return -1;
  }
  ssize_t result = correct_reed_solomon_decode(rs, temp, sizeof(temp), corrected);
  LOG_DBG("correct_reed_solomon returned %d", result);
  correct_reed_solomon_destroy(rs);
  LOG_DBG(" Destroyed");
  if (memcmp(temp, corrected, fx25_codes[correlation_tag_index].data_bytes) != 0) {
    tnc_state.stats.fx25_codeblocks_corrected ++;
    for (int i = 0; i < fx25_codes[correlation_tag_index].data_bytes; i++) {
      if (corrected[i] != temp[i]) {
	tnc_state.stats.fx25_samples_corrected ++;
      }
    }
  }
  memset(output, 0x7E, 255);
  memcpy(output, corrected, fx25_codes[correlation_tag_index].data_bytes);
  return result;
}

int fx25_select_encoding(size_t data_size, enum fx25_encoding fx25_encoding) {
  int number_of_roots;
  switch (fx25_encoding) {
  case FX25_ENCODING_OFF:
    return -1;
  case FX25_ENCODING_AUTO:
    if (data_size > 128) {
      number_of_roots = 32;
    } else {
      number_of_roots = 16;
    }
    break;
  case FX25_ENCODING_MIN:
    number_of_roots = 16;
    break;
  case FX25_ENCODING_MAX:
    number_of_roots = 64;
    break;
  default:
    number_of_roots = 16;
    break;
  }
  int chosen_encoding = -1;
  // This search loop depends on the fact that for any given number
  // of roots, the encodings in the table appear with the largest
  // data-block size first.  The code will find the smallest encoding
  // which will fit the data and has the desired number of roots.
  for (int i = 0; i < fx25_code_count; i++) {
    if (fx25_codes[i].data_bytes >= data_size &&
	fx25_codes[i].codeblock_bytes - fx25_codes[i].data_bytes ==
	number_of_roots) {
      chosen_encoding = i;
    }
  }
  return chosen_encoding;
}

ssize_t fx25_encode(int correlation_tag_index, uint8_t *message, size_t msglen) {
  if (msglen != fx25_codes[correlation_tag_index].data_bytes) {
    LOG_ERR("Encode msglen %d bytes, expected %d bytes", msglen,
	    fx25_codes[correlation_tag_index].data_bytes);
    return -1;
  }
  int32_t num_roots = fx25_codes[correlation_tag_index].codeblock_bytes -
    fx25_codes[correlation_tag_index].data_bytes;
  LOG_DBG("Initializing a Reed-Solomon encoder with %d roots", num_roots);
  correct_reed_solomon *rs =
    correct_reed_solomon_create(correct_rs_primitive_polynomial_8_4_3_2_0,
				1, 1, num_roots);
  if (!rs) {
    LOG_ERR("Can't allocate Reed-Solomon encoder");
    return -1;
  }
  uint8_t input[255];
  uint8_t output[255];
  memset(input, 0, sizeof(input));
  memcpy(input, message, msglen);
  ssize_t result =
    correct_reed_solomon_encode(rs, input, sizeof(input) - num_roots, output);
  LOG_DBG("encode_reed_solomon returned %d", result);
  correct_reed_solomon_destroy(rs);
  LOG_DBG(" Destroyed");
  // libcorrect always returns 255, as that's the block length of the
  // basic Reed-Solomon bytewide algorithm.
  if (result != 255) {
    return result;
  }
  //  hexdump(input, "Reed-Solomon encode input", sizeof(input));
  //  hexdump(output, "Reed-Solomon encode input", sizeof(output));
  // The output[] array contains msglen worth of encoded bytes, followed
  // by num_roots bytes of parity.  Write the parity bytes back to the
  // input area.
  LOG_DBG("Copy %d bytes from output offset %d to message offset %d",
	  num_roots, fx25_codes[correlation_tag_index].data_bytes,
	  msglen);
  memcpy(message + msglen, &output[255 - num_roots], num_roots);
  return fx25_codes[correlation_tag_index].codeblock_bytes;
}
