/*
 * Copyright (c) 2023-2024, David C. Platt
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#ifndef _H_MOD
# define _H_MOD

#include "board.h"
#include "hdlc.h"
#include "pulses.h"

// These definitions control the size of the encoded-waveform tables.
// Pulse waveforms in pulses.c must not exceed these limits.

#define G3RUH_SAMPLES_PER_BAUD 16
#define G3RUH_SPAN_IN_BAUDS 8

// sample_burst holds one baud's worth of waveform samples, encoded
// in a board-specific fashion.  There's enough room for four bytes
// per sample. The specific layout of the data in the sample_burst
// is entirely up to the board/chipset... it may be PWM, or
// delta-sigma, or something else.  The modulator-setup code outputs
// the data as floats, scaled to within (-1,1).  Board-specific code
// should rescale and reformat as required for its hardware.

struct sample_burst {
  union {
    uint32_t int_format;
    float float_format;
  } samples[G3RUH_SAMPLES_PER_BAUD];
};

// Private state for a modulator.  Currently optimized
// for the G3RUH modulator.
struct modulator_state {
  // G3RUH transmit scrambler.
  uint32_t scrambler;
  // The pattern of bits we've transmitted.  By convention,
  // new bits are shifted onto the LSB, old ones fall off of
  // the MSB.  The low bits in this value are used as an index
  // into the bursts array.
  uint32_t transmit_bit_history;
  // The mask of meaningful bits in the history.
  uint32_t transmit_bit_mask;
};

// Modulator data which can be saved across multiple transmissions,
// if the configuration data from which it was derived has not
// changed.
struct modulator_cache {
  // Is it valid at all?
  bool valid;
  // Configuration parameters used to calculate the G3RUH pulse, taken
  // from struct tnc_state.
  int32_t pulse_shape;
  int32_t txeq_mode;
  int32_t txeq_peak_frequency;
  int32_t txeq_peak_gain_centibels;
  // How many samples are there, actually, per baud, in the
  // current waveform bursts?  This may be less than the limit,
  // depending on which pulse was used.
  uint32_t samples_per_baud;
  // Format indicator for the encoded sample bursts.  Board-
  // independent modulator setup code outputs the bursts in
  // single-precision float format.  Board-specific code will
  // translate to whatever the chipset hardware needs.
  bool bursts_are_in_float_format;
  // Encoded sample bursts.  There's one encoded burst in this
  // array for each pattern corresponding to the most recently
  // transmitted set of bits - by convention the most recently
  // transmitted bit corresponds to the LSB.
  struct sample_burst bursts[1 << G3RUH_SPAN_IN_BAUDS];
};

extern struct modulator_state G3RUH_modulator_state;
extern struct modulator_cache G3RUH_modulator_cache;

extern const struct pulse sinc;
extern const struct pulse raised_cosine_20;
extern const struct pulse raised_cosine_50;
extern const struct pulse raised_cosine_90;
extern const struct pulse raised_cosine_95;

// Utility routine(s) - maybe move these to a different #include
// since they aren't necessarily tied to the modulator

// Given a large buffer (must be a power-of-two number of
// bytes) and information about the required sample rate
// and sample size, reduce the buffer size by factors of 2
// until it's just small enough for a single data transfer
// to fall within the configuration-specified latency limit.
// Will also decrease the ring_bits value by 1 for each
// reduction-by-2, if a ring_bits pointer is provided.

void adjust_buffer_size(int32_t samples_per_second,
			int32_t bytes_per_sample,
			size_t *buffer_size,
			int32_t *ring_bits,
			int32_t *hz);

#endif

