# Copyright (c) 2018, Linaro Limited
# Copyright (c) 2023, David Platt
# SPDX-License-Identifier: Apache-2.0

description: |
  This allows you to define a group of virtual LEDs. Each
  LED in the group can drive an (optional) GPIO, and can provide an
  optional set of mappings from the LED "virtual value" to a WS2812
  R/G/B tuple. Each LED is defined in a child node of the virtual-leds
  node.  VLEDs can also have a period and a hang-time, both measured
  in ticks (roughly 1/60 Hz).  A VLED with a non-zero period "N" will be
  set to the value 1 every "N" ticks.    A VLED with a non-zero hang
  time "H" will be automatically reset to zero "H" ticks after it was
  set.

  Here is an example which defines three LEDs in the node /leds:

  / {
        vleds {
                compatible = "vnd,virtual-leds";
                led_0 {
                        gpios = <&gpio0 1 GPIO_ACTIVE_LOW>;
                        ws2812-mappings = <1 0 255 0 0>;
                };
                led_1 {
                        ws2812-mappings = <1 0 0 255 0>;
                };
                led_2 {
                        gpios = <&gpio1 15 (GPIO_PULL_UP | GPIO_ACTIVE_LOW)>;
                };
                led_3 {
                        ws2812-mappings = <-1 2 0 6 5>;
                };
                led_4 {
                        ws2812-mappings = <1 3 128 128 0>, <2 4 64 0 64>;
                };
        };
  };

  Above:

  - led_0 is pin 1 on gpio0. The LED is on when the pin is low,
    and off when the pin is high.  When the VLED is on (value 1)
    it sets WS2812 #0 to a value of R=255 G=0 B=0
  - led_1 has no GPIO connection.  When the VLED is on (value 1)
    it sets WS2812 #0 to a value of R=0 G=255 B=0
  - led_2 is pin 15 on gpio1. The LED is on when the pin is low,
    and the pin's internal pull-up resistor should be enabled.
    This VLED does not affect any WS2812.
  - led_3 has no GPIO connection.  Whenever the VLED is on (value
    greater than zero) WS2812 #1 is set to a blue-green color, with
    the intensitity increasing linearly with the value of the VLED.
  - led_4 has no GPIO connection.  When the VLED has value 1, WS2812
    #3 will be set to a yellowish color;  when the VLED has value 2,
    WS2812 #4 will be set to a purplish color.

compatible: "virtual-leds"

child-binding:
  description: GPIO LED child node
  properties:
    struct-field:
      type: string
      required: true
      description: |
        Used to construct the name of the VLED data-storage field
        in the driver, and to create the name of the corresponding
        "set" function.  E.g. struct-field = "foo_bar" will result
        in the creation of a set_foo_bar_led() function.
    gpios:
      type: phandle-array
      required: false
      description: |
        If specified, it binds the VLED to a specific GPIO pin and
        output flag.  Two or more VLEDs may be bound to the same
        pin, as long as the output flags are consistent;  the
        results are not guaranteed to be visually consistent since
        the most recent VLED updated generally controls the state
        of the pin.
    hang-time:
      type: int
      required: false
      description: |
        If specified and non-zero, the hang-time specifies the number
        of 60-Hz ticks through which a VLED's non-zero value should
        be automatically retained;  the VLED will be reset to zero
        after this many ticks.
    period:
      type: int
      required: false
      description: |
        If specified and non-zero, the period indicates that the VLED
        should be periodically be set to a value of 1 - this occurs
        approximately every "period" 60-Hz ticks.
    ws2812-mappings:
      type: array
      required: false
      description: |
        Consists of an array of 5-tuples of integers, each being
        <match lamp red green blue>.  "match" is a value to match
        against the LED's virtual value, "lamp" is the index number
        of a WS2812 in the display string, and "red" "green" and "blue"
        are values in the range of 0-255.  If "match" is between 0 and
        255, it's compared against the value of the virtual LED, and if
        equal, the WS2812 lamp's color components are incremented by the
        specified "red" "green" and "blue" values (clipping at 255).  If
        "match" is less than 0, it's a match-all, and the WS2812's color
        components are incremented by specified "red" "green" and "blue"
        values multiplied by the virtual LED's value.  The first match
        wins.  Multiple VLEDs can be mapped to the same WS2812 - the
        intensities add linearly.
    label:
      type: string
      description: |
        Human readable string describing the LED. It can be used by an
        application to identify this LED or to retrieve its number/index
        (i.e. child node number) on the parent device.
