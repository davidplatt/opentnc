<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## Some technical details

The OpenTNC is designed to operate with the squelch open.  It "detects
carrier" when it successfully locks onto the HDLC bit-stream from a
demodulated signal - this lights up the "carrier" LED.  It won't
attempt to transmit when it's detecting HDLC carrier.  It can also be
configured to operate with the squelch closed, inhibiting transmission
based on either the presence of audio, or on the "RF detected,
squelch is open" signal that some radios can provide.

Another LED flashes briefly when a good packet is decoded.

The analog signal pathways are straightforward.  The incoming signal
simply goes through an attenuator potentiometer and an AC-coupling
capacitor, is biased up to half of the board's 3.3-volt rail, and fed
through a GPIO pin to the analog-to-digital converter.  The signal is
sampled 38400 times/second, filtered down to 9600 samples/second, and
then fed to one of several different demodulator algoriths.  Not a lot
of signal is required - I see good, solid 1-versus-0 discrimination
with as little as 30 millivolts peak-to-peak, and up to about 2 volts
peak-to-peak is OK (the "clip" LED will flash if the level is too
high).  The default receive algorithm (demodulator 4) is based on a
chain of fixed- or floating-point biquad bandpass filters, and has
been the most effective one I've tried out so far. [Actually, it's the
most effective _three_ I've found.  It consists of three complete sets
of filters with different equalization characteristics, and three
independent HDLC decoders running in parallel.  Kinda crazy but it
really does seem to work well!]

The transmit pathway is a bit more ornate.  Neither the STM32F411 nor
the RP2040 includes a digital-to-analog converter section, and I
didn't want to require the use of an "outboard" DAC chip, so it's
necessary to simulate one with a digital output and a low-pass
reconstruction filter.

The filter creates a transmission-quality analog sine wave from the
output at a digital GPIO pin being switched at a high frequency
(either pulse-width or pulse-density modulation).  The filter I've
chosen to use is a four-pole RCLCRC design, with -3 dB points in the
12 kHz - 15 kHz range. I've optimized the design for the v0.5 Pico
board using SPICE, giving a nice clean roll-off.  I don't claim that
this filter design is optimal; feel free to experiment.  The signal
then goes through a DC-blocking capacitor, and a potentiometer to set
the output level to what the transmitter you're using requires.

There are two different modulators available to create the digital
output signal.  Modulator 0 uses fast pulse-width modulation, based on
one of the chip's internal timers/counters.  The timer creates a PWM
waveform having a period of around 500 kHz, and a duty cycle which
varies along with the desired sine-wave amplitude.  Modulator 1 uses
delta-sigma modulation with second-order noise shaping, emitting a
precomputed bit stream at between 1.5 MHz and 2.5 MHz (depending on
the board) using the chip's SPI or PIO peripheral module.  By good
fortune, both of these outputs can be routed to the same pin, so only
a single set of low-pass circuitry is needed to reconstruct a decent
sine wave from the chosen modulator.

## AX.25 support

The OpenTNC supports the simple KA9Q "KISS" mode of operation
(including support for SMACK checksums on the KISS packets). This
allows you to use the OpenTNC with AGWPE or Dire Wolf or the Linux
kernel's AX.25 stack, if you should so desire.

A more traditional interactive "connected mode" is also supported,
using the OpenTNC's own (new) implementation of this protocol layer.
I've tested this software for interoperability with a couple of
traditional TNCs (an MFJ-1270 clone of the TNC-2, and a Kantronics
KPC-3), with the Linux kernel AX.25 stack (using the KPC-3 as a KISS
interface), and over the air with the JNOS BBS, all with good results.
I don't claim that this layer is completely conformant or perfectly
implemented but it seems to work well in practice.

The OpenTNC has the ability to randomly damage HDLC frames during
transmission and/or reception (corrupting the FCS so that the frame
fails to validate).  This allows testing to simulate frame loss due to
noise, interference, or on-air packet collisions, and permits exercising
of the error-recovery ability of the higher layers in the stack.  The
```droprx``` and ```droptx``` commands can be used for this purpose,
e.g. ```droprx 7``` will corrupt and drop roughly 1 incoming frame
out of every 7.

I've successfully performed some long file transfers between OpenTNC
and the other TNCs, with "damage ratios" as high as 1-in-4 (one way
at a time) and 1-in-6 (with both transmitted and received frames being
vulnerable to damage).  The OpenTNC was able to maintain the connection
even in the face of such high frame loss rates - there were a lot of
REJ frames and retransmissions, but the data did get through.

Both KISS and connected-mode operation utilize P-persistence when
transmitting.  I have not implemented the older-style "fixed
transmission delay" approach.

Data flow control is implemented in both directions.  The TNC
will stall the USB "serial port" interface if you try to send
data faster than the peer can keep up - this has the same effect
as traditional "hardware flow control" on an RS-232 serial port.
Similarly, if your host software fails to read incoming data from
the USB "serial port", the TNCs internal buffers will fill up
and the TNC will start sending RNR to the peer until the host
catches up with the data.  RNR can be forced during testing...
```RNR 64``` will stall roughly every fourth set of incoming
packets for a few seconds, ```RNR 128``` will stall roughly every
other set of packets for a longer period of time, and ```RNR 0```
eliminates the forced stalls.

## Receive sensitivity

When using the WA8LMF TNC Test CD track 2 data, I've measured the
STM32F411 version of OpenTNC as correctly decoding as many as 970
frames.  The RP2040-based Raspberry Pi Pico boards aren't yet quite as
good, delivering anywhere from 920 to 950 frames.  I suspect that the
difference is the quality of the chips' analog-to-digital converters -
the RP2040 converter suffers from some substantial nonlinearities.
Also, the Pico has a very noisy switching power regulator on board,
and its noise hashes up the received signal somewhat.  I hope for
better results with the latest OpenTNC-Pico board, which adds a
lower-noise voltage regulator.

</BODY>
</HTML>
