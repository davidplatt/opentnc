<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## Features

- Full “connected mode” keyboard-to-keyboard communication.  Inbound
and outbound CONNECT/CONVERSE, UNPROTO round-robin, and MONITOR
modes are all supported.

- KISS mode is supported, for use with host-PC networking utilities
(e.g. APRS, xastir, JNOS, Linux AX.25 networking, etc.).

- Works with any serial-port terminal emulator (e.g. minicom or screen
on Linux, TeraTerm or PuTTY on Windows, etc.).  It does not require
installation of specialized AX.25 networking software on the PC.

- USB-connected, USB-powered.  It appears as a “virtual COM port”
using a class-standard USB interface, and does nor require
installation of additional drivers on Linux, MacOS, or recent
Windows systems.  No separate USB-to-serial-port adapter, or “wall
wart” power supply or power cord is needed.

- Supports FX.25 error correction, for improved data-transmission performance when noise is present.

- Command interface mimics the TNC-2, and will be familiar to users of MFJ and Kantronics TNCs.

- Connects to the radio using standard cables – both the
TNC-2/MFJ-style connector (5-pin DIN) and the Kantronics-style
connector (9-pin DB/DE) can be installed on the board.  TNC-to-radio
cables using these connectors are available from multiple on-line
sources.

- Tested successfully over the air with MFJ-1270 and Kantronics KPC-3
TNCs, and with the Dire Wolf “sound modem” software TNC.

- Good receive performance.  My latest build (based on the 0.5 printed
circuit board) decoded an average of 941 packets per run from the
WA8LMF TNC Test CD track 2 APRS data capture.

- Open-source hardware design.  Full schematics, board layout, and
ready-to-manufacture PC-board Gerber files are available under a
Creative Commons license.

- Open-source firmware.  The source code, and ready-to-use binary
images are available under the GNU Public License.

- Components are common, easily available, and inexpensive.  Total
component cost is around $30 (not including case or shipping).

- No surface-mount soldering equipment required – everything
is “through-hole” and can be hand-soldered.

</BODY>
</HTML>
