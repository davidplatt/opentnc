<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## Porting to other boards or processors

The code is currently running on

- a WeAct Black Pill 3.2 board, which uses an STM32F411 Cortex M4 processor running at 96 MHz.

- the Raspberry Pi Pico, which uses an RP2040 Cortex M0+ running at
125 MHz.  (The chip's higher clock speed is more than offset by its
simpler execution pipeline, lack of floating point, less-functional
integer multiplier, and lack of internal full-speed flash memory for
code).

- POSIX native on Linux (partial functionality)

Most of the higher-level code (console/converse, Layer 3 and
connection, and demodulation) should be quite portable to
different boards and/or processors, as most board-specific
details have been abstracted out.  The signal baud rate, tones,
and the audio sampling rate (38400 samples/second) would require
some effort to change.

Most GPIO accesses have been abstracted through the Zephyr
device tree and GPIO APIs, and the board-specific Zephyr device tree
overlay.  GPIOs can be reassigned by changing the overlay.

Other boards with these processors should work but you may need to
write a new board-definition overlay to deal with GPIO pinout
differences.  Other processors may work but you may need to clone and
modify the processor-specific board-driver file to account for
hardware differences. At minimum, you'll probably need a processor
which has at least 128k of flash, at least 64k of RAM, 80 MHz or
above, and hardware floating point support (e.g. a WeAct Black Pill
3.0 using an STM32F401CC might well work OK). If you decide to start
with a different board or processor, I'd recommend picking one on the
Zephyr "supported boards" list, as much of the work will already have
been done for you.

The actual details of the low-level signal capture (receiving) and
creation (transmitting) are somewhat board-specific.  The Zephyr driver
for ADCs doesn't support timer-based sampling, or the asynchronous
transfer of samples to memory via DMA, so I had to implement this
using the low-level STM32 ADC and timer and DMA APIs.  Similarly, the
logic to feed sample values to the timer PWM block during transmission
(PWM modulator) and to send delta-sigma bit sequences through the SPI
block, are board- and system-clock-speed specific and use low-level
APIs and ISRs.  For any significantly different board or processor, it
will probably be necessary to copy the "board_stm32f411.c" file,
hand-modify the hardware-specific details, and change the
CMakeLists.txt file accordingly.

The delta-sigma bitstream used by modulator #1 is tied to the SPI data
rate (currently 1.5 MHz), and would need to be recomputed to support a
board or processor with a different SPI data rate.  See the
```gentable-simple.c``` program in the ```noiseshaping``` directory.
You can modify the constants in the code to specify a different system
clock rate, clock divider, etc. and then run the program to create a
new noiseshaping_data.h file for your new platform.

The RP2040/Pico has hardware capabilities generally similar to those
of the STM32, but with a lot of differences in design and approach.
I've used the ADC (with its associated DMA channel), PWM block
(ditto), and a trivially-simple PIO program (also ditto) for the
interface to the analog world.

At some point I'd like to change the build process so that many of
these clock-rate-specific features are handled automatically at
build time.  For example, the noiseshaping and PWM data could be
computed during each build, based on the chosen board's configured
system-clock rate and SPI divisor.  Doing this will require more
knowledge of cmake, and the Zephyr build process than I currently
possess.

Parameters are saved on command into the microprocessor's internal
flash memory, using the Zephyr flash driver APIs.  The board
device-table overlay is used to specify one of the predefined
partitions in flash to use for this.  The flash holds multiple sets of
saved parameters, with the most recent (highest revision number) being
loaded on startup.  The partition selected for parameter storage is
erased only when it's entirely filled up with saved-parameter sets,
and there's no room for another.  If you choose to port to a board
which has a flash layout incompatible with this approach, you'll have
to figure out a different method of managing parameter storage (maybe
a small SPI flash chip?)

</BODY>
</HTML>
