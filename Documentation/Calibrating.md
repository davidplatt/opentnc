<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## Transmit calibration

You'll need to adjust the "TX audio level" potentiometer so that your
radio transmits the audio signal with the proper amount of FM deviation
(2.5 to 3.5 kHz is usual in most areas in IARU Region 2).

The exact amount of signal you need will depend on your radio.  Most
of the radios I've looked at, expect or recommend between 40 and 60
millivolts peak-to-peak in order to provide proper deviation.  The
OpenTNC-Pico output network is designed to provide a maximum of
approximately 100 millivolts peak-to-peak.  If you set the
potentiometer to roughly 40% of its full-scale value (somewhere
roughly around 10 o'clock on the dial, if you're using a 1-turn pot)
you should be in the right ball-park.

The best way to set the proper level is to use the CALIBRATE command,
have your radio transmit a signal into a good dummy load, and then
actually measure the FM deviation level.  If you're lucky enough to
have access to a service monitor, or a calibrated deviation meter, or
a friend who has another reliable way to measure FM deviation, do
it this way.

If you have access to a good RF spectrum analyzer, you can use the
Bessel-null method.  A frequency span of 10 kHz, and a resolution
bandwidth of 100 Hz or less is recommended; unfortunately the popular
TinySA spectrum analyzer's narrowest resolution bandwidth is too broad
for this analyzer to see the Bessel carrier null properly.  Monitor
the packet-radio frequency you're using with the analyzer and a sense
antenna (*not* a direct connection between your radio and the
spectrum analyzer input, or you'll fry the analyzer's front end!).
Turn the "TX audio level" control all the way down.  Start a
CALIBRATE, and press L to transmit the low (1200 Hz) tone.  You should
see the carrier frequency pop up on the spectrum analyzer.  Slowly
turn the "TX audio level" control upwards.  You should begin to see
sidebands appear, every 1.2 kHz on either side of the carrier
frequency.  As you turn up the "TX level control", the amplitudes of
the sidebands will increase, and the amplitude of the carrier will
begin to decrease.  Increase "TX level control" until the amplitude of
the carrier decreases deeply (as close to zero as possible) for the
first time.  This will occur at an FM modulation index of 2.405 - that
is, when the FM peak deviation is 2.405 * 1.2 kHz, or 2.866 kHz.
Nudge "TX level control" upwards just a bit if you wish, to come up to
3 kHz or thereabouts, and you're done.

If you don't have access to this sort of test equipment, you can set
the level by ear in either of several ways:

- Monitor the packet-radio frequency you're using with another radio.
Listen to the "loudness" of other packet stations in your area.  Start
with the "TX audio level" pot turned all the way down.  Start a
CALIBRATE, press ENTER, slowly turn up the pot until the signal has
the same loudness as the other stations you listened to, and stop the
CALIBRATE.  You're done.  Or, rather than "listen", you can hook the
monitor radio's output to the input of an oscilloscope, measure the
peak-to-peak voltage, and match levels that way.

- Monitor the packet-radio frequency you're using with another radio.
Start with the "TX audio level" pot turned all the way down.
Start a CALIBRATE, press H to send the higher-frequency tone (2200
Hz), slowly turn up the pot until the signal begins to sound distorted
or overdriven, and then turn the pot down to well below that level and
stop the CALIBRATE.

## Receive calibration

This is simple.  Turn the "RX audio level" potentiometer all the way
to the top (clockwise).

Connect the OpenTNC-Pico to your computer via USB.  The left-most LED
should begin flashing, once a second.  It'll also blink briefly when
you communicate with the TNC via USB.

Then, connect your OpenTNC-Pico to your radio, turn on the radio,
and open the squelch.  

If the left-most LED start flashing a lot, or stays on solidly, and if
this stops if you close the radio squelch, it means the radio's audio
level is too high, and the TNC's input circuitry is being overdriven
into distortion.  Either turn down the radio audio level (if you can),
or turn down the "RX audio level" potentiometer, until the left-most
LED goes back to its benign one-flash-a-second signal.

Most radios will probably not need an adjustment to the potentiometer,
as their audio output level is safely below 2 volts peak-to-peak.

</BODY>
</HTML>
