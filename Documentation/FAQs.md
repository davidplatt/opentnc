<HTML>
<meta charset="UTF-8">
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## Frequently Asked (perhaps) Questions

### Are you selling complete, ready-to-use TNCs?

No.

### Are you selling kits?

No - at least, not directly.

What I have done, is make a convenient "shopping list" available.  The
bill-of-materials file for the OpenTNC-Pico lists everything you'll
need (except for PC board, cables, and enclosure), and it has
distributor part numbers for both DigiKey and Mouser.  You can upload
the BOM spreadsheet file to the distributor's web site, go through a
few steps, and have all of the in-stock parts added to your "cart".

You might have to find a substitute for out-of-stock parts, but due to
the "jellybean" nature of almost everything in the BOM this won't be
difficult. You can remove parts from the cart if you already have
suitable ones in your supply box.

Place your order, and in a week or so you'll get a box containing the
kitted parts.  Both companies are very reliable, and you'll get what
you need.  You'll probably pay _less_ than if I were to sell you a
kit, with less chance of error, because the whole process is highly
automated.  I built my most recent prototype using the "Mouser kit"
and everything fits fine and works correctly.

## Are you selling the PC board?

Not currently.  I _might_ do so if there's the right amount
of interest, but I really don't want the amount of work which
would be involved in bulk-buying and shipping out a whole bunch
of boards all by myself.  If I do this at some point, I don't
promise to sell them at a bargain price - I can't match the
shipping and packing volumes (or costs!) of JLCPCB and similar
companies.

The PC board can be purchased through any of several companies...
upload the ZIP file of Gerber plots to their website, place
your order, and get boards in the mail in 1-3 weeks.  The
minimum order is usually 5 boards.

I've used JLCPCB for my prototypes, and several earlier projects as
well.  The quality of the boards I've received has been excellent, and
if one selects the slow "China post" mail delivery (2-3 weeks) the
cost is extremely low.  PCBWAY and Elecrow provide similar
"manufacture on demand" services, and there are numerous other board
manufacturers you can check out.

You can knock down the cost-per-TNC if you and a few friends
get together, order a 5-pack of PC boards, and place a single
Mouser or DigiKey order for 5 "kit packages".  You'll save
on the shipping cost, and start getting volume discounts on
some of the components.

## Can this design be made commercially?

Sure - just comply with the open-source licence agreements (GPLv3 for
the firmware, and CC-BY-SA for the hardware), and you're good to go.
If I learn you're violating the licenses, I reserve the right to send
the lawyercats in your direction armed with a +10 Sword of Cease And
Desist... so, play nice, please.

I would _not_, however, try to build the OpenTNC-Pico in its current
form at a commercial scale.  The PC board layout and component types
were specifically chosen to make it easy to assemble by hand at
home.  It requires lots of hand-soldering and so it's just about
exactly wrong for volume manufacturing these days.

Were I to want to make an OpenTNC commercially, I'd spin a very
different PC board.  I'd switch most or all of the components from
through-hole to a reasonable surface-mount equivalent (choosing ones
that the board manufacturer-and-assembler has in stock).  I'd almost
certainly get rid of the Pico per se, and integrate a Pico's
components (RP2040, oscillator, flash memory, etc.) onto the main
board.  I'd seriously consider switching back to a spiffier micro
(e.g. an STM32F411 or similar, or port to an ESP) in order to get more
horsepower and a better analog-to-digital converter.  I'd install a
rugged USB-C connector in place of the micro-B on the Pico.  I might
add a separate SPI EEPROM for data storage (think "mailbox" or "BBS")
and/or a battery-backed clock.

## It's a four-layer PCB. Can we have a 2-layer PCB which we can expose and etch ourselves?

Sure!  Go right ahead.  Start with the 4-layer layout in the KiCAD
directory, rip up all the traces, remove the two inner layers, and
re-route everything.  Make sure that all of your traces to
through-hole components are on the bottom, unless you're sure that the
component lead will be exposed and accessible on the top of the board.
Expect to use a lot of vias to route traces over and under one
another.  Getting good ground connections and a good ground plane is
going to be a real hassle.

Then, expose and etch and drill and tin-plate.

Remember that you're not going to be able to make plated-through
holes, so you'll have to solder a lot of the component leads to pads
on both sides of the PC board.  Miss just one, and you have an
open circuit and a dead TNC.

Seriously: why bother?  In the past few years, the cost penalty for
making a 4-layer board (compared to a 2-layer board of the same size)
has fallen sharply. For this project, and my previous two or three, it
just hasn't made sense to try to make a 2-layer design... let alone go
through all the hassle of making a toner-transfer board, or exposing
and developing a photosensitive board myself.  The OpenTNC-Pico
4-layer board can be had for under $3 each (delivered from China!) in
small quantities, or maybe $30 each if you want to order a few from
a premium-price U.S. PCB-maker.  I can't see the sense in making a
board of this sort at home, these days.

## Was it really worth going to all this work to support such an old technology?

Well, I had fun, learned a bunch, and got the satisfaction of making
something which might be useful to others.

It was an interesting challenge, and I had a sense that there might be
a need. From what I could tell, there are only a small handful of
commercially-available TNCs on the market today.  MFJ is no longer
making their TNC-2 clones, it seems (just their version of the TNC-X,
which is a nice KISS TNC with no AX.25 software stack).  I haven't
seen any physical TNCs which support FX.25 error correction, nor any
with a modern open-source implementation of AX.25.

OpenTNC is a design that other people can take, use, experiment with,
and (I hope) improve upon.


</BODY>
</HTML>
