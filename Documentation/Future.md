<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## Future ideas

### Hardware variations

The example schematics show a simple version of the OpenTNC for
the Black Pill, as well as the schematic for the Pico version
that I've used as the basis for the first "official" PC-board
design.  Both come with a number of options available to the builder.

-  You can use either a TNC-2-style or Kantronics-style radio
jack.

-  The schematic shows an ARM-standard 10-pin debug-and-programming
connector, wired up to the Black Pill's single-wire-debug pins.
You can omit this connector, and the connection to the Black Pill
entirely, and simply solder a 4-pin right-angle pin set to the
Black Pill to access these pins (a suitable set of pins is usually
included with the Pill), or omit this connection entirely if
you don't need flash-and-debug access.  On the Pico version I've
omitted the 10-pin connector entirely - if you want to use SWD for
flashing or debugging, just solder pins to the three pads on the end
of the Pico.

- The Black Pill board schematic shows a USB B jack, connected to the
Black Pill's D+/D- pins through 10-ohm resistors.  You can omit all of
these if you intend to mount the Black Pill in an orientation which
allows you to connect a cable to its onboard USB C jack.

- The schematic shows two separate "grounds" (TNC-and-computer, and
radio) and the use of two line-coupling transformers to isolate the
TNC-and-computer ground from the radio ground.  In many situations
this isolation is unnecessary.  You can build the TNC with a single
ground, omitting the transformers, and connecting the audio signal
circuits directly to the "TX audio" and "RX audio" pins on the radio
connector.  The schematic includes "closed" jumpers which make
these connections - you'd need to open these jumpers if you include
transformers in your design.  The Pico schematic shows a similar
set of transformers, with "ferrite bead on wire" jumpers as an
alternative - the board design uses the latter.

I've included a sample Black Pill board design with the sample
schematic.  This quick-and-dirty board is simply a manufactured-PCB
version of a prototype board, with connection points for the Black
Pill.  It fits in a 10 cm x 10 cm boundary, which is one which many of
the online PC-board houses will make quite inexpensively.  It's a
four-layer board with internal ground and power planes - this requires
less fiddly vias than a two-layer board would, and the increased board
cost these days is minimal.

Please _do not just go ahead and manufacture this PC board_ without
extensive checking.  I haven't made one yet and there may very well be
errors or incompatibilities. In particular, the connector footprints I
chose may, or may not match up with whatever USB and DE9 connectors
you can actually acquire, and the surface-mount transformer footprints
are for a model which is probably obsolete and unavailable.  Use it as
a starting point, modify as required to suit your chosen connectors
and components, and check everything before you make any.

A fancier version (better suited for mass production) would eliminate
the Black Pill, and put down a "bare" STM32F411 or similar processor
and the oscillators, capacitors, switches, and regulators it needs to
operate.

Similarly, a mass-production version based on the RP2040 would
eliminate the Pico board itself, absorbing the RP2040 and other
necessary components from the Pico schematic onto the unified board.
I would _strongly_ encourage replacing the Pico's buck/boost switching
regulator with a quieter linear regulator!  The boost/buck chip is
lamentably quite noisy and it seriously degrades the performance of
the RP2040's ADC.  On some Pico boards it generates enough switching
noise to be "seen" in the OpenTNC's transmitted-audio output - this
seems to vary from board to board.

### Different demodulation techniques

One demodulator I haven't yet tried is a digital phase-locked
loop.  I'm sceptical about this approach due to the very limited
number of zero-crossings per baud - I'm not sure it would work
any better than the crude "zero crossing counter" and "zero crossing
time" demods I've tried with limited success.

### DAMA

It wouldn't be difficult to add DAMA-slave capability, so that the TNC
would honor a "You are connected in DAMA mode" response from the
remote peer (DAMA master), and would transmit only immediately
after being polled by the master.  Unfortunately I lack access to
a DAMA-master system to use to test out this mode.

### Additional TNC functions which might be implemented

- 2400 baud AFSK modulation (might be do-able with just firmware
changes?)

- Mailbox capability.  Currently, messages would have to be stored
either in RAM (volatile) or in a special region in the flash (existing
or new).  The basic capabilities could be added in firmware.  Adding a
separate flash or EEPROM or pseudo-static-RAM chip to the board, for
message storage, would probably be wise and would require minor changes
to the board.

- Full BBS capability... requirements and approach similar to the
mailbox capability, but on steroids and Red Bull.

- Multi-mode operation: allowing a host BBS to operate using the KISS
interface, while intercepting inbound connections to a special
callsign and handling them internally.  I've laid some foundations for
this possibility in Version 3, in that the connection logic is capable
of supporting multiple connections in parallel, to different
"endpoints" each identified by a callsign.  Currently the console
session is the only real endpoint, but I've implemented a simple "echo
back what you hear" internal connection handler as a proof of concept,
and this idea could be extended to other capabilities.

- Telemetry handler - reading sensors via SPI or I2C, beaconing out
the results periodically via UNPROTO.  The internal-endpoint-handler
capability could be used to accept command-and-control connections
over the air for device control.

- VARA-FM compatibility?  This is probably a pipe dream for the existing
hardware - I don't believe the Pico has anywhere near enough CPU
horsepower to do OFDM properly.  Higher-end ARM Cortex processors
probably could (e.g. Cortex M7 or similar).  A bigger issue (I think)
is that the VARA protocols appear to be entirely closed and proprietary -
it's an "unpublished digital code" in FCC terms - and there's no easy way
to do an open-source implementation which can interoperate with VARA.

- How about going completely crazy, and turning the OpenTNC into an
actual repeater controller?  It could be used to add packet-based
repeater-system control capabilities to an existing system fairly
easily, and with some serious work it could probably become a full
controller.  The Goertzel tone-detector design I used initially (still
in the code although not actively used) ought to make a fine PL-tone
detector, and some of the unused GPIOs could be used for repeater
control.  Add a large-enough SPI flash chip and I'll bet you could
implement a voice recorder!  A lot of the single-board and
full-product repeater controllers on the market a few years ago are
now gone (I think Link Communications, NHRC, and Computer Automation
Technology are all out of business) and there appears to be an un-met
need out there...

