<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## A quick overview of the project

The OpenTNC is a simple but very functional TNC which can be
constructed at low cost (under $50).  It requires no exotic analog
integrated circuits (e.g. modulators, demodulators, PLLs), just a
microcontroller board and a handful of common components and
connectors.  It supports both KISS mode (for interfacing with PC-based
networking stacks) and a traditional interactive "connected mode" of
operation.  It connects to a computer using USB, presenting itself as
a class-standard USB "composite asynchronous communication device" (a
"virtual serial port"), requiring no special drivers on Linux,
Macintosh, and modern Windows systems.  The command set is based on,
and largely compatible with that of the TAPR TNC-2 and its clones.
FX.25 error correction is supported when communicating with compatible
TNCs (e.g. Dire Wolf).

The code used here is a new implementation of AX.25 - it was not
derived from any existing TNC's code, nor from Dire Wolf nor the Linux
kernel implementation of AX.25.  It runs on two different ARM Cortex
systems so far:

- the WeAct Black Pill 3.2 development board utilizing an STM32F411 Cortex-M4 processor.

- the Raspberry Pi Pico board utilizing an RP2040 Cortex-M0+ processor

It should be largely portable to many similar boards
(e.g. STMicroelectronics Nucleo), other ARM Cortex-M processors,
RISC-V cores, and so forth.  The code runs as an application on the
open-source Zephyr kernel, utilizing standard Zephyr APIs and drivers
wherever practical.  A subset of the code can be run as a native Linux
application, using the Zephyr "native_posix64" board definition.

KiCAD schematics and board layouts (with a number of possible
parts-stuffing options) are included in the project.  The
board design for the Raspberry Pi Pico has been manufactured
and tested, and Gerber plots are available for your use.

The parts cost to build a single Pico version is somewhere on the
order of $40 (a five-PCB order from JLCPCB, and single-board parts
purchase from DigiKey) plus the cost of whatever sort of case you
decide to put it in.  The cost per unit can be reduced significantly
if you order boards and parts for e.g. five or ten at a time.


</BODY>
</HTML>
