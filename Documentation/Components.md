<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## Component selection

The bill-of-materials cost for a working OpenTNC might run from $20
to $40, depending on what you decide to include, how you decide to
order it, and how well your junk box is stocked. Although it's
possible to build one based on the WeAct Black Pill 3.2, I now
recommend the Raspberry Pi Pico platform for new builds, as the
Pico board should be reliably available for the next ten years
or so.

Most of the components are non-critical "jellybean" parts.  If you
have a well-stocked junk box, you may already have most of the parts
you need.  I've provided a bill-of-materials file, in CSV form, which
you can upload to either DigiKey or Mouser and use as the basis for a
shopping list; it has suggested part ordering numbers for both
vendors (the brands and types vary, depending on what they had in
stock at the time I made up the list).  Feel free to substitute as you
see necessary.

### Processor module

One Raspberry Pi Pico board.  You can probably use a Pi Pico W board
(with the on-board WiFi/Bluetooth chip) with the current board layout
and firmware.  I have not personally tested this board for
compatibility, there is as yet no support in the firmware for any
wireless connectivity, and the "heartbeat" LED on the Pico W will not
function with the current firmware.

### Voltage regulator

One low-dropout 3.3-volt linear regulator in a TO-92 three-lead
through-hole package.  In the bill-of-materials file I suggest an
LP2950 (a 100 mA regulator made by TI and onsemi).  Alternatives
include the LE33CZ (100 mA), L4931 (250 mA, but now obsolete), AP7831
(150 mA), or MCP1700 or MCP1702 (250 mA but with a different pinout,
lead bending required!). I've measured the current drawn by the
3.3-volt rail at just under 70 mA when transmitting, and it should be
under 80 mA even with all LEDs illuminated, so any of the regulators
named above should work.

Although the Pico has an on-board 3.3-volt regulator of its own, it is
_noisy_ (as Bob Pease wrote, switching regulators don't belong near
sensitive analog circuitry since they create every kind of noise known
to humanity, and several kinds which are not).  The voltage-regulator
switching noise limits the performance of the RP2040's
analog-to-digital converter, and it even leaks into the transmitted
audio signal.  As shipped, the OpenTNC-Pico board disables the Pico's
switching regulator (by grounding the 3VE_EN pin) and powers the 3V3
rail through the linear regulator, and this mode of operation is _strongly_
recommended.  If you can't purchase or don't want to use one of the
suggested linear regulators, you don't have to: don't install
C102/C103/U102, and cut open the middle of grounding jumper J101 (this
will un-ground 3V3_EN and re-enable the Pico's switching regulator).

### Optocoupler(s)

4N32, 4N35 or similar through-hole Darlington, for the push-to-talk
circuit.  Buy two, if you want to use the radio's "RF detected,
squelch open" signal.

If you don't wish to use optocouplers, you can substitute cheap
general-purpose NPN switching transistors such as PN2222 or 2N3904.
Almost any junk-box NPN switching transistor rated at 25 volts or above
should work.  The base lead goes to pin 2 of the optocoupler
footprint, emitter to pin 4, and collector to pin 5 (pins are counted
counterclockwise from the upper left corner, with the notch in the
footprint pointing upwards).  The disadvantage of this simple
substitution is that an over-voltage applied to the radio connector
would be more likely to damage the Pico (rather than just the
"sacrificial" optocoupler).

### Resistors

All are standard-value, 1/4 or 1/8 watt through-hole.  Carbon-film
is fine.  Use metal-film if you have 'em and want to - they usually
cost more and there's no practical advantage in this circuit.  When
using standard 1/4-watt resistors, bend the leads down sharply right
at the ends of the resistor case in order to fit the resistors neatly
into the PC-board footprints.

### Capacitors

The electrolytic caps are standard value (use 16-volts and above, 2.5
mm lead spacing). For the small-value caps I recommend using plastic
film (typically metallized-polyester).  If you have cheap ceramic caps
of the proper values, you can use these in a pinch - they're not as
good as plastic film but in this circuit it's not likely to matter
much (I've built a couple of prototypes using Xicom Z5U ceramics and
they seem to work OK).  The small-capacitor footprints on the board
have double pads and holes, and should easily accept caps with lead
spacings of anywhere from 2mm to 5mm.

### Inductor

One 10 mH (that's millihenry, _not_ microhenry) ferrite
inductor.

### Trimmer resistors

Two (the RV01 and RV02 components) for the receive and transmit gain
setting circuits.  With the current design, single-turn trimmers
should be good enough, and I've specified good-quality parts from
Bourns in the BOM.  If you want finer control over the transmit
deviation, you can substitute a 5, 10, or even 20-turn trimmer having
the same value and footprint - this will add a couple of dollars to
the BOM cost.

## Connectors

A jack for the connection to your radio.  The schematic and board
support both a DIN-5 180-degree female (TNC-2 standard, often sold as
a "MIDI jack") and a DE-9 female (Kantronics standard).  Feel free to
wire up either or both.  Don't connect radios to both connectors
at once, as the results are unpredictable.

### LEDs

The bill-of-material calls for using five standard 5mm LEDs, with
470-ohm series resistors (giving roughly 4 mA of drive current, which
is the default current limit for the RP2040 GPIOs).  I've suggested
some suitable high-efficiency (low-current) diffused-lens red LEDs in
the parts list.

Feel free to use other colors if you wish, or to mix-and-match.  I'd
suggest testing your chosen LEDs with different series resistors,
driving them from a 3.3-volt DC supply, in order to match the
brightness of each LED type to what you want.  If you "dry fit" an
LED, and its series resistor into the proper holes in the PC board
(and don't solder them yet!)  you can use the "LAMPTEST ON" command to
light it up and check the brightness.  Power down and try different
resistors if necessary.

### Optional components

Optionally, a strip of WS2812B "NeoPixel" or "smart LED" R/G/B
emitters, to be installed in place of the five single-color LEDs in
the standard build.  The stock firmware image is configured to use the
first 5 emitters in the strip, displaying additional information
about the TNC's operation by using several colors per emitter.

A 5-emitter length cut from a larger "60 LEDs per meter" spool will
fit nicely across the width of the PC board, and a 1-meter strip
(cheap on eBay) will supply enough for ten or twelve OpenTNC-Pico
boards.  You can use other WS2812B strips or bars, or construct your
own, with any number of emitters in any pattern you wish; you may need
to build your own custom firmware image (after editing the
rpi_pico.overlay file) to map the "virtual LEDs" into the emitters and
colors you want.

Optionally, three small "ferrite bead on a wire" RF blockers, for
connecting the audio signals and ground between the main board and the
connectors. The BOM suggests some inexpensive Fair-Rite parts.  Feel
free to use whatever you have in your junk box.  If you don't have any
of these handy and don't want to order them, you can just install
short jumper wires in the FB1-FB3 positions.

Optionally, a pair of 1:1 audio isolation transformers (telephone
or modem "line coupling" transformers are ideal).  These are required
only if you want separate, isolated grounds for the TNC and radio.
Footprints for these are _not_ included on the OpenTNC-Pico PCB I've
laid out - you'll need to design your own board for this.

### PC board

You'll need some sort of printed-circuit or prototyping board to which
you'll solder all of the above, and run jumpers as necessary.  The
OpenTNC-Pico release includes ready-to-manufacture Gerber files for a
tested board design.  You can upload the Gerber .ZIP file to
JLCPCB.COM, or another board house of your choice, and order as many
boards as you wish.  If you choose the slower "China Post" shipping
option from JLCPCB, you can get a batch of 5 boards for around $11
(delivered) in 2-3 weeks.


</BODY>
</HTML>
