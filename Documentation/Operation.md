<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## Operation

Once built, flashed with the firmware code, and reset, your board
should enumerate on USB as a "OpenTNC" and a CDC-ACM "virtual serial
port" should appear.  You can connect to this using 'most any
terminal-emulator program (baud rate doesn't matter), and the TNC
command prompt should appear.

Type "help" to get a list of commands supported.

Remember to set your callsign (with "MYCALL xxxx" followed by "SAVE!")
before connecting the TNC to your radio and trying to transmit.

To use KISS mode, connect to the TNC as above, wait for the prompt,
and send the command "KISS ON".  The TNC will switch to KISS mode
immediately.  To exit from KISS mode, either cycle the power, or
send the KISS exit sequence (0xC0 0xFF 0xC0) to the TNC, or (on
Linux, if you have the interface up) incant `kissparams -x $PORTNAME`

## LEDs

The standard build of the OpenTNC-Pico contains five single-color
LEDs.

The default behaviors of the single-color LEDs are as follows.  LEDs are
numbered from left to right.

1. RUN function.  Blinks briefly once a second to show that the TNC
firmware is running.  Blinks briefly whenever data is sent to/from
the host PC via USB.  Blinks longer, or remains on, if the "RX audio"
signal from the radio exceeds roughly 2 volts peak-to-peak and is
causing the analog-to-digital converter to "clip".
2. CONNECT function.  On when the TNC is establishing, or has
established a connection to another TNC, or when the TNC is operating
in KISS mode.
3. CARRIER function.  On when carrier (a valid HDLC signal) is detected in
the received audio.  On if the TNC is operating in "CARRIER AUDIO" and any
audio signal is present.  On if the TNC is operating in "CARRIER SQUELCH"
and the radio is providing a positive voltage on the "RF detected,
squelch is open" signal line.  Off if none of the above is true.
4. PACKET function.  Blinks when a valid AX.25 packet is received.  On
when the TNC has any AX.25 packets waiting to be sent.
5. PTT function.  On when the TNC is asserting "push to talk" and is
transmitting a packet.

As an option, one can substitute a strip of five WS2812B
multi-color "NeoPixel" smart LEDs, which provide additional
operating information about the state of the TNC.

The default behaviors of the WS2812B LEDs are as follows.  LEDs are
numbered from left to right.

1. RUN function.  Blinks green briefly once a second to show that the TNC
firmware is running.  Blinks blue briefly whenever data is sent to/from
the host PC via USB.  Blinks red longer, or remains red, if the "RX audio"
signal from the radio exceeds roughly 2 volts peak-to-peak and is
causing the analog-to-digital converter to "clip".
2. CONNECT function.  Red when the TNC is establishing
a connection to another TNC.  Green when a normal AX.25 connection
has been established.  Blue when an AX.25 connection has been established,
and the connection is using FX.25 error correction.  Yellowish when an
AX.25 connection is being torn down.  Whitish when the TNC is operating
in KISS mode.
3. CARRIER function.  Green when carrier (a valid HDLC signal) is detected in
the received audio.  Red if the TNC is operating in "CARRIER AUDIO" and any
audio signal is present.  Blue if the TNC is operating in "CARRIER SQUELCH"
and the radio is providing a positive voltage on the "RF detected,
squelch is open" signal line.  Off if none of the above is true.
4. PACKET function.  Blinks green when a valid AX.25 packet is received.
Blinks blue when a valid FX.25-protected AX.25 packet is received.
Red when the TNC has any AX.25 packets waiting to be sent.
5. PTT function.  Red when the TNC is asserting "push to talk" and is
transmitting pre- or -post-packet guard tones (e.g. the TXDELAY
idle pattern, or ABORTs).  Green when transmitting an AX.25 frame.
Blue when transmitting an FX.25-protected frame.

</BODY>
</HTML>
