<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## Building an OpenTNC-Pico

Actually assembling the OpenTNC-Pico shouldn't be difficult if
you have prior kit-building experience.  Use a good-quality soldering
iron (temperature-stabilized if possible), and good-quality rosin-core
electronics solder (I personally prefer a 63/37 eutectic).

The Raspberry Pi Pico module should be assumed to be static-sensitive.
Using a properly-made anti-static wrist strap (that is, one which
incorporates a resistor between you and ground) and working on a
static-dissipative surface is a good idea.

Note that there are components which are optional.  If you're
building a "simple" board with five ordinary LEDs, you do not
need to buy or install the components which power the WS2812B
strip (D306, C301, and R306).  Similarly, if you're going
to use a WS2812B, you'll need to install those three, but
don't need to buy or install R301-R305 and LEDs D301-D305.
You can, of course, install all of these and have a double
set of display LEDs if you wish.

Similarly, if you aren't going to be using the "squelch open"
feature, you may omit U202 and R208.

![OpenTNC-Pico printed circuit board 0.5](../photos/OpenTNC-Pico-board-brighter.jpg)

Recommended order of installation:

- Install all of the resistors (fixed and trimmer) _except_ for
resistor R203 to the left of the Pico footprint.  If you're
using the common 1/4-watt axial resistors such as the ones I've
suggested in the BOM, bend the leads sharply downwards right
at the ends of the resistor casing in order to get the leads to
fit cleanly into the holes in the PC board.

- Install all of the capacitors (electrolytic and film) _except_ for
capacitors C101, C102, C202 and C203 to the left of the Pico
footprint.

- Install optocoupler U203, and also U202 if you're using the
"squelch open" feature.  Install D306 if you're using a
WS2812B strip.

- Install either ferrite-beads-on-wires, or simple wire jumpers,
in FB201-FB203.

- Install the five LEDs D305-D305 if you are making a "simple"
board with mono-color LEDs.  Mount the LEDs high enough above
the board so that you can bend them downwards and forwards
towards the (eventual) front panel, if you wish.

- Install the Raspberry Pi Pico (U101).  Take precautions against
static-electricity damage.  See below for details.

- Install the voltage regulator (U102), the remaining resistor (R203),
and capacitors (C101, C102, C202 and C203) to the left of the Pico.

- Install either or both connectors (J201, J202).  Solder all
of the pins, including the big mechanical-hold-down pins!

- If you're using a strip of five WS2812B tricolor LEDs, connect the
Power/Data-in/Ground pads on the first WS2812B to the corresponding
holes on J301 using short lengths of flexible wire, or install pin
jumpers in J301 and connect a set of three wires with pin receptacles
onto the WS2812B.  Mount the WS2812N strip however you wish (I stuck
mine to a 3.5"-long, .5"-square strip of scrap wood, and glued this in
the rectangle marked along the front edge of the board, covering the
D301-D305/R301-R305 positions).

You're done.  Check your work, looking for any poorly-soldered
junctions (re-heat and re-solder if necessary) and any accidental
solder bridges (re-heat and clean them up).  Then, hold down the
BOOTSEL button on the Pico, plug it into USB, and flash the
firmware.

### Soldering the Raspberry Pi Pico onto the board.

You have at least three options for how you can mount the Raspberry Pi
Pico onto the board.  Two require the use of pins, and one of
these requires the use of pin sockets.  I have _not_ included headers
or sockets in the BOM spreadsheet - you'll need to order these
yourself if you want to use them.

### Option 1: pins and sockets.

This option allows you to replace the Pico easily, if you should ever
need to do so, but it's mechanically a bit less secure than a
fully-soldered connection.  For this option, you'll solder a strip of
pins into the holes along each side of the Pico.  It's important to
make sure that the two strips of pins remain parallel during the
soldering process - otherwise they won't go into the sockets properly.
I suggest that you use a piece of perforated prototyping board to
ensure this.  Support the prototyping board somehow, stick the long
ends of the strip pins down through the holes (with the two strips the
proper distant apart), and then lower the Pico down so that the short
ends of the pins project upwards through the proper holes along the
long edges of the Pico.  Once everything is lined up properly, solder
the pins in the four corners of the Pico (thus fixing the pins into
position), then solder each of the other pins.  Prepare the
OpenTNC-Pico board by soldering a 20-position pin-socket strip along
each edge of the Pico footprint (make sure that the sockets are
sitting flat on the board before you solder all of the pins!).  You
can now insert the pin-equipped Pico into the pin sockets (make sure
you position it correctly!).

### Option 2 - pins, but no sockets.

This approach provides a securely-soldered connection (not as easily
repaired, if the Pico should fail!) and makes all of the pin signals
easily available for debugging if necessary.  In this approach, you
will first solder a 20-position strip of pins into each side of the
Pico footprint on the board (short ends of the pins into the board,
long ends sticking up).  Make _very_ certain that the strips are flat
on the board and that the pins are vertical!  After soldering the pins
into the board, you can slide the Pico down over the long ends of the
pins, and then solder each pin connection to the Pico.

### Option 3 - direct surface mounting (no pins, no sockets).

The Pico is made to allow this, even for hand-soldering, as each pad
position along the side of the board is "crenelated" (it has a
gold-plated half-hole to which one can solder).  I've modified the
Pico footprint on the PC board, extending the length of each pad
position so that it's easy to make contact with the pad.  Place the
Pico directly down on the footprint on the board, and insert a set of
pins (or some resistor leads) through several of the holes along each
side of the Pico, and down through the corresponding holes in the PC
boar.  Doing so will force the Pico to sit in the correct position
over the pads on the board.  Now, solder the four pad positions in the
corners of the board to fix the Pico securely in place, remove the
aligning pins, and solder all the rest of the pads.  For each pad:
make sure your soldering iron tip is clean; wet it with a bit of
solder; hold it so that it contacts both the crenelated edge of the
Pico pad, and the pad corresponding pad on the PC board; wait a second
or so; touch the junction of the Pico and board pads with your solder
until the solder melts; gently "wipe" the junction with the
soldering-iron tip to ensure good wetting; remove the soldering iron
tip and allow the solder to cool.

I used Option 1 for the initial Black Pill prototype, and Option 2 for
the Pico prototype.  These were hand-wired boards built on generic
proto-board.  For my first custom-PCB prototypes (Pico boards v0.3 and
v0.5) I used Option 3; it was quicker and gives a cleaner-looking
result than the options using pins, and it has worked out fine so far.

I built one "simple" and one "fully loaded" Pico v0.3 board, in one
afternoon... and this included the time needed to find, and patch
around an error in the v0.3 board Gerbers which had resulted in an
open trace (this error is corrected in the v0.5 board).  The 0.5
prototype took a couple of hours to assemble.

</BODY>
</HTML>
