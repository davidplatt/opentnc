<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## G3RUH 9600-baud mode

The 4.0 version of the OpenTNC firmware, and the modified
0.8 schematic, add experimental support for G3RUH 9600-baud
FSK modulation and demodulation.  Inter-operability has been
confirmed with the current release of Direwolf.

### Initial notes

9600-baud packet radio is usually less reliable than 1200-baud
operation.  Because of the high rate of symbol transmission, it's
more easily disturbed by noise bursts and by multipath reflections
(which cause inter-symbol interference).  Having a good line of
sight between stations, antennas mounted up as high as possible,
a good strong RF signal, and some amount of antenna gain (and
directionality) can help a good deal.  Attempting to use 9600
baud mode while operating mobile is not likely to be a rewarding
experience.

FX.25 error correction can help to some extent, but it's no
panacea.  Due to the pseudo-random "scrambler" used in this mode,
a single-bit error in transmission is multipled into 3 bit errors
after descrambling  At least two of these are certain to fall
into different bytes in the FX.25 Reed-Solomon data and will need
to be corrected separately.  The FX.25 error correction "budget"
will be used up more rapidly, for this reason.  Turning the FX.25
error correction level up to "maximum" will give the best
correction results, at the expense of more overhead during packet
transmission.

### Radio connection

9600-baud operation requires a fairly direct connection to a radio
transceiver's modulator and demodulator circuitry, bypassing the
microphone preamplifier, and the transmit pre-emphasis and receive
de-emphasis circuits.  Many of the better-quality Japanese-built
mobile radios have a suitable "data" or "packet" jack on the
back - usually a 6-pin mini-DIN connector - and a "9600 baud"
mode of operation which can be selected through a front-panel
menu.  These are what you need.

The direct modulator connection requires a higher signal voltage
than is used with the "microphone level" input normally used
for 1200-baud AFSK operation.  Most radios with a "data" jack
seem to require 1 volt peak-to-peak for the proper amount of
modulation.  The OpenTNC output circuitry recommended in the
0.8 schematic can produce up to approximately 2 volts peak-to-peak
and so should have no difficulty driving these radios properly...
just turn the multi-turn output level potentiometer up to roughly
the half-way position.  Remember to turn it back down if you
switch back to 1200-baud AFSK operation, as most radios' microphone
inputs require only about 50 millivolts.

### Bandwidth and pulse selection

In G3RUH modulation, each single bit of data is represented as
a positive-going or negative-going pulse, with an "ideal" pulse
shape selected to have a flat bandwidth out to 4800 Hz and then
a very rapid fall-off thereafter (effectively a "brick wall"
by 6000 Hz).  This signal fits well into the audio bandwidth
provided by most voice-grade radios, and allows effective use
of FM radio channels on 20 kHz spacing with minimal "splatter"
into adjacent channels.

The pulses for neighboring bits actually overlap quite a lot, but the
pulse shape is chosen so that there is little or no interference
between the pulses at the center of each bit's pulse (the neighboring
pulses are at 0 at this instant).

Based on my reading of the original G3RUH paper (and other sources) I
concluded that the desired pulse shape is that of a normalize sinc
(that is, sin(pi*x)/(pi*x)), with a suitable windowing function
applied.  I chose a Blackman window, and the pulse shapes match those
shown in the G3RUH paper.

One of the quirks of this use of the sinc function is that while there
is no inter-symbol in the _center_ of each pulse, there's quite a lot
on the _shoulders_ of the pulse... and these are the points at which
the waveform crosses zero. As a result, there's a lot of "jitter" in
the time when the combined waveform crosses the zero axis even when
the signal is transmitted and received perfectly, and this complicates
the job of the circuitry (or software) which keeps track of the
zero-crossing timing for clock recovery.  G3RUH alludes to this issue
in the original TAPR paper, and is quite correct.

I've added experimental support for pulses which use a more aggressive
window function, replacing the Blackman window with a "raised cosine"
function with any of several different beta values.  As one increases
the beta value, the pulses become shorter (narrower shoulders) and
there's less inter-symbol interference and jitter at zero-crossing
time and less jitter.  "Narrower in time" necessarily means "broader in
frequency", and these pulses have a more gradual frequency roll-off
above 4800 Hz.  I believe they should still be compatible with proper
amateur-radio practice (minimizing splatter) but it may be necessary
to turn down the transmit-level control on the OpenTNC a bit (reducing
peak deviation) to keep the modulation fringes within the chosen
channel.

Type "help pulse" to see the pulse shapes available.

### Equalization

G3RUH modulation depends on having a flat frequency response through
the audio channel, between the sending and receiving TNCs.  Any
frequency-response errors or phase shift will result in distortion of
the transmitted waveform.  This will tend to show up as increased
jitter at the zero-crossing point, and as amplitude modulation in the
center of the bit period.  A signals engineer would refer to a
deterioration in the quality of the "eye diagram" of the audio
waveform as seen on an oscilloscope.

The likeliest type of frequency response error to be noticed, will
probably be a roll-off in response above 3 kHz (the top of the
nominal "voice bandwidth").  The original G3RUH modem contains
a ROM-based equalization capability, which can selectively boost
the frequency response of the transmitted signal in order to
compensate for rolloff in the transmitter, receiver, or both.
Support was provided for the measured frequency responses of
a number of radios popular at the time.

I've provided a somewhat similar "transmit-side" equalization feature,
implemented in a different way.  The "TXEQ" command can be used to
apply a simple (2- or 3-tap) FIR filter to the pulse shape used for
waveform generation.  This has the effect of applying a "peaking
filter" to the signal, with a user-selectable peak frequency (from
about 2 kHz on up) and a user-selectable amount of gain (measured in
centiBels).  The filter can be one which has a phase lead, or is
phase-neutral (symmetric in time), or a trailing phase.  So, for
example,

  txeq leading 5000 15

will equalize the transmitted signal with a gentle peak of
1.5 deciBels, centered at 5 kHz, in a way which adds a
leading phase shift.  This would compensate fairly well for
a radio whose frequency response starts to droop around
3 kHz and is down by 1.5 dB at 5 kHz.

Since the R-C rolloff characteristic of analog transmitter and
receiver circuitry has a lagging or trailing phase characteristic,
using a leading-phase equalization seems likely to result in the
best waveform fidelity.

I have not yet attempted to add a similar equalization feature
to the "receive signal" path... due to the way the G3RUH
receiver software works, it would be a good deal more difficult
to implement.

