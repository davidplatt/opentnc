<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## More about the project rationale

Why in the world would anyone design and build yet another sort of
1200-baud packet TNC today?  Isn't this sort of packet transmission
something which died with the dinosaurs?  Why bother?

_"Narns, humans, Centauri, we all do what we do for the same
reason: because it seemed like a good idea at the time."_ (Ambassador G'kar, _Babylon 5_)

It seemed like a good idea, because 1200-baud AX.25 works.  It's been
well tested, it's an open public standard, there are numerous products
and implementations which all "play together nice", and you can plug a
TNC into almost any cheap VHF amateur radio with a simple cable and be
on-the-air.  No central network authority or management is required -
anyone with a Technician-or-above amateur radio license can play.

For the past couple of decades I've been participating in my city and
county's ARES/RACES organizations.  These groups have found it
beneficial to develop a county-wide network of packet-radio BBS
systems for passing emergency traffic in a time of need.  Even at 1200
baud (a terribly slow speed by today's usual standards) it's possible
to pass messages more quickly by packet than is possible via voice
(simplex or via repeater).  The packet group makes heavy use of the
Outpost Packet Message Manager software originally developed by Jim
KN6PE (see <https://www.outpostpm.org/>) and the PackItForms system
(see <https://www.scc-ares-races.org/data/packet/about-packitforms.html>)
to improve efficiency.

Participating in this network requires owning a TNC, and this
is a significant issue for many operators.  The choices are
somewhat limited:

- Full-function TNCs such as the Kantronics KPC-3+, the TAPR TNC-2 and
its clones from e.g. MFJ, and so forth.  These are well-tested and
featureful.  They're not inexpensive if purchased new, and older
"hamfest" models may not be reliable.

- Reduced-function TNCs built into many higher-end VHF/UHF radios.
These are "free" when they're present, and often worth what you paid
for them :-).  They're usually adequate for packet-spotting
operations, APRS, and similar "fire and forget" tasks, but often have
buffer-size limitations and/or protocol bugs which make their use in
"connected" mode a very painful and frustrating one.

- Simple microcontroller-based "KISS" TNCs, which handle transmission
through the radio at the level of individual packets, but have no user
interface and no built-in support for user-to-user connections. They
appear to the PC as a simple serial port of some sort, "talking" a
specialized packet-interchange protocol.  These are often inexpensive,
but performance of these KISS TNCs tends to vary greatly, depending on
the sophistication of whatever hardware and firmware they use to
perform the necessary audio modulation and demodulation. They must be
used with PC software which implements the AX.25 "layer 3" connection
protocols, packet acknowledgement and retransmission, and so forth.

    There are several such software packages available such as AGWPE
(Windows) and the AX.25 network support built into the Linux kernel.
Each of these requires some specialized knowledge and configuration to
use.

- "Sound card" TNCs.  These are an extension of the "KISS TNC"
approach, where the KISS TNC is a specialized application running on
Linux or Windows, utilizing the PC's sound hardware as an interface to
the radio, and presenting a virtual "serial port" which speaks the
KISS protocol.  Once again, they require KISS-aware PC software which
implements the higher levels of the AX.25 network stack, creates a
user interface, etc.

    The excellent Dire Wolf package (hi, John!) is a very-high-quality
sound-card TNC combined with an AX.25 stack, and there are several
sound card KISS applications available for Linux which work with the
kernel's AX.25 stack.

## Evolution of the OpenTNC

About ten years ago, I started wondering whether there might be a
different approach possible today: a full-featured TNC (something
roughly akin to a TAPR TNC-2) built using a modern high-performance
microcontroller, using digital signal processing techniques rather
than fancy (and often hard-to-get) analog ICs to do the modulation and
demodulation.  It could (potentially) be inexpensive, flexible, and
relatively easy for an amateur operator to construct quickly and
easily.  The firmware could be made open-source, and might potentially
serve as a platform for further experimentation.  It
would give me a chance to play with the new generation of inexpensive,
high-performance microcontrollers and development boards, learn some
things, and make something at least vaguely useful.  And, it would
let me play around in a bunch of areas I've enjoyed for many
decades: electronics in general, audio, digital signal processing,
amateur radio, and embedded computing.

1200-baud packet radio modulation isn't complex.  It uses audio
frequency shift keying (AFSK), switching between two audio tones at
1200 Hz and 2200 Hz, transmitting and receiving a stream of bits
encoded using the HDLC protocol.  It's based on the old Bell 202
telephone modem standard (and I imagine that the original crop of
packet radio systems used repurposed Bell 202 modems).  Transmitting
(modulating) requires creating 1200 and 2200 Hz sine wave tones,
switching between them as an HDLC bit stream is processed.  Receiving
(demodulating) requires distinguishing these two tones with accurate
timing, and recovering the transmitted HDLC bit stream.  None of these
seemed to be beyond the capability of an inexpensive processor running
at close to 100 MHz.

Early experimentation with algorithms running on Linux suggested that
the audio processing could work, and writing the low-level HDLC frame
encoding and decoding software wasn't difficult. I was able to port
one version of this code to run successfully on a $3 "Blue Pill clone"
development board, and decode a test suite of packets from a standard
recording, with the packets being sent via USB and KISS to my Linux
workstation for analysis.  I used the
[WA8LMF TNC Test CD](http://www.wa8lmf.net/TNCtest/)
data, tracks 1 and 2 as my primary test corpus.

The performance wasn't what I had hoped for (especially on noisy
signals or where equalization was an issue), and I put the project on
the shelf for some years until after I retired.  In the meantime I
thought up some possible improvements.

A month or so of recent work got things to a very usable state...
what I consider good-to-very-good demodulation performance.  Getting
it there required moving up to a somewhat more powerful
microcontroller (a Cortex M4 with hardware floating point support).
In recent tests, the OpenTNC-BlackPill has successfully received
between 976 and 980 packets from the WA8LMF track 1 recording.

![Black Pill prototype, overhead view](../photos/Overhead_view.jpg)

![Black Pill prototype, oblique view](../photos/Oblique_view.jpg)

In order to support easy "plug and play" compatibility with Outpost,
and to eliminate the need for users to set up and manage AGWPE or
Dire Wolf, it was apparent that basic AX.25 Layer 3 connection support
would be needed.  So, I sat down and wrote it.  The OpenTNC
implementation is targeted at the classic AX.25 2.0 protocol
specification.  I haven't tried to implement the more complex features
in AX.25 2.2 (e.g. protocol parameter negotiation, or SABM-E extended
modulus support), although I did take a few simplifications from 2.2
where they made life easier.
   
The overall TNC design turned out to be simple enough that I decided
to take it to a form which might be easy for hams to build, using
parts that aren't likely to become unobtanium anytime soon.  I
ported the code to run on the popular Raspberry Pi Pico board, and
laid out a system board on which the Pico could easily be mounted.
It works quite well, even though the Pico's RP2040 is in some respect
inferior to the STM32F411 used on the Black Pill board.

The OpenTNC-Pico doesn't yet perform quite as well as the Black Pill
at receiving - on the WA8LMF Track 2 data it decodes around 941
packets, and the decoding performance varies slightly from board to
board.  The RP2040 processor's analog-to-digital converter is known
to suffer from some significant non-linear behavior ("glitches" at
several signal levels) and I suspect that this may reduce the TNC's
signal sensitivity on low-level or noisy signals. The use of fixed-point
point math in the biquadratic filter code (as compared to the
hardware-based floating point implementation used on the Black Pill)
may also be contributing to the slightly reduced performance.

</BODY>
</HTML>
