<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## Limitations

Only a single connection at a time is allowed - there's no support for
STREAMSW.  APRS digipeating has limited functionality and
flexibility.

There's no real support yet for 9600-baud packet operations.  It's not
impossible by any means, but would probably require running the input
and output sections at a 4x-higher sample rate.  A different signal
output path would also be required, feeding data directly to the
FM radio's modulator.


</BODY>
</HTML>
