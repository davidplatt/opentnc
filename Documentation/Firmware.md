<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## Firmware

### OpenTNC-Pico

If you're building an OpenTNC-Pico, installing the firmware should
generally be quick and easy.

- Download the latest Pico firmware image .uf2 file from
[the images directory](../images/).

- Hold down the "BOOTSEL" button on the Pico board, while
plugging the Pico USB cable into your computer.

- Your operating system should "see" the Pico appear, in the
guise of a USB mass-storage device.

- Mount (open, access) the mass-storage device.  You should
see a couple of .html files in the directory.

-  Copy the .uf2 file to the mounted filesystem directory.  This should
take a few seconds.  When the copy is complete, the Pico will
automatically reboot itself, the mass-storage directory should
"disappear", the heartbeat LED on the Pico board should begin to
flash, and the TNC should appear on USB as a new "virtual COM
port" (/dev/ttyACM0 or similar under Linux).

### OpenTNC v2.0 board based on WeAct 3.2 board

This one is a bit trickier.

I'll try to include an installable image file in this repo for
each major version, so you won't need to build the firmware yourself
unless you want to.  No promises.

To flash via usb:

```
dfu-util -D OpenTNC.dfu:leave
```

### Building firmware from the source code

If you do want to build it:

-  You'll need to install the Zephyr kernel, and its associated
SDK.  Visit https://zephyrproject.org/ to learn about Zephyr and how
it's installed and used.  Install the current version of Zephyr and
SDK (warning - this take a while and quite a bit of disk space!)

-  Use the "activation" process described in the Zephyr documentation
to activate the Zephyr virtual environment.

- Build the code for the Black Pill

```
	cd zephyrproject/zephyr/apps/
	git clone https://gitlab.com/davidplatt/opentnc
	cd opentnc
	git submodule update --init --recursive
	west build -p -b blackpill_f411ce
```

-  Or, build the code for the Pico

```
	cd zephyrproject/zephyr/apps/
	git clone https://gitlab.com/davidplatt/opentnc
	cd opentnc
	git submodule update --init --recursive
	west build -p -b rpi_pico
```

### Flashing the Black Pill board

Zephyr, and the Black Pill board support at least two ways of flashing an image
to your board: via USB, or via an external probe hooked up to the
board's single-wire-debug (SWD) pins.  You'll need to configure Zephyr
to know which flashing "runner" you want to use (see the
documentation).

#### SWD flashing

I can definitely recommend the BlackMagic Probe as a good development tool
for a project like this.  It supports flashing via SWD, it supports
interactive debugging (running its own GDB-compatible remote server in
the probe), and it provides an auxiliary UART which can be connected
to the debug-console UART on the OpenTNC.  You can buy a finished
BlackMagic probe, or you can build one yourself from a cheap "blue pill"
board (download the BlackMagic source code, build it using the "swlink"
target, flash it into a Blue Pill, and solder on a few wires with Dupont
connectors).

Many manufactures' development boards (e.g. the STMicroelectronics
Nucleo series) come with onboard debug probes which support SWD
flashing.  ST boards have an STLink, others have a CMSIS-DAP, etc.
Vendor-specific software can be used with these probes, in order to
flash firmware onto a target device.

#### DFU flashing

Zephyr supports code flashing via USB DFU (Device Firmware Update).
On the Black Pill, you can (sometimes) get into DFU mode by holding
down the "BOOT" button, then pressing and releasing "RSET", and then
releasing "BOOT".  The Black Pill should enumerate on USB in DFU mode,
and you can then

```
west flash
```

or, if you're using a pre-built .dfu image,

```
dfu-util -D OpenTNC.dfu
```

A word of warning - the STM32F411 bootloader is notoriously prone to
becoming confused if any of the interface pins it looks at are active,
floating, or are being pulled in the wrong direction when it starts
up.  It mistakes these as an attempt to bootload from (e.g.) a UART,
SPI, or I2C interface, and it fails to enumerate USB and present a
DFU interface.  I've struggled with this problem extensively when
working with my prototype board.  I _think_ I've included enough
of the proper pull-up and pull-down resistors in the sample schematic
to avoid the problem, but as I haven't yet built a board in this
style I can't be certain.

If you can't get the board to enumerate properly on USB, you can try
unplugging the WeAct board and flashing it stand-alone - that
sometimes works.  If not, you'll have to use SWD flashing, using
a BlackMagic probe, an ST-Link v2 or one of the cheap clones, etc.

### Flashing the Pico board

There are at least three ways to flash the TNC Zephyr image onto
a Raspberry Pi Pico.

#### Mass-storage flashing

Hold down the BOOTSEL button on the Pico board while connecting
the board to the computer's USB port.  The board should appear
as a mass-storage device (like a USB flash drive).

Mount/access the mass-storage device.  You'll see a couple of
HTML files present - one describes the bootloader version, the
other will redirect you to the Raspberry Pi Pico documentation
site.

Copy the Zephyr UF2 image to the mass-storage filesystem.
This could be a .uf2 file you've downloaded from
[the images directory on this site](../images/) or it could
be an image you've built yourself (in the apps/tnc/build/zephyr/zephyr.uf2
file).

It should take a couple of seconds for the image to be downloaded
to the Pico.  The Pico should then reboot itself (the mass storage
device will disconnect), the heartbeat and RUN LEDs should start
to flash, and the TNC virtual COM port should connect.

### Using picotool

On Linux (and perhaps elsewhere) you download and build
[the picotool utility](https://github.com/raspberrypi/picotool)
which gives you several useful ways to interact with a Pico's
boot-loader.

Hold down the BOOTSEL button on the Pico board while connecting
the board to the computer's USB port.  The board should appear
as a mass-storage device (like a USB flash drive).  Then,

```
picotool load IMAGEFILENAME.uf2 ; picotool reboot
```

It should take a couple of seconds for the image to be downloaded
to the Pico.  The Pico should then reboot itself (the mass storage
device will disconnect), the heartbeat and RUN LEDs should start
to flash, and the TNC virtual COM port should connect.

### Using SWD flashing

I understand it's possible to flash images to a Pico using the
board's SWD port (the three pins at the end), using a Raspberry
Pi debug probe, a BlackMagic probe, or similar.  I haven't
done so myself.

</BODY>
</HTML>
