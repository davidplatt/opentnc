<HTML>
<meta charset="UTF-8">
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

# Public Release Version 4.0

This release adds two new major features (both somewhat
experimental):

- 9600-baud FSK modulation, compatible with the original G3RUH design
and TNC-2 "add-on" boards.  This change requires minor modifications
to the transmit audio path filter circuitry - removing a few
components and changing a couple of values.  The result is a flatter
frequency response through the voice-audio range (up to 10 kHz) which
I have learned is essential for G3RUH modulation.  The changes are all
compatible with the current PC board layout, and will not affect
1200-baud operation with the new firmware.

- AX.25 V2.2 support, featuring extended packet sequence numbers,
larger receive windows, SREJ selective packet loss error recovery, and
XID negotiation of window and packet sizes.  V2.2 support is automatic
when receiving inbound connections, and optional when making outbound
connections.

The syntax and capabilities of the CONNECT command have changed.
Digipeaters are now specified using repeated "via CALLSIGN"
clauses ("via" is required for each one).  Other options may be used
to set the AX.25 version number, MAXFRAME, PACLEN, FRACK, and RETRIES
values for the connection being made.

New commands have been added to set the pulse shape, and transmit
equalization to be used for a G3RUH 9600-baud connection.

# Public Release Version 3.1

This release adds support for connecting through
digipeaters, and for digipeating.

You can now "CONNECT dest VIA digi1 digi2 digi3..." in order to route
your connection through one or more accessible digipeaters.  Up to six
digipeaters are supported, per the AX.25 2.0 standard.  Most people
seem to feel that trying to use more than two digis is usually an
exercise in frustration, due to packet loss and timeouts, but you're
free to experiment.

OpenTNC will accept inbound connections which came via digipeaters
(as long as you've selected "CONOK ON") and will automatically
reverse the digipeater path when sending packets in response.

OpenTNC can act as a standard digipeater, if you enable this
feature with "DIGIPEAT ON" (the default is OFF).

OpenTNC can act as an APRS UI-frame digipeater, if you enable
this feature using "APRSDIGI WIDE1-1" or "APRSDIGI WIDE2-1"
or "APRSDIGI WIDE2-2".  Please don't do this unless you're
familiar with the implications of APRS digipeating!  A poorly
configured APRS digipeater can hurt things much more than it
helps, and this feature is definitely experimental for
OpenTNC!

A new command "LAMPTEST ON" can be used to check the health of
the front-panel LEDs (standard and/or WS2812B).  The lamp-test
function runs automatically for a second or so when the TNC is
first powered on.

A special test function can be activated, to allow your
OpenTNC to respond automatically to inbound connections to
a special SSID and act as an "echo server".  To try this
feature, issue the command "ECHOCALL yourcall-N" (giving
your callsign, and an SSID "N" between 1 and 15 that you
don't otherwise use).  Once this feature is enabled, the
TNC will automatically accept inbound connection requests
to yourcall-N.  Any packet that the remote peer sends to
this connection, will be echoed back in response.  The
carrier, packet-received/pending, and PTT LEDs will flash,
but the CONNECT LED will not light up when a connection of
this sort is in progress.  Issue the command "ECHOCALL NOCALL"
to turn this feature off.

# Public Release Version 3

This release supports the OpenTNC-Pico board port, based on the
Raspberry Pi Pico.  I now recommend the Pico system for all new builds
of the OpenTNC.

The CALIBRATE command has been enhanced.  Keys pressed during
CALIBRATE can switch between low-tone (1200 Hz), high tone
(2200 Hz), and alternating tones.  The output modulator
can be switched between PWM (0) and delta-sigma PDM (1).
An 8-minute calibration timeout is enforced.

Support for system-display LEDs has been greatly improved.
The LEDs "seen" by the OpenTNC application are now
virtualized and multi-valued.  A virtual LED can be "bound"
to a specific GPIO on the board (as before), with any non-zero
value assigned to the LED being interpreted as "turn me on".

Virtual LEDs can also be "mapped" to a set of RGB color values on one
of a series of WS2812B "smart LEDs", and several virtual LEDs can be
mapped to the same WS2812B (the color values are additive).

Virtual LEDs which are neither bound to a GPIO or mapped to a WS2812B
have direct physical effect when set by the application - in effect
they turn into "no-op" operations.

All virtual-LED bindings and mappings are done via the board-specific
Zephyr device-tree file, and can be changed by editing this file and recompiling.

WS2812B support is currently available only on the OpenTNC-Pico
board.

# Public Release Version 2

Changes in this release include a major re-working of the
OpenTNC's method of storing and processing AX.25 frames.
It also adds a number of user-visible features which utilize
these changes.

## Internal code architecture changes

In Version 1, AX.25 frames were stored in a set of fairly
simple circular-buffer structures, and handled solely in
a first-in, first-out fashion.  This was fine for
KISS-mode operation (which is where I started out) and
served OK for basic connected-mode opertion, but it's not
flexible enough to allow the development of more advanced
packet processing capabilities.

In Version 2, AX.25 frames are stored in individual
packet buffers, allocated as required from a dynamic
heap, and managed on a set of linked lists in a
thread-safe fashion.

The internal frame-processing logic has been reorganized,
with packet prioritization, de-duplication, and FX.25
processing running in a new "layer 2" thread.

## User-visible changes

### Performance improvement

I've modified the Black Pill build configuration to select
the use of the chip's single-precision floating point
hardware, rather than the default use of software-emulated
floating point.

This has reduced the CPU utilization of the preferred
demodulator chain from 65% to about 15%.  The "RX CPU
busy" LED will be dimmer, as a result :-)

### FX.25

The biggest user-visible change is the implementation of
FX.25 forward error correction.  When enabled, this feature
"wraps" each transmitted AX.25 frame inside an FX.25 Reed-Solomon
error correction code-block.  When transmitted to a remote system
whose TNC implements FX.25, the error-correction coding can
correct up to 8 (or 16 or 32) corrupted 8-bit symbols per
frame, depending on the error-correction coding used.  When
transmitted to a legacy TNC which doesn't implement FX.25,
the FX.25 "wrapper" is ignored and the AX.25 frame is decoded
and checked in the usual fashion.

This feature does add a significant amount of transmission-time
overhead to each frame, with the amount depending on the frame
length and the amount of error-correction robustness selected.
Due to this overhead it's not recommended to enable FX.25
correction unless you know that the TNC at the receiving end
also supports FX.25.

The "FX25" command may be used to select the amount of error
correction used... "off" (no FX.25 wrapper transmitted), "minimum" (16
bytes per frame), "maximum" (64 bytes per frame), or "auto" (16-64
bytes per frame, based on the frame length).

OpenTNC will always look for FX.25 wrappers while receiving, and
will automatically apply FX.25 error correction to any such
frames it sees.  Processing an FX.25 frame requires about
10 milliseconds, adding roughly this much delay to the
reception of the enclosed AX.25 frame.

For testing purposes, two new commands "SMASHFX25RX" and
"SMASHFX25TX" may be used to introduce artificial data
corruption into encoded FX.25 frames during reception or
transmission.  Values in the range of 50-100 are useful
(e.g. at a value of 73, there is 1 chance in 73 that any
given 8-bit symbol in the FX.25 codeblock will be
deliberately scrambled).

I have tested this implementation against the current release
of Dire Wolf, with successful error correction occurring in
both directions.

### DAMASK

This release includes a highly-experimental feature which may help
mitigate the "hidden node" problem (and the resulting packet
collisions and loss) for BBS nodes which support multiple simultaneous
connections from clients "in the field".

When activated, DAMASK will automatically re-order, schedule and
transmit "outbound" packets from the BBS node in a "round robin"
fashion.  During each transmission, the TNC will transmit packets to
only one client at a time, and will then pause briefly (waiting for a
response) before proceeding on to transmit to the next client.  This
is somewhat similar to the way that the DAMA "you may only speak when
spoken to" system operates, but it does not require that the client
systems support or enable DAMA-client mode, and does not require a
centralized "DAMA master" which is "in charge" of all transmission
timing.

DAMASK is meaningful only in KISS mode.  It's intended to be
completely transparent to its KISS host, and to client TNCs, except
for its deliberate alteration in packet transmission order and timing.

### Statistics

The DISPLAY command now outputs some TNC statistics,
including the number of frames sent and received, and
details of FX.25 frame processing.


</BODY>
</HTML>
