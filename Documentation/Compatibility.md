<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## Operating-system compatibility

The OpenTNC enumerates as a USB composite device, supporting a
CDC-ACM "virtual serial port".

### Linux

The OpenTNC should be "plug and play" on any reasonably recent Linux
system, as generic CDC-ACM support has been in the Linux kernel for
years.  The device will appear as (e.g.) /dev/ttyACM0.  A udev rule
can be used to give it a more interesting (and permanent) name, such
as /dev/OpenTNC.

### MacOS

I understand that recent versions of MacOS will also recognize such
CDC-ACM devices as virtual serial ports, without any specific drivers
being required.  I haven't tried.

### Windows

Similarly, Windows 11, and newer versions of Windows 10 will also
recognize CDC-ACM devices and use a generic built-in "USB serial port"
driver.  I've tested this successfully on a Windows 10 system.

Windows 7 requires a device-specific .inf file in order to use the
generic "USB serial port" driver.

Windows 8 requires a *signed* device-specific .inf file, which I have
no ability to write and sign.  If there's anyone out there who has a
Windows driver-signing certificate, and is willing to contribute a
suitably-signed .inf to the cause, I'll be glad to distribute it!


</BODY>
</HTML>
