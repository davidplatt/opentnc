<HTML>
<meta charset="UTF-8"> 
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

## Digipeating

### Use of digipeaters

OpenTNC (version 3.1 and later) supports AX.25 2.0 digipeating, with
up to 6 digipeaters in the path:

    cmd:connect dest via rpt1-3 rpt2 rpt3-7

No commas between digipeater callsigns, please.

It's generally agreed that trying to connect through more than one or
two digipeaters is usually a bad idea, prone to cause packet and hair
loss, connection timeouts, excessive retries, extreme frustration,
poor air-time utilization and lack of sleep.  Your mileage may vary;
offer void where prohibited.

### Acting as a normal digipeater

OpenTNC can act as a standard AX.25 digipeater:

    cmd:digipeat on

In this mode it will digipeat only those packets which give the TNC's
MYCALL as an explicit digipeater ID (the callsign and SSID must match
exactly).  Address aliases are not supported.  If FX25Encoding is
enabled (any value other than "off") OpenTNC will use this FX.25
encoding for any digipeated packets it receives _if_ the incoming
packets were FX.25-encoded.  Normal AX.25 packets will be digitpeated
without any added FX.25 wrapper.

### Acting as an APRS digipeater

OpenTNC can act as an APRS digipeater:

    cmd:aprsdigi WIDE1-1 (or WIDE2-1 or WIDE2-2 or OFF)

This support is very experimental, and has limited flexibility and
configurability.  It was designed to comply with the intent of the
"new APRS paradigm" which prefers using WIDE1-1 and WIDE2-2, favors
full path tracing, and deprecates the old WIDE, RELAY, and TRACE
specifications.

The "WIDE1-1" setting will digipeat only packets with that exact
specification.  "WIDE2-1" will digipeat both WIDE2-1 and WIDE1-1.
"WIDE2-2" will digipeat WIDE2-2, WIDE2-1, and WIDE1-1.  In all cases,
OpenTNC will decrement the SSID in the specified digipeater
specification, set the "has been repeated" flag if the new SSID is
zero, and will insert its own callsign in the path prior to the
modified digipeater specification.

OpenTNC maintains a list of the CRC32 of packets recently digipeated.
It will not digipeat two packets having the same payload, within a
30-second window.

Regional flooding aliases are not yet implemented.

I do not recommend putting up an OpenTNC node as a WIDE2
digi at this time (except perhaps experimentally, on an
experimental packet channel).  It may be useful as a
WIDE1 fill-in digi in areas not so served.

OpenTNC does not automatically alter its transmission parameters
when APRS digipeating is activated.  I recommend reading through
relevant APRS specifications and best-current-practices documents
to figure out the right set of settings... for example:

-  Shortening the TXDELAY
-  Deliberately choosing "fratricidal" timing behavior (digipeat
each packet as soon as possible, "on top of" other digipeaters)
by altering SLOTTIME and PERSIST.

</BODY>
</HTML>
