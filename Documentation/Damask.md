<HTML>
<meta charset="UTF-8">
<HEAD>
<LINK REV=MADE HREF="mailto:dplatt@radagast.org">
</HEAD>
<BODY>

# The DAMA Scheduling Kluge

This hack is intended to be a way of gaining some of the packet-congestion
reductions possible with DAMA, without requiring a DAMA master, or more
than slight changes in the AX.25 connection parameters used by a node
and its clients.

What appears here are my original design ideas for DAMASK.  As
implemented in the OpenTNC firmware (Version 2.1 and later), DAMASK Mode
1 is implemented, with minimally-intrusive frame re-ordering and
idle-time enforcement, and without any modification of the frame
contents.

DAMASK has not yet been extensively tested.

## Overview

DAMASK is an operational mode for a "smart" KISS TNC.  It would
modify the timing, and to some extent the content of connection-mode
packets being sent between its host (typically a BBS or node) and
one or more remote clients.  Ideally, it will do so in a way which is
transparent to both host and clients, except for frame timing.

DAMASK would keep track of active connections between the node, and
remote clients.

1. In its simplest mode of operation, DAMASK would re-order the frames
it's receiving from the host, along DAMA-like principles.

It would re-order frames received from the host, sending them to
clients in a round-robin fashion.  During each transmission, frames would
only be sent to a single client.  DAMASK would enforce a brief period of
"listen only" before switching transmission to the next client, thus allowing
the first client to respond (or another station to break in with e.g. a SABM).

This round-robin scheduling process would mean that after a client's
frames are passed on to the host, any responses by the host to that
client will not be transmitted immediately if more than one client is
active.  Rather, they'll have to wait for the round-robin scheduler to
return to the client.  This will have the effect of delaying
acknowledgement of the client's frames... and will usually deter that
client from trying to transmit spontaneously and running the risk of a
hidden-node collision.  This technique is used by DAMA proper, and
should be reasonably effective even with DAMA-naive clients.

The round-robin scheduling can alter the length of the "listen only" period,
perhaps having a longer quiet-time interval once each time around the
robin, to provide an opportunity for new stations to SABM and start
connections.

2. During the round-robin process, DAMASK might send an RR/RNR/REJ to
each client (using the parameters for that client most recently
received from the host) if there's no outbound traffic for that
client.  This technique is used by DAMA as an explicit "You may send
now!" offer to each DAMA client.  It could be beneficial even in a
DAMASK mode, as it could allow clients a clean, quick opportunity to
retransmit any frames previously lost in transmission without having
to wait for a timeout.

3. In DAMA, the round-robin process is priority-based.  DAMA clients which
didn't "have anything to say" during the last polling cycle are moved to
a lower priority, and are then polled less frequently.  This allows
active clients to get more airtime, without unfairly penalizing those
which are temporarily inactive.  DAMASK can implement a similar technique.

4. DAMASK could intervene in the packet traffic between host and client,
"turning on" the "DAMA is in use" signalling in the AX.25 frame header
(clearing one reserved bit in the SSID).  This will signal DAMA-aware
clients that they should not transmit until polled (and hence this mode
would require that feature (2) be in use!).  We'll need to see whether
signalling DAMA mode has any unfortunate effect on non-DAMA-aware
TNCs.

Mode 1 is the least intrusive, mode 2 is more so, mode 4 is the most.

## Implementation overview

In V1 of the OpenTNC codes, AX.25 frames are stored in a couple of
"first in, first out" circular buffer structures.  One packetring
is used for packets being transmitted to the radio, the other for
packets received from the radio and being handed to either the
connection logic (in converse mode) or to the KISS interface to
the host.  The packetring structure doesn't allow for any sort
of packet re-ordering or manipulation.

In V2 of the OpenTNC code, I've replaced the packetring system with a
heap-based "ax25buffer" system.  Once allocated, the buffers can be
placed onto Zephyr doubly-linked lists, and processed in arbitrary
order.  Currently there's one list for outbound packets destined for
the radio (the KISS-data-from-host logic puts packets here) and
another for inbound packets from the radio (the KISS-data-to-host
logic pulls packets from here).

In order to support DAMASK, the existing V2 buffer handling process
used in KISS mode will need to be extended:

-  Inbound frames will not be placed on the "to host" list immediately.
   Instead, they will first be "spied upon" to keep track of active
   connections.  I don't anticipate that packets will be altered, or
   significantly delayed by this.

-  Outbound frames will not be placed on the "to radio" list immediately.
   They will instead be handed to the DAMASK module.

   Some packets will result in DAMASK recognizing the appearance of a
   "new connection".  Some state will be allocated to keep track of this
   connection, and the packet placed on this connection's outbound-traffic
   list.

   Some packets will match an existing connection, and will be placed on
   that connection's outbound-traffic list.  DAMASK may parse and examine
   these packets to (e.g.) keep track of RR/RNR/REJ status, and the
   host's N_R and N_S values for this connection.  In Mode 4, DAMASK may
   alter the headers of these packets, in order to signal "DAMA in use".

   Some packets (e.g. beacons) will not match any known connection.
   They'll be placed on a "miscellaneous" list, and eventually
   transmitted without alteration.

A new thread (or a new task running as part of an existing thread)
will perform the outbound scheduling for DAMASK.  It will loop through
the list of known connection (considering the "miscellanous" list as
a dummy connection), and move each connection's pending traffic (if
any) to the "to radio" list.  The normal transmit-to-radio logic will
work just as it does today - when it "sees" packets queued to transmit
it will perform the usual P-persist CSMA-CA bid for airtime, and then
transmit the queued packets.  The DAMASK scheduler will not queue
packets for the next connection until it "sees" that the transmitter
has gone active and then inactive again.

In modes 2 and 4, DAMASK may periodically construct and queue spontaneous
RR/RNR/REJ frames for a client from which it hasn't "heard lately",
helping keep the connection alive.

## Data structures needed

DAMASK will require storage space to keep track of active
connections.

- Node and client call-signs
- Priority (highest for active stations) and "how long since we spoke?"
  counters or timers.
- Host's RR/RNR/REJ state, and N_R/N_S values (taken from outbound S and I
  frames to the client).

</BODY>
</HTML>
