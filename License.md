## License

Unless otherwise noted, firmware code in this repository was written
by David Platt AE6EO, and is released under the terms of the [GNU
General Public License, Version 3.0](LICENSE.md).  The hardware
schematics, board designs, and board Gerber files are released under
the Creative Common CC BY-SA license.

Certain other open-source codes and libraries are used by or
incorporated into the OpenTNC firmware and have their own
licenses:

- [The interactive command shell](https://github.com/memfault/interrupt?tab=License-1-ov-file#readme): Creative
Commons Attribution - ShareAlike 4.0

-  [libcorrect](https://github.com/DavidCPlatt/libcorrect/blob/master/LICENSE) - BSD 3-clause "revised"

I thank and acknowledge the authors of libcorrect and the memfault
interrupt project for making their code available for use under these
open-source licenses.

Commercial use of the OpenTNC design is permitted, as long as you
comply with all of the above licenses.  If you decide to build and
sell TNCs based on this design, it would be gracious of you to send me
one as a thank-you (not required, but nice).

